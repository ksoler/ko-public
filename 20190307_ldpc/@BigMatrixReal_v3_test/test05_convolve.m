function [ok, msg] = test05_convolve(obj)
% [ok, msg] = test05_convolve(BigMatrixReal_v3_test)
% Tests convolution for the BigMatrixReal_v3 class.
%

% Usage:
if 0
  test05_convolve(BigMatrixReal_v3_test);  
end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Standard initialization for all tests.                                  %
u = utility01_class;                                                      %
ok  = 1;                                                                  %
msg = 'No problems detected.';                                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tol = 1e-6;  % Tolerance for checking values.

if 1
if ok
  x = BigMatrixReal_v3([ 1 2 3 ]);
  y = BigMatrixReal_v3([ 1 ]);
  z = conv(x,y);
  expected_z = [ 1 2 3];
  [ok, msg]  = checkEqualVectors_tol(u,z.array,expected_z,tol);
end;
end;

if 1
if ok
  x = BigMatrixReal_v3([ 1 2 3 ]);
  y = BigMatrixReal_v3([ 1 1 1 ]);
  z = conv(x,y);
  expected_z = [ 1 3 6 5 3 ];
  [ok, msg]  = checkEqualVectors_tol(u,z.array,expected_z,tol);
end;
end;


if ok
  x = BigMatrixReal_v3(1:10);
  y = BigMatrixReal_v3([1]);  
  z = conv(x,y, 'block size', 3);
  expected_z = conv(x.array, y.array);
  [ok, msg]  = checkEqualVectors_tol(u,z.array,expected_z,tol);
end;
if ~ok, keyboard; end;

if ok
  nx = 100;
  ny = 215;
  x = BigMatrixReal_v3(rand(nx,1));
  y = BigMatrixReal_v3(rand(ny,1));  
  z = conv(x,y, 'block size', 10);
  expected_z = conv(x.array, y.array);
  [ok, msg]  = checkEqualVectors_tol(u,z.array,expected_z,tol);
end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Standard completion for all tests.                                                               %
%
% Prepend class and function name to msg.
msg = sprintf('In %s::%s %s', class(obj), whoAmI(u, 'my name'), msg);
%
% If the return values are not used, then notify the user of any errors.                           %
if nargout ==0                                                                                     %
  if ok                                                                                            %
    fprintf(1, 'In %s::%s, all tests passed.\n', class(obj), whoAmI(u, 'my name'));                %
  else                                                                                             %
    fprintf(1, 'In %s::%s, one or more tests failed.\n', class(obj), whoAmI(u, 'my name'));        %
    fprintf(1, 'Failure message:  %s\n', msg);                                                     %
    %error('Test failed.'); % Do I want this?                                                       %
  end;                                                                                             %
end;                                                                                               %
return;                                                                                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%