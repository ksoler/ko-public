function [ok, msg] = test06_play_sound(obj)
% [ok, msg] = test05_play_sound(BigMatrixReal_v3_test)
% Tests sound playback.
%

% Usage:
if 0
  test06_play_sound(BigMatrixReal_v3_test);  
end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Standard initialization for all tests.                                  %
u = utility01_class;                                                      %
ok  = 1;                                                                  %
msg = 'No problems detected.';                                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if 1
Hz = 1;
dB = 1;

d = 'F:\2019-matlab data\wavRead\';
f1 = 'listen to the man, short.wav';
f2 = 'listen to the man.wav';

[ x, sampleRate ] = wav_read(u, fullfile(d, f1));
%playSound(x,sampleRate, 'block size', 1e5);

h = ones(10,1);
h = conv(h,h);
h = conv(h,h);
%h = h / max(h);
% h = pmfilter(u, ...
%   'sample rate', sampleRate, ...
%   'pass', 0*Hz, 1000*Hz, 0*dB, 1*dB, ...
%   'stop', 1200*Hz, sampleRate/2, 30*dB, ...
%   'verbose', 1, ...
%   'plots', 1);
[ x, sampleRate ] = wav_read(u, fullfile(d, f2));
r1 = round(x.numRows*0.10);
r2 = round(x.numRows*0.12);
x = x.BigMatrix(r1,0).size(r2-r1+1,1);

%resize(x, x.numRows, 1);
y = conv(x, BigMatrixReal_v3(h));
[m,r,c] = max(y);
fprintf(1, 'max(y) is %g, at (%d,%d).\n', m, r, c);
y = y / m; % Scale the signal.
playSound(x,sampleRate, 'block size', 1e6);
playSound(y,sampleRate, 'block size', 1e6);
end;





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Standard completion for all tests.                                                               %
%
% Prepend class and function name to msg.
msg = sprintf('In %s::%s %s', class(obj), whoAmI(u, 'my name'), msg);
%
% If the return values are not used, then notify the user of any errors.                           %
if nargout ==0                                                                                     %
  if ok                                                                                            %
    fprintf(1, 'In %s::%s, all tests passed.\n', class(obj), whoAmI(u, 'my name'));                %
  else                                                                                             %
    fprintf(1, 'In %s::%s, one or more tests failed.\n', class(obj), whoAmI(u, 'my name'));        %
    fprintf(1, 'Failure message:  %s\n', msg);                                                     %
    %error('Test failed.'); % Do I want this?                                                       %
  end;                                                                                             %
end;                                                                                               %
return;                                                                                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%