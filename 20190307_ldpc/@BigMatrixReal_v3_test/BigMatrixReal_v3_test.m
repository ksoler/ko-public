function y = BigMatrixReal_v3_test(x)
% This class contains test code for the class BigMatrixReal_v3.
%

% Create a class object.
y.value = [];
y = class(y, 'BigMatrixReal_v3_test');

% Execute a test.
if nargin > 0
switch x
  case 1, test01(y);
  case 2, test02(y);
  case 3, test03(y);
  case 4, test04(y);
  case 5, test05_convolve(y);
  case 6, test06_play_sound(y);
  otherwise
    error('Unsupported request.');
end;
end;
return;
