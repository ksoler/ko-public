function transpose(x)
% transpose(x)
% Transposes a big matrix.  
%
% Arguments:
% x is the BigMatrixReal_v3 object to transpose, and on exit, it holds the result.
%
% Note that the output matrix replaces the input, so if you do this:
% y = transpose(x), then the original x is no longer available.  
%

file = BigMatrixReal_v3.privateGenerateFilename; % Generate a temporary filename.

if isempty(x.handle)
  was_open = 0;
  open(x);
else
  was_open = 1;
end;

h = fopen(file, 'w+');

block_size = 1e4;

for col = 0 : x.numCols - 1
  privateSeek(x, 0, col);
  remainingRows = x.numRows;
  while remainingRows >= block_size
    d = fread(x.handle, block_size, 'double', (x.numCols-1)*x.bytesPerSample);
    fwrite(h, d, 'double');
    remainingRows = remainingRows - block_size;
  end;
  if remainingRows > 0
    d = fread(x.handle, remainingRows, 'double', (x.numCols-1)*x.bytesPerSample);
    fwrite(h,d, 'double');
  end;
end;

  fclose(h);
  close(x);
  movefile(file, x.filename);
  nr = x.numRows;
  nc = x.numCols;
  x.numRows = nc;
  x.numCols = nr;
  
  if was_open
    open(x);
  end;
  return;
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  