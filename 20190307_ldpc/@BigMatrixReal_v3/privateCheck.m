function [ok, msg] = privateCheck(x)
% [ok, msg] = privateCheck(x)
% Checks the integrity of an object.
%
% Arguments:
% x is an object of this class.
% ok is 1 if all tests pass, 0 otherwise.
% msg is a description of the problem found, if any.
%
% If this function is invoked without outputs and a problem is discovered, then an error message is
% printed.
%
% If you use privateCheck, then it verifies that the object is a BigMatrixReal_v3 object.
%
% If you use check, and the object is not a BigMatrixReal_v3 object, then the check function from
% another class might be invoked.
%
u = utility01_class;


ok = 1;
msg = 'No problem discovered.';

if ok && nargin<1
  ok=0;
  msg = 'Missing input argument.';
end;

if ok && ~isa(x, 'BigMatrixReal_v3')
  ok = 0;
  msg = sprintf('Expected a %s object.', 'BigMatrixReal_v3');
end;

% Check that the object has the expected attributes.  If this test fails, that usually means that
% this function needs to be updated because the class's attributes have changed.
%if ok
%  expected_attributes = [ 'numRows, numCols, handle, filename, bytesPerSample' ];
%  [temp_ok, temp_msg] = checkAttributeList(u, x, expected_attributes);
%  if ~temp_ok, ok=0; msg = [ 'Unexpected attibute(s): ' temp_msg ]; end;
%end;
% Done checking the attribute list.

% Check if the object has been deleted.
if ok && ~isvalid(x)
  ok = 0;
  msg = 'Encountered handle for deleted object.';
end;

% filename
if ok && ~exist(x.filename, 'file')
  ok = 0;
  if ~checkString(u, x.filename)
    msg = 'filename attribute is not a string.';
  else
    msg = ['Data file does not exist, ' x.filename];
  end;
end;

% handle
if ok
  if isempty(x.handle)
    % Ok.
  else
    if numel(x)~=1
      ok = 0;
      msg = 'Invalid file handle.';
    end;
    
    if ok
      try
        y = fopen(x.handle);
      catch
        ok = 0;
        msg = 'File handle is not empty, but does not hold valid handle.';
      end;
    end;
  end;
end;

% numRows
if ok
  if ~checkScalarInt(u,x.numRows)
    ok = 0;
    msg = 'Problem with numRows attribute.';
  end;
end;

% nCols
if ok
  if ~checkScalarInt(u,x.numCols)
    ok = 0;
    msg = 'Problem with numCols attribute.';
  end;
end;

if ok
  if isempty(x.handle)
    n1 = fileSizeInBytes(u,x.filename);
    n2 = n1 / x.bytesPerSample; % Number of doubles stored in file.
    if n2 < x.numRows * x.numCols  %
      msg = sprintf('Size disagreement.  File is should hold %d elements, but size is only %d bytes.\n', x.numRows * x.numCols, n1);
      ok = 0;
    end;
  else
    n1 = fileSizeInBytes(u, x.handle);
    n2 = n1 / x.bytesPerSample; % Number of doubles stored in file.
    if n2 < x.numRows * x.numCols  %
      msg = sprintf('Size disagreement.  File is should hold %d elements, but size is only %d bytes.\n', x.numRows * x.numCols, n1);
      ok = 0;
    end;
  end;
end;



if ~ok && nargout==0
  fprintf(1, 'Error: %s\n', msg);
  error('Integrity test failed.');
end;

if nargout==0
  clear ok
  clear msg
end;

end % function check

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
