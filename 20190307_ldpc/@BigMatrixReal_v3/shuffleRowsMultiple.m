function shuffleRowsMultiple(varargin)
% shuffleRowsMultiple(x, y, z,...)
% Re-orders the rows of multiple BigMatrix objects.  The same permutation is applied to all of them.
% 
% Inputs:
% x, y, z,... are class objects. 
%

% Algorithm:
% Create a permutation file.  Use it to re-order all of the input arguments.

% Future Improvement:  When the number of remaining elements to be moved is less than 1e6, then
% switch to using a Matlab array to hold the indices of markers.
% More important, compactify the source file when it is about 50% empty or to.

n = length(varargin);

if BigMatrixReal_v3.privateValidation
  u = utility01_class;
  for k = 0 : n-1
    privateCheck(varargin{k+1});
    assertion(u, varargin{k+1}.numRows == varargin{1}.numRows); % Check that all matrices have the same number of rows.
  end;
  % Done checking inputs.
end;

num_rows = varargin{1}.numRows;
new_order = BigMatrixReal_v3.privateRandomPermutation(numRows);

for k = 1 : n
  reorderRows(varargin{k}, new_order);
  % This preserves the open/closed state of each BigArray.
end;
delete(new_order.filename); % Delete the file because we don't need it any more.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
