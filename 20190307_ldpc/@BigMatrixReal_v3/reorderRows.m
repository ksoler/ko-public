function reorderRows(x, order)
% reorderRows(x, order)
% Re-orders rows of x.
%
% Inputs:
% x is a BigMatrix.
%
% order is a BigMatrix with one column.  Its elements have values 0...x.numRows-1, which specify the
% rows to read from x.  If x has [0 10 20 30]' and order has [0 3 2 1] then x is changed to 
% [0 30 20 10].
%
% 

  u = utility01_class;

if BigMatrixReal_v3.privateValidation
  privateCheck(x);
  privateCheck(order);
  assertion(u, order.numCols==1);
end;

if isempty(x.handle)
  x_was_closed = 1;
  open(x);
else
  x_was_closed = 0;
end;

if isempty(order.handle)
  order_was_closed = 1;
  open(order);
else
  order_was_closed = 0;
end;

% Now, x and order are open.
file = BigMatrixReal_v3.privateGenerateFilename;
h = fopen(file, 'w');
t1  = clock;
for k = 0 : x.numRows-1
  r = fread(order.handle, 1, 'double');
  
  privateSeek(x, r, 0);
  BigMatrixReal_v3.privateCopyFileData(x.handle, h, x.numCols);
      if etime(clock,t1) > 10
      fprintf(1, 'In %s, k = %d of %d.\n', whoAmI(u, 'my name'), k, x.numRows);
      pause(0.1);
      checkLockFile(u);
      t1 = clock;
    end;

end;
if order_was_closed
close(order);
end;
fclose(h);

close(x);
movefile(file, x.filename); % Replace x's original file.
if ~x_was_closed
  open(x);
end;

return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
