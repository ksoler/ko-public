function resize(x, nr, nc)
% Resizes a BigMatrix.
%
% x is a class object.  Keeps data as much as possible.
%
% nr is the new number of rows.
%
% nc is the new number of columns.
% 
% Preconditions:
% The datafile may be open or closed.
% 
% Postconditions:
% The datafile is in the same open/closed state as before, but its file pointer may have been moved.
%

if x.privateValidation
  u = utility01_class;
  check(x);
  checkScalarInt(u, nr);
  checkScalarInt(u, nc);
  assertion(u, 0 <= nr);
  assertion(u, 0 <= nc);
end;

if nc==x.numCols && nr==x.numRows
  return;
end;

if isempty(x.handle)
  was_closed = 1;
else
  fclose(x.handle);
  was_closed = 0;
  x.handle = [];
end;

if nc ~= x.numCols
  % If the number of columns has changed, then we need to change the file.
  y = BigMatrixReal_v3;  
  x.handle = fopen(x.filename, 'r+');
  y.handle = fopen(y.filename, 'r+');
  BigMatrixReal_v3.privateWriteRepeatedValue(y.handle, 0, nr*nc); % Resize the new matrix.
  y.numRows = nr;
  y.numCols = nc;
  privateMoveRectangle(x, 0, 0, y, 0, 0, min(nr, x.numRows), min(nc, x.numCols));
  fclose(y.handle); 
  y.handle = [];
  fclose(x.handle); 
  x.handle = [];
  
  % Move back to the original filename, just to keep debugging simpler.
  movefile(y.filename, x.filename);
  x.numRows = nr;
  x.numCols = nc;
  delete(y);
  if ~was_closed
    x.handle = fopen(x.filename, 'r+');
  end;
  return;
end;

if nr > x.numRows
  % If the number of rows increases, then we need to increase the file size.
  x.handle = fopen(x.filename, 'r+');
  privateSeek(x, x.numRows, 0);
  BigMatrixReal_v3.privateWriteRepeatedValue(x.handle, 0, (nr-x.numRows)*x.numCols);
  x.numRows = nr;
  if was_closed
    fclose(x.handle);
    x.handle = [];
  end;
  return;
end;

if nr < x.numRows
  % If the number of rows decreases, then just leave the file alone.
  x.numRows = nr;
  if ~was_closed
    x.handle = fopen(x.filename, 'r+');
  end;
  return;
end;

error('This should not happen.');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
