function [y, ok] = ldivide(x1,x2,default)
% [y, ok] = ldivide(x1, x2, default)
% Matlab ".\" operator, elementwise left division, for BigMatrixReal_v3.
% This is the same as elementwise right division.
%
% Arguments:
% x1 is the first input.
%
% x2 is the second input.
%
% default is the value to use for elements affected by division by zero.  This argument is optional;
% the default for it is 0.
%
% y is the BigMatrixReal_v3 result.
%
% ok is 0 if any problems were found (typically, division by 0).
%
% x1 and x2 must both be BigMatrixReal_v3 objects.
%


if nargin < 3
  default = 0;
end;

ok = 1;

if BigMatrixReal_v3.privateValidation
  u = utility01_class;
  privateCheck(x1);
  privateCheck(x2);
  if x1.numRows ~= x2.numRows || x1.numCols ~= x2.numCols
    error('Tried to divide BigMatrix objects of different size.');
  end;
end;

% Special case:  Repeated input.
if same(x1, x2)
  temp = copy(x1);
  y = ldivide(x1, temp);
  delete(temp);
  return;
end;

  
  
y = BigMatrixReal_v3(x1.numRows, x1.numCols);
open(y);

if isOpen(x1)
  was_closed1 = 0;
  privateSeek(x1,0,0);
else
  was_closed1=1;
  open(x1);
end;
if isOpen(x2)
  was_closed2 = 0;
  privateSeek(x2,0,0);
else
  was_closed2=1;
  open(x2);
end;

N = x1.numRows*x1.numCols;
blockSize=1e4;
t1 = clock;
while N>0
  n = min(N, blockSize);
  data1 = fread(x1.handle, n, 'double');
  data2 = fread(x2.handle, n, 'double');
  if any(data2==0)
    % Special handling for division by 0.
    data = zeros(size(data1)) + default;
    data(data2~=0) = data1(data2~=0) .\ data2(data2~=0);
    ok = 0;
  else
    data = data1 ./ data2;
  end;
  fwrite(y.handle, data, 'double');
  N = N - n;
  if etime(clock, t1) > 10
    fprintf(1, 'In %s::%s, ', class(x), whoAmI(u, 'my name'));
    fprintf(1, 'processing element %d of %d.\n', x.numRows * x.numCols - N, x.numRows * x.numCols);
    t1 = clock;
    pause(0.1);
  end;
end;
close(y);
if was_closed1
  close(x1);
end;
if was_closed2
  close(x2);
end;
if ~ok && nargout <  2 && nargin < 3
  warning(u, 'Attempted division by zero.');
end;
  
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
