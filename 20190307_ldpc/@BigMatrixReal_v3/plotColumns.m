function plotColumns(x, varargin)
% plotColumns(x,...)
% Plots columns of numeric data in a BigMatrix.  These are the optional tags:
% rows, cols, figure, xlabel, ylabel, title, legend, grid, clf, linetypes, size, line width.
%
% Inputs:
% x is the BigMatrix object to use.  This function does not check if it is too large to be plotted
% or stored in a Matlab array.  The remaining arguments are optional tag/value pairs. 
%
% 'rows' is an array of the rows to read, e.g. 1:10 to read the first 10 rows of the file.  Use
% 'all' to read all rows.  This is the default.
%
% 'cols' is an array of the columns to plot, e.g. [2 3 4] to read the second, third, and fourth
% columns.  Use 'all' to read all columns.  This is the default.
%
% 'figure' is the figure number to use.  Use 'new' to generate a new figure using the 'figure'
% command.  Use 'current' to use the current figure (if there is one).
%
% 'xlabel' is the x axis text label, or [] to ignore.
%
% 'ylabel' is the y axis text label, or [] to ignore.
%
% 'title' is the title to use, or [] to ignore.  By default, this function disables the tex
% interpreter for labels, and titles.
%
% 'legend' is a cell array of legend labels to use with the plotted columns.  If empty, then no
% legend is generated.
%
% 'grid' is 0 or 1.  Default is 1.
%
% 'clf' is 0 or 1.  1 is the default.  If 1, the figure is cleared before plotting.
%
% 'linetypes' is a cell array of line types (e.g. {'b-' 'g0' 'r*'}).
%
% 'size'
%
% 'line width'
%
% Future Improvement:  Return an array of line handles.
% Future Improvemets:  Allow user to specify x axis values.  For now, they are a count, starting at
% 0.  Allow user to specify lines, markers.  Return data and/or line handles.  Maybe figure or axis
% handles.  Allow user to specify linear, log scale.
%

default_line_types = { ...
  'b-' 'g-' 'r-' 'c-' 'y-' 'm-' ...
  'bo-' 'go-' 'ro-' 'co-' 'yo-' 'mo-' ...
  'bs-' 'gs-' 'rs-' 'cs-' 'ys-' 'ms-' };
u = utility01_class;

getArg( u, ...
  'rows     ->rows       ', {'all'},     ...
  'cols     ->cols       ', {'all'},     ...
  'figure   ->fig        ', {'current'}, ... 
  'xlabel   ->xlabel_text', [],          ...
  'ylabel   ->ylabel_text', [],          ...
  'title    ->title_text ', [],          ...
  'legend   ->legend_text', [],          ...
  'grid     ->gridFlag   ', 1,           ...
  'clf      ->clf_flag   ', 1,           ...
  'size     ->size_arg   ', [20 10],     ...
  'line width->lineWidth ', 2,           ...
  'linetypes->linetypes', { default_line_types } );

% For now, nr is the number of rows, nc is the number of columns.

% Downselect rows, columns.
if isequal(rows, 'all')
  rows = 0:x.numRows-1;
end;
if isequal(cols, 'all')
  cols = 0:x.numCols-1;
end;
if checkString(u, legend_text)
  legend_text  = split(u, legend_text, ',');
end;
% Done downselecting rows, columns.

% Select figure number.
if isequal(fig, 'new')
  fig = figure;
elseif isequal(fig, 'current') || isequal(fig, 'same')
  fig = gcf;
else
  % We expect that fig is a figure number.
  figure(fig);
end;
% Done selecting figure number.

% Handle the "size" argument.
if ~isempty(size_arg)
  setFigureSize(u, gcf, size_arg(1), size_arg(2));
end;

% Handle clf_flag.
if clf_flag
  clf;
end;

% Update the number of columns to plot.
nc = length(cols);

hold_state = ishold;


if isempty(x.handle)
  open(x);
  was_closed = 1;
else
  was_closed = 0;
end;

handles = [];
for c = 1 : nc
  s = linetypes{ mod(c-1, length(linetypes))+1 };
  data = privateReadRectangle(x, 0, cols(c), x.numRows, 1);
  data = data(rows+1); % Downselect rows.  
  handles = [ handles plot(rows, data, s) ]; % This supports handles which are a number or an object.
  if ~isempty(lineWidth)
    set(handles(end), 'LineWidth', lineWidth);
  end;
  hold on;
end;
if was_closed
  close(x);
end;

if ~isempty(legend_text)
  legend(handles, legend_text{:});
end;

if ~isempty(xlabel_text)
  xlabel(xlabel_text, 'Interpreter', 'none');
end;

if ~isempty(ylabel_text)
  ylabel(ylabel_text, 'Interpreter', 'none');
end;
  
if ~isempty(title_text)
  title(title_text, 'Interpreter', 'none');
end;

if gridFlag
  grid on;  
end;

if ~ishold
  hold off;
end;

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
