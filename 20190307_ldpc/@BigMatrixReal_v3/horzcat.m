function y = horzcat(varargin)
% x = horzcat(varargin)
% Appends columns from one matrix onto another.
%
% Arguments:
% x is the first input matrix.  The remaining matrix arguments are concatented onto x.
%
% Outputs:
% x is the updated matrix object.
%

n = length(varargin);
if BigMatrixReal_v3.privateValidation
  u = utility01_class;
  for k = 1  : n
    privateCheck(varargin{k});
    assertion(u, varargin{k}.numRows == varargin{k}.numRows);
  end;
end;

total_cols = 0;
for k = 1 : n
  total_cols = total_cols + varargin{k}.numCols;
end;
nr = varargin{1}.numRows;

y = BigMatrixReal_v3(nr, total_cols);
open(y);

c = 0; % Column index.
for k = 1 : n
  if isOpen(varargin{k})
    privateMoveRectangle(varargin{k}, 0, 0, y, 0, c, nr, varargin{k}.numCols);
    c = c + varargin{k}.numCols;
  else
    open(varargin{k});
    privateMoveRectangle(varargin{k}, 0, 0, y, 0, c, nr, varargin{k}.numCols);
    close(varargin{k});
    c = c + varargin{k}.numCols;
  end;
end;
close(y);


if BigMatrixReal_v3.privateValidation
  privateCheck(y);
end;
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
