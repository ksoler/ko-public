function y = privateStealthRead(x, r, c, n)
% y = privateStealthRead(x, r, c, n)
% Reads data, with no changes to file state (including file pointer).
%
% Inputs:
% x is a BigMatrix object.
% r is the row to start reading from.
% c is the column to start reading from.
% n is the number of elements to read.  If this goes past the last column, it wraps to the next row.
% 
% Outputs:
% y is a column vector of the retrieved data.
%

if BigMatrixReal_v3.privateValidation
  if r*x.numCols+c >= x.numRows * x.numCols
    error('Tried to read past end of data.');
  end;
  u = utility01_class;
  checkScalarInt(u,r);
  checkScalarInt(u,c);
  checkScalarInt(u,n);
  assertion(u, r>=0 && c>=0 && n>=0);
end;

if isempty(x.handle)
  % The file was closed.
  x.handle = fopen(x.filename, 'r');
  privateSeek(x, r, c);
  y = fread(x.handle, n, 'double');
  fclose(x.handle);
  x.handle = [];
  return;
else
  % The file was open.
  pos = ftell(x.handle);
  privateSeek(x, r, c);
  y = fread(x.handle, n, 'double');
  fseek(x.handle, pos, 'bof');
  return;
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
