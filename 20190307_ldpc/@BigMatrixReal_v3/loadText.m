function loadText(x, filename)
% loadText(x, filename)
% Reads text from a text file.
% 
% Arguments:
% x is a BigMatrixReal_v3 object.
%
% filename is the file to read from.  Each line of the file becomes one matrix row.
% All of the lines need to have the same number of values.
%
% Future Improvement: Use fscanf instead of fgetl and sscanf, to speed this up.
% Warning:  This function cannot handle lines too long to fit into a Matlab array.  This may be
% fixed in a future revision.
%
u = utility01_class;
if BigMatrixReal_v3.privateValidation
  % No tests.
end;

if isempty(x.handle)
  was_closed = 1;
else
  close(x);
  was_closed = 0;
end;
% At this point, we know that x's data file is closed.

x.numRows = 0;
x.numCols = 0;

% Create the matrix file with dimensions 0,0.
x.handle = fileOpen(u, x.filename, 'w+');

% Read the first line from the data file.
h = fopen(filename, 'rt');
s = fgetl(h);
if isequal(s,-1)
  fclose(h);
  if was_closed
    x = close(x);
    return;
  end;
end;
data = sscanf(s,'%g');
x.numCols = length(data);
fileSeek(u, h, 0, 'bof'); % Rewind to the start of the text file.

t1 = clock;
done = 0;
while ~done
  s = fgetl(h);
  if isequal(s,-1)
    done = 1;
    break;
  end;
  data = sscanf(s, '%g');
  if length(data) ~= x.numCols
    warning(u, 'Unexpected number of values in a row, aborted reading.');
    done = 1;
    break;    
  end;
  fwrite(x.handle, data, 'double');
  x.numRows = x.numRows + 1;
  if etime(clock, t1) > 10
    fprintf( 1, 'Read %d lines, %.0f%% of file %s.\n', x.numRows, filePositionPercent(u, h), fileBareName(u, filename));
    pause(0.1);
    t1 = clock;
  end;
end;

if was_closed
  fclose(x.handle);
  x.handle = [];
end;

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
