function y = mtimes(x1,x2)
% y = mtimes(x1,x2)
% Matlab "*" operator, matrix multiplication, for BigMatrixReal_v3.
%
% Arguments:
% x1 is the first input.
% x2 is the second input.
% y is the BigMatrixReal_v3 result.
%
% x1 and x2 can be BigMatrixReal_v3 objects, or one of them can be a Matlab scalar.
%

isScalar1 = isnumeric(x1) && numel(x1)==1;
isScalar2 = isnumeric(x2) && numel(x2)==1;

if BigMatrixReal_v3.privateValidation
  u = utility01_class;
  if isScalar1
    checkScalarReal(u, x1);
  else
    privateCheck(x1);
  end;
  if isScalar2
    checkScalarReal(u, x2)
  else
    privateCheck(x2);
  end;
end;


if isa(x1, 'BigMatrixReal_v3') && isa(x2, 'BigMatrixReal_v3')
  % (BigMatrix) + (BigMatrix)
  y = multiply_BigMatrix_BigMatrix(x1,x2);
elseif isa(x1, 'BigMatrixReal_v3') && isScalar2
  % (BigMatrix) + (scalar)
  y = multiply_BigMatrix_scalar(x1, x2);
elseif isScalar2 && isa(x1, 'BigMatrixReal_v3')
  % (scalar) + (BigMatrix)
  y = multiply_BigMatrix_scalar(x1, x2);
else
  error('Invalid inputs.');
end;
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function y = multiply_BigMatrix_scalar(x, scalar)
u = utility01_class;

if BigMatrixReal_v3.privateValidation
  privateCheck(x);
end;

y = BigMatrixReal_v3(x.numRows, x.numCols);
open(y);
if isOpen(x)
  was_closed = 0;
  privateSeek(x,0,0);
else
  was_closed=1;
  open(x);
end;
N = x.numRows*x.numCols; % Number of remaining samples.
blockSize=1e4;
t1 = clock;
while N>0
  n = min(N, blockSize);  % Number of samples to process in this pass.
  data = fread(x.handle, n, 'double');
  data = data * scalar;
  fwrite(y.handle, data, 'double');
  N = N - n;
  if etime(clock, t1) > 10
    fprintf(1, 'In %s::%s, ', class(x), whoAmI(u, 'my name'));
    fprintf(1, 'processing element %d of %d.\n', x.numRows * x.numCols - N, x.numRows * x.numCols);
  end;
end;
close(y);
if was_closed
  close(x);
end;
return;
% End of multiply_BigMatrix_scalar.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function z = multiply_BigMatrix_BigMatrix(x, y)
% Matrix multiplication of two BigMatrix objects.
u = utility01_class;

% Special case:  Repeated input.
if x1 == x2
  y = multiply_BigMatrix_BigMatrix(x1, copy(x1));
  delete(x1);
  return;
end;

if BigMatrixReal_v3.privateValidation
  privateCheck(x);
  privateCheck(y);
  if x.numCols ~= y.numRows 
    error('Tried to perform matrix multiplications of BigMatrix objects with incompatible sizes.');
  end;
end;

z = BigMatrixReal_v3(x.numRows, y.numCols);
open(z);

if isOpen(x)
  was_closedx = 0;
else
  was_closedx=1;
  open(x);
end;
if isOpen(y)
  was_closedy = 0;
  privateSeek(y,0,0);
else
  was_closedy=1;
  open(y);
end;


% If one row x1, and one column of x2, are small enough to fit into memory, then do it that way.
t1 = clock;
if x.numCols <=1e5
  % Do multiplication, keeping a column of y in memory.
  for j = 0 : y.numCols-1
    y_column = privateReadRectangle(y, 0, j, y.numRows, 1);
    privateSeek(x,0,0);
    
    for i = 0: x.numRows-1
      x_row = fread(x.handle, x.numCols, 'double');
      privateSeek(z, i, j); % This could be more efficient using fwrite with skip, but it's challenging because it skips before writing.
      fwrite(z.handle, x_row' * y_column, 'double');
    end;
    if etime(clock, t1) > 10
      fprintf(1, 'In %s::%s, ', class(x), whoAmI(u, 'my name'));
      fprintf(1, 'processing element %d of %d.\n', x.numRows * x.numCols - N, x.numRows * x.numCols);
      t1 = clock;
      pause(0.1);
    end;

  end;

% Done multiplication, keeping a complete column of x2 in memory.
else
  for i = 0 : x.numRows-1
    for j = 0 : y.numCols-1
      N = x.numCols; % Remainig number of samples to read.
      privateSeek(x, i, 0);
      privateSeek(y, 0, j);
      s = 0;
      block_size = 1e4;
      while N > 0
        n = min(N, block_size); % Number of samples to read in this pass.
        x_data = fread(x.handle, n, 'double');
        y_data = fread(y.handle, n, 'double', y.numCols-1);
        s = s + sum(x_data .* y_data);
      end;
      privateSeek(z,i,j);
      fwrite(z, s, 'double');      
    end;
        if etime(clock, t1) > 10
      fprintf(1, 'In %s::%s, ', class(x), whoAmI(u, 'my name'));
      fprintf(1, 'processing element %d of %d.\n', x.numRows * x.numCols - N, x.numRows * x.numCols);
      t1 = clock;
      pause(0.1);
    end;

  end;
  % Done multiplication, not keeping a complete column of x2 in memory.
end;

close(z);
if was_closedx
  close(x);
end;
if was_closedy
  close(y);
end;
return;
% End of multiply_BigMatrix_BigMatrix.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
