function z = conv(x,y, varargin)
% z = conv(x,y)
% Convolves two vectors.
%
% Arguments:
% x is a BigVectorMatrixReal_v3 object with numRows=1 or numCols=1.  It cannot be empty.
% y is a BigVectorMatrixReal_v3 object with numRows=1 or numCols=1.  It cannot be empty.
%
% Optional tag/value pairs can appear after y.
% 'block size' is the block size for convolution, the recommended value is 1e5.
%
% z is the result.  It is always a column vector (or should it be a row vector if x and y are?).
%
% Warning:  Check for a duplicated input!
% Later, implement a correlation detector too!
% Other functions to implement:  FFT, sort, correlation, etc.  Do a really long FFT, too!
% Other functions to publish:  my filter design function.


verbose = 0;


u = utility01_class;
if verbose
  fprintf(1, 'Starting %s.\n', whoAmI(u, 'my name'));
end;


getArg(u, 'block size->block_size', 1e5);

if BigMatrixReal_v3.privateValidation
  privateCheck(x);
  privateCheck(y);
  if x.numRows~=1 && x.numCols~=1
    error('x.numRows or x.numCols must be 1.');
  end;
  if y.numRows~=1 && y.numCols~=1
    error('y.numRows or y.numCols must be 1.');
  end;
  if x.numRows==0 || x.numCols==0 || y.numRows==0 || y.numCols==0
    error('Inputs cannot be empty.');
  end;
end;

nx = x.numRows * x.numCols;
ny = y.numRows * y.numCols;

if isempty(x.handle)
  x_was_closed = 1;
  open(x);
else
  x_was_closed = 0;
  %privateSeek(x,0,0);
end;

if isempty(y.handle)
  y_was_closed = 1;
  open(y);
else
  y_was_closed = 0;
  %privateSeek(y,0,0);
end;
% No, we don't care:  (Now, both x and y are open and at the beginning of their files.)
nz = nx + ny - 1;
z = BigMatrixReal_v3;
open(z); % The dimensions of z have not yet been set.
BigMatrixReal_v3.privateWriteRepeatedValue(z.handle, 0, nz);
z.numRows = nz; % Was this alreaady done?
z.numCols = 1;


% x and y may be too long to hold in memory.

% Algorithm:  Partition x and y into blocks of manageable size.
% Convolve all x-y pairings, and add them into the result at their respective indices.
% Reverse-order x.  do this in the outer loop so we don't reverse it many (variable 'my') times.
% In the loop, we use fseek instead of privateSeek because we might be processing row or column
% vectors, and privateSeek wants us to be explicit about that.

%block_size = 1e4; % Nominal block size.
mx = ceil(nx/block_size); % Number of blocks of x.
my = ceil(ny/block_size); % Number of blocks of y.
num_x_remaining = nx;
%for ix = mx-1 : -1 : 0
for ix = 0 : mx-1
  if verbose, fprintf(1, 'ix=%d\n', ix); end;
  fseek(x.handle, ix*block_size*x.bytesPerSample, 'bof');
  if ix==mx-1
    x_data = fread(x.handle, nx - ix*block_size, 'double');
  else
    x_data = fread(x.handle, block_size, 'double');
  end;
  if verbose, fprintf(1, 'x_data= [%s]\n', printVector(u, x_data)); end;
  
  fseek(y.handle, 0, 'bof');
  for iy = 0 : my-1
    if verbose, fprintf(1, 'iy=%d\n', iy); end;
    if iy==my-1
      % Read the last block of y.
      y_data = fread(y.handle, ny-iy*block_size, 'double');
    else
      % Read a block from y, not the last block.
      y_data = fread(y.handle, block_size, 'double');
      
    end;
      if verbose, fprintf(1, 'y_data= [%s]\n', printVector(u, y_data)); end;
    
    % Now, we want to convolve x_data nad y_data
    z_data = conv(x_data, y_data);
    %iz = (mx-1-ix) + iy; % For ix, reverse the index ordering.
    iz = ix + iy;
    if verbose, fprintf(1, 'iz=%d\n', iz); end;
    fseek(z.handle, iz*block_size*z.bytesPerSample, 'bof');
    original_z_data = fread(z.handle, length(z_data), 'double');
    z_data = original_z_data + z_data;
    fseek(z.handle, -length(z_data) *z.bytesPerSample, 'cof');
    fwrite(z.handle, z_data, 'double');
  end;
end;
close(z);

if x_was_closed
  close(x);
end;

if y_was_closed
  close(y);
end;


if BigMatrixReal_v3.privateValidation
  privateCheck(z);
end;

return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
