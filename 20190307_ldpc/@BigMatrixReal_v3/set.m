function set(x, varargin)
% y = set(x, name, value)
% Sets an attribute value.
%
% Inputs:
% x is a class object.
% name is a string containing the name of the requested attributes.   The supported attributes are:
%      'numRows'
%      'numCols'
%      'handle'
%      'filename'
%
% Derived attributes:
%      'fromMatlabArray' - Replaces data with a Matlab array.
%
% Static class attributes:
%      'validation' - enables validation tests for all objects of this class.
%      'dataDirectory'
%
% This function accepts additional attribute/value pairs of arguments.
%
% Outputs:  None. This is a handle class.
%

n = length(varargin);

if BigMatrixReal_v3.privateValidation
  u = utility01_class;
  privateCheck(x);  

assertion(u,mod(n,2)==0);  % We need an even number of arguments, for the tag/value pairs.
end;

for k = 1 : 2 : n-1
  name = varargin{k};
  value = varargin{k+1};
if BigMatrixReal_v3.privateValidation
  checkString(u,name);
end;

  switch name
    case 'numRows'
      resize(x, value, x.numCols);
    case 'numCols'
      resize(x, x.numRows, value);
    case 'h'
      error('The user is not allowed to change the handle (h) attribute.');
      x.handle = value;
    case 'filename'
      error('The user is not allowed to change the filename attribute.');
      x.filename = value;
    case 'validation'
      if islogical(value)
        value = double(value);
      end;
      if ~isequal(value,0) && ~isequal(value,1)
        error('The validation value must be 0 or 1.');
      end;
      BigMatrixReal_v3.privateValidation(value);
    
    case 'fromMatlabArray'
      fromMatlabArray(x, value);

    case 'dataDirectory'
      BigMatrixReal_v3.privateDataDirectory(value);
            
    otherwise
      error('Invalid attribute.');
  end; % switch
  
end; % for loop

if BigMatrixReal_v3.privateValidation
  privateCheck(x);
end;

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
