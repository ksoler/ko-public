function load(x, filename)
% load(x, filename)
% Loads a saved data file.
%
% Arguments:
% x is a class object.  Its data is replaced with the contents of the file.
%
% filename is the name of the file to load from.  This file should have been created with the 'save'
% method.
%
% The saved file has the same format as a working data file, except that the number of rows and 
% columns are appended to the file. 
%

if BigMatrixReal_v3.privateValidation
  u = utility01_class;
  privateCheck(x);
  checkString(u,filename);
  if ~exist(filename, 'file')
    error('File does not exist.');
  end;
  if ~BigMatrixReal_v3.privateIsValidSavedFile(filename)
    error('File format is not valid.');
  end;
  
end;

if isempty(x.handle)
  was_closed = 1;
else
  was_closed = 0;
  fclose(x.handle);
  x.handle = [];
end;

copyfile(filename, x.filename); % Overwrite x's data file.  Leave the metadata at the end.
h = fopen(x.filename, 'r');
fseek(h, -3*x.bytesPerSample, 'eof'); % Remember, 8 bytes per number!
s = fread(h, 3, 'double');
if s(1) ~=3
  warning(u, 'Did not find expected magic number in saved data file.');
end;
x.numRows= s(2);
x.numCols = s(3);
fclose(h);

if ~was_closed
  open(x);
end;
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  