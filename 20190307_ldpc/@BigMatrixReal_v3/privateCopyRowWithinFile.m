function privateCopyRowWithinFile(x, r1, r2)
% privateCopyRowWithinFile(x, r1, r2)
% Copies a row of data within data file.
%
% Arguments:
% x is a class object.  Its file handle should be open for read/write access.
% r1 is the row to read from.
% r2 is the row to write to.
%
% This function works even if a row is too large to hold in memory at once.
%

n = x.numCols;
block_size = 1e4;
pos1 = r1 * x.numCols * x.bytesPerSample;
pos2 = r2 * x.numCols * x.bytesPerSample;

while n > block_size
  fseek(x.handle, pos1, 'bof');
  data = fread(x.handle, block_size, 'double');
  fseek(x.handle, pos2, 'bof');
  fwrite(x.handle, data, 'double');
  pos1 = pos1 + x.bytesPerSample*block_size;  % This is in bytes.
  pos2 = pos2 + x.bytesPerSample*block_size;  % This is in bytes.
  n = n - block_size;          % This is in doubles.
end;

if n > 0
  fseek(x.handle, pos1, 'bof');
  data = fread(x.handle, n, 'double');
  fseek(x.handle, pos2, 'bof');
  fwrite(x.handle, data, 'double');
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
