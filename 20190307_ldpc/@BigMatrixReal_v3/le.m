function y = le(x1,x2)
% y = le(x1,x2)
% Matlab "<=" (less than or equal) operator for BigMatrixReal_v3.
%
% Arguments:
% x1 is the first input.
% x2 is the second input.
% y is the BigMatrixReal_v3 result.
%
% x1 and x2 can be BigMatrixReal_v3 objects, or one of them can be a Matlab scalar.
%

isScalar1 = isnumeric(x1) && numel(x1)==1;
isScalar2 = isnumeric(x2) && numel(x2)==1;

if BigMatrixReal_v3.privateValidation
  u = utility01_class;
  if isScalar1
    checkScalarReal(u, x1);
  else
    privateCheck(x1);
  end;
  if isScalar2
    checkScalarReal(u, x2);
  else
    privateCheck(x2);
  end;
end;

if isa(x1, 'BigMatrixReal_v3') && isa(x2, 'BigMatrixReal_v3')
  % (BigMatrix) < (BigMatrix)
  y = le_BigMatrix_BigMatrix(x1,x2);
elseif isa(x1, 'BigMatrixReal_v3') && isScalar2
  % (BigMatrix) < (scalar)
  y = le_BigMatrix_scalar(x1, x2);
elseif isScalar1 && isa(x2, 'BigMatrixReal_v3')
  % (scalar) < (BigMatrix)
  y = le_scalar_BigMatrix(x1, x2);
else
  error('Invalid inputs.');
end;
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function y = le_scalar_BigMatrix(scalar, x)
u = utility01_class;

if BigMatrixReal_v3.privateValidation
  privateCheck(x);
end;

y = BigMatrixReal_v3(x.numRows, x.numCols);
open(y);
if isOpen(x)
  was_closed = 0;
  privateSeek(x,0,0);
else
  was_closed=1;
  open(x);
end;
N = x.numRows*x.numCols; % Number of remaining samples.
blockSize=1e4;
t1 = clock;
while N>0
  n = min(N, blockSize);  % Number of samples to process in this pass.
  data = fread(x.handle, n, 'double');
  data = scalar <= data;
  fwrite(y.handle, data, 'double');
  N = N - n;
  if etime(clock, t1) > 10
    fprintf(1, 'In %s::%s, ', class(x), whoAmI(u, 'my name'));
    fprintf(1, 'processing element %d of %d.\n', x.numRows * x.numCols - N, x.numRows * x.numCols);
    t1 = clock;
    pause(0.1);
  end;
end;
close(y);
if was_closed
  close(x);
end;
return;
% End of le_scalar_BigMatrix.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function y = le_BigMatrix_scalar(x, scalar)
u = utility01_class;

if BigMatrixReal_v3.privateValidation
  privateCheck(x);
end;

y = BigMatrixReal_v3(x.numRows, x.numCols);
open(y);
if isOpen(x)
  was_closed = 0;
  privateSeek(x,0,0);
else
  was_closed=1;
  open(x);
end;
N = x.numRows*x.numCols; % Number of remaining samples.
blockSize=1e4;
t1 = clock;
while N>0
  n = min(N, blockSize);  % Number of samples to process in this pass.
  data = fread(x.handle, n, 'double');
  data = data <= scalar;
  fwrite(y.handle, data, 'double');
  N = N - n;
  if etime(clock, t1) > 10
    fprintf(1, 'In %s::%s, ', class(x), whoAmI(u, 'my name'));
    fprintf(1, 'processing element %d of %d.\n', x.numRows * x.numCols - N, x.numRows * x.numCols);
    t1 = clock;
    pause(0.1);
  end;
end;
close(y);
if was_closed
  close(x);
end;
return;
% End of le_BigMatrix_scalar.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function y = le_BigMatrix_BigMatrix(x1, x2)
u = utility01_class;

if BigMatrixReal_v3.privateValidation
  privateCheck(x1);
  privateCheck(x2);
  if x1.numRows ~= x2.numRows || x1.numCols ~= x2.numCols
    error('Tried to compare BigMatrix objects of different size.');
  end;
end;

% Special case:  Repeated input.
if same(x1, x2)
  temp = copy(x1);
  y = le_BigMatrix_BigMatrix(x1, temp);
  delete(temp);
  return;
end;


y = BigMatrixReal_v3(x1.numRows, x1.numCols);
open(y);

if isOpen(x1)
  was_closed1 = 0;
  privateSeek(x1,0,0);
else
  was_closed1=1;
  open(x1);
end;
if isOpen(x2)
  was_closed2 = 0;
  privateSeek(x2,0,0);
else
  was_closed2=1;
  open(x2);
end;

N = x1.numRows*x1.numCols;
blockSize=1e4;
t1 = clock;
while N>0
  n = min(N, blockSize);
  data1 = fread(x1.handle, n, 'double');
  data2 = fread(x2.handle, n, 'double');
  data = data1 <= data2;
  fwrite(y.handle, data, 'double');
  N = N - n;
  if etime(clock, t1) > 10
    fprintf(1, 'In %s::%s, ', class(x), whoAmI(u, 'my name'));
    fprintf(1, 'processing element %d of %d.\n', x.numRows * x.numCols - N, x.numRows * x.numCols);
    t1 = clock;
    pause(0.1);
  end;
end;
close(y);
if was_closed1
  close(x1);
end;
if was_closed2
  close(x2);
end;
return;
% End of le_BigMatrix_BigMatrix.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
