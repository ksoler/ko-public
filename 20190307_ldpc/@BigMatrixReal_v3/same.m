function y = same(x1,x2)
% y = same(x1,x2)
% Checks if two handles point to the same object.
%
% Arguments:
% x1 is a BigMatrixReal_v3 object (a handle).
% x2 is a BigMatrixReal_v3 object (a handle).
% y is 1 if x1 and x2 refer to the same object, 0 otherwise.
%

y = eq(x1,x2,'handle');
return;
