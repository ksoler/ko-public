function [y, ok] = power(x1,x2,default)
% [y, ok] = power(x1, x2, default)
% Matlab ".^" operator, elementwise exponent, for BigMatrixReal_v3.
%
% Arguments:
% x1 is the first input.
%
% x2 is the second input.
%
% default is the value to use for elements affected by illegal values.  This argument is optional; 
% the default for it is 0.
%
% y is the BigMatrixReal_v3 result.
%
% ok is 0 if any illegal operations were detected.
%
% Modes of operation:
% If x1 and x2 are both BigMatrix objects, then y(i,j) = x1(i,j) ^ x2(i,j).
% If x1 is BigMatrix and x2 is scalar, then y(i,j) = x1(i,j) ^ x2.
% If x1 is scalar and x2 is BigMatrix, then y(i,j) = x1 ^ x2(i,j).
%
%
% The illegal operations are:
%  - Raise 0 to a negative power.
%  - Raise a negative number to a non-integer power.
%

if nargin < 3
  default = 0;
end;

ok = 1;
isScalar1 = isnumeric(x1) && numel(x1)==1;
isScalar2 = isnumeric(x2) && numel(x2)==1;

if BigMatrixReal_v3.privateValidation
  u = utility01_class;
  if isScalar1
    checkScalarReal(u, x1);
  else
    privateCheck(x1);
  end;
  if isScalar2
    checkScalarReal(u, x2);
  else
    privateCheck(x2);
  end;
  checkScalarReal(u, default);
end;

if isa(x1, 'BigMatrixReal_v3') && isa(x2, 'BigMatrixReal_v3')
  % (BigMatrix) .^ (BigMatrix)
  [y, ok] = power_BigMatrix_BigMatrix(x1, x2, default);
elseif isa(x1, 'BigMatrixReal_v3') && isScalar2
  % (BigMatrix) .^ (scalar)
  [y, ok] = power_BigMatrix_scalar(x1, x2, default);
elseif isScalar1 && isa(x2, 'BigMatrixReal_v3')
  % (scalar) .^ (BigMatrix)
  [y, ok] = power_scalar_BigMatrix(x1, x2, default);
else
  error('Invalid inputs.');
end;
if ~ok && nargout <  2 && nargin < 3
  warning(u, 'Attempted illegal operation in BigMatrixReal_v3::power.');
end;


return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [y, ok] = power_BigMatrix_BigMatrix(x1, x2, default)

u = utility01_class;
if BigMatrixReal_v3.privateValidation
  privateCheck(x1);
  privateCheck(x2);
  if x1.numRows ~= x2.numRows || x1.numCols ~= x2.numCols
    error('Tried to divide BigMatrix objects of different size.');
  end;
end;


% Special case:  Repeated input.
if same(x1, x2)
  temp = copy(x1);
  y = power_BigMatrix_BigMatrix(x1, temp);
  delete(temp);
  return;
end;

y = BigMatrixReal_v3(x1.numRows, x1.numCols);
open(y);

if isOpen(x1)
  was_closed1 = 0;
  privateSeek(x1,0,0);
else
  was_closed1=1;
  open(x1);
end;
if isOpen(x2)
  was_closed2 = 0;
  privateSeek(x2,0,0);
else
  was_closed2=1;
  open(x2);
end;

ok = 1; % Set to 0 if we find a problem.

N = x1.numRows*x1.numCols;
blockSize=1e4;
t1 = clock;
while N>0
  n = min(N, blockSize);
  data1 = fread(x1.handle, n, 'double');
  data2 = fread(x2.handle, n, 'double');
  % The two conditions for illegal operations are (1) 0 raised to a negative power, and (2) negative
  % number raised to non-integer power.
  i = (data1==0 & data2<0 ) | (data1 < 0 & data2~=round(data2));
  % Now, i has flags for the indices of invalid operations.
  if any(i)
    data = zeros(size(data1)) + default;
    data(i) = data1(i) .^ data2(i);
    ok = 0;
  else
    data = data1 .^ data2;
  end;
  fwrite(y.handle, data, 'double');
  N = N - n;
  if etime(clock, t1) > 10
    fprintf(1, 'In %s::%s, ', class(x), whoAmI(u, 'my name'));
    fprintf(1, 'processing element %d of %d.\n', x.numRows * x.numCols - N, x.numRows * x.numCols);
    t1 = clock;
    pause(0.1);
  end;
end;
close(y);
if was_closed1
  close(x1);
end;
if was_closed2
  close(x2);
end;
  
return;
% End of power_BigMatrix_BigMatrix.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [y, ok] = power_BigMatrix_scalar(x, scalar, default)
% 
u = utility01_class;
if BigMatrixReal_v3.privateValidation
  privateCheck(x);
  checkScalarReal(u, scalar);
end;

y = BigMatrixReal_v3(x.numRows, x.numCols);
open(y);

if isOpen(x)
  was_closed = 0;
  privateSeek(x,0,0);
else
  was_closed=1;
  open(x);
end;

N = x.numRows * x.numCols; % Number of samples remaining.
blockSize = 1e4;
t1 = clock;
while N>0
  n = min(N, blockSize); % Number of samples to process in this loop.
  data1 = fread(x.handle, n, 'double');
  % The two conditions for illegal operations are (1) 0 raised to a negative power, and (2) negative
  % number raised to non-integer power.
  i = (data1==0 & scalar<0 ) | (data1 < 0 & scalar~=round(scalar));
  % Now, i has flags for the indices of invalid operations.
  if any(i)
    data2 = zeros(size(data1)) + default;
    i = ~i;
    data2(i) = data1(i) .^ scalar;
    ok = 0;
  else
    data2 = data1 .^ scalar;
  end;
  fwrite(y.handle, data2, 'double');
  N = N - n;
  if etime(clock, t1) > 10
    fprintf(1, 'In %s::%s, ', class(x), whoAmI(u, 'my name'));
    fprintf(1, 'processing element %d of %d.\n', x.numRows * x.numCols - N, x.numRows * x.numCols);
    t1 = clock;
    pause(0.1);
  end;
end;
close(y);
if was_closed
  close(x);
end;
  
return;
% End of power_BigMatrix_scalar.















%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [y, ok] = power_scalar_BigMatrix(scalar, x, default)
% 
u = utility01_class;
if BigMatrixReal_v3.privateValidation
  privateCheck(x);
  checkScalarReal(u, x);
end;

y = BigMatrixReal_v3(x.numRows, x.numCols);
open(y);

if isOpen(x)
  was_closed = 0;
  privateSeek(x,0,0);
else
  was_closed=1;
  open(x);
end;

N = x.numRows * x.numCols; % Number of samples remaining.
blockSize = 1e4;
t1 = clock;
while N>0
  n = min(N, blockSize); % Number of samples to process in this loop.
  data1 = fread(x.handle, n, 'double');
  % The two conditions for illegal operations are (1) 0 raised to a negative power, and (2) negative
  % number raised to non-integer power.
  i = (scalar==0 & data1<0 ) | (scalar < 0 & data1~=round(data1));
  % Now, i has flags for the indices of invalid operations.
  if any(i)
    data2 = zeros(size(data1)) + default;
    data2(i) = scalar .^ data1(i);
    ok = 0;
  else
    data2 = scalar .^ data1;
  end;
  fwrite(y.handle, data2, 'double');
  N = N - n;
  if etime(clock, t1) > 10
    fprintf(1, 'In %s::%s, ', class(x), whoAmI(u, 'my name'));
    fprintf(1, 'processing element %d of %d.\n', x.numRows * x.numCols - N, x.numRows * x.numCols);
    t1 = clock;
    pause(0.1);
  end;
end;
close(y);
if was_closed
  close(x);
end;
  
return;
% End of power_scalar_BigMatrix.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
