function out = privateDataDirectory(in)
% out = privateDataDirectory(in)
% Sets or reads the static class attribute dataDirectory.
%
% Usage 1: Read the attribute value.
% value = privateDataDirectory
%
% Usage 2: Set the attribute value.
% privateDataDirectory(newValue)
% 
% By default, the data directory is set to Matlab's default folder for temporary files.
% You should set the data directory to a folder which you can easily check and clean out, becuase
% this class creates data files to hold object data, and they are not automatically deleted.
%

% This function accesses the static variable which speciifes the directory to use for data files.
persistent dataDirectory
if isempty(dataDirectory)
  % Default value is Matlab's default folder for temporary files.
  x = tempname;
  [dir1, file1, ext1] = fileparts(x);
  dataDirectory = dir1;
end;

if nargin==0
  out = dataDirectory;
else
  dataDirectory = in;
  if ~exist(dataDirectory, 'dir')
    error('Invalid data directory.');
  end;
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
