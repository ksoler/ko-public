function playSound(x, sampleRate, varargin)
% playSound(x, sampleRate)
% Plays sound data in a BigMatrix.
%
% Arguments:
% x is a BigMatrix.  We use the first 1 or 2 columns of it for stereo channels; remaining columns
% are ignored.
%
% sampleRate is the sample rate in Hertz.
%
% Optional tag/value pairs:
% 'block size' is the maximum number of samples to play at once.
%

u = utility01_class;
getArg(u, 'block size->block_size', 1e6);

if BigMatrixReal_v3.privateValidation
  privateCheck(x);
  if x.numCols < 1
    error('Need at least 1 column of data.');
  end;
  if x.numCols > 2
    warning(u, 'Ignoring extra columns, we only use up to 2.');
  end;
end;

%block_size = 1e6; % We play up to 1e6 samples at a time.

if isClosed(x)
  open(x);
  was_closed = 1;
else
  was_closed = 0;
  privateSeek(x, 0, 0); % Go to beginning of file.
end;

if x.numCols==1
  % Mono playback.
  N = x.numRows; % Number of remaining samples.
  first_time = 1;
  while N > 0
    n = min(N, block_size);
    d = fread(x.handle, n, 'double');
    playerObj = audioplayer(d,sampleRate);
    %while ~first_time && isplaying(playerObj)
    %  pause(0.01);
    %end;
    N = N - n;
    fprintf(1, 'Playing a chunk of mono sound, %.1fM of %.1fM samples.\n', (x.numRows-N)/1e6, x.numRows/1e6); 
    playblocking(playerObj);
    first_time = 0;
  end;
    %while isplaying(playerObj)
    %  pause(0.01);
    %end;
  % Done mono playback.
  
elseif x.numCols>1
  % Stereo playback.
  N = x.numRows; % Number of remaining samples.
  first_time = 1;
  while N > 0
    n = min(N, block_size);
    d = fread(x.handle, 2*n, 'double');
    d1 = d(1:2:end);
    d2 = d(2:2:end);
    d = [ d1 d2 ];
    playerObj = audioplayer(d,sampleRate);
    %while ~first_time && isplaying(playerObj)
    %  pause(0.01);
    %end;
    N = N - n;
    fprintf(1, 'Playing a chunk of stereo sound, %.1fM of %.1fM samples.\n', (x.numRows-N)/1e6, x.numRows/1e6); 
    playblocking(playerObj);
    first_time = 0;
  end;
    %while isplaying(playerObj)
    %  pause(0.01);
    %end;
  % Done stereo playback.
end;


if was_closed
  close(x);
end;
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
