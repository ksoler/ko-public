function vcat(x,varargin)
% x = vcat(x,varargin)
% Appends rows from one matrix onto another.
%
% Arguments:
% x is the first input matrix.  The remaining matrix arguments are concatented onto x.
%
% Outputs:
% x is the updated matrix object.
%

n = length(varargin);
if BigMatrixReal_v3.privateValidation
  u = utility01_class;
  privateCheck(x);
  for k = 1  : n
    privateCheck(varargin{k});
    assertion(u, varargin{k}.numCols == x.numCols);
  end;
end;


if isempty(x.handle)
  x_was_closed = 1;
  x.handle = fopen(x.filename, 'r+');
else
  x_was_closed = 0;
end;

privateSeek(x, x.numRows,0); % Move to the end of the file.

for k = 1 : n
  y = varargin{k};
  if isempty(y.handle)
    y_was_closed = 1;
    y.handle = fopen(y.filename, 'r');
  else
    fseek(y.handle, 0, 'bof');
    y_was_closed = 0;
  end;
  
  BigMatrixReal_v3.privateCopyFileData(y.handle, x.handle, y.numRows*y.numCols);
  if y_was_closed
    close(y);
  end;
  x.numRows = x.numRows + y.numRows;
end;

if x_was_closed
  close(x);
end;

if BigMatrixReal_v3.privateValidation
  privateCheck(x);
end;
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
