function x = subsasgn(x,s,value)
% x = subsasgn(x,s,value)
% Subscript assignment function for this class.
% 
% Inputs:
% x is a class object object.
%
% s is a Matlab structure array with fields s.type and s.subs.  Type "help
% subsref" for more information.
%
% value is the value to assign to the selected reference(s).
% 
% Outputs:
% x is the updated object.
%
% Modes:
% (1) Attribute assignment.
% Example:  x.numRows = 20
%
% (2) Assign all data.
% Example:  x.array = rand(5,2)
%
% (3) Round-bracket assignment with Matlab data.
% Example:  x.data(0:9,0:9) = rand(10)
%
% (4) Round-bracked assignment with BigMatrix data.
% Example:  x.BigMatrix(0,0).size(1e8,1) = x.BigMatrix(1e8,1);
%


% Check inputs.
if BigMatrixReal_v3.privateValidation
  u = utility01_class;
  privateCheck(x);
  assertion(u,isa(s,'struct'));
  if length(s) < 1
    error('This should not happen.');
  end;
end;

if length(s) < 1
    error('This should not happen.');
end;

type1 = s(1).type;
ref1  = s(1).subs;

type2 = 'none';
ref2  = 'none';
type3 = 'none';
ref3  = 'none';
type4 = 'none';
ref4  = 'none';

if length(s)>1
  type2 = s(2).type;
  ref2  = s(2).subs;
  if length(s)>2
    type3 = s(3).type;
    ref3  = s(3).subs;
    if length(s) > 3
      type4 = s(4).type;
      ref4  = s(4).subs;
    end;
    if length(s) > 4
      error('This function does not support more than 4 reference levels.');
    end;
  end;
end;

% Syntax: x.array
% Returns a Matlab array with all of the data values.
if isequal(type1, '.') && isequal(ref1, 'array') && length(s) ==1
  if BigMatrixReal_v3.privateValidation
    checkArray2dReal(u,value);
  end;  
  fromMatlabArray(x,value);
  return; 
end;


% Syntax:  x.data(r,c) = value (Matlab array)
% Conditions:  r, c are 1D Matlab arrays.
% If value has one row, then it is repeated across rows as required.
% If value has one column, then it is repeated across columns as required.
if isequal(type1, '.') && isequal(ref1, 'data')
  if BigMatrixReal_v3.privateValidation
    if ~isequal(type2, '()'), error('Expected round-bracket reference after ".data".'); end;
    if length(ref2)~=2, error('Expected exactly two values in round brackets after ".data".'); end;
    r = ref2{1};
    c = ref2{2};
    nr = length(r);
    nc = length(c);
    if ~checkVectorInt(u, r) || ~checkVectorInt(u,c)
      error('Expected 1D Matlab integer arrays for index values after ".data".');
    end;
    if any(r<0) || any(c<0)
      error('Index out of range.');
    end;
%     if any(r>=x.numRows) || any(c>=x.numCols)
%       % If the user tries to assign to a row or column outisde of the defined size, then
%       % automatically resize it.  This mimics Matlab's array behavior.
%       if any(c>=x.numCols)
%         resize(x, x.numRows, max(c)+1);
%       end;
%       if any(r>=x.numRows)
%         resize(x, max(r)+1, x.numCols);
%       end;
%       % error('Index out of range.');
%     end;
    if length(s) > 2
      error('Unexpected references, only expected 2.');
    end;
    checkArray2dReal(u, value);
    if size(value,1)~=1 && size(value,1)~=nr
      error('Invalid number of rows in new value.');
    end;
    if size(value,2)~=1 && size(value,2)~=nc
      error('Invalid number of columns in new value.');
    end;
    
  end; % Validation tests.
  
  r = ref2{1};
  c = ref2{2};
  nr = length(r);
  nc = length(c);

  if any(r>=x.numRows) || any(c>=x.numCols)
    % If the user tries to assign to a row or column outisde of the defined size, then
    % automatically resize it.  This mimics Matlab's array behavior.
    if any(c>=x.numCols)
      resize(x, x.numRows, max(c)+1);
    end;
    if any(r>=x.numRows)
      resize(x, max(r)+1, x.numCols);
    end;
  end;

  % Repeat value across rows, columns as required.
  % This implementation has a weakness:  It instantiates the new value as a Matlab array equal
  % in size to the selected area.  However, if the user invokes x.data(1:1e6,1:1e6)=2 then this
  % function could write the values without trying to instantiate the whole thing as a single Matlab
  % array.
  if size(value,1) == 1
    value = repmat(value, nr,1);
  end;
  if size(value,2) == 1
    value = repmat(value, 1, nc);
  end;
    
  if all(diff(r)==1) && all(diff(c)==1)
    % If the referenced area is contiguous, then write to it as a contiguous rectangle.
    if isempty(x.handle)
      x.handle = fopen(x.filename, 'r+');
      privateWriteRectangle(x, r(1), c(1), value);
      fclose(x.handle);
      x.handle = [];
    else
      privateWriteRectangle(x, r(1), c(1), value);
    end;
    
  else
    % Because the referenced area is not contiguous, read it one sample at a time.
    y = zeros(nr,nc);
    if isempty(x.handle)
      was_closed = 1;
      x.handle = fopen(x.filename, 'r+');
    else
      was_closed = 0;
    end;
    
    for kr = 1 : nr
      for kc = 1 : nc
        privateSeek(x, r(kr), c(kc));
        fwrite(x.handle, value(kr,kc), 'double');
      end;
    end;
    
    if was_closed
      fclose(x.handle);
      x.handle = [];
    end;
    % Done reading non-contiguous references.
  end;
  y = x;
  return;
end; % Done handling ".data" tag with a Matlab array for the value.

% Syntax:  x.BigMatrix(r,c).size(nr,nc) = value (BigMatrix)
% Conditions:  r, c, nr, nc are scalar integers.
if isequal(type1, '.') && isequal(ref1, 'BigMatrix')
  if BigMatrixRead_v3.validation
    if ~isequal(type2, '()'), error('Expected round-bracket reference after ".BigMatrix".'); end;
    if length(ref2)~=2, error('Expected exactly two values in round brackets after ".BigMatrix".'); end;
    r = ref2{1};
    c = ref2{2};
    if ~checkScalarInt(u, r) || ~checkScalarInt(u,c)
      error('Expected 1D Matlab scalar integers for index values after ".BigMatrix".');
    end;
    if ~isequal(type3, '.') || ~isequal(ref3, 'size')
      error('Expected third reference to be ".size".');
    end;    
    if ~isequal(type4, '()') 
      error('Expected fourth reference to be "()".');
    end;    
    if length(ref4)~=2, error('Expected exactly two values in round brackets after ".size".'); end;
    nr = ref4{1};
    nc = ref4{2};
    if ~checkScalarInt(u, nr) || ~checkScalarInt(u,nc)
      error('Expected 1D Matlab scalar integers for index sizes after ".size".');
    end;
    if r<0 || c<0 || nr<0 || nc<0 || r+nr>=x.numRows || c+nc>=x.numCols     
      error('Index out of range.');
    end;
    if length(s) > 4
      error('Unexpected references, only expected 4.');
    end;
    privateCheck(x);
    privateCheck(value);
    if nr ~= value.numRows || nc ~= value.numCols
      error('Size disagreement.');
    end;
  end; % Validation tests.
  
  
  % privateMoveRectangle requires both files to be open.
  value.handle = fopen(value.filename, 'r');
  if isempty(x.handle)
    x.handle = fopen(x.filename, 'r+');
    privateMoveRectangle(y, 0, 0, x, r, c, nr, nc)
    fclose(x.handle);
    x.handle = [];
  else
    privateMoveRectangle(y, 0, 0, x, r, c, nr, nc)
  end;    
  fclose(value.handle);
  value.handle = [];
  y = x;
  return;
end; % Done handling ".BigMatrix" tag.

% Syntax:  x.numRows, x.numCols, x.handle, x.filename, x.validation, x.dataDirectory.
if isequal(type1, '.') && isequal(ref1, 'numRows') || isequal(ref1, 'numCols') || isequal(ref1, 'handle') || isequal(ref1, 'filename') || isequal(ref1, 'validation') || isequal(ref1, 'dataDirectory')
  if length(s)==1
    % Example: x.numRows = 1;
    set(x, ref1, value);
  else
    % Example:  x.filename(4) = 'd';
    temp = get(x,ref1);
    temp = subsasgn(temp, s(2:end), value);
    x = set(x,ref1,temp);
  end;
  return;
end;

error('Unhandled case in subsasgn.');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


