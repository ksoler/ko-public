function privateMoveRectangle(x, rx, cx, y, ry, cy, nr, nc)
% privateMoveRectangle(x, rx, cx, y, ry, cy, nr, nc)
% Copies a rectangular region of data from one matrix to another.
%
% Arguments:
% x is a class object.  Its file must be open.
% rx is the row index of the top left corner in x.
% cx is the column index of the top left corner in x.
% y is a class object.  Its file must be open.
% ry is the row index of the top left corner in y.
% cy is the column index of the top left corner in y.
% nr is the size of the region in rows.
% nc is the size of the region in columns.
%
%
% Postconditions:
% The rectangle of data has been copied from x to y, and the data files for x and y are still open.
% Note that the region must exist within x and y.  This function does not automatically resize
% either.
%
% FUTURE IMPROVEMENT:  Write a specialized case to handle this efficiently if nc=1, i.e. this is a
% column vector, for source and destination.
%
if BigMatrixReal_v3.privateValidation
  u = utility01_class;
  check(x);
  check(y);
  checkScalarInt(u, rx);
  checkScalarInt(u, cx);
  checkScalarInt(u, ry);
  checkScalarInt(u, cy);
  checkScalarInt(u, nr);
  checkScalarInt(u, nc);
  assertion(u, 0 <= rx && 0<=ry);
  assertion(u, 0 <= cx && 0<=cy);
  assertion(u, 0 <= nr && rx+nr<=x.numRows && ry+nr<=y.numRows);
  assertion(u, 0 <= nc && cx+nc<=x.numCols && cx+nc<=y.numCols);
  assertion(u, ~isempty(x.handle)); % We expect the file to be open.
  assertion(u, ~isempty(y.handle)); % We expect the file to be open.
end;

bytesPerSample = x.bytesPerSample;

privateSeek(x, rx, cx);
privateSeek(y, ry, cy);

% Optimized case:  Moving from one column vector to another column vector.
if x.numCols==1 && y.numCols==1 && nc==1
  BigMatrixReal_v3.privateCopyFileData(x.handle, y.handle, nr);
  return;
end;
% End of optimized case:  Moving from one column vector to another column vector.

% To do:  Create additional specialized cases, for things such as moving a column of data to or
% from a matrix.

if nc==x.numCols && nc==y.numCols
  % If we are moving a rectangle as wide as x and y, then the block of data to move is contiguous
  % and we can do it more efficiently.
    privateCopyFileData(x.handle, y.handle, nr*nc*bytesPerSample);
  
elseif nc < 1e6 % We will hold one row of data in a Matlab array.
  t1 = clock;
  for i = 0 : nr-2
    data = fread(x.handle, nc, 'double');
    fwrite(y.handle, data, 'double');
    fseek(x.handle, (x.numCols-nc)*bytesPerSample, 'cof'); % Advance to the next row, at the position we want to read from.
    fseek(y.handle, (y.numCols-nc)*bytesPerSample, 'cof'); % Advance to the next row, at the position we want to read from.
    if etime(clock, t1) > 10
      fprintf(1, 'In %s::%s, ', class(x), whoAmI(utility01_class, 'my name'));
      fprintf(1, 'moved row %.0fK of %.0fK.\n', i/1e3, nr/1e3);
      pause(0.1);
      t1 = clock;
    end;
  end;
  data = fread(x.handle, nc, 'double');
  fwrite(y.handle, data, 'double');
  
else % We assume that the number of columns we transfer is too large for a Matlab array.
  t1 = clock;
  for i = 0 : nr-2
    privateCopyFileData(x.handle, y.handle, nc);
    fseek(x.handle, (x.nc-nc)*bytesPerSample, 'cof'); % Advance to the next row, at the position we want to read from.
    fseek(y.handle, (y.nc-nc)*bytesPerSample, 'cof'); % Advance to the next row, at the position we want to read from.
    privateCopyFileData(x.handle, y.handle, nc);
    if etime(clock, t1) > 10
      fprintf(1, 'In %s::%s, ', class(x), whoAmI(utility01_class, 'my name'));
      fprintf(1, 'moved row %.0fK of %.0fK.\n', i/1e3, nr/1e3);
      pause(0.1);
      t1 = clock;
    end;
  end;
end;

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 