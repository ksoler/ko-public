function y = subsref(x,s)
% y = subsref(x,s)
% Subscript index function for this class.
% 
% Inputs:
% x is a class object object.
%
% s is a Matlab structure array with fields s.type and s.subs.  Type "help
% subsref" for more information.
%
% Outputs:
% y is the value of the requested attribute or reference from the object.
%
% Examples of supported modes:
% x.numCols     read an attribute value
% x.data(r,c)   return a Matlab array.
% x.array       returns all of the data in a Matlab array.
% x.BigMatrix(r,c).size(nr,nc) returns a BigMatrix object.
%
% Supported Modes:
% This function supports subsript references of object attributes.  Set the 'get' function
% for this class.
%
% Also, with the virtual fieldname 'data', this function supports round-bracket indexing of data.
% There must be exactly two indices (i.e. x(i,j)), and their indices must be contiguous.
%
% Also, the virtual field name 'array' returns a Matlab array of all of the object's data.
%

% Check inputs.
if BigMatrixReal_v3.privateValidation
  u = utility01_class;
  privateCheck(x);
  assertion(u,isa(s,'struct'));
  % Done checking inputs.
  
  % There are three main types of references, which might be found in s.type.
  % Dot-field, round brackets, and curly brackets.  This function supports them as follows:
  %
  % x.size     accesses an object attribute (size, handle, filename).
  % x(1:10).array returns a Matlab array.
  % x(1:10).file returns a new class object (bigVectorReal1_class).
  %
  if length(s) < 1
    error('This should not happen.');
  end;
end;

ref_type = s(1).type;
ref      = s(1).subs;
more_refs = s(2:end);  % Note that this might be empty.

% There are three main types of references, which might be found in s.type.
% Dot-field, round brackets, and curly brackets.  This function only
% supports the dot-field references.

if length(s) < 1
    error('This should not happen.');
end;

type1 = s(1).type;
ref1  = s(1).subs;

type2  = 'none';
ref2   = 'none';
type3 = 'none';
ref3  = 'none';
type4 = 'none';
ref4  = 'none';

if length(s)>1
  type2 = s(2).type;
  ref2  = s(2).subs;  if length(s)>2
    type3 = s(3).type;
    ref3  = s(3).subs;
    if length(s) > 3
      type4 = s(4).type;
      ref4  = s(4).subs;
    end;
    if length(s) > 4
      error('This function does not support more than 4 reference levels.');
    end;
  end;
end;

% Syntax: x.array
% Returns a Matlab array with all of the data values.
if isequal(type1, '.') && isequal(ref1, 'array') && length(s) ==1
  y = toMatlabArray(x);
  return; 
end;


% Syntax:  x.data(r,c)
% Conditions:  r, c are 1D Matlab arrays.
if isequal(type1, '.') && isequal(ref1, 'data')
  if BigMatrixReal_v3.privateValidation
    if ~isequal(type2, '()'), error('Expected round-bracket reference after ".data".'); end;
    if length(ref2)~=2, error('Expected exactly two values in round brackets after ".data".'); end;
    r = ref2{1};
    c = ref2{2};
    if ~checkVectorInt(u, r) || ~checkVectorInt(u,c)
      error('Expected 1D Matlab integer arrays for index values after ".data".');
    end;
    if any(r<0) || any(r>=x.numRows) || any(c<0) || any(c>=x.numCols)
      error('Index out of range.');
    end;
    if length(s) > 2
      error('Unexpected references, only expected 2.');
    end;
  end; % Validation tests.
  
  r = ref2{1};
  c = ref2{2};
  nr = length(r);
  nc = length(c);
  if all(diff(r)==1) && all(diff(c)==1)
    % If the referenced area is contiguous, then read it as a contiguous rectangle.
    if isempty(x.handle)
      x.handle = fopen(x.filename, 'r');
      y = privateReadRectangle(x, r(1), c(1), nr, nc);
      fclose(x.handle);
      x.handle = [];
    else
      y = privateReadRectangle(x, r(1), c(1), nr, nc);
    end;
    
  else
    % Because the referenced area is not contiguous, read it one sample at a time.
    y = zeros(nr,nc);
    if isempty(x.handle)
      was_closed = 1;
      x.handle = fopen(x.filename, 'r');
    else
      was_closed = 0;
    end;
    
    for kr = 1 : nr
      for kc = 1 : nc
        privateSeek(x, r(kr), c(kc));
        y(kr,kc) = fread(x.handle, 1, 'double');
      end;
    end;
    
    if was_closed
      fclose(x.handle);
      x.handle = [];
    end;
    % Done reading non-contiguous references.
  end;
  return;
end; % Done handling ".data" tag.

% Syntax:  x.BigMatrix(r,c).size(nr,nc)
% Conditions:  r, c, nr, nc are scalar integers.
if isequal(type1, '.') && isequal(ref1, 'BigMatrix')
  if BigMatrixReal_v3.privateValidation
    if ~isequal(type2, '()'), error('Expected round-bracket reference after ".BigMatrix".'); end;
    if length(ref2)~=2, error('Expected exactly two values in round brackets after ".BigMatrix".'); end;
    r = ref2{1};
    c = ref2{2};
    if ~checkScalarInt(u, r) || ~checkScalarInt(u,c)
      error('Expected 1D Matlab scalar integers for index values after ".BigMatrix".');
    end;
    if ~isequal(type3, '.') || ~isequal(ref3, 'size')
      error('Expected third reference to be ".size".');
    end;    
    if ~isequal(type4, '()') 
      error('Expected fourth reference to be "()".');
    end;    
    if length(ref4)~=2, error('Expected exactly two values in round brackets after ".size".'); end;
    nr = ref4{1};
    nc = ref4{2};
    if ~checkScalarInt(u, nr) || ~checkScalarInt(u,nc)
      error('Expected 1D Matlab scalar integers for index sizes after ".size".');
    end;
    if r<0 || c<0 || nr<0 || nc<0 || r+nr>x.numRows || c+nc>x.numCols     
      error('Index out of range.');
    end;
    if length(s) > 4
      error('Unexpected references, only expected 4.');
    end;
  end; % Validation tests.
  
  y = BigMatrixReal_v3;
  resize(y, nr, nc);
  
  % privateMoveRectangle requires both files to be open.
  y.handle = fopen(y.filename, 'r+');
  if isempty(x.handle)
    x.handle = fopen(x.filename, 'r');
    privateMoveRectangle(x, r, c, y, 0, 0, nr, nc)
    fclose(x.handle);
    x.handle = [];
  else
    privateMoveRectangle(x, r, c, y, 0, 0, nr, nc)
  end;    
  fclose(y.handle);
  y.handle = [];
  return;
end; % Done handling ".BigMatrix" tag.

% Syntax:  x.numRows, x.numCols, x.handle, x.filename, x.validation, x.dataDirectory.
if isequal(type1, '.') && isequal(ref1, 'numRows') || isequal(ref1, 'numCols') || isequal(ref1, 'handle') || isequal(ref1, 'filename') || isequal(ref1, 'validation') || isequal(ref1, 'dataDirectory')
  y = get(x, ref1);
  if length(s)>1
    y = subsref(y, s(2:end));
  end;
  return;
end;

error('Unhandled case in subsref.');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
