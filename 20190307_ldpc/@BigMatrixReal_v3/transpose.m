function y = transpose(x)
% y = transpose(x)
% Non-conjuating transpose operator, e.g. x',
% Since BigMatrixReal_v3 class supports real, not complex values, this is the same as the 
% complex transpose operation.
%
% Arguments:
% x is the input.
% y is the BigMatrixReal_v3 result.
%

u = utility01_class;
if BigMatrixReal_v3.privateValidation
  privateCheck(x);
end;

y = copy(x);
y.numRows = x.numCols;
y.numCols = x.numRows;

if x.numCols==1 || x.numRows==1
  % Optimized implementation for vectors.
  return;
end;

open(y);
if isOpen(x)
  was_closed = 0;
  privateSeek(x,0,0);
else
  was_closed=1;
  open(x);
end;

blockSize = 1e4;
t1 = clock;
for col = 0 : x.numCols - 1
  % Read one column from x at a time.
  privateSeek(x, 0, col);
  N = x.numRows; % Number of remaining samples.
  while N > 0
    n = min(N, blockSize);  % Number of samples to process in this pass.
    data = fread(x.handle, n, 'double', (x.numCols-1)*x.bytesPerSample);
    fwrite(y.handle, data, 'double');
    N = N - n;
  end;
  if etime(clock, t1) > 10
    fprintf(1, 'In %s::%s, ', class(x), whoAmI(u, 'my name'));
    fprintf(1, 'processing element %d of %d.\n', x.numRows * x.numCols - N, x.numRows * x.numCols);
    t1 = clock;
    pause(0.1);
  end;
end;
close(y);
if was_closed
  close(x);
end;
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
