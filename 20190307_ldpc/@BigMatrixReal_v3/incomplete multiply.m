function z = multiply(x, y, z)
% z = multiply(x,y,z)
% Matrix multiplication.
%
% x is the first input matrix.
% y is the second input matrix.
% z is the matrix that will hold the result.  If this is omitted, then a new matrix is created.
%
% Outputs:
% z is the result.
%
u = utility01_class;

% Create the output matrix if required.
if nargin < 2
  z = BigMatrixReal_v3;
  assertion(u, nargout==1);
end;
if nargin==3
  assertion(u, nargout==0);
end;

% These multiplication functions have not been implemented yet!  
% We should also implment and add function too, and multiplication by a scalar.
error('This function has not been completed!');

% To do:  Do something about opening files!
if checkArray2dReal(u,x) && isa(y, 'BigMatrixReal_v3')
  z = privateMutliply_matlabArray_bigMatrix(x, y, z);
elseif isa(x, 'BigMatrixReal_v3') && checkArray2dReal(u,y)
  z = privateMutliply_bigMatrix_matlabArray(x, y, z);
elseif isa(x, 'BigMatrixReal_v3') && isa(y, 'BigMatrixReal_v3')
  privateMultiply_bigMatrix_bigMatrix(x,y,z);
else
  error('Unsupported datatypes.');
end;

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  