function [m, r, c] = min(x)
% [m,r,c] = min(x)
% Finds minimum value in data.
%
% Arguments:
% x is a BigMatrixReal_v3 object.  It must not be empty.
% m is the minimum value.
% r is the row index of the minimum value.
% c is the column index of the minimum value.
%

verbose = 0;


u = utility01_class;
if verbose
  fprintf(1, 'Starting %s.\n', whoAmI(u, 'my name'));
end;


if BigMatrixReal_v3.privateValidation
  privateCheck(x);
  if x.numRows==0 || x.numCols==0
    error('Input must not be empty.');
  end;
end;

if isClosed(x)
  open(x);
  was_closed = 1;
else
  was_closed = 0;
  privateSeek(x,0,0);
end;

t1 = clock;
block_size = 1e5;
N = x.numRows * x.numCols; % Remaining number of samples.
m = [];
while N > 0
  n = min(N, block_size);
  data = fread(x.handle, n, 'double');
  if isempty(m) || m < min(data)
    [m,i] = min(data);
    i = i - 1;
    i = i + x.numRows * x.numCols - N;
    % Now, i is the sample index in the file.
    r = floor(i/x.numCols);
    c = mod(i, x.numCols);
  end;
  if etime(clock,t1) > 10
    fprintf(1, 'In %s::%s, ', class(x), whoAmI(u, 'my name'));
    fprintf(1, 'processed %.1fM of %.1fM samples.\n', (x.numRows * x.numCols - N)/1e6, (x.numRows * x.numCols)/1e6);
  end;
    N = N - n;
end;

if was_closed
  close(x);
end;

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
