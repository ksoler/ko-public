function delete(x)
% Deletes a BigMatrixReal_v3 object, including the data file.

u = utility01_class;
if ~isvalid(x)
  return;
  warning(u, sprintf('Encountered invalid handle in %s::delete.\n', class(x)));
  return;
end;

if isOpen(x)
%  error('You must close the file before deleting the object.');
  warning(u, 'Closing file of %s object before deleting it.\n', class(x));
  close(x);
end;
if exist(x.filename, 'file')
delete(x.filename);
else
  warning(u, 'File %s does not exist.  That''s probably okay.\n', x.filename);
end;
return;
