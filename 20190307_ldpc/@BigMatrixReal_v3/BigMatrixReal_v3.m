classdef BigMatrixReal_v3 < handle
  % BigMatrixReal_v3
  %
  % This class impelments a data structure which is a matrix (2D numeric array).
  %  - Data is stored in a file instead of in memory.
  %  - Very large matrix sizes are supported.
  %  - These are handle objects.  When used as function arguments, they are passed by reference, not
  %    by value.  Assignment creates an alias, not a copy.
  %
  % Tips for usage:
  % - Data access is different from Matlab arrays, to help prevent confusion between the two,
  %   e.g. x.data(i,j).
  % - Turn off validation diagnostics to speed up execution, like this:
  %   set(BigMatrixReal_v3, 'validation', 0);
  % - Set the data directory to a folder you can clean out easily when it gets too large, like this:
  %   set(BigMatrixReal_v3, 'dataDirectory', 'F:\2019-matlab data\BigMatrix'.  Old data is not
  %   autoamtically deleted.
  %
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % PROPERTIES                                                                                     %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  properties
    numRows  = 0;
    numCols  = 0;
    filename = '';
    handle   = [];
  end % properties
  
  properties (Constant)
    bytesPerSample = 8;
  end % constant properties
  
  properties (Dependent)
    % These properties are treated as static (class-wide) properties.
    validation
    dataDirectory
  end;
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % PRIVATE STATIC METHODS                                                                         %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  methods (Static, Access = private)
    privateCopyFileData(h1, h2, n)
    out = privateDataDirectory(in)
    out = privateValidation(in)
    y = privateFileHandleToString(h)
    s = privateGenerateFilename
    privateWriteRepeatedValue(handle, value, n)
    y = privateIsValidSavedFile(filename)
    y = privateRandomPermutation(numRows)
  end  % private static methods
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % PUBLIC STATIC METHODS                                                                          %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  methods (Static, Access = public)
    
  end  % public static methods
  
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % PRIVATE METHODS                                                                                %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  methods (Access = private)
    % Note that in general, private methods may require a data file to be open or closed.
    privateCopyRowWithinFile(x, r1, r2)
    privateMoveRectangle(x, rx, cx, y, ry, cy, nr, nc)
    privateSeek(x, row, col)
    y = privateReadRectangle(x, r, c, nr, nc)
    privateWriteRectangle(x, r, c, data)
    [ok, msg] = privateCheck(x)
    y = privateStealthReadRectangle(x, r, c, nr, nc)
    y = privateStealthRead(x, r, c, n)
  end % Private methods.
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % PUBLIC METHODS                                                                                 %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %  Argument name conventions:
  %    - x, y, z are BigMatrixReal_v3 objects.
  %    - All other argumets are Matlab arrays.
  %    - There are some exceptions, for example, when an argument can be a BigMatrix or a Matlab
  %      array.
  %  File state conventions:
  %    - All public methods accept objects with open or closed data files, and preserve the
  %      open/close state.
  %    - There are some reasonable exceptions, such as close, isClosed, open, isOpen.
  %
  methods
    % Conversion
    load(x, filename)
    save(x, filename)
    loadText(x, filename)
    saveText(x, filename)
    arr = toMatlabArray(x)
    fromMatlabArray(arr, d)
    
    % Diagnostics and user interface.
    [ok, msg] = check(x)
    s = disp(obj,level)
    examineFile(x)
    plotColumns(x, varargin)
    playSound(x, sampleRate, varargin)
    
    % Non-data utilities.
    set(x,name,value)
    value = get(x,name)
    value = subsref(x,s)
    y = subsasgn(x,s,value)
    open(x)
    close(x)
    flag = isOpen(x)
    flag = isClosed(x)
    delete(x)
    y = same(x1,x2)
    
    % Math and signal processing.
    z = multiply(x,y,z)
    value = sum(x)
    arr = sumAlongColumns(x)
    z = conv(x,y,varargin)
    [m, r, c] = max(x);
    [m, r, c] = min(x);
    [m, r, c] = maxAbs(x);
    
    % Operators.
    y = plus(x1,x2)
    y = minus(x1,x2) % a - b	Binary subtraction
    y = uminus(x) % -a	Unary minus
    y = uplus(x) % +a	Unary plus
    y = times(x1,x2) % a.*b	Element-wise multiplication
    y = mtimes(x1,x2) % a*b	Matrix multiplication
    [y,ok] = rdivide(x1,x2,default) % a./b	Right element-wise division
    [y,ok] = ldivide(x1,x2,default) % a.\b	Left element-wise division
    [y,ok] = mrdivide(x1,x2,default) % a/b	Matrix right division
    [y,ok] = mldivide(x1,x2,default) % a\b	Matrix left division
    [y,ok] = power(x1,x2, default) % a.^b	Element-wise power
    y = mpower(x1,x2) % x1^x2	Matrix power
    y = lt(a,b) % a < b	Less than
    y = gt(a,b) % a > b	Greater than
    y = le(a,b) % a <= b	Less than or equal to
    y = ge(a,b) % a >= b	Greater than or equal to
    y = ne(a,b) % a ~= b	Not equal to
    y = eq(a,b,s) % a == b	Equality
    y = and(a,b) % a & b	Logical AND
    y = or(a,b) % a | b	Logical OR
    not(a) % ~a	Logical NOT
    % colon(a,d,b) % colon(a,b)	a:d:b
    % a:b	Colon operator
    y = ctranspose(a) % a'	% Complex conjugate transpose
    y = transpose(a) % a.'	 Matrix transpose
    y = horzcat(varargin) % [a b]	% Horizontal concatenation
    y = vertcat(varargin) % [a; b]	% Vertical concatenation
    % subsref(a,s) % a(s1,s2,...sn)	% Subscripted reference
    % % subsasgn(a,s,b) % a(s1,...,sn) = b	Subscripted assignment
    % subsindex(a) % b(a)	Subscript index
    %
    
    % Data utilities.
    appendRow(x,rowData)
    y = copy(x)
    reorderRows(x, order)
    shuffleRows(x)
    shuffleRowsMultiple(varargin)
    reshapeColumns(x, nc)
    vcat(x,varargin)
    
    function x = BigMatrixReal_v3(varargin)
      % Usage:
      % x = BigMatrixReal_v3                 % Createa a default object.
      % x = BigMAtrixReal_v3(3,4)            % Creates and sets size.
      % x = BigMatrixReal_v3(rand(3,4))      % Creates from a Matlab array.
      % x = BigMatrixReal_v3('test.wav', 'Fs') % Import a WAVE file. Sample rate is assigned to Fs.
      % x = BigMatrixReal_v3('saved.dat');   % Import a saved BigMatrix file.
      % x = BigMatrixReal_v3('saved.txt');   % Import a saved text file.
      % x = BigMatrixReal_v3('numRows', 10); %  Tag/value pairs passed to 'set' function.
      %
      u = utility01_class;
      x.filename = BigMatrixReal_v3.privateGenerateFilename;
      x.handle = fopen(x.filename, 'w');
      fclose(x.handle);
      x.handle = [];
      
      % Create a default object.
      if nargin==0
        return;
      end;
      
      % Convert from a Matlab array.
      if nargin==1 && ~ischar(varargin{1})
        fromMatlabArray(x, varargin{1});
        return;
      end;
      
      % Import a WAVE file.
      % Check if there are one or two arguments, and the first is a string, ending in '.wav'.
      % The second optional argument is the name of the variable for the sample rate.
      % I would rather return the sample rate as a second output argument, but Matlab doesn't allow
      % that.
      if nargin<=2 && ischar(varargin{1}) && stringPatternMatch(u,'*.wav', lower(varargin{1}), 's2 wildcards', 0)
        % This discards the BigMatrix that we just created.
        delete(x);
        [ x, sampleRate ] = wav_read(u, varargin{1});
        if nargin==2
          if isempty(varargin{2})
            % Ignore the second argument if it is empty.
          elseif checkString(u, varargin{2})
            assignin('caller', varargin{2}, sampleRate);
          else            
            error('The second argument for wave file conversion should be a valid variable name, or empty.');
          end;
        else
          fprintf( 1, 'sampleRate=%g\n', sampleRate);
        end;
        return;
      end;  % Done importing a WAVE file.
      
      % Import a saved BigMatrix data file.
      % Check if there is one argument, a string, ending in '.dat'.
      if nargin==1 && ischar(varargin{1}) && stringPatternMatch('*.dat', lower(varargin{1}), 's2 wildcards', 0)
        % This discards the BigMatrix that we just created.
        load(x, varargin{2});
        return;
      end;
      
      % Import a saved text data file.
      % Check if there is one argument, a string, ending in '.txt'.
      if nargin==1 && ischar(varargin{1}) && stringPatternMatch('*.txt', lower(varargin{1}), 's2 wildcards', 0)
        % This discards the BigMatrix that we just created.
        loadText(x, varargin{2});
        return;
      end;
      
      % Set size.
      if nargin==2 && ~ischar(varargin{1})
        resize(x, varargin{1}, varargin{2});
        return;
      end;
      
      % Pass tag/value arguments to 'set' method.
      set(x, varargin{:});
      
    end %  constructor.
    
    
    %     function y = get.validation(obj)
    %       y = BigMatrixReal_v3.privateValidation;
    %     end;
    %
    %     function set.validation(obj,value)
    %       BigMatrixReal_v3.privateValidation(value)
    %     end;
    %
    %     function y = get.dataDirectory(obj)
    %       y = BigMatrixReal_v3.privateDataDirectory;
    %     end;
    %
    %     function set.dataDirectory(obj,value)
    %       BigMatrixReal_v3.privateDataDirectory(value);
    %     end;
    
  end % methods
  
  
  
end % classdef

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
