function privateWriteRectangle(x, r, c, data)
% privateWriteRectangle(x, r, c, data)
% Writes to a rectangular region within a matrix.
%
% x is a class object.  Its file must be open.
% r is the row index of the top left corner.
% c is the column index of the top left corner.
% data is the 2D Matlab array to write into the matrix.
%
% Note that the region must exist within the matrix.  This function does not automatically resize 
% the matrix.
%

if BigMatrixReal_v3.privateValidation
  u = utility01_class;
  privateCheck(x);
  checkScalarInt(u, r);
  checkScalarInt(u, c);
  checkArray2dReal(u, data);
  nr = size(data,1);
  nc = size(data,2);
  assertion(u, 0 <= r && r < x.numRows);
  assertion(u, 0 <= c && c < x.numCols);
  assertion(u, 0 <= nr && r+nr<=x.numRows);
  assertion(u, 0 <= nc && c+nc<=x.numCols);
  assertion(u, ~isempty(x.handle)); % We expect the file to be open.
else
  nr = size(data,1);
  nc = size(data,2);
end;

result= fseek(x.handle, (r*x.numCols+c)*x.bytesPerSample, 'bof');  % Don't forget, 8 bytes per number.
if result==-1
  error('fseek failed.');
end;

if size(data,2)==1
  % Specialized case for a column vector.
  if x.numCols==1
    fwrite( x.handle, data, 'double');
  else
    if length(data)>0
      fwrite( x.handle, data(1), 'double');
    end;
    if length(data)>1
      fwrite( x.handle, data(2:end), 'double', (x.numCols-size(data,2))*x.bytesPerSample);
    end;
  end;
  return;
end;

for i = 0 : nr-2
  fwrite(x.handle, data(i+1, :), 'double');  
  fseek(x.handle, (x.numCols-nc)*x.bytesPerSample, 'cof'); % Advance to the next row, at the position we want to read from.
end;
  fwrite(x.handle, data(nr, :), 'double');
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 