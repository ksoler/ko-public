function [y, ok] = mrdivide(x1,x2,default)
% [y, ok] = mrdivide(x1, x2, default)
% Matlab "/" aoperator, matrix right division, for BigMatrixReal_v3.
% This is the same as matrix left division.
%
% Arguments:
% x1 is the first input.
%
% x2 is the second input.
%
% default is the value to use for elements affected by division by zero.  This argument is optional;
% the default for it is 0.
%
% y is the BigMatrixReal_v3 result.
%
% ok is 0 if any problems were found (typically, division by 0).
%
% Exactly one of x1,x2 must be a BigMatrixReal_v3, and exactly one must be a real scalar.
% If x1 is scalar, then this function returns a BigMatrix the same size as x2, and each element is 
% x1 / (corresponding element of x2).
%

if nargin < 3
  default = 0;
end;

ok = 1;


isScalar1 = isnumeric(x1) && numel(x1)==1;
isScalar2 = isnumeric(x2) && numel(x2)==1;

if BigMatrixReal_v3.privateValidation
  u = utility01_class;
  if isScalar1
    checkScalarReal(u, x1);
  else
    privateCheck(x1);
  end;
  if isScalar2
    checkScalarReal(u, x2);
  else
    privateCheck(x2);
  end;
  checkScalarReal(u, default);
end;


if isa(x1, 'BigMatrixReal_v3') && isScalar2
  % (BigMatrix) / (scalar)
  [y, ok] = div_BigMatrix_scalar(x1, x2, default);
elseif isScalar1 && isa(x2, 'BigMatrixReal_v3')
  % (scalar) / (BigMatrix)
  [y, ok] = div_scalar_BigMatrix(x1, x2, default);
else
  error('Invalid inputs.');
end;
if ~ok && nargout <  2 && nargin < 3
  warning(u, 'Attempted division by zero.');
end;


return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [y, ok] = div_BigMatrix_scalar(x, scalar, default)
u = utility01_class;

if BigMatrixReal_v3.privateValidation
  privateCheck(x);
end;

y = BigMatrixReal_v3(x.numRows, x.numCols);
open(y);
if isOpen(x)
  was_closed = 0;
  privateSeek(x,0,0);
else
  was_closed=1;
  open(x);
end;

if scalar==0
  % Division by zero.
  N = x.numRows*x.numCols; % Number of remaining samples.
  BigMatrixReal_v3.privateWriteRepeatedValue(y.handle, default, N)
  ok = 0;
else
  ok = 1;
  N = x.numRows*x.numCols; % Number of remaining samples.
  blockSize=1e4;
  t1 = clock;
  while N>0
    n = min(N, blockSize);  % Number of samples to process in this pass.
    data = fread(x.handle, n, 'double');
    data = data / scalar;
    fwrite(y.handle, data, 'double');
    N = N - n;
    if etime(clock, t1) > 10
      fprintf(1, 'In %s::%s, ', class(x), whoAmI(u, 'my name'));
      fprintf(1, 'processing element %d of %d.\n', x.numRows * x.numCols - N, x.numRows * x.numCols);
      t1 = clock;
      pause(0.1);
    end;
  end;
end;

close(y);
if was_closed
  close(x);
end;
return;
% End of div_BigMatrix_scalar.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [y, ok] = div_scalar_BigMatrix(scalar, x, default)
u = utility01_class;

if BigMatrixReal_v3.privateValidation
  privateCheck(x);
end;

y = BigMatrixReal_v3(x.numRows, x.numCols);
open(y);
if isOpen(x)
  was_closed = 0;
  privateSeek(x,0,0);
else
  was_closed=1;
  open(x);
end;

  ok = 1;
  N = x.numRows*x.numCols; % Number of remaining samples.
  blockSize=1e4;
  t1 = clock;
  while N>0
    n = min(N, blockSize);  % Number of samples to process in this pass.
    data = fread(x.handle, n, 'double');
    if any(data==0)
      ok = 0;
      i = data~=0;
      original_data = data;
      data = 0*data + default;
      numerator = repmat(scalar, sum(i), 1);
      data(i) = numerator ./ original_data(i);
    else
      data = (scalar+zeros(size(data))) ./ data;
    end;
    fwrite(y.handle, data, 'double');
    N = N - n;
    if etime(clock, t1) > 10
      fprintf(1, 'In %s::%s, ', class(x), whoAmI(u, 'my name'));
      fprintf(1, 'processing element %d of %d.\n', x.numRows * x.numCols - N, x.numRows * x.numCols);
      t1 = clock;
      pause(0.1);
    end;
  end;

close(y);
if was_closed
  close(x);
end;
return;
% End of div_scalar_BigMatrix.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
