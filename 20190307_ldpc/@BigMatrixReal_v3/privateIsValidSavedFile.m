function y = privateIsValidSavedFile(filename)
% y = privateIsValidSavedFile(filename)
% Checks if a file conforms to this class's format for saved data files.
%
% Arguments:
% filename is ostensibly the filename of a saved file (created using the save method).
%
% y is 1 if the file is valid, 0 otherwise.
%

u = utility01_class;
n = fileSizeInBytes(u, filename);
if n < 24
  y = 0;
  return;
  % This cannot be a valid saved file because it is not large enough to hold the metadata.
end;


h = fopen(filename, 'r');
result = fseek(h, -3*BigMatrixReal_v3.bytesPerSample, 'eof'); % Remember, 8 bytes per number.
if result==-1
  error('fseek failed.');
end;
  
d = fread(h, 3, 'double');
fclose(h);

magic_number = d(1);
nr = d(2);
nc = d(3);

y = 0;
if magic_number ~= 3, return, end;

% Check that the file is the expected size:  nr*nc numbers, and 3 more for the metadata.
% 8 bytes per number.
% Note that this class allows working data files to have excess, ignored data.  However, when
% creating a saved file, there is not excess data.  Therefore, the file size should be exactly as we
% expect.
if (nr*nc+3)*BigMatrixReal_v3.bytesPerSample ~= n
  return;
end;
y = 1;
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
