function saveText(x, filename)
% Write matrix data to a text file.
%
% Arguments:
% x is a BigMatrixReal_v3 object.
% filename is the file to write to.
%
% You can use the 'loadText' method to read it.
%

u = utility01_class;
if BigMatrixReal_v3.privateValidation
  checkString(u, filename);
  privateCheck(x);
end;

if isempty(x.handle)
  open(x);
  was_closed = 1;
else
  was_closed = 0;
end;

t1 = clock;
h = fileOpen(u, filename, 'wt');
fseek(x.handle, 0, 'bof');

if 1
  blockSize = 1e4;
  for row = 0 : x.numRows-1    
    % Write one row of data.
    num_remaining = x.numCols;
    while num_remaining > 0
      num_transfer = min(num_remaining, 1e4);
      data = fread(x.handle, num_transfer, 'double');
      fprintf(h, '%g ', data);
      num_remaining = num_remaining - num_transfer;
    end;
    fprintf(h, '\n');    
    % Done writing one row of data.
    if etime(clock, t1) > 10
      fprintf( 1, 'Wrote %d of %d lines to file %s.\n', row, x.numRows, fileBareName(u, filename));
      pause(0.1);
      t1 = clock;
    end;
  end;
end;
fclose(h);  


if 0
  % This assumes that we can hold an entire row in a Matlab array.  For robustness, we should use 2
  % loops and transfer up to 1e4 samples at a time.
  for row = 0 : x.numRows-1
    data = fread(x.handle, x.numCols, 'double');
    fprintf(h, '%g ', data);
    fprintf(h, '\n');
    if etime(clock, t1) > 10
      fprintf( 1, 'Wrote %d of %d lines to file %s.\n', row, x.numRows, fileBareName(u, filename));
      pause(0.1);
      t1 = clock;
    end;
  end;
end;
fclose(h);  

if was_closed
  close(x);
end;

  
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
