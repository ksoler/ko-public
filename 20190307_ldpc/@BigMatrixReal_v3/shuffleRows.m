function shuffleRows(x)
% x = shuffleRows(x)
% Re-orders the rows of a BigMatrix randomly.
% 
% Inputs:
% x is a class object.  On exit, its rows have been re-ordered randomly.
% 

% Algorithm:
% Create empty, new file.
% Loop
%   Select one of the rows of x, at random.
%   Copy that row from x to y.
%   Delete that row from x, by copying the last row of x over it, and decrementing numRows.
% End loop
% Copy the new file from y to x.
%
% Future Improvement:  When the number of remaining elements to be moved is less than 1e6, then
% switch to using a Matlab array to hold the indices of markers.
%
u = utility01_class;

if BigMatrixReal_v3.privateValidation
  u = utility01_class;
  privateCheck(x);
  % Done checking inputs.
end;

if isempty(x.handle)
  was_open = 0;
else
  was_open = 1;
  fclose(x.h);
  x.handle = [];
end;

y = BigMatrixReal_v3;
resize(y, x.numRows, x.numCols);

open(x);
open(y); % We assume this puts file pointer at the beginning.
num_rows = x.numRows;

if x.numCols < 1e5
  % Hold a row in a Matlab array.
  
  t1 = clock;
  for k = 0 : num_rows-1
    % Algorithm:  Select a row of x at random, copy it to y.  In x, move the last row in to replace the removed row.
    % That way, we keep the elements of x contiguous.
    i = floor(rand * x.numRows); % Randomly selected index, of remaining rows of x.
    rowData = privateReadRectangle(x, i, 0, 1, x.numCols); % Read the selected row from x.
    fwrite(y.handle, rowData, 'double');                   % Write that row into y.
    
    % Delete the selected from from x by moving x's last row over it.
    rowData = privateReadRectangle(x, x.numRows-1, 0, 1, x.numCols);
    privateWriteRectangle(x, i, 0, rowData);
    x.numRows = x.numRows - 1; % Reduce number of rows in x.
    if etime(clock,t1) > 10
      fprintf(1, 'In %s, k = %d of %d.\n', whoAmI(u, 'my name'), k, num_rows);
      pause(0.1);
      checkLockFile(u);
      t1 = clock;
    end;
    
  end;
  
else
  % Assume one row may be too large to hold in a Matlab array.
t1 = clock;
for k = 0 : num_rows-1
% Algorithm:  Select a row of x at random, copy it to y.  In x, move the last row in to replace the removed row.
% That way, we keep the elements of x contiguous.
  i = floor(rand * x.nr); % Randomly selected index, of remaining rows of x.
  privateSeek(x, i, 0);
  
  privateCopyFileData(x.handle, y.handle, x.numCols); % Copy a row from x into y.
  privateCopyRowWithinFile(x, x.numRows-1, i);
  x.numRows = x.numRows - 1; % Reduce number of rows in x.
    if etime(clock,t1) > 10
      fprintf(1, 'In %s, k = %d of %d.\n', whoAmI(u, 'my name'), k, num_rows);
      pause(0.1);
      checkLockFile(u);
      t1 = clock;
    end;
end;
end;

% Now, we have copied the data into y.
close(x);
close(y);
%delete(x.filename);
movefile(y.filename, x.filename);
x.numRows = num_rows;
% y is now defunct - its file is gone.



if was_open
  open(x);
end;
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
