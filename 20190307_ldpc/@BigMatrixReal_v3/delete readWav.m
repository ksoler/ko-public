function sampleRate = readWav(x, filename)
% readWav(x, filename)
% Reads data from a WAVE audio file.
% 
% Inputs:
% x is the BigMatrix to e read into.  This function updates it with the data from the file.
% filename is the filename to read from.
% 
% Outputs:
% samplerRate is the sample rate in Hertz.
%
% 

u = utility01_class;

if BigMatrixReal_v3.privateValidation
  privateCheck(x);
end;

% Find the data chunks.
[ types, offsets, sizes, numChunks, sampleRate] = wav_read_chunk_info(u, filename);





dataChunkOffsets = [];
dataChunkSizes = [];
numDataChunks = 0;
for k = 1 : numChunks
  if isequal(types{k}, 'data')
    dataChunkOffsets(end+1) = offsets(k);
    dataChunkSizes(end+1) = sizes(k);
    numDataChunks = numDataChunks + 1;
  end;
end;

if numDataChunks ~= 1
  fprintf(1, 'Warning in %s::%s:  ', class(x), whoAmI(u, 'my name'));
  fprintf(1, 'WAVE file has %d data chunks, expected just one.\n', numDataChunks);
end;

if numDataChunks==0


if numDataChunks==1
  
for 
[ dataChunkOffsets, dataChunkSizes, sampleRate ]  = 

