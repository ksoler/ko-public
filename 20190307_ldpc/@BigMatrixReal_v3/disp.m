function y = disp(obj,level)
% s = disp(x, level)
% x is a class object.  Unlike most function, this one preserves the file pointer position.
%
% level selects the output format and content.
%   0: A string with one line of information about the object.
%   1: One line of information about the object.
%   2: A moderate amount of information.
%   3: A lot of information.
%
% Outputs:
%  y is a cell array of strings (or a single string, for level=0).  If this argument
%  is omitted, then the information is printed to standard output.
%

u = utility01_class;

if nargin < 2
  level = 2;
end;

if ischar(level)
  level = 2;
end;

 
[ok, msg] = check(obj);


switch level
  case 0   % String output.
    y = disp(obj, 1);
    y = y{1};
    return;
  case 1 % One line of output.
    if ~isvalid(obj)
      y = 'BigMatrixReal_v3, deleted handle.';
    else
      
    [dir1, file1, ext1] = fileparts(obj.filename);
    file1 = [ file1 ext1 ];
    y = sprintf('BigMatrixReal_v3, size (%d,%d), handle %s, file %s', ...
      obj.numRows, ...
      obj.numCols, ...
      obj.privateFileHandleToString(obj.handle), ...
      file1 );
    if ~ok
      y = [ y 'integrity failed, msg="' msg '"' ];
    end;
    end;
    
    y = { y };
  
  case 2 % A few lines of output.
    y = {};
    y{end+1} = sprintf(' class           %s', class(obj));
    if isvalid(obj)
    y{end+1} = sprintf(' numRows         %d', obj.numRows);
    y{end+1} = sprintf(' numCols         %d', obj.numCols);
    y{end+1} = sprintf(' handle          %s', obj.privateFileHandleToString(obj.handle));
    [dir1, file1, ext1] = fileparts(obj.filename);
    y{end+1} = sprintf(' dir             %s', dir1);
    y{end+1} = sprintf(' file            %s', [file1 ext1]);
    y{end+1} = sprintf(' integrity       %d (%s)', ok, msg);
    if exist(obj.filename, 'file')
      y{end+1} = sprintf(' file size       %g samples', fileSizeInBytes(u, obj.filename) / obj.bytesPerSample);
    else
      y{end+1} = ' file does not exist!';
    end;
    
    % Show some of the data values.
    s = ' data:           ';
    for count = 0 : obj.numRows * obj.numCols-1
      if count > 15
        s = [ s '...' ]; % Indicate that we have omitted some values.
        break;
      end;
      row = floor(count/obj.numCols);
      col = mod(count, obj.numCols);
      s = [ s sprintf('%g', privateStealthRead(obj, row, col, 1)) ]; % Use stealth read because we don't want to change the file pointer.
      if col == obj.numCols-1
       s = [ s ';' ];
      end;
      s = [ s ' ' ];
    end;
    y{end+1} = s;
    else
      y{end+1} = ' Deleted handle';
    end;
    
  case 3
    y = {};
    y{end+1} = sprintf(' class                %s', class(obj));
    if isvalid(obj)
    y{end+1} = sprintf(' numRows              %d', obj.numRows);
    y{end+1} = sprintf(' numCols              %d', obj.numCols);
    y{end+1} = sprintf(' handle               %s', obj.privateFileHandleToString(obj.handle));
    [dir1, file1, ext1] = fileparts(obj.filename);
    y{end+1} = sprintf(' dir                  %s', dir1);
    y{end+1} = sprintf(' file                 %s', [file1 ext1]);
    y{end+1} = sprintf(' integrity            %d (%s)', ok, msg);
    if exist(obj.filename, 'file')
      y{end+1} = sprintf(' file size            %g samples', fileSizeInBytes(u, obj.filename) / obj.bytesPerSample);
    else
      y{end+1} = ' file does not exist!';
    end;
    
    % Show some of the data values.
    nr = min(obj.numRows, 15); % Number of rows to print.
    nc = min(obj.numCols, 15); % Number of columns to print.
    x = privateStealthReadRectangle(obj, 0, 0, nr, nc);
    z = printTable2(u, 'corner', ' data:', 'data', x, 'left', repmat(' ', 1, 21), 'data format', '%g');
    z = z(2:end);
    z{1}(2:6) = 'data ';
    y = [ y(:); z(:) ];
    y{end+1} = sprintf(' class dataDirectory  %s', BigMatrixReal_v3.privateDataDirectory);
    y{end+1} = sprintf(' class validation     %d', BigMatrixReal_v3.privateValidation);
    else
      y{end+1} = ' Deleted handle.';
    end;
    
  otherwise
    error('Invalid level.');
end;
if nargout==0
  if ischar(y)
    fprintf(1, '%s\n', y);
  else
    for k = 1 : length(y)
      fprintf(1, '%s\n', y{k});
    end;
  end;
  clear y
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
