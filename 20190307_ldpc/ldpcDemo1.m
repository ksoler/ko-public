function ldpcDemo1
%
% See this document:
% AN INTRODUCTION TO ERROR CORRECTING CODES, Part 3
% Jack Keil WolfECE 154 CSpring 2010
%

% Received soft code bit values, nominally 0 or 1.
rx_code_symbols = [ 0.9 0.5 0.4 0.3 0.9 0.9 0.9 0.9 0.9 0.9 0.9 0.9 ];


% Parity check matrix.  
% 12 columns correspond to n=12 code symbols.
% 9 rows correspond to 9 equations.
H = [ ...
  0 0 1 0 0 1 1 1 0 0 0 0
  1 1 0 0 1 0 0 0 0 0 0 1
  0 0 0 1 0 0 0 0 1 1 1 0
  0 1 0 0 0 1 1 0 0 1 0 0
  1 0 1 0 0 0 0 1 0 0 1 0
  0 0 0 1 1 0 0 0 1 0 0 1
  1 0 0 1 1 0 1 0 0 0 0 0
  0 0 0 0 0 1 0 1 0 0 1 1
  0 1 1 0 0 0 0 0 1 1 0 0 ];

num_symbols = 12;
num_nodes   =  9;

saved_output = BigMatrixReal_v3(0,num_symbols);

% Initialize the array of working code symbol estimates with the received values.
% These are the values which are fed to the nodes, after combining results across different nodes.
working_code_symbols = H;
for j = 1 : num_symbols
  working_code_symbols(:,j) = working_code_symbols(:,j) * rx_code_symbols(j);
end;

% Make each constraint (node) calculate estimates for each soft code bit.
num_iterations = 20;
for n = 1 : num_iterations
updated_code_estimates = zeros(size(H)); % Holds soft values, after updates from nodes, before combining reuslts from multiple nodes.
  for k = 1 : num_nodes
    % Get the four code symbols which feed into current node.
    code_symbol_indices = find(H(k,:) == 1);
    c = working_code_symbols(k, code_symbol_indices); % Get the symbol estimates intended for the current node.
    
    new_c = zeros(size(c));
    c1 = c(1);
    c2 = c(2);
    c3 = c(3);
    c4 = c(4);
    new_c(1) = c2*(1-c3)*(1-c4) + (1-c2)*c3*(1-c4) + (1-c2)*(1-c3)*c4 + c2*c3*c4;
    new_c(2) = c1*(1-c3)*(1-c4) + (1-c1)*c3*(1-c4) + (1-c1)*(1-c3)*c4 + c1*c3*c4;
    new_c(3) = c1*(1-c2)*(1-c4) + (1-c1)*c2*(1-c4) + (1-c1)*(1-c2)*c4 + c1*c2*c4;
    new_c(4) = c1*(1-c2)*(1-c3) + (1-c1)*c2*(1-c3) + (1-c1)*(1-c2)*c3 + c1*c2*c3;
    % Store these new estimates in an array.
    updated_code_estimates(k, code_symbol_indices(1)) = new_c(1);
    updated_code_estimates(k, code_symbol_indices(2)) = new_c(2);
    updated_code_estimates(k, code_symbol_indices(3)) = new_c(3);
    updated_code_estimates(k, code_symbol_indices(4)) = new_c(4);
  end;
  
  % Now, combine results form nodes into the working code symbols.
    final_codes  = zeros(size(rx_code_symbols));
  for j = 1 : num_symbols
    row_indices = find(H(:,j)==1);
    x = updated_code_estimates(row_indices,j);
    % Each code symbol has 3 "vote" values from the 3 nodes it connects to .
    x1 = x(1);
    x2 = x(2);
    x3 = x(3);
    xch = rx_code_symbols(j);
    K = xch*x2*x3 + (1-xch)*(1-x2)*(1-x3);
    working_code_symbols(row_indices(1),j) = xch*x2*x3/K;
    
    K = xch*x1*x3 + (1-xch)*(1-x1)*(1-x3);
    working_code_symbols(row_indices(2),j) = xch*x1*x3/K;
    
    K = xch*x1*x2 + (1-xch)*(1-x1)*(1-x2);
    working_code_symbols(row_indices(3),j) = xch*x1*x2/K;
    
    % Also, calculate a "final" output estimate for each code symbol, not to be used until we have
    % decided to stop iterating.
    K = xch*x1*x2*x3 + (1-xch)*(1-x1)*(1-x2)*(1-x3);
    final_codes(j) = xch*x1*x2*x3/K;
  end;
    appendRow(saved_output, final_codes);
plotColumns(saved_output);
pause;
  
  fprintf(1, '\n-------------------------------------------------------------\n');
  
  fprintf(1, 'Iteration %d\n', n);
  display(updated_code_estimates);
  display(working_code_symbols);
end;
figure(1);
clf;
plotColumns(saved_output);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  