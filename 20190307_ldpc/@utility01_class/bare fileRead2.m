function y = fileRead2(name, varargin)
% h = fileRead(name)
% Reas numeric data from a file.
% Prints error message if there is a problem.
% Speed has been improved by preallocating array elements. space.
%
% Inputs:
% name is the filename of the file to read.
%
% Outputs:
% y is an array of the data read from the file.
%
% Future improvement:  User-configurable progress reporting.  Report percentage of file completed.
% Estimate remaining time.
%
% Copyright 2019, Kevin Oler, All rights reserved.

h = fileOpen(name, 'rt');
N = 1e4; % Block size for preallocation.
n = 0;   % Number of valid samples.

done = 0;
y = [];
t1 = clock;
while ~done
    if n == length(y)
      y = [ y(:); zeros(N,1) ];  % Increase size of y.
    end;
      
    s = fgetl(h);
    if isequal(s,-1)
        done = 1;
        continue;
    end;
    a = sscanf(s, '%g');
    a = a(:);
    
    y(n+(1:length(a))) = a;
    n = n + length(a);
    if etime(clock,t1) > 10
      [d1, f1, e1] = fileparts(name);
      fprintf(1, 'In %s, read %.0fK samples from %s%s.\n', whoAmI('my name'), n/1e3, f1, e1);
      t1 = clock;
      
      
      pause(0.1);
    end;
    
end;
y = y(1:n);

fclose(h);

return;

