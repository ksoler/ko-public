function y = printVector(u, x, varargin)
% y = printVector(u,x, options...)
% Converts a numerc array to a string.
%
% Inputs:
% u is a vestigial utility01_class object.
% x is a 1D numeric vector.
%
% Outputs:
% y is a string.
%
% The options are optional tag/value pairs.
% 'limit' is the maximum number of elements to display.  Excess elements
% are replaced by an elippsis ("...").
%
% 'format' is a format string applied to real and imaginary components,
% such as '%g'.
%
% 'separator'
%
% Idea:  Make a short version of this function, name it "pv", and allow it
% to use options without tags, automatically recognizing format strings and
% maximum length.

assertion(u, isnumeric(x));
assertion(u, numel(x)==length(x));
assertion(u, length(size(x))<3);

getArg(u, ...
    'limit->limit', {'none'}, ...
    'format->fmt', {'%g'}, ...
    'separator->separator', {' '});
    

if isempty(x)
    y = '(empty)';
    return;
end;

n = length(x);
if isequal(limit,'none') || n<limit
    % Show all elements.
    
    y = cell(n,1);
    for k = 1 : n
        y{k} = printNum(x(k), fmt);
    end;
    y = join(u,y, separator);
    
else
    % Show a limited number of elements.
    m = ceil(limit/2);
    if m > n/2
        m = m - 1;  % Don't let the beginning and end segments overlap.
    end;
    y1 = cell(m,1);
    y2 = cell(m,1);
    for k = 1 : m
        y1{k} = printNum(x(k), fmt);
    end;
    for k = 1 : m
        y2{k} = printNum(x(k+n-m), fmt);
    end;
    y =  join(u, [ y1(:); {'...'}; y2(:) ], separator);
end;


return;

function y = printNum(x, fmt)
% Converts a single number to a string.
if imag(x) ~=0
    y = sprintf([fmt '+j' fmt], real(x), imag(x) );
else
    y = sprintf(fmt, x);
end;
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


