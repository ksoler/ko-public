function ok = checkScalarInt(u, x, varargin)
% ok = checkScalarInt(u, x, ...)
% Checks for an integer scalar.
%
% Inputs:
% u is a vestigial utility01_class object.
%
% x is the value to be checked.  It is checked for being scalar and
% integer.  NaN, Inf, and complex values are not allowed.
%
% The remaining arguments are optional tags, some of which require
% additional parameters.  The supported tags are:
%  'int16' requires the numerical limits of signed 16-bit numbers.
%
% Outputs:
% ok is 1 if x passes all tests, 0 otherwise.
%
% If this function is invoked without an output argument, and x fails any
% test, then an error message is printed.
%

u = utility01_class;
getArg(u, ...
    'int16->->int16_flag' );

ok = 1;

if ok && any(isnan(x))
    ok = 0;
end;

if ok && any(isinf(x))
    ok = 0;
end;

if ok && any(imag(x)~=0)
    ok = 0;
end;

if ok && length(size(x))>2
    ok = 0;
end;

if ok && numel(x)~=1
    ok = 0;
end;

if ok && x~=round(x)
    ok = 0;
end;

if ok && int16_flag
    if x<-2^15 || x >= 2^15
        ok = 0;
    end;
end;

if nargout==0 && ~ok
    error('Test for scalar integer failed.');
end;
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2017, Kevin Oler
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in the
%       documentation and/or other materials provided with the distribution.
%     * The name of the Kevin Oler may not be used to endorse or promote products
%       derived from this software without specific prior written permission.
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
% 