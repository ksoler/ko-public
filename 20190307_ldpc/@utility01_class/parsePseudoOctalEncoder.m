function varargout = parsePseudoOctalEncoder( u, varargin)
% [outputs...] = parsePseudoOctalEncoder(u, inputs...)
% Interprets encoder polynomial in pseudo-octal format.
%
% Inputs:
% u is a vestigial utility01_class object, used only to access the
% namesapce of this class.
%
% The remaining arguments are tag/value pairs.  The tags are:
%  'encoder' 
%  'outputs'
%
% The outputs are:
%   'binary encoder'
%   'packed binary encoder'
%   'constraint length'
%   'code ratio'
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Detailed Description of Inputs
%
% 'encoder' is a 1D array of numbers.  This function interprets them 
% as pseudo-octal.  Each digit of the base 10 representation provides 3
% bits for the encoder polynomial.  It's like BCD except that 16 and 10 are
% replaced by 10 and 8.
%
% 'outputs' is a string which names the output arguments you want.  You can
% ask for: the encoder and constraint length like this: 
%    'encoder, constraint length'
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Detailed Description of Outputs
%
% 'constraint length' is the constraint length of the encoder.
%
% 'binary encoder' is a 2D array of values 0,1.  If the input encoder is 
% [15 17] then the binary encoder is:
%                   [ 1 1 0 1 
%                     1 1 1 1 ]
%
% 'packed binary encoder' is a 1D array of encoder values.  The bits of
% the base 2 representation of each value are the encoder connections.
% If the octal representation is [15 17], then this result is [13 15].
%
% 'code ratio' is the number of code symbols produced for each data bit.
%
q = 1;

getArg( u, ...
    'encoder->g', ...  % encoder in pseudo-octal format.
    'outputs->outputs', {'encoder,constraint length'});

checkVectorInt(u, g);
checkString(u, outputs);

% Figure out the constraint length.
R = length(g);
L = 0;

for j = 0 : R-1
    n = 0;  % This will be the constraint length of g(j)
    a = g(j+q);
    i = 0;
    while a>10
        a = floor(a/10);
        n = n + 3;
        i = i + 1;
        if i>100, error('This should not happen.'); end;
    end;
    i = 0;
    while a>0
        a = floor(a/2);
        n = n + 1;
        i = i + 1;
        if i>100, error('This should not happen.'); end;
    end;
    L = max(L,n);
end;

G = zeros(R,L);

% Now, L is the constraint length of the full convolutional encoder.
packed_encoder = zeros(R,1);
for j = 0 : R-1
    a = g(j+q);
    i = 0;
    for k = 0 : floor((L-1)/3) % The number of iterations is ceil(L/3)?
        digit = mod(a,10);
        a = floor(a/10);
        bit0 = mod(floor(digit/1),2);
        bit1 = mod(floor(digit/2),2);
        bit2 = mod(floor(digit/4),2);
        packed_encoder(j+q) = packed_encoder(j+q) + 2^i * bit0; i=i+1;
        packed_encoder(j+q) = packed_encoder(j+q) + 2^i * bit1; i=i+1;
        packed_encoder(j+q) = packed_encoder(j+q) + 2^i * bit2; i=i+1;
        column = L-3*k;
        if column>0
            G(j+q, column) = bit0;
        end;
        column = L-3*k-1;
        if column>0
            G(j+q, column) = bit1;
        end;
        column = L-3*k-2;
        if column>0
            G(j+q, column) = bit2;
        end;
        
            
    end;
end;


%powers_of_2 = 2.^(0:L-1);
%for k = 1 : R
%    packed_encoder(k) = sum(G(k,:).*powers_of_2);    
%end;

% Assign outputs.
args = split(u, outputs, ',', 'trim');
args = removeEmptyCells(u,args);
n = length(args);
if n~=nargout
    error('Invalid number of output arguments.');
end;
varargout = cell(n,1);
for k = 1 : n
    switch args{k}
        case 'binary encoder',           varargout{k} = G; % binary encoder
        case 'constraint length',        varargout{k} = L; % constraint length
        case 'packed binary encoder',    varargout{k} = packed_encoder;
        case 'code ratio',               varargout{k}  = R;
        otherwise
            fprintf( 1, 'Unsupported output argument: %s\n', args{k});
            error('Invalid output argument.');
    end;
end;
% Done assigning outputs.
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


