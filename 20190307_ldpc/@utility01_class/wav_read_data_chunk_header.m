function [string, num_samples] = read_wav_data_chunk_header(u, filename, chunk_start, bits_per_sample, num_channels)
% [string, num_samples] = read_wav_data_chunk_header(u, filename, chunk_start, bits_per_sample, num_channels)
% Reads and parses the header of a data chunk in a WAVE file.
%
% Inputs:
% 
% [y, done] = read_wav_data(u, varargin)
% Reads data from a WAV file.  Use read_wav_format first to get file information.
%
% Inputs: data start, bits per sample, num channels, sample index, num samples
% 
%---------------------------------------------------------------------------------------------------
% Detailed Description of Inputs
%
% u is a vestigial utility01_class object.  The remaining inputs are tag/value pairs.
%
% 'file handle' is a file handle for the file.
%
% 'data start' is the byte offset of the first raw data sample in the file.
%
% 'bits per sample' is the number of bits per sample, e.g. 16 for 16-bit samples.
%
% 'num channels' is the number of channels, e.g. 1 for mono, 2 for stereo.
%
% 'channel number' is the index of the desired channel, 0...num_channels-1.
%
% 'sample index' is the index of the next sample to read.  Use 0 for the first sample.  Use [] to
% read from the current position.
%
% 'num samples' is the number of samples to read.  If omitted, this function will choose a number.
% If the requested number of samples is not available, this function returns as many as possible and
% sets done=1.
%
%---------------------------------------------------------------------------------------------------
% Detailed Description of Outputs
%
% y is the sample data.
%
% done is 1 if no more samples are available, 0 otherwise.
%

getArg(u,                                 ...
  'file handle    ->h              ',     ...
  'data start     ->data_start     ', [], ...
  'bits per sample->bits_per_sample',     ...
  'num channels   ->num_channels   ',     ...
  'channel number ->channel_number ', 0,  ...
  'sample index   ->sample_index   ', [], ...
  'num samples    ->num_samples    ', [] );

if isempty(num_samples)
  num_samples = 1e3;
end;

switch bits_per_sample
  case 8
    format_string = 'uint8';
    sample_offset = -128;
    sample_scale = 1/128; % This might be slightly inaccurate.
  case 16
    format_string = 'int16';
    sample_offset = 0;
    sample_scale = 1/32768;
  case 32
    format_string = 'int32';
    sample_offset = 0;
    sample_scale = 1/2^31;
  otherwise
    error('Unsupported precision.');
end;

    
    
skip_bytes = (bits_per_sample / 8) * (num_channels - 1);

if ~isempty(sample_index)
  fileSeek(u, h, data_start + bits_per_sample/8 * num_channels * sample_index + channel_number * bits_per_sample/8);
end;

y = fread(h, num_samples, format_string, skip_bytes);
y = y + sample_offset;
y = y * sample_scale;
if length(y) < num_samples
  done = 1;
else
  done = 0;
end;

return; 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


