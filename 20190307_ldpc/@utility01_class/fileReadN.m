function [y, done] = fileReadN(u, h, nRow, nCol)
% h = fileReadN(u, h, nRow, nCol)
% Reads numeric data from a file to fill an array of size (nRow, nCol).
% Note that this function disregards the organization of file data into lines.  If you tell it to 
% read a 3x3 array and the file data has 2 numbers per line, it will just read the data in the order
% it comes.
%
% Inputs:
% u is a vestigial utility01_class object.
%
% h is a file handle to read from.
%
% nRow is the number of rows of data to read.
%
% nCol is the number of columns of data to read.
%
% Outputs:
% y is an array of the data read from the file.
%
% Future improvement:  User-configurable progress reporting.  Report percentage of file completed.
% Estimate remaining time.
%
% ALTERNATIVE:  If h is a filename, this opens and read the complete file.  It uses nCols, but nRows
% should be [].
%
% Unfortunately, this function doesn't currently have the ability to tell the caller when it has
% successfully read the requested data, but there is no more data.  It has to try, and fail.

% Alternative:  If h is a filename, then this reads the whole file.
% If user provided a filename instead of handle, then read the entire file.
if checkString(u, h)
  if ~isempty(nRow), warning(u, 'Ignoring value for nRow.'); end;
  x = fileRead2(u, h);
  n = length(x);
  x=  reshape(x, nCol, n/nCol);
  y = x.';
  done = 1;  
  return;
end;

  
N = nRow*nCol; % Number of samples to read.
n = 0;   % Number of valid samples.

y = [];
for k = 1 : N
  a = fscanf(h, '%g', 1);
  if numel(a) ~=1
    break;
  end;
  y(end+1) = a;
  if 1
  fprintf(1, 'Read value: %g\n', a);
  end;
end;

if numel(y) ~=N
  warning(u, 'Failed to read requested number of samples.');
  done = 1;
  if ~isempty(y)
  keyboard; % error('Failed.'); % For now, I consider this to be a fatal error.
  end;
  y = [];  % If the data was incomplete then return nothing.
  
  return;
%  error('Failed to read data.');
end;

done = 0;
y = reshape(y, nCol, nRow).';

return;

