function [same, unique1, unique2] = compareStringLists(u, x1, x2)
% [same, unique1, unique2] = compareStringLists(u, x1, x2)
% Checks if two lists of strings are the same (regardless of order).
%
% Inputs:
% u is a vestigial utility01_class object.
%
% x1 is a 1D cell array of strings.
% x2 is a 1D cell array of strings.
%
% Outputs:
% same is 1 if x1 and x2 contain the same strings. 0 otherwise.
% unique1 are the strings found in x1, missing from x2.
% unique2 are the strings found in x2, missing from x1.
%
% If this function is invoked without outputs, then an analysis is printed
% to standard output.
%
% Future Improvements:
% Be more explicit about how repeated values are handled.

%checkStringList(u, x1);  % Consider omitting these calls to increase speed.
%checkStringList(u, x2);

x1 = sort(x1(:));
x2 = sort(x2(:));
unique1 = {};  % Strings present in x1 and absent from x2.
unique2 = {};  % Strings absent from x1 and present in x2.

if isequal(x1,x2)
    same = 1;
else
    
    
    for k = 1 : length(x1)
        if ~findStringInList(u, x1{k}, x2)
            unique1(end+1) = x1(k);
        end;
    end;
    
    for k = 1 : length(x2)
        if ~findStringInList(u, x2{k}, x1)
            unique2(end+1) = x2(k);
        end;
    end;
    
    if isempty(unique1) && isempty(unique2)
        same = 1;
    else
        same = 0;
    end;
end;

if nargout == 0
    if same
        fprintf( 1, 'The two lists of strings are identical (disregarding order).\n');
    else
        fprintf( 1, 'The two lists of strings are not identical.\n');
        fprintf( 1, 'These are unique to the first: %s\n', join(unique1, ','));
        fpritnf( 2, 'These are unique to the second: %s\n', join(unique2, ','));
    end;
end;
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


