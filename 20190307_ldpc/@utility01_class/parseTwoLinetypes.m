function [ok, line1, line2] = parseTwoLinetypes(u,s)
% [ok, line1, line2] = parseTwoLinetypes(u,s)
% Attempts to parse linetypes from a string.
%
% Inputs:
% s is a string which may contain one linetype, or two separated by a space.  If it has more, then
% the extras are ignored.
% 
% Outputs:
% ok is 1 if the string was parsed successfully, 0 otherwise.
%
% line1 is the first parsed linetype, or a default linetype if none could be parsed.
%
% line2 is the second linetype, or a copy of the first if there was only one.  Or, a default if none
% were parsed.
%
% If s 
ok = 1; % Set to 0 if we find a problem.

if ok && ~checkString(u, s)
  ok = 0;
end;

if ok
s = split(u, s, ' ', 'trim');
s = removeEmptyCells(u,s);
end;

if ok && isempty(s)
  ok = 0;
end;

if ok && isLinetypeString(u, s{1})
  line1 = s{1};
  line2 = s{1}; % The default for line2 is s{1}, this may be overwritten.
else 
  ok = 0;
end;

if ok && length(s)>1
  if isLinetypeString(u, s{2})
    line2 = s{2};
  else
    warning(u, 'Second linetype in strimg is not valid.');
    % But ok is still 1.
  end;
end;

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


