function y = fileRead2(u, name, varargin)
% h = fileRead(u, name)
% Reas numeric data from a file.
% Prints error message if there is a problem.
% Runs faster because it preallocates output space.
%
% Inputs:
% u is a vestigial utility01_class object.
%
% name is the filename of the file to read.
%
% Outputs:
% y is an array of the data read from the file.
%
% Future improvement:  User-configurable progress reporting.  Report percentage of file completed.
% Estimate remaining time.
%

h = fileOpen(u, name, 'rt');
N = 1e4; % Block size for preallocation.
n = 0;   % Number of valid samples.

done = 0;
y = [];
t1 = clock;
while ~done
    if n == length(y)
      y = [ y(:); zeros(N,1) ];  % Increase size of y.
    end;
      
    s = fgetl(h);
    if isequal(s,-1)
        done = 1;
        continue;
    end;
    a = sscanf(s, '%g');
    a = a(:);
    
    y(n+(1:length(a))) = a;
    n = n + length(a);
    if etime(clock,t1) > 10
      [d1, f1, e1] = fileparts(name);
      fprintf(1, 'In %s, read %.0fK samples from %s%s.\n', whoAmI(u, 'my name'), n/1e3, f1, e1);
      t1 = clock;
      
      
      pause(0.1);
    end;
    
end;
y = y(1:n);

fclose(h);

return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


