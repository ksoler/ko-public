function [ formatCode, numChannels, sampleRate, bitsPerSample] = wav_read_fmt(u, file, start, len)
% [ formatChunk, dataChunk, allChunk] = wav_read_chunk_info2(u, file)
% Reads format chunk of a WAVE file.
%
% Inputs:
% u is a vestigial utility01_class object.
%
% file is a filename.
%
% start is the start of the format chunk (after the 'fmt ' string and chunk size).
%
% len is the nominal chunk size.
%
% Outputs:
% formatCode 1 for PCM.
% numChannels 1 for mono, 2 for stereo, etc.
% sampleRate 
% bitsPerSample 8, 16, etc.
%

msg = 'No problem found.'; 
ok  = 0;  % Change to 1 if we reach the end of main loop successfully.

h = fileOpen(u, file, 'rb');
fileSeek(u, h, start, 'bof');


formatCode = fread(h, 1, 'uint16');
if formatCode ~= 1
  fprintf(1, 'Warning in %s: non-pcm format code, %d\n', whoAmI(u, 'my name'), formatCode);
end;

numChannels = fread(h, 1, 'uint16');

sampleRate = fread(h, 1, 'uint32');

bytesPerSecond = fread(h, 1, 'uint32'); % We ignore this.
blockAlign     = fread(h, 1, 'uint16'); % We ignore this.
bitsPerSample = fread(h, 1, 'uint16');

% For now, we ignore the remainder of the format chunk, if any.
fclose(h);

if nargout==0
fprintf( 1, 'WAVE file %s\n', file);
fprintf( 1, '  format code %d\n', formatCode);
fprintf( 1, '  num channels %d\n', numChannels);
fprintf( 1, '  sampleRate   %d\n', sampleRate);
fprintf( 1, '  bitsPerSample %d\n', bitsPerSample);
fprintf( 1, '\n');
end;


return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


