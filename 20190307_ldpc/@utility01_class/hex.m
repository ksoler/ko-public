function y = hex(u, x, n)
% y = hex(u,x,n)
% WARNING - COMMENTS ARE OUT OF DATE!
% u is a vestigial utility01_class object.
%
%--------------------------------------------------------------------------
% Mode 2: decimal to hex
%
% x is a nonempty vector of integers.
%
% n is the number of hex digits to use.  If not specified, then this function
% uses as many as required.
%
% y is a string containing the hex representation of x.  If x contains
% multiple numbers, then y is a cell array of strings.
%--------------------------------------------------------------------------
% Mode 2:  hex to decimal
%
% x is a string containing a hex number.
%
% n should be omitted.
%
% y is a scalar integer whose hex representation is the value of x.
%
% Future Improvements:  In mode 2, allow user to specify that 2's
% complement arithmetic applies so that FFFF would be -1.
% Consider ignoring a 0x prefix on strings.
% Consider supporting hex decimal numbers, such as 0.f = 15/16.
%

if checkString(u,x)
    % Mode 2: hex to decimal
    % assertion(u,nargin<3);
    if nargin>2 && isequal(n,'signed') 
        twos_complement = 1;
    else
        twos_complement = 0;
    end;

    y = 0;
    for k = 1:length(x)
        [d,ok] = convert_hex_digit(x(k));
        if ~ok
            fprintf(1, 'Warning: ignored invalid hex digit.\n');
        end;
        y = 16*y + d;
    end;
    return;
    
elseif checkVectorInt(u,x)
    % Mode 1: decimal to hex
    
    if checkScalarInt(u,x)
        if nargin < 3
            n = floor(log(abs(x))/log(16))+1;  % Number of hex digits; this might fail sometimes.
            n = max(n,1);  % If the number is zero, we will show at least one digit.
        end;
        if x<0
            if nargin<3, n = n + 1; end;
            x = x + 16^n;
            % error('This function does not yet support negative numbers.\n');
        end;
        y = zeros(n,1);
        for k = 0 : n-1
            y(k+1) = mod(x,16);
            x = floor(x/16);
        end;
        for k = 0 : n-1
            if y(k+1)<10
                y(k+1) = y(k+1) + '0';
            else
                y(k+1) = y(k+1) + 'A' - 10;
            end;
        end;
        y = y(end:-1:1);
        y = char(y(:).');
        if nargout == 0
            fprintf(1, '%s\n', y);
        end;
        
    elseif checkVectorInt(u,x) && ~isempty(x)
        if nargin < 3
            max_length = 1;
            for k = 1 : length(x)
                max_length = max( max_length, length( hex(u, x(k)) ));
            end;
            n = max_length;
        end;
        m = length(x);
        y = cell(m,1);
        for k = 1 : m
            y{k} = hex(u, x(k), n);
        end;
        if nargout ==0
            for k = 1 : length(y)
                fprintf(1, '%s\n', y{k});
            end;
        end;
    else
        error('Invalid input.');
    end;
    
    return;
else
    error('Invalid input.');
end;


function [d,ok] = convert_hex_digit(x)
% Converts a single hex digit (character) to a number.
%
u = utility01_class;

checkString(u,x);
assertion(u,length(x)==1);
x = upper(x);

if '0'<=x && x<='9'
    d = x - '0';
    ok = 1;
elseif 'A'<=x && x<='F'
    d = x - 'A'+10;
    ok = 1;
else
    ok = 0;
    d = 0;
end;
return;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


