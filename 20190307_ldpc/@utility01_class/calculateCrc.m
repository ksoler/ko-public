function y = calculateCrc(u, data, poly)
% y = calculateCrc(u, data, poly)
% Calculates a Cyclic Redundancy Check.
%
% Inputs:
% u is a utility01_class object.
% 
% data is a 1D array of bit values (0,1).
%
% poly is a 1D array of nonnegative integers which represents the
% CRC polynomial.  Each number represents the exponent of a nonzero term.
% For the polynomial x^6+x+1, use [6 1 0]
% 
% Outputs:
% y is a 1D array containing the CRC bits.
%

% This implementation is based on the example in http://en.wikipedia.org/wiki/Cyclic_redundancy_check
% Other implementations are available, which might differ in aspects such
% as forward/reverse ordering, and intermediate results, but should yield
% the same error-detection performance.

% Check arguments.
%checkVectorBinary(u,data);
%checkVectorInt(poly);
assertion(u, all(poly>=0));
assertion(u, length(poly)==length(unique(poly)));
% Done checking arguments.

n_data = length(data);  % length of unpadded message
poly = sort(poly(:));
data = data(:);
n = max(poly);

mask = zeros(n+1,1);
mask(poly+1) = 1;  % Bit mask used in CRC calculation.

data = [ data; zeros(n,1) ];  % pad message
for k = 1 : n_data
    if data(k)
        data(k:k+n) = xor(data(k:k+n), mask);
    end;
end;
y = data(end-n+1:end);
return;




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% From http://en.wikipedia.org/wiki/Cyclic_redundancy_check.
% Start with the message to be encoded:
% 11010011101100
% 
% 
% This is first padded with zeroes corresponding to the bit length n of the CRC. Here is the first calculation for computing a 3-bit CRC:
% 11010011101100 000 <--- input right padded by 3 bits
% 1011               <--- divisor (4 bits) = x�+x+1
% ------------------
% 01100011101100 000 <--- result
% 
% 
% The algorithm acts on the bits directly above the divisor in each step. The result for that iteration is the bitwise XOR of the polynomial divisor with the bits above it. The bits not above the divisor are simply copied directly below for that step. The divisor is then shifted one bit to the right, and the process is repeated until the divisor reaches the right-hand end of the input row. Here is the entire calculation:
% 11010011101100 000 <--- input right padded by 3 bits
% 1011               <--- divisor
% 01100011101100 000 <--- result (note the first four bits are the XOR with the divisor beneath, the rest of the bits are unchanged)
%  1011              <--- divisor ...
% 00111011101100 000
%   1011
% 00010111101100 000
%    1011
% 00000001101100 000 <--- note that the divisor moves over to align with the next 1 in the dividend (since quotient for that step was zero)
%        1011             (in other words, it doesn't necessarily move one bit per iteration)
% 00000000110100 000
%         1011
% 00000000011000 000
%          1011
% 00000000001110 000
%           1011
% 00000000000101 000 
%            101 1
% -----------------
% 00000000000000 100 <--- remainder (3 bits).  Division algorithm stops here as quotient is equal to zero.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


