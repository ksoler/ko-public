function varargout = read_text_file_with_include(u, varargin)
% [outputs...] = read_text_file_with_include(u, input...)
% Reads a text file, with support for including other files.
%
% Inputs: dir, file, outputs, max lines, report interval
% Outputs: text, line numbers, filenames, paths
%
%---------------------------------------------------------------------------------------------------
% Detailed Description of Inputs
%---------------------------------------------------------------------------------------------------
% u is a vestigial utility_01_class object.  The remainig arguments are tag/value pairs.
%
% 'dir' is a directory to use.
%
% 'file' is the filename of the file to use.  This can be a full pathname, if you leave the 'dir'
% argument empty.  Or, you can split up the path between these two arguments.
%
% 'outputs' is a string which specifies the requested output arguments, separated by commas.
%
% 'max lines' is a limit of the maximum number of lines to be read.
%
% 'report interval' is the number of seconds to elapse between status reports.  If 0, then every
% line read gets reported.  If empty, then reporting is suppressed.
%
%---------------------------------------------------------------------------------------------------
% Detailed Description of Outputs
%---------------------------------------------------------------------------------------------------
% 'text' is a cell array of all of the lines of text from the specified file, and any files which it
% included.
%
% 'line numbers' is a vector of the line numbers (in files) of the files from which each line was read.
%
% 'filenames' is a list of the filenames from which each line was read.
%
% 'paths' is like the 'filenames' output, but includes full pathnames.
%
% To do:  Check for recursion, perhaps have a limit on the maximum number of lines.
%

verbose = 0;

u = utility01_class;

% Parse input arguments.
getArg( u, ...
  'dir            ->dir_arg            ', {''}, ...
  'file           ->file_arg           ', {''}, ...
  'outputs        ->outputs_arg        ', {''}, ...
  'max_lines      ->max_lines_arg      ', {''}, ...
  'report interval->report_interval_arg', {''} );

% Inputs: dir, file, outputs, max lines, report interval
% Outputs: text, line numbers, filenames, paths


f = fullfile(dir_arg, file_arg);
if ~exist(f, 'file')
  fprintf( 1, 'Failed to find this file: %s.\n', f);
  error('Requested file not found.');
end;

[ p1, f1, ext1] = fileparts(f);

h = fileOpen(u, f, 'rt');
done = 0;

% Initialize outputs.
text = {};
line_numbers = [];
filenames = {};
paths = {};


line_number = 0;
t1 = clock;
while ~done
  s = fgetl(h);
  if isequal(s,-1)
    done = 1;
    break;
  end;
  line_number = line_number + 1;
  
  if verbose
    fprintf( 1, 'Read line %d of file %s, "%s"\n', line_number, f1, s);
  end;
  
  
  % Handle #include statement.
  s1 = strtrim(s);
  % Actually, we want to disallow wildcard characters in the second string.  And have the function
  % return the matched part.
  [match, include_file] = stringPatternMatch(u, '#include *', s1, 'outputs', 'match, matched string');
  if match
    include_file = strtrim(include_file);
    [match, include_file] = stringPatternMatch(u, include_file, '"*"*', 'outputs', 'match, matched string'); % Ignore everything after the final quote.
    if ~match, error('Invalid include syntax.'); end;
    
    % Look for the included file in two locations:
    % (1) Relative to the directory of the current file.
    % (2) As an absolute path.
    include_file1 = fullfile(p1, include_file);
    include_file2 = include_file;
    
    if exist(include_file1, 'file')
      include_file3 = include_file1;
    elseif exist(include_file2, 'file')
      include_file3 = include_file2;
    else
      fprintf( 1, 'Could not find this file: %s\n', include_file);
      error('Included file not found.');
    end;
    
    [included_text, included_line_numbers, included_filenames, included_paths] = ...
      read_text_file_with_include(u, ...
      'dir', '', ...
      'file', include_file3,  ...
      'outputs', 'text, line numbers, filenames, paths', ...
      'report interval', report_interval_arg );
    % This does not currently support the max lines argument.
    text =  [ text(:); included_text(:) ];
    line_numbers = [ line_numbers(:); included_line_numbers(:) ];
    filenames = [ filenames(:); included_filenames(:) ];
    paths = [ paths(:); included_paths(:) ];
    continue;
  end;
  % Done handling included files.
  text = [ text; {s} ];
  line_numbers = [ line_numbers; line_number ];
  filenames = [ filenames; {f1} ];
  paths = [ paths; {f} ];
  
  if ~isempty(report_interval_arg) && etime(clock, t1) > report_interval_arg
    fprintf( 1, 'Read %d lines of file %s.\n', line_number, f1);
    pause(0.1);
    t1 = clock;
  end;
end;
fclose(h);


% Assign outputs.
outputs = split(u, outputs_arg, ',', 'trim');
outputs = removeEmptyCells(u, outputs);
n = length(outputs);
assertion(u, nargout==n);
varargout = cell(n,1);
for k = 1 : n
  
  switch outputs{k}
    case 'text'
      varargout{k} = text;
    case 'line numbers'
      varargout{k} = line_numbers;
      
    case 'filenames'
      varargout{k} = filenames;
    case 'paths'
      varargout{k} = paths;
    otherwise
      error('Invalid output.');
  end;
end;



return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


