function wav_write(u, file, x, sampleRate, bitsPerSample)
% wav_write(u, file, x, sampleRate, bitsPerSample)
%
% Writes data to a WAVE file.
%
% Inputs:
% u is a vestigial utility01_class object.
%
%
% Outputs:
%

msg = 'No problem found.'; 
ok  = 0;  % Change to 1 if we reach the end of main loop successfully.

switch bitsPerSample
case 8
  bytesPerSample = 1;
  fmt = 'uint8';
  scale = 2^7;
  offset = 128;
  lowerLimit = 0;
  upperLimit = 255;
case 16
  bytesPerSample = 2;
  fmt = 'int16';
  scale = 2^15;
  offset = 0;
  lowerLimit = -2^15;
  upperLimit =  2^15-1;
case 32
  bytesPerSample = 4;
  scale = 2^31;
  offset  =0;
  lowerLimit = -2^31;
  upperLimit =  2^31-1;
otherwise
error('Unsupported word size');
end;
h = fileOpen(u, file, 'wb');

% Write the "RIFF" header.
fwrite(h, 'RIFF', 'char');

% Write the riff size placeholder.
riff_size_position = ftell(h);
fwrite(h, 0, 'uint32');

% Write the WAVE header.
fwrite(h, 'WAVE', 'char');

% Write hte format chunk heaer.
fwrite(h, 'fmt ', 'char');

% Write hte format chunk size placeholder.
fmt_size_position = ftell(h);
fwrite(h, 0, 'uint32');

% Write format code.
fmt_chunk_position = ftell(h);  % Start of the format chunk, after type string and size.
fwrite(h, 1, 'uint16');  % Just PCM for now.

% Write the number of channels.
fwrite(h, x.numCols, 'uint16');

% Write the sample rate.
fwrite(h, sampleRate, 'uint32');

% Write the average number of bytes per second.
fwrite(h, bytesPerSample*x.numCols*sampleRate, 'uint32');

% Write the block Align (bytes for 1 sample, across all channels).
fwrite(h, bytesPerSample*x.numCols, 'uint16');

% Write bits per sample.
fwrite(h, bitsPerSample, 'uint16');

% Write the format extension size field.
fwrite(h, 0, 'uint16');

% Write the format chunk size.
p = ftell(h);
fileSeek(u, h, fmt_size_position, 'bof');
fwrite(h, p - fmt_chunk_position, 'uint32');
fseek(h, p, 'bof'); % Return to end of file.

% Write the data header.
fwrite(h,'data', 'char');

% Write the data chunk size.
data_size_position = ftell(h);
fwrite(h, bytesPerSample*x.numRows*x.numCols, 'uint32');

% Write the data.
if 0
 % Slow way.
 


else
  % Fast way.
  if isOpen(x)
    was_open = 1;
    close(x);
  else
    was_open = 0;
  end;
  h2 = fileOpen(u, x.filename, 'rb');
  blockSize = 1e4;
  num_samples = x.numRows * x.numCols;
  t1  = clock;
  clipped = 0;
  while num_samples >= blockSize
    y  = fread(h2, blockSize, 'double');
    y  = round(y * scale);
    y  = y + offset;
    if any(y<lowerLimit) || any(y>upperLimit), clipped = 1; end;
    y(y<lowerLimit) = lowerLimit;
    y(y>upperLimit) = upperLimit;

    fwrite(h, y, fmt);
    if etime(clock,t1) > 10
      fprintf(1, 'In %s, %f.0K samples remaining.\n', whoAmI(u, 'my name'), num_samples/1e3);
      pause(0.1);
      t1 = clock;
    end;
    num_samples = num_samples - blockSize;
  end;

  if num_samples > 0
    y  = fread(h2, num_samples, 'double');
    y  = round(y * scale);
    y  = y + offset;
    if any(y<lowerLimit) || any(y>upperLimit), clipped = 1; end;
    y(y<lowerLimit) = lowerLimit;
    y(y>upperLimit) = upperLimit;
    fwrite(h, y, fmt);
  end;
  fclose(h2);

  if clipped
   warning(u, 'Data sample(s) were clipped.');
  end;
 end; % Select slow/fast way.
if was_open
  open(x);
end;

p = ftell(h);
fileSeek(u, h, riff_size_position, 'bof');
fwrite(h, p - riff_size_position - 4);
fclose(h);
return;


% test code
if 0
  u= utility01_class;
  [x, sampleRate] = wav_read(u, 'F:\2019-matlab data\wavRead\listen to the man, short.wav');
  wav_write(u, 'F:\2019-matlab data\wavRead\test.wav', x, sampleRate, 8);
end;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


