function [ok, msg] = checkReal2dArraySize(u, x, nRows, nCols)
% ok = checkReal2dArraySize(u, x, nRows, nCols)
% Checks that the input is a 2D array of real values, with specified dimensions.
%
% Inputs:
% u is a vestigial utility01_class object.
%
% x is the value to be checked.  It is checked for being a 2D array with real values.
% NaN, Inf, and complex values are not allowed.  Its dimensions are also checked.
%
% nRows is the expected number of rows for x.  If empty, we don't check.
%
% nCols is the expected number of columns for x.  If empty, we don't check.
%
% Outputs:
% ok is 1 if x passes all tests, 0 otherwise.
%
% msg is a description of the failure, if any.
%
% If this function is invoked without an output argument, and x fails any
% test, then an error message is printed.
%

ok = 1;
msg = 'No problem found.';

if ok && any(isnan(x(:)))
    ok = 0;
    msg = 'Contains NaN.';
end;

if ok && any(isinf(x(:)))
    ok = 0;
    msg = 'Contains inf.';
end;

if ok && any(imag(x(:))~=0)
    ok = 0;
        msg = 'Contains imag.';
end;

if ok && length(size(x(:)))>2
    ok = 0;
    msg = 'Has more than 2 dimensions.';
end;

if ok && ~isempty(nRows) && size(x,1) ~= nRows
    ok = 0;
    msg = sprintf('Does not have expected dimensions.  Expected %d rows, found %d.', nRows, size(x,1));
end;

if ok && ~isempty(nCols) && size(x,2) ~= nCols
    ok = 0;
    msg = 'Does not have expected dimensions.';
    msg = sprintf('Does not have expected dimensions.  Expected %d columns, found %d.', nCols, size(x,2));
end;

if nargout==0 && ~ok
    fprintf(1, 'Test for real 2D array failed with this message: %s\n', msg);
    error('Test for real 2D array failed.');
end;

if nargout==0
  clear ok msg % Don't return anything if the user didn't ask for it.
end;

return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


