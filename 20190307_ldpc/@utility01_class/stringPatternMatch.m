function varargout = stringPatternMatch(u, s1, s2, varargin)
% y = stringPatternMatch(u, s1, s2, ...)
% Checks if two strings match, with some wildcard matching capabilities.
%
% Inputs:
% u is a vestigial utility01_class object.
% s1 is one string, which may contain wildcard characters '?' and '*'.
% s2 is the other string, which may contain wildcard characters '?' and '*'.
%
% A '?' matches any single character, and '*' matches any number of
% characters.
%
% The remaining input arguments are tag/value pairs.  The tags are:
% 'outputs' is a string which specifies the requested output arguments, separated by commas.
% The default is 'match'.
%
% 's2 wildcards' is 0 or 1.  0 disables wildcards in the s2 argument.
%
% Outputs:
% 'match' is is 1 if the strings match, 0 if they do not match.
% 'matched strings' is a cell array of the strings which matched to wildcards.
% 'matched string' is the first string matched to wildcards.
%
% Future Improvements:
% Use flags to enable or disable pattern matching in either of the inputs.
% Optionally return the part of the string which matches an astrisk in a string.
%

getArg( u, ...
  's2 wildcards->s2w', 1, ...
  'outputs->outputs_arg', {'match'} );

checkString(u, s1);
checkString(u, s2);

n1 = length(s1);
n2 = length(s2);

ok = 1; % Set to 0 if we discover that the strings do not match.
k = 0;
done = 0;
matched_strings  = {}; % List of all strings matched by wildcards.

while ~done
  k = k + 1;
  
  % '*' in s1.
  if k<=n1 && s1(k)=='*'
    % Try matching the rest of s1 against all possible substrings of s2
    % from at or after the current character, to the end of s2.
    for j = k:n2+1
      [temp_match, temp_matched_strings] = stringPatternMatch(u, s1(k+1:end), s2(j:end), 's2 wildcards', s2w,  'outputs', 'match, matched strings');
      if temp_match
        done = 1;
        matched_strings{end+1} = [ s2(k:j-1); temp_matched_strings(:) ];
        break;
      end;
    end;
    % If the previous for loop doesn't find a match for one of the
    % substrings, then the two strings don't match.
    if ~done
      ok = 0;
      done = 1;
    end;
    continue;
  end;
  
  % '*' in s2.
  if k<=n2 && s2(k)=='*' && s2w
    % Try matching the rest of s1 against all possible substrings of s2
    % from at or after the current character, to the end of s2.
    for j = k:n1+1
      if stringPatternMatch(u, s1(j:end), s2(k+1:end))
        matched_strings{end+1} = s1(k:j-1);
        done = 1;
        break;
      end;
    end;
    % If the previous for loop doesn't find a match for one of the
    % substrings, then the two strings don't match.
    if ~done
      ok = 0;
      done = 1;
    end;
    continue;
  end;
  
  % We have reached the end of both strings.
  if k>n1 && k>n2
    done = 1;
    continue;
  end;
  
  % We reached the end of one of the strings.
  if k>n1 || k>n2
    ok = 0;
    done = 1;
    continue;
  end;
  
  if s1(k)=='?'
    matched_strings{end+1} = s2(k);
    continue;
  end;

  if s2(k)=='?' && s2w
    matched_strings{end+1} = s1{k};
    continue;
  end;
  
  if s1(k) ~= s2(k)
    ok = 0;
    done = 1;
    continue;
  end;
  
end;

% Assign outputs.
outputs = split(u, outputs_arg, ',', 'trim');
outputs = removeEmptyCells(u, outputs);
n = length(outputs);
assertion(u, nargout==n);
varargout = cell(n,1);
for k = 1 : n
  switch outputs{k}
    case 'match'
      varargout{k} = ok;
    case 'matched string'
      if isempty(matched_strings)
        varargout{k} = '';
      else
      varargout{k} = matched_strings{1};
      end;
      
    case 'matched strings'
      varargout{k} = matched_strings;
    otherwise
      error('Invalid output.');
  end;
end;


return;
% test code
if 0
  s1 = 'dog cat fish cat bat cat bird';
  s2 = 'cat';
end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


