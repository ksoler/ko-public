function getArg(u, varargin)
 % getArg(inputs...)
 % Scans the varargin argument in the caller's workspace for tags and
 % values.
 %
 % Inputs:
 % A string argument specifies the tag name, and the names of variables in
 % which to store the subsequent values, like this: 'frequency->freq'.
 % Note that the tag cannot include the character sequence '->'.
 %
 % The number of subsequent values is signalled by the number of variable
 % names provided, and can be 0, 1, or many.
 % 'optimization on'   % No values, just the tag by itself.
 % 'frequency->freq'   % One value.
 % 'co-ordinates->x,y' % Two values.
 %
 % The user can specify default values by following the string argument with
 % a cell array containing the defaults.  For any given tag, defaults must
 % be provided for all or none of its associated variables.
 % If there is one argument and its default value is not a string, then the
 % cell container can be omitted.
 % 'co-ordinates->x,y', {0,0}
 % 'frequency->freq', 1000
 %
 % Also, the user can request that another variable be set as a flag to
 % indicate whether or not the specified tag was found.  That is done by
 % appending a second '->' and the name of the flag, like this:
 % 'frequency->freq->freq_present'
 %
 % This function removes the scanned tags and values from varargin in the
 % caller's workspace.  Normally, this function expects that varargin will be empty
 % when it is done parsing - if not, an error message is printed.
 %
 % If the string '...' is included in the arguments, then this function 
 % does not require varargin to be enpty after parsing tags.
 %
 % Alternative Usage:  You can invoke getarg without arguments after
 % extracting all expected arguments.  At that point, if varargin is not 
 % empty, an error message will be printed.
 %
 % Future Improvment:
 % Implement an option which will suppress checking for empty caller's
 % varargin after this function is done parsing arguments from it.
 %
 % Limitation:  Because this function uses assignin, it cannot assign values
 % within complex data structures such as r.field1(23).  Such has not been
 % my practice, but it might be nice to have that capability.  This could be
 % supported using 'evalin' and the assignment operator - that is a
 % potential future improvement.
 %
 % Bugs:
 % It is suspected that Matlab does not handle correctly a variable
 % assigned using getArg() if it is first used in a SWITCH statement.  See
 % Kevin's bug 143 on this issue.
 %
 % Future Improvmement:
 % Validation of datatypes and values, for the parameter values.
 % Allow the user to retrieve the name of a particular argument, or its
 % index in the argument list so that he can use inputname on it.

% Check that 'varargin' is present in the caller's workspace.
 try
     if ~evalin('caller', 'exist(''varargin'',''var'')')
         error('This function requires that the variable varargin be present in the caller''s workspace.');
     end;
     % Done checking that 'varargin' is present in the caller's workspace.
 catch
     error('Encountered error attempting to check if varargin exists in caller''s workspace.');
 end;

% Retrieve varargin from caller's workspace.
 try
     caller_varargin = evalin('caller', 'varargin');
 catch
     error('Failed to retrieve varargin from caller''s workspace.');
 end;
 % Done retrieving varargin from caller's workspace.

% If getarg is invoked without arguments, then check that varargin is
 % empty.
 if isempty(varargin)
     if ~isempty(caller_varargin)
         error('Unexpected elements left in varargin.');
     end;
 end;    
 % Done checking for empty varargin.

%allow_leftover_args = 0;  % Change to 1 if the user includes '...' in arguments.

% Parse the arguments to this function, into the following structure:
 x = struct( ...
     'tag_name',           {}, ... % string, name of the tag to search for in arg list
     'num_values',         {}, ... % integer, 0 or more, number of values to follow tag
     'value_names',        {}, ... % cell array of strings, names of variables to assign values
     'use_presence_flag',  {}, ... % boolean, whether or not to set a presence flag
     'presence_name',      {}, ... % string, name of variable to assign presence flag
     'defaults_available', {}, ... % boolean, whether or not default values are available
     'default_values',     {}  ... % cell array of the default values
     );
 done = 0;
 allow_extra_args = 0; % Set to 1 if we find "..." in the inputs.
 
 while ~done
     if isempty(varargin)
         done = 1;
         continue;
     end;

    k = length(x)+1;  % Index of current tag definition.

    % We expect a string like one of these:
     %       'frequency'
     %       'frequency->freq'
     %       'co-ordinates->x,y'
     %       'co-ordinates->x,y->co_ordinates_present'

    tag_definition = varargin{1};
     varargin = varargin(2:end);     
     checkString(u,tag_definition);
     
     % Check for the ellipsis option.
     if strcmp(tag_definition, '...')
         allow_extra_args = 1;
         continue;
     end;
     % Done checking for the ellipsis option.
     
     tag_definition = split(u,tag_definition,'->');
     n = length(tag_definition);
     switch n
         case 0
             error('This should not happen.');
         case 1
             % A tag without any variable names nor a presence flag is suspicicious,
             % but is allowed.  That means that there is a bare tag in the argument
             % list, but we don't check its presence with a flag.
             warning(u, 'failed to find "->" in argument.');
             x(k).tag_name          = tag_definition{1};
             x(k).num_values        = 0;
             x(k).value_names       = {};
             x(k).use_presence_flag = 0;
             x(k).presence_name     = '';
             x(k).defaults_available = 0;
             x(k).default_values = {};
         case 2
             % The user specified a tag, arrow, and zero or more variable
             % names for the values.
             x(k).tag_name = tag_definition{1};
             if isempty(tag_definition{2})
                 % The user used this syntax: 'frequency->'
                 x(k).num_values = 0;
                 x(k).value_names = {};
                 x(k).use_presence_flag = 0;
                 x(k).presence_name = '';
             x(k).defaults_available = 0;
             x(k).default_values = {};
             else
                 s = tag_definition{2};  % This should be a comma-separate list of variable names.
                 s = split(u,s,',');
                 x(k).num_values  = length(s);
                 x(k).value_names = s;
                 x(k).use_presence_flag = 0;
                 x(k).presence_name = '';
             x(k).defaults_available = 0;
             x(k).default_values = {};
             end;

        case 3
             % The user provided a tag, array, zero or more variable names
             % for the values, an arrow, and zero or one variable or names
             % for the presence flag.
             x(k).tag_name = tag_definition{1};
             if isempty(tag_definition{2})
                 % The user used this syntax: 'frequenccy->'
                 x(k).num_values = 0;
                 x(k).value_names = {};
                 x(k).use_presence_flag = 1;
                 x(k).presence_name = tag_definition{3};
             else
                 s = tag_definition{2};  % This should be a comma-separate list of variable names.
                 s = split(u,s,',');
                 x(k).num_values  = length(s);
                 x(k).value_names = s;
                 if isempty(tag_definition{3})
                     x(k).use_presence_flag = 0;
                     x(k).presence_name = '';
                 else
                     x(k).use_presence_flag = 1;
                     x(k).presence_name = tag_definition{3};
                 end;
             end;
         otherwise
             error('Invalid syntax in string.');
     end;
     % Done parsing a tag definition argument.
     % Now we need to check for optional default values.
     if ~isempty(varargin) && ~ischar(varargin{1})
         % The user has supplied optional default values after the current
         % tag definition.
         default_values = varargin{1};
         x(k).default_available = 1;
         varargin = varargin(2:end);
         if isa(default_values, 'cell')
             % The user has supplied a cell array of default values.
             x(k).default_values = default_values;
             x(k).defaults_available = 1;
         else
             % Since the argument is not a cell array, we assume that it is
             % a default value for a single variable.
             x(k).default_values = { default_values };
             x(k).defaults_available = 1;
         end;
     else
         % The user has not supplied optional default values after the
         % current tag definition.
         x(k).defaults_available = 0;
         x(k).default_values = {};
     end;

    % Check that the number of defaults is consistent with the number of
     % values.
     if x(k).defaults_available
         if length(x(k).default_values)~=x(k).num_values
             display(x(k));
             error('Number of default values does not match number of arguments for a tag.');
         end;
     end;
     % Done checking the number of defaults.

end;
 % Done parsing the arguments to this function (getArg).


% Now that we have finished parsing the arguments to getArg, we can go
 % ahead and parse the arguments to the function that called getArg.

% To prepare for the next loop, we put all of the tag names into one list.
 tag_names = cell(length(x),1);
 for k = 1 : length(x)
     tag_names{k} = x(k).tag_name;
 end;
 % Done preparing the list of tag names.

% In this next loop, each time we find a match between an element of x and
 % caller_varargin, we delete the corresponding elements from both
 % locations.
 done = 0;
 leftover_args = {};
 
 if isempty(caller_varargin)
     done = 1;
 end;
 while ~done

    tag = caller_varargin{1};
     caller_varargin = caller_varargin(2:end);
    
     if allow_extra_args && (~checkString(u,tag) || findStringInList(u,tag,tag_names)==0)
         leftover_args{end+1} = tag;
         if isempty(caller_varargin), done = 1; end;
         continue;
     end;
     
     % Check the first element in caller_varargin, presumed to be a tag,
     % against the parsed tags in x.
     
     
     checkString(u,tag);
     k = findStringInList(u,tag, tag_names);
     if k==0
         fprintf( 1, 'This tag does not match any expected tags: %s.\n', tag);
         error('Unsupported tag.');
     end;
     
     y = x(k);  % Extract the entry for the current tag.
     x(k) = [];  % Remove matched tag from the list.
     tag_names(k) = []; % Remove matched tag from the list.
     
     if length(caller_varargin) < y.num_values
         error('Insufficient arguments present for tag.');
     end;

    % Assign values from the current tag.
     for j = 1: y.num_values
         value = caller_varargin{1};
         caller_varargin = caller_varargin(2:end);
         variable_name = y.value_names{1}; % We could have used value_names{j} and not deleted them.
         y.value_names = y.value_names(2:end);
         assignin('caller', variable_name, value);
     end;
     % Done assinging values from the current tag.

    % Assign the presence flag.
     if y.use_presence_flag
         assignin('caller', y.presence_name, 1);
     end;
     % Done assigning the presence flag.

    if isempty(caller_varargin)
         done = 1;
     end;

end;

% Now, we have assigned all values from caller_varargin.  Next, we need to
 % assign any defaults left in x.  Note that we have deleted entries from x
 % corresponding to tags which have been discovered, so that we won't set
 % defaults for tags which appear in caller_varargin.
 for k = 1 : length(x)

    % If there are any entries left in x that need values but don't have defaults, then we
     % print an error messsage.
     if ~x(k).defaults_available && x(k).num_values > 0 
         fprintf(1, 'This tag is missing from the argument list: %s.\n', x(k).tag_name);
         error('Tag is missing from argument list.');
     end;

    for j = 1 : x(k).num_values
         assignin('caller', x(k).value_names{j}, x(k).default_values{j});
     end;

    % Assign the presence flag.
     if x(k).use_presence_flag
         assignin('caller', x(k).presence_name, 0);
     end;
     % Done assigning the presence flag.

end;
 % Done assigning default values.

 % Assign unused arguments back to the caller's workspace.
 if allow_extra_args
     assignin('caller', 'varargin', leftover_args);
 end;
 
return;
