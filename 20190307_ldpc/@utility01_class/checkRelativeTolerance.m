function [ok, measured_tolerance] = checkRelativeTolerance(u, x1, x2, expected_tolerance)
% Checks that two numbers agree within a speciifed relative tolerance.
%
% Inputs:
% x1 is the first number to check.
% x2 is the second number to check.
% expected_tolerance is the expected tolerance.  If this is 0.01, then they should agree within 1%.
%
% Ouputs:
% ok is 1 if the numbers agree within the specified tolerance.
% measured_tolerance is the measured tolerance.
%
% Alternate Usage:  If this function is invoked with no return arguments, and the teat fails, an
% error message is printed.
%
% Alternate Usage: If x1 and x2 are arrays with identical dimensions, then all elements are checked.
%
% This works with complex numbers.  The absolute tolerance is proportional to the complex magnitude.
%

% The absolute error allowed is proportional to the larger of the two numbers.use the larger of the two numbers 

if ~isequal(size(x1), size(x2))
  error('Inputs must have identical dimensions.');
end;
ok = 1;
measured_tolerance = zeros(size(x1));
for k = 1 : numel(x1)
  [temp_ok, measured_tolerance(k)] = checkRelativeTolerance1(x1(k), x2(k), expected_tolerance);
  ok = ok && temp_ok;
end;
if ~ok && nargout==1
  error('Tolerance check failed.');
end;
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ok, measured_relative_tolerance] = checkRelativeTolerance1(x1, x2, expected_tolerance)

% Note that the abs function might be operating on complex inputs here.
magnitude = max(abs(x1), abs(x2));
measured_absolute_tolerance = abs(x1-x2);
measured_relative_tolerance = measured_absolute_tolerance / magnitude;
if measured_relative_tolerance <= expected_tolerance
  ok = 1;
else
  ok = 0;
end;
return;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


