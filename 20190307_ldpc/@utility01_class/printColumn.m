function y = printColumn(u,x, varargin)
% y = printColumn(u,x,...)
% Formats a cell vector for printing, by padding all strings to the same
% witdth.
% 
% Inputs: 
% u is  a utility01_class object.
% x is a 1D cell array of strings.
%
% The remaining arguments are tag/value pairs.
%  'align' can be 'left' or 'right'.  I might support 'center' in the
%  future.
%
% Outputs:
% y is the updated cell array.  If omitted, then the strings are printed
% to standard output.
%
% Future Improvements:
% I might support conversion of numbers to string with printf formats in
% the future, maybe complex numbers too.
%

% Parse input arguments.
getArg(u,'align->align_value', {'left'} );
assertion(u, findStringInList(u, align_value, 'left, right, center')~=0);
% getArg(u);  % Chect that there is nothing left in varargin. 
% Done parsing input arguments.

% Check the other arguments.
checkStringList(u,x);
% Done checking the other arguments.

% Find the largest string in the list.
max_length = 0;
for k =1 : length(x)
    max_length = max(max_length, length(x{k}));
end;
% Done finding the largest string in the list.

% Ensure all of the strings are row vectors.
for k = 1 : length(x)
    if size(x{k},1)>1
        s = x{k};
        x{k} = s(:).';
    end;
end;
             
% Pad all strings to the same length.
for k = 1 : length(x)
    n = max_length - length(x{k});
    switch align_value
        case 'left'
            x{k} = [ x{k} repmat(' ', 1, n)];
        case 'right'
        x{k} = [ repmat(' ', 1, n) x{k} ];
        otherwise
            error('Invalid option.');
    end;
end;
% Done padding all strings to the same length.

y = x;
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


