function [y, msg] = checkAttributeList(u, obj, attr)
% [y, msg] = checkAttributeList(u, obj, attr)
% Checks that an object (structure, typically) has the expected attributes.
%
% Inputs:
% obj is a structure, or may be a class object.
%
% attr is a list of expected attributes.  It can be a cell array of strings, or a string with one or 
% more attribute names separated by commas.
%
% Outputs:
% y is 1 if the object has the specified attributes, 0 otherwise.
%
% msg is a description of the results.  If there is a discrepancy, it lists the missing or extra
% attributes.
%
% If this function is invoked without an output argument, then a report of differences is printed to
% standard output.
%

% Convert the input object to a structure, if required.
if isa(obj, 'struct')
  obj = obj;
else
  obj = struct(obj);
end;

if checkString(u, attr)
  attr = split(u, attr, ',', 'trim');
  attr = removeEmptyCells(u, attr);
end;

obj  = fieldnames(obj);
obj  = sort(obj);
attr = sort(attr);

[same, unique1, unique2] = compareStringLists(u, obj, attr);

if same
  msg = 'The attribute lists match.';
else
  msg = '';
  if ~isempty(unique1)
    msg = [ msg 'The object has these extra attributes:' ];
    for k = 1 : length(unique1)
      msg = [ msg ' ' unique1{k} ];
    end;
    msg = [ msg '.  ' ];
  end;
  if ~isempty(unique2)
    msg = [ msg 'The object is missing these attributes:'];
    for k = 1 : length(unique2)
      msg = [ msg ' ' unique2{k} ];
    end;
    msg = [ msg '.  ' ];
  end;
end;
% Now, msg describes our findings.

if nargout==0
  %fprintf(1, '%s\n', msg);
end;

% Return the status result if the user has provided an output arugment.
if nargout>0
  y = same;
end;
return;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


  