function test_viterbi02(u)
% test_viterbi02(utility01_class)
% Tests out the function "viterbi02".
% Since viterbi02 is the next evolution of viterbi_deocde_no_frills, we
% will do some comparisions to viterbi_decode_no_frills.
%
% Inputs:
% u is a vestigial utility01_class object.
%
% This function prompts the user to select a test, then runs that test.
%
%

fprintf( 1, 'Starting %s.\n', whoAmI(u, 'my name'));

short_names = { ...
    'test01' 'test02' 'test03' 'test04' 'test05' 'test06' 'test07' 'test08' 'test09' 'test10' ...
    'test11' 'test12' 'test13' 'test14' 'test15' 'test16' 'test17' 'test18' 'test19' 'test20' };

long_names = { ...
    '01 simple '
    '02 xact encoder'
    '03 ber test'
    '04 examine bit metrics'
    '05 check path metric'
    '06 '
    '07 '
    '08 '
    '09 '
    '10 '
    '11 '
    '12 '
    '13 '
    '14 '
    '15 '
    '16 '
    '17 '
    '18 '
    '19 '
    '20 ' };
k = menu( 'tests for viterbi_decode_no_frills:',  long_names{:});
%k = 1;
choice = short_names{k};
switch choice
    case 'test01', test01;
    case 'test02', test02;
    case 'test03', test03;
    case 'test04', test04;
    case 'test05', test05;
    case 'test06', test06;
    case 'test07', test07;
    case 'test08', test08;
    case 'test09', test09;
    case 'test10', test10;
    case 'test11', test11;
    case 'test12', test12;
    case 'test13', test13;
    case 'test14', test14;
    case 'test15', test15;
    case 'test16', test16;
    case 'test17', test17;
    case 'test18', test18;
    case 'test19', test19;
    case 'test20', test20;
    otherwise, error('This should not happen.');
end;
fprintf( 1, 'Done %s.\n', whoAmI(u,'my name'));
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function test01
% R=1, bits '0000', simple test.
u = utility01_class;
fprintf( 1, 'Starting %s.\n', whoAmI(u, 'my name'));

if 1
    tx_bits         = [ 1 1  ];
    encoder         = [ 5 7 ];
    constraint_bits = [ 2 2 ];
    rx_code_symbols = [ -1 -1 -1 1];
    test_bits       = [ 0 0 ];
end;

[ detected_bits, decision_metrics ] =   ...
    viterbi_decode_no_frills( u,        ...
    'constraint bits', constraint_bits, ...
    'encoder',         encoder,         ...
    'rx code symbols', rx_code_symbols, ...
    'test bits',       test_bits,       ...
    'plot',            0,               ...
    'outputs',         'detected bits, decision metrics' );
[ detected_bits, decision_metrics ] =   ...
    viterbi_decode_no_frills( u,        ...
    'constraint bits', constraint_bits, ...
    'encoder',         encoder,         ...
    'rx code symbols', rx_code_symbols, ...
    'test bits',       detected_bits,       ...
    'plot',            0,               ...
    'outputs',         'detected bits, decision metrics' );
display(detected_bits);
display(decision_metrics);
[ detected_bits, decision_metrics ] =   ...
    viterbi02( u,                       ...
    'constraint bits', constraint_bits, ...
    'encoder',         encoder,         ...
    'rx code symbols', rx_code_symbols, ...
    'test bits',       detected_bits,       ...
    'plot',            0,               ...
    'outputs',         'detected bits, decision metrics' );
display(detected_bits);
display(decision_metrics);


fprintf( 1, 'Done %s.\n', whoAmI(u, 'my name'));
return;
% End of test01.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function test02
% Xact encoder, R=2.
u = utility01_class;
fprintf( 1, 'Starting %s.\n', whoAmI(u, 'my name'));
%     case 1, g = 1;
%     case 2, g = [ 133 171         ];  % interpreted as octal
%     case 3, g = [ 133 171 165     ];
%     case 4, g = [ 133 171 165 147 ];
if 1
    tx_bits         = [ 0 1 0 0 1 1 0 0 0 1 1 1 0 0 0 0 1 1 1 1 ];
    %tx_bits         = 0;
    encoder_binary = [ base2dec('133',8) base2dec('171',8)];
    encoder_octal  = [ 133 171 ];
    constraint_bits = 2+zeros(size(tx_bits));
    test_bits = zeros(size(constraint_bits));
    rx_code_symbols = 1-2*convolutionalEncoder(u, ...
        'data bits',            tx_bits,          ...
        'pseudo-octal encoder', encoder_octal,    ...
        'add flush bits',       0,                ...
        'outputs',              'code symbols');
end;

[ detected_bits, decision_metrics ] =   ...
    ... viterbi_decode_no_frills( u,        ...
    viterbi02( u,        ...
    'constraint bits', constraint_bits, ...
    'encoder',         encoder_binary,  ...
    'rx code symbols', rx_code_symbols, ...
    'test bits',       test_bits,       ...
    'plot',            0,               ...
    'outputs',         'detected bits, decision metrics' );
[ detected_bits, decision_metrics ] =   ...
    ...viterbi_decode_no_frills( u,        ...
    viterbi02( u, ...
    'constraint bits', constraint_bits, ...
    'encoder',         encoder_binary,  ...
    'rx code symbols', rx_code_symbols, ...
    'test bits',       detected_bits,   ...
    'plot',            0,               ...
    'outputs',         'detected bits, decision metrics' );
display(decision_metrics(:).');
if all(detected_bits(:)==tx_bits(:))
    fprintf(1, 'Viterbi decoder decoded bits successfully.\n');
else
    fprintf(1, 'Viterbi decoder did not decode bits successfully.\n');
end;

fprintf( 1, 'Done %s.\n', whoAmI(u, 'my name'));
return;
% End of test02.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function test03
% BER test.
u = utility01_class;
million = 1e6;
thousand = 1e3;
fprintf( 1, 'Starting %s.\n', whoAmI(u, 'my name'));
%     case 1, g = 1;
%     case 2, g = [ 133 171         ];  % interpreted as octal
%     case 3, g = [ 133 171 165     ];
%     case 4, g = [ 133 171 165 147 ];

% Configure.
ebno_list = 0:1:10;
bits_per_block = 50;
encoder_octal  = [5 7];
%encoder_octal = [ 133 171 165 147 ];
%encoder_octal = 1;
bit_limit = 100*thousand;
error_limit = 20;
% Done configuring.

% Initialize.
ber_list = zeros(size(ebno_list));

[encoder_binary, constraint_length] = parsePseudoOctalEncoder(u, 'encoder', encoder_octal, ...
    'outputs', 'packed binary encoder, constraint length');
constraint_bits = [ 2+zeros(bits_per_block,1); zeros(constraint_length-1,1) ];
test_bits = zeros(bits_per_block+constraint_length-1,1);
% Done initializing.
t1  = clock;
for j = 1 : length(ebno_list)
    error_count = 0;
    bit_count = 0;
    done = 0;
    while ~done
        tx_bits = double(rand(bits_per_block,1)<0.5);        
        %tx_bits = zeros(bits_per_block,1);
        tx_bits = [ tx_bits(:); zeros(constraint_length-1,1) ]; % flush bits
        tx_symbols = 1-2*convolutionalEncoder(u, ...
            'data bits',            tx_bits,          ...
            'pseudo-octal encoder', encoder_octal,    ...
            'add flush bits',       0,                ...
            'outputs',              'code symbols');
        tx_symbols = signal1_class('data', tx_symbols);
        noise = gaussianNoise(tx_symbols, 'snr', ebno_list(j), 'num bits', bits_per_block, 'complex', 0);
        rx_symbols = tx_symbols + noise;
        if 0
            % Use viterbi decoder
            [rx_bits, decision_metrics] = viterbi_decode_no_frills( u, ...
                'constraint bits', constraint_bits, ...
                'encoder',         encoder_binary,  ...
                'rx code symbols', rx_symbols.data,      ...
                'test bits',       test_bits,       ...
                'plot',            0,               ...
                'outputs',         'detected bits, decision metrics' );
        elseif 1
            % Use viterbi decoder
            [rx_bits, decision_metrics] = viterbi02( u, ...
                'constraint bits', constraint_bits, ...
                'encoder',         encoder_binary,  ...
                'rx code symbols', rx_symbols.data,      ...
                'test bits',       test_bits,       ...
                'plot',            0,               ...
                'outputs',         'detected bits, decision metrics' );
        elseif 0
            % Use both viterbi decoders.
            [rx_bits1, decision_metrics1] = viterbi_decode_no_frills( u, ...
                'constraint bits', constraint_bits, ...
                'encoder',         encoder_binary,  ...
                'rx code symbols', rx_symbols.data,      ...
                'test bits',       test_bits,       ...
                'plot',            0,               ...
                'outputs',         'detected bits, decision metrics' );

            [rx_bits2, decision_metrics2] = viterbi02( u, ...
                'constraint bits', constraint_bits, ...
                'encoder',         encoder_binary,  ...
                'rx code symbols', rx_symbols.data,      ...
                'test bits',       test_bits,       ...
                'plot',            0,               ...
                'outputs',         'detected bits, decision metrics' );
            if ~isequal(rx_bits1, rx_bits2), keyboard; end;
            rx_bits = rx_bits1;
        else
            % Use hard slicer, only applicable for R=1.
            rx_bits = rx_symbols.data<0;
        end;
        
        % Later on, I want to investigate the correlation between decision metrics
        % and bit errors.
        bit_count = bit_count + bits_per_block;
        error_count = error_count + sum(rx_bits(1:bits_per_block)~=tx_bits(1:bits_per_block));
        if bit_count > bit_limit || error_count > error_limit
            done = 1;
        end;
        if j > 1 && ber_list(j-1)==0
            done = 1;
        end;
        if etime(clock,t1) > 10
            fprintf( 1, 'Simulated %d bits at ebno=%d.\n', bit_count, ebno_list(j));
            pause(0.1);
            t1  = clock;
            checkLockFile(u);
        end;
    end;
    ber_list(j) = error_count / bit_count;
    
    figure(1);
    clf; setFigureSize(u, gcf, 20,10);
    semilogy(ebno_list, ber_list, 'bo-');
    grid on;
    xlabel('Eb/No (dB)');
    ylabel('BER');
    title(sprintf('BER Test, encoder [%s]', printVector(u,encoder_octal)));
    pause(0.1);
    
end; % for j = 1 : length(ebno_list)
figure(1);
clf; setFigureSize(u, gcf, 20,10);
semilogy(ebno_list, ber_list, 'bo-');
grid on;
xlabel('Eb/No (dB)');
ylabel('BER');
title(sprintf('BER Test, encoder [%s]', printVector(u,encoder_octal)));




fprintf( 1, 'Done %s.\n', whoAmI(u, 'my name'));
return;
% End of test03


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function test04
% Examine relationship between bit errors and bit metrics.
u = utility01_class;
million = 1e6;
thousand = 1e3;
dB = 1;
fprintf( 1, 'Starting %s.\n', whoAmI(u, 'my name'));
%     case 1, g = 1;
%     case 2, g = [ 133 171         ];  % interpreted as octal
%     case 3, g = [ 133 171 165     ];  % These are the xact encoders.
%     case 4, g = [ 133 171 165 147 ];

% Configure.
ebno = 3*dB;
bits_per_block = 30;
encoder_octal  = [1];
%encoder_octal = [ 133 171 165 147 ];
%encoder_octal = 1;
% Done configuring.
num_collect = 1000;  % Collect this many correct bits, and this many bit errors.

% Initialize.
encoder_binary = parsePseudoOctalEncoder(u, 'encoder', encoder_octal, ...
    'outputs', 'packed binary encoder');
constraint_bits = 2+zeros(bits_per_block,1);
test_bits = zeros(bits_per_block,1);
% Done initializing.

% Collect metrics for bit errors.
t1  = clock;
done = 0;
metric_error = [];
while ~done
    %tx_bits = double(rand(bits_per_block,1)<0.5);
    tx_bits = zeros(bits_per_block,1);
    tx_symbols = 1-2*convolutionalEncoder(u, ...
        'data bits',            tx_bits,          ...
        'pseudo-octal encoder', encoder_octal,    ...
        'add flush bits',       0,                ...
        'outputs',              'code symbols');
    tx_symbols = signal1_class('data', tx_symbols);
    noise = gaussianNoise(tx_symbols, 'snr', ebno, 'num bits', bits_per_block, 'complex', 0);
    rx_symbols = tx_symbols + noise;
    if 1
        % Use viterbi decoder
        [rx_bits, decision_metrics] = viterbi_decode_no_frills( u, ...
            'constraint bits', constraint_bits, ...
            'encoder',         encoder_binary,  ...
            'rx code symbols', rx_symbols.data,      ...
            'test bits',       test_bits,       ...
            'plot',            0,               ...
            'outputs',         'detected bits, decision metrics' );
    else
        % Use hard slicer, only applicable for R=1.
        rx_bits = rx_symbols.data<0;
    end;
    
    bit_error_ind = find(tx_bits ~= rx_bits);
    metric_error = [ metric_error; decision_metrics(bit_error_ind) ];
    if length(metric_error) >= num_collect
        done = 1;
    end;
    checkLockFile(u);
    fprintf(1, '%d\n', length(metric_error));
    
end;
% Done collecting metrics for bit errors.

% Collect metrics for correct bits.
t1  = clock;
done = 0;
metric_correct = [];
while ~done
    %tx_bits = double(rand(bits_per_block,1)<0.5);
    tx_bits = zeros(bits_per_block,1);
    tx_symbols = 1-2*convolutionalEncoder(u, ...
        'data bits',            tx_bits,          ...
        'pseudo-octal encoder', encoder_octal,    ...
        'add flush bits',       0,                ...
        'outputs',              'code symbols');
    tx_symbols = signal1_class('data', tx_symbols);
    noise = gaussianNoise(tx_symbols, 'snr', ebno, 'num bits', bits_per_block, 'complex', 0);
    rx_symbols = tx_symbols + noise;
    if 1
        % Use viterbi decoder
        [rx_bits, decision_metrics] = viterbi_decode_no_frills( u, ...
            'constraint bits', constraint_bits, ...
            'encoder',         encoder_binary,  ...
            'rx code symbols', rx_symbols.data,      ...
            'test bits',       test_bits,       ...
            'plot',            0,               ...
            'outputs',         'detected bits, decision metrics' );
    else
        % Use hard slicer, only applicable for R=1.
        rx_bits = rx_symbols.data<0;
    end;
    
    bit_correct_ind = find(tx_bits == rx_bits);
    metric_correct = [ metric_correct; decision_metrics(bit_correct_ind) ];
    if length(metric_correct) >= num_collect
        done = 1;
    end;
    checkLockFile(u);
    fprintf(1, '%d\n', length(metric_error));
end;
% Done collecting metrics for bit errors.

bins = min([metric_correct(:); metric_error(:)]):0.1: max([metric_correct(:); metric_error(:)]);

figure(1);
clf;
y1 = histogram( u, 'data', metric_correct, 'bins', bins, 'outputs', 'counts');

y2 = histogram( u, 'data', metric_error,   'bins', bins, 'outputs', 'counts');
plot(bins, y1, 'r', bins, y2, 'b');
keyboard;

fprintf( 1, 'Done %s.\n', whoAmI(u, 'my name'));
return;
% End of test04


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function test05
% Examine the winning path metric.
u = utility01_class;
dB = 1;
fprintf( 1, 'Starting %s.\n', whoAmI(u, 'my name'));
%     case 1, g = 1;
%     case 2, g = [ 133 171         ];  % interpreted as octal
%     case 3, g = [ 133 171 165     ];  % These are the xact encoders.
%     case 4, g = [ 133 171 165 147 ];

% Configure.
ebno = 10*dB;
bits_per_block = 50;
encoder_octal  = [13 15];
% Done configuring.

% Initialize.
encoder_binary = parsePseudoOctalEncoder(u, 'encoder', encoder_octal, ...
    'outputs', 'packed binary encoder');
constraint_bits = 2+zeros(bits_per_block,1);
test_bits = zeros(bits_per_block,1);
% Done initializing.

tx_bits = double(rand(bits_per_block,1)<0.5);
%tx_bits = zeros(bits_per_block,1);
tx_symbols = 1-2*convolutionalEncoder(u, ...
    'data bits',            tx_bits,          ...
    'pseudo-octal encoder', encoder_octal,    ...
    'add flush bits',       0,                ...
    'outputs',              'code symbols');
tx_symbols = signal1_class('data', tx_symbols);
noise = gaussianNoise(tx_symbols, 'snr', ebno, 'num bits', bits_per_block, 'complex', 0);
rx_symbols = tx_symbols + noise;

[rx_bits, decision_metrics, win_metric] = viterbi_decode_no_frills( u, ...
    'constraint bits', constraint_bits, ...
    'encoder',         encoder_binary,  ...
    'rx code symbols', rx_symbols.data,      ...
    'test bits',       tx_bits,       ...
    'plot',            0,               ...
    'outputs',         'detected bits, decision metrics, winning path metric' );

bit_error_ind = find(rx_bits ~= tx_bits);
if isempty(bit_error_ind)
    fprintf( 1, 'There are not bit errors.\n');
else
    fprintf( 1, 'There are %d bit errors, at these indices: %s\n', length(bit_error_ind), printVector(u, bit_error_ind-1));
end;
fprintf( 1, 'The winning path metric is %d.\n', win_metric);
fprintf( 1, 'These are the bit metrics: %s\n', printVector(u, decision_metrics));
figure(1);
clf;
stem(0:bits_per_block-1, decision_metrics);
title('viterbi bit decision metrics');




fprintf( 1, 'Done %s.\n', whoAmI(u, 'my name'));
return;
% End of test05



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function test06
% Compare against viterbi_decoder1_class.

u = utility01_class;
million = 1e6;
thousand = 1e3;
fprintf( 1, 'Starting %s.\n', whoAmI(u, 'my name'));
%     case 1, g = 1;
%     case 2, g = [ 133 171         ];  % interpreted as octal
%     case 3, g = [ 133 171 165     ];
%     case 4, g = [ 133 171 165 147 ];

% Configure.
ebno_list = 1:1:5;
bits_per_block = 50;
encoder_octal  = [13 15];
%encoder_octal = [ 133 171 165 147 ];
%encoder_octal = 1;
bit_limit = 0.5*thousand;
error_limit = 20;
% Done configuring.

% Initialize.
ber_list1 = zeros(size(ebno_list)); % BER for viterbi_decode_no_frillls.
ber_list2 = zeros(size(ebno_list)); % BER for viterbi_decode1_class.

encoder_binary = parsePseudoOctalEncoder(u, 'encoder', encoder_octal, ...
    'outputs', 'packed binary encoder');
constraint_bits = 2+zeros(bits_per_block,1);
test_bits = zeros(bits_per_block,1);
% Done initializing.
t1  = clock;
for j = 1 : length(ebno_list)
    error_count1 = 0; % Counts errors for viterbi_decode_no_frills.
    error_count2 = 0; % Counts errors for viterbi_decode1_class.
    bit_count = 0;
    done = 0;
    while ~done
        tx_bits = double(rand(bits_per_block,1)<0.5);
        %tx_bits = zeros(bits_per_block,1);
        tx_symbols = 1-2*convolutionalEncoder(u, ...
            'data bits',            tx_bits,          ...
            'pseudo-octal encoder', encoder_octal,    ...
            'add flush bits',       0,                ...
            'outputs',              'code symbols');
        tx_symbols = signal1_class('data', tx_symbols);
        noise = gaussianNoise(tx_symbols, 'snr', ebno_list(j), 'num bits', bits_per_block, 'complex', 0);
        rx_symbols = tx_symbols + noise;
        if 1
            % Use viterbi decoder
            [rx_bits1, decision_metrics] = viterbi_decode_no_frills( u, ...
                'constraint bits', constraint_bits, ...
                'encoder',         encoder_binary,  ...
                'rx code symbols', rx_symbols.data,      ...
                'test bits',       test_bits,       ...
                'plot',            0,               ...
                'outputs',         'detected bits, decision metrics' );
        else
            % Use hard slicer, only applicable for R=1.
            rx_bits1 = rx_symbols.data<0;
        end;
        
        if 1
            % Use viterbi_decoder1_class.
            x = viterbi_decoder1_class;
            x.encoder_octal = encoder_octal;
x.num_bits = bis;
%             x.test_bits.data = [ 1 1 0 0 1 1 1 0 0 0 ];
%             x.state = 'run';
%             x = encode_test_bits(x);
%             figure(1);
%             clf;
%             setFigureSize(u,1,20,10);
%             for t = 0 : x.ND-1
%                 x = privateRunOneBit(x);
%                 
%                 clf;
%                 plot_trellis_lines(x, 'line type', ':.', 'line colour', [0.5 0.5 0.5]);
%                 %plot_trellis_state_numbers(x);
%                 plot_trellis_path(x, 'line colour', [0 0 0], 'bits', x.test_bits);
%                 plot_trellis_survivor_metrics(x);
%                 plot_trellis_branch_metrics(x);
%                 plot_trellis_candidate_metrics(x);
%                 title('Augmented Viterbi Trellis');
%                 pause(1);
%                 checkLockFile(u);
%             end;
%             x = privateTraceback(x);
%             display(x.rx_bits);
%             calc_metrics(x);
%             keyboard;
%             return; % End of test4().

        
            % Done decoding with viterbi_decoder1_class.
            
        end;
        
        % Later on, I want to investigate the correlation between decision metrics
        % and bit errors.
        bit_count = bit_count + bits_per_block;
        error_count = error_count + sum(rx_bits~=tx_bits);
        if bit_count > bit_limit || error_count > error_limit
            done = 1;
        end;
        if j > 1 && ber_list(j-1)==0
            done = 1;
        end;
        checkLockFile(u);
        if etime(clock,t1) > 10
            fprintf( 1, 'Simulated %d bits at ebno=%d.\n', bit_count, ebno_list(j));
            pause(0.1);
            t1  = clock;
            checkLockFile(u);
        end;
    end;
    ber_list(j) = error_count / bit_count;
    
    figure(1);
    clf; setFigureSize(u, gcf, 20,10);
    semilogy(ebno_list, ber_list, 'bo-');
    grid on;
    xlabel('Eb/No (dB)');
    ylabel('BER');
    title(sprintf('BER Test, encoder [%s]', printVector(u,encoder_octal)));
    pause(0.1);
    
end; % for j = 1 : length(ebno_list)
figure(1);
clf; setFigureSize(u, gcf, 20,10);
semilogy(ebno_list, ber_list, 'bo-');
grid on;
xlabel('Eb/No (dB)');
ylabel('BER');
title(sprintf('BER Test, encoder [%s]', printVector(u,encoder_octal)));




fprintf( 1, 'Done %s.\n', whoAmI(u, 'my name'));
return;
% End of test06


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


