function y = checkArray2dReal(u,x)
% y = checkArray2dReal(u,x)
% Checks that an argument is a 2D array with real values.
% It can be empty.  Inf and Nan are not allowed.
%
% Inputs:
 % u is an utility01_class object.
%
% x the argument to check.
%
% Outputs:
% y is 1 if the test passed, 0 if it failed.
% If this output argument is omitted and the test fails, 
% then an error message is printed.
 %
% This test might allow an empty string, or logical values, to pass.
ok = 1;

% I use "ok &&" in each of these tests so that Matlab's short-circuit
% evaluation will bypass execution of the test if a previous test has
 % already failed.
if ok && ~isnumeric(x) 
  ok = 0;
end;

if ok && ndims(x)>2
    ok = 0;
end;

% if ok
%     if any(x~=round(x))
%         ok = 0;
%     end;
% end;

% if ok
%     if any(x(:)<0) || any(x(:)>1)
%         ok = 0;
%      end;
% end;

if ok
  if any(imag(x(:)))
    ok = 0;
  end;
end;

if ok && any(isinf(x(:)))
    ok = 0;
end;

if ok && any(isnan(x(:)))
    ok = 0;
end;

% Done tests.

if nargout == 0
    if ~ok
        error('Check for real 2D array failed.');
     end;   
elseif nargout == 1
    y = ok;
else
    error('Invalid number of arguments.');
end;
return;
 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


