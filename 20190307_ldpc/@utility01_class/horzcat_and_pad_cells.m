function y = horzcat_and_pad_cells(u, varargin)
% y = horzcat_and_pad_cells(u, a, b, c,...)
% Converts numeric arrrays to cell arrays, pads them as required so they all have the same
% number of rows, and concatenates them horizontally.  This is useful for printTable2.
%
% Inputs:
% u is a vestigial utility01_class object.  The remaining arguments are numeric arrays.
%
% Outputs:
% y is a cell array consisting of the input arrays, horizontally concatenated, with empty cells used
% as padding.
%

inputs = varargin;
num_inputs = length(inputs);


% Check that all inputs have no more than 2 dimensions.
for k = 1 : num_inputs
  if ndims(inputs{k})>2
    error('Invalid input.');
  end;
end;

% Determine the largest number of rows in all of the inputs.
num_rows = size(inputs{1}, 1);
for k = 1 : num_inputs
  num_rows = max(num_rows, size(inputs{k},1));
end;

% Convert inputs to cell arrays.
for k = 1 : num_inputs
  inputs{k} = convert_numeric_array_to_cell(inputs{k});
end;

% Pad all inputs as required, to the same number of rows.
for k = 1 : num_inputs
  n = size(inputs{k}, 1); % Number of rows in current array.
  n = num_rows - n;       % Number of rows to append to current array.
  num_cols = size(inputs{k}, 2);
  inputs{k} = [ inputs{k}; cell(n, num_cols) ];
end;

% Concatenate all inputs, horzontally.
y = inputs{1};
for k = 2 : num_inputs
  y = [ y inputs{k} ];
end;
return;

%---------------------------------------------------------
% convert_numeric_array_to_cell
%---------------------------------------------------------
% Converts a numeric array to a cell array.  If it's arleady a cell array, 
% this function leaves it alone.  Assumes that 
function y = convert_numeric_array_to_cell(x)
if ndims(x) > 2
  error('Invalid input.');
end;

if isnumeric(x)
  y = cell(size(x));
  for i = 1 : size(x,1)
    for j = 1 : size(x,2)
      y{i,j} = x(i,j);
    end;
  end;
elseif isa(x, 'cell')
  % Do nothing, leave it alone.
  y = x;
else
  error('Invalid input.');
end;
return;
% End of convert_numeric_array_to_cell.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


