function compareFiles(u)
% compareFiles(utility01_class)
% Compares two files.  Treats them as binary files.
%
%

persistent dir1 file1 dir2 file2

if isempty(dir1)
  dir1 =  'C:\Users\oler\Documents\2017_work\10_projects\';
end;

if isempty(dir2)
  dir1 =  'C:\Users\oler\Documents\2017_work\10_projects\';
end;

if isempty(file1)
  file1 = 'file1.txt';
end;

if isempty(file2)
  file2 = 'file2.txt';
end;

menu_choices = { ...
  '1. Select file1'
  '2. Select file2'
  '3. Compare files.'
  '4. Quit.'   };

done =0;

while ~done
  
  k = uimenu(whoAmI(u, 'my name'), menu_choices{:});
  choice = menu_choices{k};
  fprintf(1, '%s\n', choice);
  
  switch menu_choices
    case '1. Select file1'
      current_dir = pwd;
      if exist(dir1, 'dir')
        cd (dir1);
      end;
      [file1, dir1] = uigetfile('*.*', 'Select file1 to compare', file1);
      
      
    case '2. Select file2'
      current_dir = pwd;
      if exist(dir2, 'dir')
        cd (dir2);
      end;
      [file1, dir2] = uigetfile('*.*', 'Select file2 to compare', file2);
      
    case '3. Compare files.'
      h1 = fileOpen(u, fullfile(dir1, file1), 'rb');
      h2 = fileOpen(u, fullfile(dir2, file2), 'rb');
      
      s1 = fileSizeInBytes(u, h1);
      s2 = fileSizeInBytes(u, h2);
      fprintf(1, 's1=%d, s2 = %d, difference = %.0f bytes.\n', s1, s2, s1-s2);
      
      s = min(s1,s2); % Number of bytes to compare.
      chunk_size = 1e3;
      num_leftover = mod(s, chunk_size);
      num_full_chunks = floor(s / chunk_size);
      first_difference = 0;
      num_different_bytes = 0;
      for k = 0 : num_full_chunks-1
        x1 = fread(h, chunk_size, 'uint8');
        x2 = fread(h, chunk_size, 'uint8');
        i = find(x1 ~= x2);
        if ~isempty(i)
          if num_different_bytes == 0
            % Record the index of the first difference in the files.
            first_difference = k * chunk_size + i(1) - 1;
          end;
          num_different_bytes = num_different_bytes + length(i);
        end;
      end;
      
      x1 = fread(h, num_leftover, 'uint8');
      x2 = fread(h, num_leftover, 'uint8');
      i = find(x1 ~= x2);
      if ~isempty(i)
        if num_different_bytes == 0
          % Record the index of the first difference in the files.
          first_difference = num_full_chunks * chunk_size + i(1) - 1;
        end;
        num_different_bytes = num_different_bytes + length(i);
      end;
      
      fclose(h1);
      fclose(h2);
      
      if s1==s2 && num_different_bytes==0
        fprintf(1, 'The two files are identical.\n');
      end;
      if num_different_bytes~=0
        fprintf(1, 'The number of byte differences is %d.\n', num_different_bytes);
      end;
      
    case '4. Quit.'
      done = 1;
      
    otherwise
      error('This should not happen.');
  end;
end;
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


