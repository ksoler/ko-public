function [h1,h2] = bar2(x,y,ind)
% Similar to Matlab's bar function, but some of the bars can be marked with different colours.
% This partitions the bars into two sets, each with its own colour and handle.
% The current default is red for the first set, blue for the second.
%
% Inputs:
% x is a vector of x values.
% y is a vector of y values.
% ind specifies the indices of the bars which are to be in the first set.
%
% Example:
% bar2(10:10:100, rand(10,1), 3); % The bar at x=30 is marked red.
%
% Copyright 2019, Kevin Oler, All rights reserved.

holdState = ishold;

x1 = x;
x2 = x;
y1 = y;
y2 = y;

p1 = zeros(size(x));
p1(ind) = 1;
p2 = 1-p1;

y1 = y1(:) .* p1(:);
y2 = y2(:) .* p2(:);

h1 = bar(x1,y1,'r');
hold on;
h2 = bar(x2,y2,'b');

if ~holdState
  hold off;
end;
return;
