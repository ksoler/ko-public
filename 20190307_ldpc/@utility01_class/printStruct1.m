function y = printStruct(u, x, name, indent)
% y = printStruct(u,x, name)
% Prints out a structure, recursively, down to all fields.
%
% u is a utility01_class object.
% x is the matlab object to be printed.
% name is a descriptive string for the x.
% indent is the number of spaces to indent.
%
% Outputs:
% y is a 1D cell array of strings containing the printout of x.
% If this function is invoked without outputs, then the text is printed to
% standard output.
%
assertion(u,nargin==2);

% If the user didn't supply the name argument, try inputname.
if nargin<3
    name = inputname(2);
    if isempty(name)
        name = [ class(x) 'object' ];
    end;
end;

if nargin<4
    indent = 0;
end;

y = internal_print_object(x, name, indent);
if nargout==0
    for k = 1 : length(y)
        fprintf( 1, '%s\n', y{k} );
    end;
end;    
return;

function y = internal_print_numeric_vector(x, name, indent)
% y = internal_print_numeric_vector(x, name)
% Creates text representation of a numeric vector.
% 
% Inputs:
% x is the object to be printed.  It should be a numeric vector.
% name is a string containing a name for the object.
% indent is the number of spaces to indent each line of text.
%
% Outputs:
% y is a cell array of strings containing the text representation of x.
%
% Future Improvements:  Consider an option for putting multiple elements on
% the same line.

u = utility01_class;
assertion(u,isnumeric(x));
sz  = size(x);
assertion(u, length(sz)==2);
assertion(u, length(x)==numel(x));

indent = repmat(' ', 1, indent);  % Convert indentation parameter to a string.
y = {};
y{end+1} =  sprintf('%s%s is a numeric vector with %d elements.', indent, name, length(x));
% Convert each number to a string.
for i = 1 : length(x)
    if isreal(x(i))
        a = sprintf('%g', x(i));        
    else
        a = sprintf('%g+i%g', real(x(i)), imag(x(i)));
    end;
    temp_name = sprintf('%s(%2d)', name, i);
    y{end+1} = sprintf(' %s%-20s= %s', indent, temp_name, a); 
end;

return;
% end of internal_print_numeric_vector().

function y = internal_print_struct(x, name, indent)
% y = internal_print_numeric_struct(x, name)
% Creates text representation of a structure.
% 
% Inputs:
% x is the object to be printed.  It should be a Matlab structure.
% name is a string containing a name for the object.
% indent is the number of spaces to indent each line of text.
%
% Outputs:
% y is a cell array of strings containing the text representation of x.
%
% Future Improvements:  None.

u = utility01_class;
assertion(u,isa(x, 'struct'));
f = fieldnames(x);
y = {};
for k = 1: length(f)
    a = internal_print_object(x.(f{k}), [name '.' f{k} ], indent+1);
    y = [ y(:); a(:) ];
end;
return;
% end of internal_print_struct().

function y = internal_print_object(x, name, indent)
% y = internal_print_obj(x, name, indent)
% Creates a text representation of a Matlab object.
%
% Inputs:
% x is the object to be represented as text.  It can be of any type.
% 
% name is a string containing a name for the object, x.
%
% indent is the number of spaces to indent the text.
%
% Outputs:
% y is a 1D cell arryay of strings containing the text representation of x.
%

u = utility01_class;
if isa(x, 'struct')
    y = internal_print_struct(x, name, indent);
elseif isa(x,'numeric') && length(x)~=1
    y = internal_print_numeric_vector(x, name, indent);
elseif isa(x, 'numeric') && length(x)==1
    y = internal_print_numeric_scalar(x, name, indent);
elseif checkString(u,x)
    y = internal_print_string(x, name, indent);
elseif isa(x, 'cell') && length(x)==numel(x)
    y = internal_print_cell_vector(x, name, indent);
else
    % We can't figure out what it is.
    indent = repmat(' ', 1, indent);
    a = sprintf('%s%s is a %s object.', indent, name, class(x));
    y = { a } ;
end;
return;
% End of internal_print_object.

function y = internal_print_numeric_scalar(x, name, indent)
% y = internal_print_numeric_scalar(x, name)
% Creates text representation of a numeric scalar.
% 
% Inputs:
% x is the object to be printed.  It should be a numeric scalar.
% name is a string containing a name for the object.
% indent is the number of spaces to indent each line of text.
%
% Outputs:
% y is a cell array of strings containing the text representation of x.
%
% Future Improvements:  None.

u = utility01_class;
assertion(u,isnumeric(x));
sz  = size(x);
assertion(u, length(sz)==2);
assertion(u, numel(x)==1);

indent = repmat(' ', 1, indent);  % Convert indentation parameter to a string.

% Convert the value to a string.
if isreal(x)
    a = sprintf('%g', x);
else
    a = sprintf('%g+i%g', real(x), imag(x));
end;
% Done converting the value to a string.

% Prepend the variable name.
a = sprintf('%-20s= %s', name, a);

% Indent.
a = [ indent a ];

% Store in a cell array.
y = {a};

return;
% end of internal_print_numeric_scalar().



function y = internal_print_string(x, name, indent)
% y = internal_print_numeric_string(x, name)
% Creates text representation of a string.
% 
% Inputs:
% x is the object to be printed.  It should be a string.
% name is a string containing a name for the object.
% indent is the number of spaces to indent each line of text.
%
% Outputs:
% y is a cell array of strings containing the text representation of x.
%
% Future Improvements:  None.

u = utility01_class;
checkString(u,x);

indent = repmat(' ', 1, indent);  % Convert indentation parameter to a string.
quote = '''';
x = strrep(x,quote, [ quote quote ]); % Replace quotes with double quotes, so that the original string is produced if the epxression is used in a Matlab assignment statement.
a = sprintf('%s%-20s= %s%s%s', indent, name, quote, x, quote );

% Store in a cell array.
y = {a};

return;
% end of internal_print_string().


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


