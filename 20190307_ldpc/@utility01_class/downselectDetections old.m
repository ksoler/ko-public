function i_out = downselectDetections(u, snr, snr_threshold, reject_dist, max_detections)
% i_out = downselect_detections(u, snr, snr_threhsold, reject_dist max_detections)
% Selects  potential detections according to criteria.
%
% Inputs:
% u is a vestigial utility01_class object.
% snr is a 1D array of snr values (or some other signal quality or likelihood metric).
%
% snr_threshold:  Potential detections below this value are discarded.  If this is an empty array,
% then this criterion is not applied.
%
% reject_dist: If the separation between any two snr values is equal to or less than this distance,
% in samples, then the lower snr is rejected.  In other words, find the highest snr value, and 
% reject other snr values within this distance.  Then proceed to the next-highest, and so on.
% If this is 0, then this criterion is effectdively disabled.  If this is 1, then downselected SNRs
% will be at least 2 samples apart.
%
% max_detections is the maximum number of detections allowed.
%
% Outptus:
% i_out is an array of the matlab indices of snr values which pass the criteria.  Thus, snr(i_out)
% are the results which pass the criteria.  The criteria are applied in this order:  snr_threshold,
% min_dist, max_detections.
%

% Throughout this function, snr_ind holds the original index of snr values in the input argument
% 'snr'.
snr_ind = 1:length(snr); 

% Apply snr threshold criterion.
if ~isempty(snr_threshold)
  i = find(snr >= snr_threshold);
  snr = snr(i);
  snr_ind = snr_ind(i);
end;

% Apply min_dist separation criterion.
done = 0;
while ~done
  d = diff(snr_ind);
  % If d(i)<=reject_dist, then snr(i) and snr(i+1) are too close.
  d = find(d<=reject_dist);
  % Now, d holds the indices of snr values that have an snr too close, on the right.
  % snr(d(i)) is too close to snr(d(i+1)).
  if isempty(d)
    done = 1;
    continue;
  end;
  % We need to reject around the highest snr value.  Why?  If we reject the 3rd highest snr because
  % it is too close to the 2nd highest snr, that might be a mistake because the highest snr might
  % force rejection of the 2nd highest, which may mean that the 3rd highest can survive.
  [max1, i1] = max(snr(d));    % Find highest SNR on the left of a pair of snrs which are too close.
  [max2, i2] = max(snr(d+1));  % Find highest SNR on the right of a pair of snrs which are too close.
  if max1>max2
    % snr(i1) is the highest snr value which is too close to another snr value, which is on its right.
    snr(i1+1) = [];
    snr_ind(i1+1) = [];
  else
    % snr(i2+1) is the highest snr value which is too close to another snr value, which is on its left.
    snr(i2) = [];
    snr_ind(i2) = [];
  end;
end;
% At this point, snr is an array of the surviving snr values, and snr_ind holds their indices in the
% original snr array (the input argument).

% Apply the criterion for the maximum number of detections.
if length(snr) > max_detections
  [~,i] = sort(snr);
  i = i(end-max_detections+1:end); % Select the highest snr values, in number equal to max_detections.
  snr = snr(i);
  snr_ind = snr_ind(i);
end;

i_out = snr_ind;
return;
  