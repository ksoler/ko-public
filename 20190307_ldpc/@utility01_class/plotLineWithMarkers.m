function [g, h] = plotLineWithMarkers(u, varargin)
% y = plotLineWithMarkers(u,... )
% Plots a line with some markers at regularly spaced intervals.
%
% Inputs: x, y, n, line, marker, logx, logy, axis, spacing, figure
%
%---------------------------------------------------------------------------------------------------
% Detailed Description of Inputs
% u is a vestigial utility01_class object.  The remaining arguments are tag/value pairs.
%
% 'x' is a 1D array of x co-ordinates of the points to plot.
%
% 'y' is a 1D array of y co-ordinates of the points to plot.
%
% 'n' is the number of markers to plot on the line.
%
% 'line' is a string which speciifes the line type of the line, e.g. 'b-'.  Type 'help plot' for
%        more details.
%
% 'marker' is a string which specifies the marker type to plot, e.g. 'ro'.  Type 'help plot' for
%        more details.
%
% 'logx' is 0 for linear x-axis, 1 for log scale, 'auto' to leave it unchanged.  If 1, then the x
% axis of the plot is set to logarithmic scale, points are spaced evenly when viewed with that
% scale.
%
% 'logy' is 0 for linear x-axis, 1 for log scale, 'auto' to leave it unchanged.  If 1, then the y
% axis of the plot is set to logarithmic scale, points are spaced evenly when viewed with that
% scale.
%
% 'axis' is a 4-element vector which specifies the axis limits to apply, as used by Matlab's built-
% in function axis().  Also, a NaN value leaves the current axis value unchanged or automatic.  A
% value of 'auto' is interpreted as [NaN NaN NaN NaN].
%
% 'spacing' is 'x' or 'd'.  If 'x', then the markers are spaced at uniform horizontal intervals.
% If 'd', then the markers are spaced at uniform distances along the curve.  For the 'd' option,
% the numeric limits and physical size of the current axis, after plotting x and y, are assumed.
% The user can preset the axis by invoking the 'axis' command before plotting.
%
% 'shift' is a number between 0 and 1.  Markers are shifted by this amount multiplied by their
% spacing.  If you are going to plot N lines together on one figure, use shift values of 0, 1/N,
% 2/N, ... (N-1)/N so that the markers aren't on top of each other.  If this is nonzero, the number
% of markers is reduced by 1.
%
% 'figure' is the figure number to use, or 'new' for a new figure, or [] to attempt to use the
% current figure (gcf).
%---------------------------------------------------------------------------------------------------
% Detailed Description of Outputs
% g is a graphics handle for the markers.
%
% h is a graphics handle for the line.
%
%---------------------------------------------------------------------------------------------------
% Description of Algorithm:
% This function places the specified number of markers on the line, spaced at even distances along
% the line
%
%---------------------------------------------------------------------------------------------------
% Future Improvements:
% Allow an option to place markers onto an existing line.
% Allow user to speciify multiple x,y arguments, with their own line types.
% Allow user to specify legend text.
% I'm not sure if this behaves well if some co-ordinates are NaN.  Try it out.
%
% To do:  Add a phse argument, or shift argument, 0-1, and symbols are shifted by this much from
% their nominal positions.  Or tell if that this is series A of B lines, and it calculates those
% phases autoamtically, so that symbols from different lines aren't on top of each other.

getArg(u, ...
  'x      ->x_arg      ',                   ...
  'y      ->y_arg      ',                   ...
  'n      ->n_arg      ', 10,               ...
  'line   ->line_arg   ', {'b-'},           ...
  'marker ->marker_arg ', {'go'},           ...
  'logx   ->logx_arg   ', {'auto'},         ...
  'logy   ->logy_arg   ', {'auto'},         ...
  'axis   ->axis_arg   ', [NaN NaN NaN NaN],...
  'spacing->spacing_arg', {'d'},            ...
  'shift  ->shift_arg  ', 0,                ...
  'figure ->figure_arg ', [] );

% Inputs: x, y, n, line, marker, logx, logy, axis, spacing

checkVectorReal(u, x_arg);
checkVectorReal(u, y_arg);
assertion(u, length(x_arg)==length(y_arg));

if isequal(axis_arg, 'auto')
  axis_arg = [ NaN NaN NaN NaN ];
end;

if ~isequal(x_arg, sort(x_arg))
  error('This function requires x co-ordinates to be in ascending order.');
end;

%if logx_arg || logy_arg
%  error('Log scale options are not currently supported.');
%end;

if isequal(figure_arg, 'new')
  fig = figure;
elseif isequal(figure_arg, [])
  fig = gcf;
else
  fig = figure_arg;
end;
% Now, fig is the number of the figure we will use.

% Plot the line.
h = plot(x_arg, y_arg, line_arg);

if isequal(logx_arg, 'auto')
  switch get(gca, 'XScale')
    case 'log'
      logx_arg = 1;
    case 'linear'
      logx_arg = 0;
    otherwise
      error('Unexpected error.');
  end;
end;
% At this point, logx_arg should be 0 or 1.

if isequal(logx_arg, 'auto')
  switch get(gca, 'XScale')
    case 'log'
      logx_arg = 1;
    case 'linear'
      logx_arg = 0;
    otherwise
      error('Unexpected error.');
  end;
end;
% At this point, logy_arg should be 0 or 1.

if logx_arg
  set(gca, 'XScale', 'log');
else
  set(gca, 'XScale', 'linear');
end;

if logy_arg
  set(gca, 'YScale', 'log');
else
  set(gca, 'YScale', 'linear');
end;

% Apply requested axis limits.
ax = axis;
if ~all(isnan(axis_arg))
  % If the user has not specified any axis limits, then leave the axis at auto or whatever.
  for k = 1 : 4
    if isnan(axis_arg(k)), axis_arg(k) = ax(k); end;
  end;
  axis(ax);
end;
% Done applying requested axis limits.  Now ax is holds the logical extent of the current axis.

% Get the physical size of the axis.
units = get(fig, 'Units');
set(fig, 'Units', 'centimeters');
position = get(fig, 'Position');
set(fig, 'Units', units);
figure_width_cm  = position(3);
figure_height_cm = position(4);

units = get(gca, 'Units');
set(gca, 'Units', 'centimeters');
position = get(gca, 'Position');
set(gca, 'Units', units);
axis_width_cm  = position(3);
axis_height_cm = position(4);
% Done getting the physical size of the axis.

% Convert line co-ordinates to physical co-ordinates as shown on the axis.
N = length(x_arg); % Number of input points.
x_cm = zeros(N,1);
y_cm = zeros(N,1);

if logx_arg
  ax(1) = log10(ax(1));
  ax(2) = log10(ax(2));
  x_arg = log10(x_arg);
  for k = 1 : N
    x_cm(k) = (x_arg(k) - ax(1))/(ax(2)-ax(1)) * axis_width_cm;
  end;
  ax(1) = 10^ax(1);
  ax(2) = 10^ax(2);
  x_arg = 10.^x_arg;
else
  for k = 1 : N
    x_cm(k) = (x_arg(k) - ax(1))/(ax(2)-ax(1)) * axis_width_cm;
  end;
end;

if logy_arg
  ax(3) = log10(ax(3));
  ax(4) = log10(ax(4));
  y_arg = log10(y_arg);
  for k = 1 : N
    y_cm(k) = (y_arg(k) - ax(3))/(ax(4)-ax(3)) * axis_height_cm;
  end;
  ax(3) = 10^ax(3);
  ax(4) = 10^ax(4);
  y_arg = 10.^y_arg;
else
  for k = 1 : N
    y_cm(k) = (y_arg(k) - ax(3))/(ax(4)-ax(3)) * axis_height_cm;
  end;
end;


switch spacing_arg
  case 'd'
    % Markers are spaced at regular intervals along the line of the curve.
    d_cm = zeros(N,1); % distance in cm from
    d_cm(1) = 0;
    d_cm(2:end) = sqrt((x_cm(2:end)-x_cm(1:end-1)).^2 + (y_cm(2:end)-y_cm(1:end-1)).^2);
    d_cm = cumsum(d_cm);
    % Now, d_cm has the cumulative distance along the plotted curve, starting from the first point.
    
    d_markers = linspace(d_cm(1), d_cm(end), n_arg); % Desired distances of markers along the curve.
    if shift_arg~=0
      d_markers = d_markers(1:end-1) + shift_arg*(d_markers(2)-d_markers(1));
    end;
    
    x_markers_cm = zeros(n_arg,1);
    y_markers_cm = zeros(n_arg,2);
    i_markers = interp1(d_cm, 1:N, d_markers);
    % Now, if i_markers(2)=2.1, then the first marker belongs 10% along the line from x_arg(2) to
    % x_arg(3).
    
    if logx_arg
      x_arg = log10(x_arg);
      
      x_markers = interp1(1:N, x_arg, i_markers);
      x_arg = 10.^x_arg;
      x_markers = 10.^x_markers;
    else
      x_markers = interp1(1:N, x_arg, i_markers);
    end;
    
    if logy_arg
      y_arg = log10(y_arg);
      y_markers = interp1(1:N, y_arg, i_markers);
      y_arg = 10.^y_arg;
      y_markers = 10.^y_markers;
    else
      y_markers = interp1(1:N, y_arg, i_markers);
    end;
    
  case 'x'
    % Markers are spaced at regular horizontal intervals.
    
    if logx_arg
      
      x_markers = linspace(log10(x_arg(1)), log10(x_arg(end)), n_arg);
      if shift_arg~=0
        x_markers = x_markers(1:end-1) + shift_arg*(x_markers(2)-x_markers(1));
      end;
      
      x_markers = 10.^x_markers;
    else
      x_markers = linspace(x_arg(1), x_arg(end), n_arg);
      if shift_arg~=0
        x_markers = x_markers(1:end-1) + shift_arg*(x_markers(2)-x_markers(1));
      end;
    end;
    
    if logy_arg
      
      y_markers = 10.^interp1(log10(x_arg), log10(y_arg), log10(x_markers));
    else
      
      y_markers = interp1(x_arg, y_arg, x_markers);
    end;
  otherwise
    error('Invalid spacing option.');
end;

hold_state = ishold;
hold on;
g = plot(x_markers, y_markers, marker_arg);
if hold_state
  hold on;
else
  hold off;
end;
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


