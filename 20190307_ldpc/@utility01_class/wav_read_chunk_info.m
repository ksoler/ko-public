function [ types, offsets, sizes, numChunks, sampleRate] = wav_read_chunk_info(u, file_arg)
% [types, offsets, sizes] = wav_read_chunk_info(u, file)
% Identifies chunks in a WAVE file.
%
% Inputs:
% u is a vestigial utility01_class object.
%
% file is a filename or file handle.  If it is a file handle, then the file is not closed at the end
% of this function.  If this argument is omitted then the user is prompted to select a file.
%
% Outputs:
% types is a list of strings which are the type strings of the chunks in the file, 
% e.g. {'fmt 'data'}.  The RIFF chunk is not included.
%
% offsets is an array with the start byte of each chunk within the file.  This gives the index of
% the type string (4 bytes), which is followed by the chunk length (4 bytes), which is followed by
% the chunk content.
%
% sizes is an array of the chunk sizes as they are reported in the WAVE file, so they do not include
% the type string and 4-bit chunk length.
%
% numChunks is the number of chunks in the file.
%

if nargin<2
  % Get the user to select a file.
  persistent saved_file;
  persistent saved_dir;
  if isempty(saved_dir)
    saved_file = '*.wav';
    saved_dir = 'C:\';
  end;
  original_dir = pwd;
  cd(saved_dir);
  [file1, dir1] = uigetfile(saved_file, 'Select a WAV file.');
  cd(original_dir);
  if isequal(file1,-1)
    fprintf(1, 'No file selected.\n');
    return;
  end;
  filename = fullfile(dir1, file1);
  saved_file = file1;
  saved_dir = dir1;
  % Done getting the user to select a file.
  verbose_mode = 1;
else
  verbose_mode = 0;
end;

h = fileOpen(u, filename, 'rb');
chunkId = char(fread(h, 4, 'uint8'))';
chunkSize = fread(h, 1, 'uint32');
format = char(fread(h, 4, 'uint8'))';

if ~strcmp(chunkId, 'RIFF')
  fclose(h);
  error('Invalid WAV file format.');
end;

if ~strcmp(format, 'WAVE')
  fclose(h);
  error('Invalid WAV file format.');
end;

% We expect a "fmt " chunk next.
formatChunkStart = ftell(h);

formatChunkString = char(fread(h, 4, 'uint8'))';
if ~strcmp(formatChunkString, 'fmt ')
  fclose(h);
  error('Unexpected format.');
end;
formatChunkSize = fread(h, 1, 'uint32');

% Byte position of the start of the next chunk.
dataChunkStart = ftell(h) + formatChunkSize;



audioFormat = fread(h, 1, 'uint16');
if audioFormat ~=1
  fclose(h);
  error('Expected audio format 1, for PCM.  This function does not handle other formats.');
end;
numChannels = fread(h, 1, 'uint16');

% Read the sample rate.
sampleRate = fread(h, 1, 'uint32');

% Read the byte rate.
byteRate = fread(h, 1, 'uint32');

% Read blockAlign, which is the number of bytes for one time sample, across all channels.
blockAlign = fread(h, 1, 'uint16');

% Read bitsPerSample.
bitsPerSample = fread(h, 1, 'uint16');

% Check blockAlign.
expected_blockAlign = bitsPerSample/8 * numChannels;
if blockAlign ~= expected_blockAlign
  warning(u, 'Unexpected value for blockAlign.');
end;

% Check byteRate.
expected_byteRate = blockAlign * sampleRate;
if byteRate ~= expected_byteRate
  warning(u, 'Unexpected value for byteRate.');
end;

if ftell(h) ~= dataChunkStart
  warning(u, sprintf('Current position is %d bytes, but dataStart is %d bytes.\n', ftell(h), dataChunkStart));
end;

%fileSeek(u, h, dataStart, 'bof');
data_tag = char(fread(h, 4, 'uint8'))';

if ~strcmp(data_tag, 'data')
  fclose(h);
  error('Failed to find data tag.');
end;

data_size = fread(h, 1, 'uint32');
numSamples = data_size / blockAlign;
samplesPerChannel = numSamples;

rawDataStart = ftell(h); % Byte position of the start of data samples.
dataStart = rawDataStart;
% Check data_size.

fseek(h, 0, 'eof');
fileSizeInBytes = ftell(h);
expected_fileSizeInBytes=  numSamples * blockAlign + rawDataStart; 

if fileSizeInBytes ~= expected_fileSizeInBytes
  warning(u, 'Unexpected file size.');
end;

fclose(h);
if verbose_mode
  [path_str, file_str, ext_str] = fileparts(filename);
  fprintf(1, 'filename: %s%s\n', file_str, ext_str);
  fprintf(1, 'sampleRate = %f kHz\n', sampleRate/1000);
  fprintf(1, 'bitsPersample =%d\n', bitsPerSample);
  fprintf(1, 'numChannels = %d\n', numChannels);
  fprintf(1, 'numSamples = %d\n', numSamples);
  fprintf(1, 'duration = %f seconds = %f minutes\n', numSamples / sampleRate, numSamples / sampleRate / 60);
end;

return;

% Read the chunkID and format fields.
%---------------------------------------------------------------------------------------------------
function y = read_string_from_file(h, start, len)
% fileSeek(h, start);
y = fread(h, len, 'uint8');
y = char(y);
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


