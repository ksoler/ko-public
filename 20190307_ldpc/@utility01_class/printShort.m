function y = printShort(u,x)
% y = printShort(u,x)
% Creates a brief string representation of an object.
%
% Inputs:
% u is a vestigial utility01_class object.
% x is the object to be represented as text.
%
% Outputs:
% y is a brief string representation of x.
%

% Empty.
if isempty(x)
    y = sprintf('(empty %s)', class(x));
    return;
end;

% Numeric scalar.
if isnumeric(x) && numel(x) == 1
    y = num2str(x);
    return;
end;

% Numeric vector.
if isnumeric(x) && numel(x)==length(x)
    n = length(x);
    open_bracket = {'['};
    close_bracket = {']'};
    ellipsis =     {'...'};
    if n < 10
        % The vector is short, so print all elements.
        a = cell(n,1);
        for k = 1 : n
            a{k} = num2str(x(k));
        end;
        a = [ open_bracket; a(:); close_bracket ];
        y = join(u,a,' ');
    else
        % The vector is long, so only show some of the elements.
        a = cell(8,1);
        for k = 1 : 4
            a{k} = num2str(x(k));
            a{end-k+1} = num2str(x(end-k+1));
        end;
        a = [ open_bracket; a(1:4); ellipsis; close_bracket ];
        y = join(u,a, ' ');
    end;
    return;
end;


% String
if checkString(u, x)
    quote = '''';
    ellipsis = '...';
    if length(x)<30
        y = [ quote x quote ];
    else
        y = [ quote x(1:30) ellipsis quote ];
    end;
    return;
end;

% Default handling of objects.
if numel(x)==1
    y = sprintf('%s object', class(x));
else
    s = size(x);
    s = printVector(u,s);  % convert to a string.
    y = sprintf('%s object, dimensions [%s]', s);
end;
return;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


