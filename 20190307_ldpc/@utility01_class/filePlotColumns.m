function filePlotColumns(u, filename, varargin)
% h = fileReadN(u, filename, varargin)
% Reads numeric data from a file and plots it.
%
% Inputs:
% u is a vestigial utility01_class object.
%
% filename is the name of the file to read.  The remaining arguments are optional tag/value pairs,
% in any order.
%
% 'rows' is an array of the rows to read, e.g. 1:10 to read the first 10 rows of the file.  Use
% 'all' to read all rows.  This is the default.
%
% 'cols' is an array of the columns to plot, e.g. [2 3 4] to read the second, third, and fourth
% columns.  Use 'all' to read all columns.  This is the default.
%
% 'figure' is the figure number to use.  Use 'new' to generate a new figure using the 'figure'
% command.  Use 'current' to use the current figure (if there is one).
%
% 'xlabel' is the x axis text label, or [] to ignore.
%
% 'ylabel' is the y axis text label, or [] to ignore.
%
% 'title' is the title to use, or [] to ignore.  By default, this function disables the tex
% interpreter for labels, and titles.
%
% 'legend' is a cell array of legend labels to use with the plotted columns.  If empty, then no
% legend is generated.
%
% 'grid' is 0 or 1.  Default is 1.
%
% 'clf' is 0 or 1.  1 is the default.  If 1, the figure is cleared before plotting.
%
% 'linetypes' is a cell array of line types (e.g. {'b-' 'g0' 'r*'}).
%
% Future Improvemets:  Allow user to specify x axis values.  For now, they are a count, starting at
% 0.  Allow user to specify lines, markers.  Return data and/or line handles.  Maybe figure or axis
% handles.  Allow user to specify linear, log scale.
%

default_line_types = { ...
  'b-' 'g-' 'r-' 'c-' 'y-' 'm-' ...
  'bo-' 'go-' 'ro-' 'co-' 'yo-' 'mo-' ...
  'bs-' 'gs-' 'rs-' 'cs-' 'ys-' 'ms-' };

getArg( u, ...
  'rows     ->rows       ', {'all'},     ...
  'cols     ->cols       ', {'all'},     ...
  'figure   ->fig        ', {'current'}, ... 
  'xlabel   ->xlabel_text', [],          ...
  'ylabel   ->ylabel_text', [],          ...
  'title    ->title_text ', [],          ...
  'legend   ->legend_text', [],          ...
  'grid     ->gridFlag   ', 1,           ...
  'clf      ->clf_flag   ', 1,           ...
  'linetypes->linetypes', { default_line_types } );

% First, read data from the file.
h = fileOpen(u, filename, 'rt');

% First, read all of the data, then downselect data using the 'rows' and 'cols' arguments.  In the
% future, it would be more efficient to take those into account while reading from the file.

% For now, nr is the number of rows, nc is the number of columns.

nr = 0;
nc = 0;
y = [];

t1 = clock;
done = 0;
while ~done
  s = fgetl(h);
  if isequal(s,-1)
    done = 1;
    continue;
  end;
  a = sscanf(s,'%g');
  if nr==0
    % We are parsing the first row, so we wil set the column size now.
    nc = length(a);
    y = a(:).';
    nr = 1;
  else
    if length(a) ~= nc
      msg = sprintf('Ignoring line.  Expected %d values, found %d.', nc, length(m));
      warning(u, msg);
      error('Error reading file.');
    end;
    y = [ y; a(:).' ];
    nr = nr + 1;
  end;
  if etime(clock, t1) > 10
    fprintf(1, 'In %s, called by %s, read %.0fK samples.\n', whoAmI(u, 'my name'), whoAmI(u, 'caller name'), floor(nr/1000));
    t1 = clock;
    pause(0.1);
  end;  
end;
fclose(h);

% We have read the data in to y.

% Downselect rows, columns.
if isequal(rows, 'all')
  % Do nothing.
else
  y = y(rows,:);
end;

if isequal(cols, 'all')
  % Do nothing.
else
  y = y(:, cols);
end;
% Done downselecting rows, columns.

% Select figure number.
if isequal(fig, 'new')
  fig = figure;
elseif isequal(fig, 'current') || isequal(fig, 'same')
  fig = gcf;
else
  % We expect that fig is a figure number.
  figure(fig);
end;
% Done selecting figure number.

% Handle clf_flag.
if clf_flag
  clf;
end;

% Update the number of columns to plot.
nc = size(y,2);

hold_state = ishold;

handles = [];
for c = 1 : nc
  s = linetypes{ mod(c-1, length(linetypes))+1 };
  handles = [ handles plot(0:nr-1, y(:, c), s) ]; % This supports handles which are a number of an object.
  hold on;
end;
if ~isempty(legend_text)
  legend(handles, legend_text{:});
end;

if ~isempty(xlabel_text)
  xlabel(xlabel_text, 'Interpreter', 'none');
end;

if ~isempty(ylabel_text)
  ylabel(ylabel_text, 'Interpreter', 'none');
end;
  
if ~isempty(title_text)
  title(title_text, 'Interpreter', 'none');
end;

if gridFlag
  grid on;  
end;

if ~ishold
  hold off;
end;

return;















%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


