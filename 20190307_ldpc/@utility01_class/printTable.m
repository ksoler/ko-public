function y = printTable(u,x, varargin)
% y = printTable(u,x, ...)
% Formats a cell array of strings for printing.
%
% Inputs:
% u is a utility01_class object.
% x is a cell array of stirngs and numeric values.
% Additional arguments are tag/value pairs.
%
%  The allowed tags are
%   'left' specifies left-alignment when padding.
%
%   'right' specifies right-alignment when padding.
%
%   'center' speciifes center-alignment when padding (but isn't supported yet).
% 
%   'numeric format' is followed by another string argument, which is used
%   in sprintf to convert any numeric cell values to strings.  But that's
%   not supported yet either.
%
%   'separate columns' keeps the columns as separate strings, instead of
%   joining them into one string per row.
%
% If x is a numeric array, it is automatically converted to a cell array of
% strings.

u = utility01_class;
assertion(u, isa(x,'cell') || isa(x,'numeric'));
assertion(u, length(size(x))<=2);  % More than 2 dimensions are not allowed.


% Parse the optional arguments.
getArg(u, ...
    'left  ->->left',    ...
    'right ->->right',   ...
    'center->->center',  ...
    'numeric format->numeric_format', {'%g'}, ...
    'separate columns->->sep_cols');
% getArg(u);  % Check that varargin is now empty.
% Done parsing the optional arguments.

% Convert x from numeric to string.
if isa(x, 'numeric')
    new_x = cell(size(x));
    for j = 1 : size(x,1)
        for k = 1 : size(x,2)
            new_x{j,k} = sprintf(numeric_format, x(j,k));
        end;
    end;
    x = new_x;
end;
% Done converting x from numeric to string.
        
if left+right+center==0
    left = 1; % default alignment
elseif left+right+center>1
    error('You can only specify one of left, center, right.');
end;

if center
    error('Centered alignment is not currently supported.');
end;
if left
    alignment = 'left';
end;
if right 
    alignment = 'right';
end;

for j = 1:size(x,2)  % Step through the columns of x.
    x(:,j) = printColumn(u,x(:,j), 'align', alignment);
end;

if sep_cols
    y = x;
else
% Join together elements of each row.
y = cell(size(x,1),1);
for j = 1 : size(x,1)
    y{j} = join(u, x(j,:), ' ');
end;
end;

% Print to standard output if no output argument is supplied.
if nargout==0
    for k = 1 : length(y)
        fprintf(1, '%s', y{k});
        fprintf( 1, '\n');
    end;
end;
% Done printing to standard output.

return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


