function y = source(u,varargin)
% y = source(u, varargin)
% Executes commands from a text file, like the unix "source" command.
%
% Inputs: file, dir
%
% Outputs:
% y is a Matlab structure whose fields are the variables created by executing the 
% statements in the specified file.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Detailed Description of Inputs:
%
% u is a vestigial utility01_class object.  The remaining arguments are 
% tag/value pairs.
%
% 'file' is a string containing the name of the file to use.
%
% 'dir' is a string containig the directory of the file to use.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Discussion:
% What workspace should these commands be executed in?  If it's not the caller's, then it's hard 
% to evalulate things meaningfully.  It would be nice if we could create a virtual workspace
% to execute in.  Maybe I can, using save and load commands; that will just slow it down.
% If I am to create a virtual workspace, then how shall I do it?  By creating a function inside of
% this one, and using save and load for each line.  Okay.
% 
getArg(u, ...
    'file->file_arg', {''}, ...
    'dir ->dir_arg ', {''}  );

mat_filename = fullfile(tempdir, 'utility01_source_m.mat');

filename = fullfile(dir_arg, file_arg);
if ~exist(filename, 'file')
    error('Did not find requested file.');
end;

[ filePartsDir, filePartsName, filePartsExt ] = fileparts(filename);
filePartsName = [filePartsName filePartsExt];

if exist(mat_filename, 'file')
    delete(mat_filename);
end;

h = fileOpen(u, filename, 'rt');
done = 0;
line_count = 0;
t1  =clock;
while ~feof(h)    
    errmsg = eval_in_controlled_workspace(h, mat_filename);
    if ~isempty(errmsg)
        fprintf(1, 'Error while runnning %s,\n', whoAmI(u, 'my name'));
        [d, f, ex] = fileparts(filename);
        fprintf(1, 'source directory: %s\n', d);
        fprintf(1, 'source file:      %s%s\n', f, ex);
        % fprintf(1, 'text:             %s\n', s );
        fprintf(1, 'error message:    %s\n', errmsg);
        error('Error in source file.');
    end;
    
    line_count = line_count  + 1;
    if mod(line_count, 100)==0 && etime(clock, t1)>10
        fprintf( 1, 'In %s, read %2.0f%% of %s.\n', whoAmI(u, 'my name'), filePositionPercent(u,h), filePartsName);
        pause(0.1);
        t1 = clock;
    end;
end;
fclose(h);

y = load(mat_filename);
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function output_argument = eval_in_controlled_workspace(handle_of_source_file, filename_of_mat_file)
% eval_in_controlled_workspace
%
% Inputs:
% handle_of_source_file is a handle for the source file.
%
% filename_of_mat_file is the name of a MAT file.  This function loads that file 
% (if it exists) before executing the Matlab statement, and then saves all variables
% to that file afterwards.
%
% Outputs:
% output_argument is empty if there were no errors, or a string containing an error 
% message if an error occurred.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 
% Define constants.
u = utility01_class;
j = sqrt(-1);
seconds = 1;
ms = 1e-3*seconds;
Hz = 1/seconds;
kHz = 1/ms;
dB = 1;
% Done defining constants.

if exist(filename_of_mat_file, 'file')
    load(filename_of_mat_file);
end;

try
    for loop_counter = 1 : 100
        string_to_evaluate = fgetl(handle_of_source_file);
        if ischar(string_to_evaluate)
            eval(string_to_evaluate);
        end;
    end;
catch exception_object
    output_argument = [ string_to_evaluate ' ' exception_object.message ];
    return;
end;

% At this point, there was no error from executing the string.
assertion(u, strcmp(filename_of_mat_file, fullfile(tempdir, 'utility01_source_m.mat')));

% Clear all variables except those created by the statement in the source file.
clear filename_of_mat_file
clear string_to_evaluate
clear loop_counter
save(fullfile(tempdir, 'utility01_source_m.mat'));

output_argument = [];
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


