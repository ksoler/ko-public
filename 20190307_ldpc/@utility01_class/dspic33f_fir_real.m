function varargout = dspic33f_fir_real(u, varargin)
% [outputs...] = dspic33f_fir_real(u, inputs...)
% Emulates FIR filtering of dsPIC33F processor.
%
% Inputs:  data, filter, flush, outputs, rounding mode, saturation mode, bypass fixed point effects
% Outputs: data
%
% See the Microchip dspPIC33F Data Sheet at http://ww1.microchip.com/downloads/en/DeviceDoc/70165d.pdf
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Detailed Description of Inputs:
%
% u is a vestigial utility01_class object.  The remaining inputs are tag/value pairs, and the
% outputs are requested by the caleer.
%
% 'filter' is an array of real filter taps.  If bypass mode is not used, then they must be within
% the range of 16-bit signed numbers.
%
% 'data' is an array of input real signal data.  If bypass mode is not used, they must be within the
% range of 16-bit signed numbers.
%
% 'saturation mode' can be 'bit31', 'bit39', 'overflow', or 'none'.
%    bit31:    accumulator saturates at 32-bit limits
%    bit39:    accumulator saturates at 40-bit limits
%    overflow: accumulator overflows at 40-bit limits.
%    none:     accumulator uses Matlab's full precision
%
% 'bypass fixed point effects' is 0 or 1.
%     0: do input quantization, accumulator rounding and saturation, and output saturation.
%     1: bypass fixed point effects, but still scales like the dsPIC.
%
% 'flush' is 0 or 1.  0: Do not insert extra zeros at the end  The last output sample is computed
% with the filter taps full of the last input samples.  1: Insert extra zeros (one less than
% the number of filter taps) to flush out the input signal, so that the last output sample is
% computed with the last input sample in the last filter tap.
%
% 'outputs' is a string which specifies the requested output arguments, separated by commas.
%
% 'rounding mode' is 'conventional' or 'convergent'.
%    'conventional' adds b15 to b16 of each final (output) accumulator result.
%    'convergent'   adds b16 to b16, unless b0:b15=0x8000.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Detailed Description of Outputs:
%
% 'data' is the result of convolving the input data with the filter.
%
% In the future, there may be support for saturation flags.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Future Improvements:
% Consider supporting a mode which performs filtering without quantization, saturation, or overflow.
%

q = 1;

getArg(u, ...
    'data           ->data_arg   ',       ...
    'filter         ->filter_arg ',       ...
    'flush          ->flush_arg  ',       ...
    'outputs        ->outputs_arg', {''}, ...
    'rounding mode  ->round_arg  ',       ...
    'bypass fixed point effects->bypass_arg', ...
    'saturation mode->sat_arg    ' );

switch sat_arg
    case 'bit31',    acc_min = -2^31; acc_max = 2^31-1;
    case 'bit39',    acc_min = -2^39; acc_max = 2^39-1;
    case 'overflow', acc_min = -2^39; acc_max = 2^39-1;
    case 'none',     acc_min = -inf;  acc_max = +inf;
    otherwise
        error('Invalid saturation mode.');
end;

% What about complex arguments!?!??  This function needs to do it twice!!!!
if any(imag(data_arg)) || any(imag(filter_arg))
    error('This function does not accept complex data or filters.');
end;

if bypass_arg
    y = conv(data_arg, filter_arg);
    y = y/2^15;
    if flush_arg
        %y = y;
    else
        y = y(1:length(data_arg));
    end;

else
    
    x = round(data_arg);
    if any(x<-2^15) || any(x>2^15-1)
        error('Input data out of range.');
    end;
    h = round(filter_arg);
    if any(h<-2^15) || any(h>2^15-1)
        error('Filter taps out of range.');
    end;
    
    
    nx = length(x);  % number of input samples
    nh = length(h);
    
    if flush_arg
        ny = nx+nh-1; % number of output samples to calculate
    else
        ny = nx;
    end;
    
    y = zeros(ny,1);
    
    for ty = 0 : ny-1
        acc = 0;
        for th = 0 : nh - 1
            tx = ty - th;
            if 0 <= tx && tx < nx
                prod = x(tx+q) * h(th+q); % Q2.30 result
                prod = prod * 2;             % Q2.31 result
                acc = acc + prod;            % Q9.31 result
                
                % Handle accumulator saturation.
                if acc<acc_min || acc>acc_max
                    warning(u, 'Accumulator exceeded numeric precision limit.');  % Later on we'll want to be able to shut off this warning.
                    switch sat_arg
                        case 'bit31'
                            acc = min(acc, acc_max);
                            acc = max(acc, acc_min);
                        case 'bit39'
                            acc = min(acc, acc_max);
                            acc = max(acc, acc_min);
                        case 'overflow'
                            if acc<acc_min, acc=acc+2^40; end;
                            if acc>acc_max, acc=acc-2^40; end;
                        case 'none'
                            % Do nothing.
                        otherwise, error('This should not happen.');
                    end;
                end;
                % Done handling accumulator saturation.                
                
                if 0 % Diagnostic output.
                    if th==0, fprintf( 1, '\n' ); end;
                    fprintf( 1, 'y(%2s) += h(%2s)*x(%2s) ', convert_decimal2hex(u, ty, 'uint16'), convert_decimal2hex(u,th, 'uint16'), convert_decimal2hex(u,tx, 'uint16'));
                    fprintf( 1, ' = %s * %s ', convert_decimal2hex( u, h(th+q), 'int16' ), convert_decimal2hex( u, data(tx+q), 'int16' ));
                    fprintf( 1, ' acc = %s', convert_decimal2hex( u, acc, 'int32' ) );
                    fprintf( 1, '\n' );
                end;
            end;
        end;
        
        % Handle rounding.
        if acc<0
            acc = acc+2^40;
            sign = -1;
        else
            acc = acc;
            sign = +1;
        end;
        b15 = mod(floor(acc/2^15),2);
        % Warning - althought it's unlikely, it is possible that this
        % rounding could drive the accumulator into overflow.
        switch round_arg
            case 'conventional'
                acc = acc + 2^16*b15;
            case 'convergent'
                if mod(acc, 2^16)~=2^15
                    acc = acc + 2^16*b15;
                end;
            otherwise
                error('Invalid rounding mode.');
        end;
       
        if sign==-1
            acc = acc - 2^40;
        end;        
        
        if acc<acc_min || acc>acc_max
            % Maybe we can disable the following error message.
            error('This function ought to include overflow behavior after accumulator rounding.  But maybe that is already taken care of by output saturation?');
        end;
         
        % Done handling rounding.
        y(ty+q) = floor(acc/2^16);  % Is this corrrect?
        
        % Apply output saturation - does the dsPIC also allow output overflow mode?
        if -2^15 <= y(ty+q) && y(ty+q) <= 2^15-1
            % No output saturation.
        else
            % There is output saturation.
            warning(u, 'Accumulator exceeded numeric precision limit for 16bit output.');  % Later on we'll want to be able to shut off this warning.
            y(ty+q) = min(y(ty+q),  2^15-1);
            y(ty+q) = max(y(ty+q), -2^15  );
        end;
        
    end;  % for t
    
end;  % if bypass_arg
    
% Assign output arguments.
outputs = split(u, outputs_arg, ',', 'trim');
outputs = removeEmptyCells(u, outputs);
n = length(outputs);
assertion(u, n==nargout);
varargout = cell(n,1);
for k = 1 : n
    switch outputs{k}
        case 'data', varargout{k} = y;
        otherwise error('Invalid output argument.');
    end;
end;
% Done assigning output arguments.

return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


