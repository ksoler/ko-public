function h = plotMulipleLines(u, varargin)
% Plots a 2D array of numeric data using multiple lines on a 2D plot.
% Each row of data is plotted as one line.
%
% (data)   data, x values, row values
% (format) legend, legend location, linetypes, row value format string
%---------------------------------------------------------------------------------------------------
% Detailed Description of Inputs
%
% u is a vestigial utility01_class object.  The remaining inputs are tag/value pairs.
%
% (data)
% 'data' is a 2D array.  Each row will be plotted as one line on the figure.
%
% 'row values' is a 1D array of values.  Each corresponds to one row of 'data'.  If this is a cell array of strings, 
% then the strings are used without further conversion.
%
% 'x values' is an array of x values to use for the plot.  The number of values must equal the 
% number of columns in 'data'. 
%
% (format)
% 'legend' enables a legend (0,1).
%
% 'legend location' is the location argment for legend command, e.g. 'SouthEast'.
%
% 'linetypes' is a cell array of linestyp strings, each corresponding to one row of 'data'.
%
% 'row value format string' is a printf-style format string used to conver the row values to labels.
%---------------------------------------------------------------------------------------------------
% Detailed Description of Outputs
%
% h is an array of line handles.
%---------------------------------------------------------------------------------------------------

getArg(u,                                                                           ...
  ... (data)
  'data                   ->data                                         ',         ...
  'x values               ->x_values                                     ',         ...
  'row values             ->row_values             ->row_values_flag     ', {[]},   ...
  ... (format)
  'legend                 ->legend_arg                                   ', 0,      ...
  'legend location        ->legend_location        ->legend_location_flag', [],     ...
  'linetypes              ->linetypes              ->linetypes_flag      ', {{''}}, ...
  'row value format string->row_value_format_string->format_string_flag  ', {''} ); 

num_rows = size(data,1);  % Number of lines to plot.
num_cols = size(data,2);  % Number of points for each line.

% Provide defaults for missing values.
if ~format_string_flag
  row_value_format_string = 'parameter=%d';
end;

if ~row_values_flag
  row_values = 1:num_rows;
end;
% Done providing defaults for missing values.

% Convert row values to a string, if required.
if isnumeric(row_values)
  row_stings = cell(num_rows,1);
  for k = 1: num_rows
    row_strings{k} = sprintf(row_value_format_string, row_values(k));
  end;
else
  row_strings = row_values; 
end;
% Done converting row values to a string, if required.

% Save the hold state.
hold_state = ishold;
% Done saving the hold state.

% Plot data.
handles = [];
for k = 1 : num_rows
  handles(k) = plot(x_values, data(k,:), linetypes{mod(k-1, length(linetypes))+1});
  hold on;
end;
% Done plotting data.

% Restore hold state.
if ~hold_state
  hold off;
end;
% Done restoring hold state.

% Create legend.
if legend_arg
  if legend_location_flag
    legend(handles, row_strings{:}, 'location', legend_location);
  else
  legend(handles, row_strings{:});  
  end;
end;
% Done creating legend.

h = handles;
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


