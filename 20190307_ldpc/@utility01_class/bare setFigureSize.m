function setFigureSize(f,width, height)
% setFigureSize(f,width,height)
% Sets the size of a figure window.
%
% Inputs:
% f is a figure handle.
% width is the desired width in centimetres.
% height is the desired height in centimetres.
%
% Copyright 2019, Kevin Oler, All rights reserved.

if ~strcmp(get(f,'Type'),'figure')
    error('Invalid handle - expected a figure.');
end;

set(f, 'PaperPositionMode', 'manual'); % 'auto' or 'manual'.  Auto makes the printing use the same size as shown on the screen.

old_units = get(f,'Units');
set(f, 'Units', 'centimeters');
p = get(f,'Position');
original_width  = p(3);
original_height = p(4);
p(2) = p(2) - (height - original_height);  % Keep the top edge at the same position, by moving the bottom edge up or down.
p(3) = width;
p(4) = height;
set(f,'Position', p);
set(f,'Units', old_units);

old_paper_units = get(f,'PaperUnits');
set(f, 'PaperUnits', 'centimeters');
p = get(f,'PaperPosition');
p(3) = width;
p(4) = height;
set(f, 'PaperPosition', p);
set(f, 'PaperUnits', old_paper_units);

return;
