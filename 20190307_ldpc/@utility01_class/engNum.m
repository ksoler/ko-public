function y = engNum(u, num, fmt, units)
% y = engNum(u, num, fmt, units)
% Shows a number with engineering units of magnitude.
%
% Inputs:
% u is a vestigial utility01_class object.  Its only purpose is to invoke
% the namespace of the utility01_class.
%
% num is a number to display.  For now, this function supports only scalars.
% This is not allowed to be complex.
%
% fmt is a printf-style format string, such as '%5.1f',
%
% units is a string containing units, such as 'Hz', 's', or 'sps'.
%
% Outputs:
% y is a string containing the number, scale factor, and units.
%


% Check inputs.
check(u, num,   'real scalar');
check(u, fmt,   'string');
check(u, units, 'string');
% Done checking inputs.

if num==0
    s = '';
    a = 1;
elseif abs(num) < 1e-6
  s = 'n';
  a = 1e-9;
elseif abs(num) < 1e-3
  s = 'u';
  a = 1e-6;
elseif abs(num) < 1e0
  s = 'm';
  a = 1e-3;
elseif abs(num) < 1e3
  s = '';
  a = 1e0;
elseif abs(num) < 1e6
  s = 'k';
  a = 1e3;
elseif abs(num) < 1e9
  s = 'M';
  a = 1e6;
else
  s = 'G';
  a = 1e9;
end;

y = sprintf( [fmt '%s%s'], num/a, s, units );
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


