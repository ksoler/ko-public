function setFigureSize(u,f,width, height)
% setFigureSize(u,f,width,height)
% Sets the size of a figure window.
%
% Inputs:
% u is a vestigial utility01_class object.
% f is a figure handle.
% width is the desired width in centimetres.
% height is the desired height in centimetres.
%

if ~strcmp(get(f,'Type'),'figure')
    error('Invalid handle - expected a figure.');
end;

%set(f, 'PaperPositionMode', 'auto'); % 'auto' or 'manual'.  Auto makes the printing use the same size as shown on the screen.
set(f, 'PaperPositionMode', 'manual'); % 'auto' or 'manual'.  Auto makes the printing use the same size as shown on the screen.

old_units = get(f,'Units');
set(f, 'Units', 'centimeters');
p = get(f,'Position');
original_width  = p(3);
original_height = p(4);
p(2) = p(2) - (height - original_height);  % Keep the top edge at the same position, by moving the bottom edge up or down.
p(3) = width;
p(4) = height;
set(f,'Position', p);
set(f,'Units', old_units);

old_paper_units = get(f,'PaperUnits');
set(f, 'PaperUnits', 'centimeters');
p = get(f,'PaperPosition');
p(3) = width;
p(4) = height;
set(f, 'PaperPosition', p);
set(f, 'PaperUnits', old_paper_units);


return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


