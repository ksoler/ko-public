function test_viterbi_fec01(u)
% test_viterbi_fec01(utility01_class)
% Tests the function viterbi_fec01.
%
% Inputs:
% u is a vestigial utility01_class object.
%

fprintf(1, 'Starting %s.\n', whoAmI(u, 'my name'));


%test01; % Basic test of viterbi decoder.
test02; % BER test.
fprintf(1, 'Done %s.\n', whoAmI(u, 'my name'));
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function test01
u = utility01_class;
fprintf(1, 'Starting %s.\n', whoAmI(u, 'my name'));

tx_bits = [ 1 0 1 0 ];
encoder = 1;
history_length = 1;
num_bits = length(tx_bits);
outputs = '';
symbol_alphabet = [+1 -1];
test_bits=  tx_bits;
constraint_bits = tx_bits;
ebno = 10*dB;

[y, L]  = convolutionalEncoder(u, ...
  'data bits', tx_bits, ...
  'pseudo-octal encoder', encoder, ...
  'outputs', 'code symbols, constraint length', ...
  'add flush bits', 1, ...
  'symbol alphabet', symbol_alphabet);

[rx_bits, bit_metrics, num_corrected] = viterbi_fec01(u, ...
  'constraint bits', [],                                 ...
  'encoder',         encoder,                            ...
  'history length',  history_length,                     ...
  'num bits',        num_bits,                           ...
  'outputs',         'detected bits, bit metrics, num corrected symbols', ...
  'symbol alphabet', symbol_alphabet,                    ...
  'test bits',       test_bits,                          ...
  'y',               y);


n = sum(tx_bits(:)~=rx_bits(:));
fprintf( 1, '%d bit errrors\n', n);

%display(rx_bits);
%display(bit_metrics);
fprintf( 1, 'bit metrics = ');
fprintf( 1, '%g ', bit_metrics );
fprintf( 1, '\n' );
%display(num_corrected);

fprintf(1, 'Done %s.\n', whoAmI(u, 'my name'));
return;
% End of test01.

%---------------------------------------------------------------------------------------------------
% test02
%---------------------------------------------------------------------------------------------------
% BER test.
function test02
%
u = utility01_class;
dB = 1;
fprintf(1, 'Starting %s.\n', whoAmI(u, 'my name'));

% Configuration.
bits_per_transmission = 100;
octal_encoder         = [ 15 17 ];
history_length        = 32;
symbol_alphabet       = [-1 +1];
ebno_list             = [ 0 : 0.5: 10 ];
max_num_bits          = 1e6;
max_num_errors        = 2e2;
use_random_bits       = 0;
tx_bits               = zeros(bits_per_transmission,1);
% Done configuration.

% Initialize.
ber_list = zeros(size(ebno_list));


% Done initializing.

t1 = clock;
for j = 1 : length(ebno_list)
  num_bits = 0;
  num_errors = 0;
  done = 0;
  if j>1 && ber_list(j-1)==0
    done = 1;  % Once the BER has dropped to zero, skip subsequent simulations.  Assumes that ebno is increasing, and BER decreases with ebno.
  end;
  while ~done
    if use_random_bits
      tx_bits = double(rand(bits_per_transmission, 1) < 0.5);
    else
      tx_bits = tx_bits;
      bits_per_transmission = length(tx_bits);
    end;
    
    % Encode.
    [ tx_symbols, L]  = convolutionalEncoder(u, ...
      'data bits', tx_bits, ...
      'pseudo-octal encoder', octal_encoder, ...
      'outputs', 'code symbols, constraint length', ...
      'add flush bits', 1, ...
      'symbol alphabet', symbol_alphabet);
    
    % Add noise.
    x = signal2_class('data', tx_symbols);
    v = gaussianNoise(x,...
      'snr', ebno_list(j), ...
      'num bits', bits_per_transmission, ... % Count flush bits as part of transmission energy.
      'complex', 0);
    y = x + v;
    
    [rx_bits, bit_metrics, num_corrected] = viterbi_fec01(u, ...
      'constraint bits', [],                                 ...
      'encoder',         octal_encoder,                      ...
      'history length',  history_length,                     ...
      'num bits',        bits_per_transmission,              ...
      'outputs',         'detected bits, bit metrics, num corrected symbols', ...
      'symbol alphabet', symbol_alphabet,                    ...
      'test bits',       tx_bits,                            ...
      'y',               y.data);
    
    current_num_errors = sum(rx_bits(:) ~= tx_bits(:));
    % Future improvement:  Plot histogram of metrics of wrong, and right, bits.
    
    num_errors = num_errors + current_num_errors;
    num_bits=  num_bits + bits_per_transmission;
    if num_errors >= max_num_errors || num_bits >= max_num_bits
      done = 1;
    end;
    
    if etime(clock, t1)>10
      % Progress report.
      fprintf( 1, 'ebno=%5.1f, %.0fK bits, %2d errors.\n', ebno_list(j), num_bits/1000, num_errors);
      pause(0.1);
      t1 = clock;
    end;
    
  %if num_errors>0, keyboard; end;
  %keyboard;
  
  end; % while ~done
  
  if num_errors==0 && num_bits==0
    ber_list(j)=0;
  else
    ber_list(j) = num_errors / num_bits;
  end;
  
  %fprintf(1, 'num_errors = %d, num_bits = %d\n', num_errors, num_bits);
  
end; % for j = 1 : length(ebno_list)

figure(1);
clf;
semilogy(ebno_list, ber_list, 'bo-');
xlabel('ebno (dB)');
ylabel('BER');
title('viterbi decoder BER test');
grid on;

fprintf(1, 'Done %s.\n', whoAmI(u, 'my name'));
return;
% End of test02.


%---------------------------------------------------------------------------------------------------
% test03
%---------------------------------------------------------------------------------------------------
% Examine confidence metrics.
function test03
%
u = utility01_class;
dB = 1;
fprintf(1, 'Starting %s.\n', whoAmI(u, 'my name'));

% Configuration.
bits_per_transmission = 20;
octal_encoder         = [ 15 17 ];
history_length        = 32;
symbol_alphabet       = [-1 +1];
ebno                  = [ 0 : 0.5: 10 ];
symbol_errors         = [ 1 5 ];  % Matlab indices of symbol errors.
use_random_bits       = 0;
% Done configuration.

% Initialize.
if use_random_bits
  tx_bits = double(rand(bits_per_transmission,1)<0.5);
end;

% Encode.
[ tx_symbols, L]  = convolutionalEncoder(u, ...
  'data bits', tx_bits, ...
  'pseudo-octal encoder', octal_encoder, ...
  'outputs', 'code symbols, constraint length', ...
  'add flush bits', 1, ...
  'symbol alphabet', symbol_alphabet);
    
    % Add noise.
    x = signal2_class('data', tx_symbols);
    v = gaussianNoise(x,...
      'snr', ebno, ...
      'num bits', bits_per_transmission, ... % Count flush bits as part of transmission energy.
      'complex', 0);
    y = x + v;
    
    [rx_bits, bit_metrics, num_corrected] = viterbi_fec01(u, ...
      'constraint bits', [],                                 ...
      'encoder',         octal_encoder,                      ...
      'history length',  history_length,                     ...
      'num bits',        bits_per_transmission,              ...
      'outputs',         'detected bits, bit metrics, num corrected symbols', ...
      'symbol alphabet', symbol_alphabet,                    ...
      'test bits',       tx_bits,                            ...
      'y',               y.data);
    
    current_num_errors = sum(rx_bits(:) ~= tx_bits(:));
    % Future improvement:  Plot histogram of metrics of wrong, and right, bits.
    
    num_errors = num_errors + current_num_errors;
    num_bits=  num_bits + bits_per_transmission;
    if num_errors >= max_num_errors || num_bits >= max_num_bits
      done = 1;
    end;
    
    if etime(clock, t1)>10
      % Progress report.
      fprintf( 1, 'ebno=%5.1f, %.0fK bits, %2d errors.\n', ebno_list(j), num_bits/1000, num_errors);
      pause(0.1);
      t1 = clock;
    end;
    
  %if num_errors>0, keyboard; end;
  %keyboard;
  
  end; % while ~done
  
  if num_errors==0 && num_bits==0
    ber_list(j)=0;
  else
    ber_list(j) = num_errors / num_bits;
  end;
  
  %fprintf(1, 'num_errors = %d, num_bits = %d\n', num_errors, num_bits);
  
end; % for j = 1 : length(ebno_list)

figure(1);
clf;
semilogy(ebno_list, ber_list, 'bo-');
xlabel('ebno (dB)');
ylabel('BER');
title('viterbi decoder BER test');
grid on;



fprintf(1, 'Done %s.\n', whoAmI(u, 'my name'));
return;
% End of test03.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


