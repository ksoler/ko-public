function y = checkEqualVectors(u,x,y)
% y = checkEqualVectors(u,x,y)
% Checks for equal vectors.
%
% Inputs:
% u is an utility01_class object.
%
% x is a 1D complex array.
% y is a 1D complex array.
%
% Outputs:
% y is 1 if the test passed, 0 if it failed.
% If this output argument is omitted and the test fails, 
% then an error message is printed.
%

ok = 1;
msg = 'No failure information is available.';

if ok
  if ~checkVectorComplex(u,x)
   ok = 0;
   msg = sprintf('First argument is not a complex vector, it is a %s.', class(x));
  end;
end;

if ok
  if ~checkVectorComplex(u,y)
   ok = 0;
   msg = sprintf('Second argument is not a complex vector, it is a %s.', class(y));
  end;
end;

if ok
  if numel(x) ~= numel(y)
   ok = 0;
   msg = sprintf('Vector lenghts are not equal, they are %d and %d, difference is %d.\n', numel(x), numel(y), numel(y)-numel(x));
  end;
end;

if ok
  if any(x(:)~=y(:))
    ok = 0;
    i = find(x(:) ~= y(:));
    msg = sprintf('Vectors disagree in %d elements, from x(%d)=%d+j%d, y(%d)=%d+j%d to x(%d)=%d+j%d, y(%d)=%d+j%d.\n', ...
      length(i), i(1), real(x(i(1))), imag(x(i(1))), i(1), real(y(i(1))), imag(y(i(1))), i(end), real(x(i(end))), imag(x(i(end))), i(end), real(y(i(end))), imag(y(i(end))) );
  end;
end;

% Done checking.

if nargout == 0
    if ~ok
      fprintf(1, 'Test failed: %s\n', msg);
        error('Check for identical vectors failed.');
    end;
elseif nargout == 1
    y = ok;
else
    error('Invalid number of output arguments.');
 end;
return;
 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


