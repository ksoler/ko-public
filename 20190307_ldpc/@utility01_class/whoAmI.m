function y = whoAmI(u, option)
% y = whoAmI(u, option)
% Returns name or other information on caller(s).
%
% Inputs:
% u is a vestigial utility01_class object.
%
% option is a string which selects the output. Unless otherwise specified,
% the output is a string.
%
% 'my name' is the name of the function that invoked whoAmI.
%
% 'my dir' is the directory of the function that invoked whoAmI.
%
% 'my parent dir' is the parent directory of the function that invoked whoAmI.
%
% 'caller name'  is the name of the caller of the function which invoked
% whoAmI.
%
% 'all names' is a cell array of all of the function names in the call
% stack, beginning with the top-level function, and ending with the
% function which invoked whoAmI.
%
% 'my line' returns a number which is the line number where whoAmI was
% invoked.
%
% 'caller line' returns the line number from the caller.
%
% 'all lines' returns a numeric array of the line numbers in the call
% stack.
%
% 'my file' is the filename (no path) of the function which called whoAmI.
%
% 'caller file' is the filename of the caller.
%
% 'all files' is a cell array of the filenames from the call stack.
%

if nargin < 2
  option = 'my name';
end;

checkString(u, option);
st = dbstack;

switch option
  case 'my name'
    y = st(2).name;
  case 'my line'
    y = st(2).line;
  case 'my file'
    s = st(2).file;
    [pathstr, name, ext] = fileparts(s);
    y = [ name ext];
  case 'my dir'
    s = st(2).file;
    [pathstr, name, ext] = fileparts(s);
    y = pathstr;
    error('I think this does not work.');
  case 'my parent dir'
    s = st(2).file;
    [pathstr, name, ext] = fileparts(s);
    y = pathstr;
    y = fullfile(pathstr, '\..\');
    error('I think this does not work.');
  case 'caller name'
    y = st(3).name;
  case 'caller line'
    y = st(3).line;
  case 'caller file'
    s = st(3).file;
    [pathstr, name, ext] = fileparts(s);
    y = [ name ext];
  case 'all names'
    n = length(st)-1;
    y = cell(n,1);
    for k = 1 : n
      y{n} = st(k+1).name;
    end;
  case 'all lines'
    n = length(st)-1;
    y = zeros(n,1);
    for k = 1 : n
      y(n) = st(k+1).line;
    end;
  case 'all files'
    n = length(st)-1;
    y = cell(n,1);
    for k = 1 : n
      s = st(k+1).file;
      [pathstr, name, ext] = fileparts(s);
      y{k} = [ name ext];
    end;
  otherwise
    error('Invalid option.');
end;
return;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


