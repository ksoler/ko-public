function [ formatChunk, dataChunk, allChunk, ok, msg] = wav_read_chunk_info2(u, file)
% [ formatChunk, dataChunk, allChunk] = wav_read_chunk_info2(u, file)
% Identifies chunks in a WAVE file.
%
% Inputs:
% u is a vestigial utility01_class object.
%
% file is a filename or file handle.  If it is a file handle, then the file is not closed at the end
% of this function.  If this argument is omitted then the user is prompted to select a file.
%
% Outputs:
% formatChunk is a structure array, with one element for each format chunk in the file, with the
% following fields:
%   start: Byte offset in file of the first byte in the format chunk, after the 'fmt ' string and
%   chunk size.
%   size:  Size in bytes of the format chunk.
%
% dataChunk is a structure array, with one element for each data chunk in the file, with the
% following fields:
%   start: Byte offset of the frist byte of the data chunk, after the 'data' string and chunk size.
%   size:  Size in bytes of the data chunk.
%
% allChunk is a structure array, with one element for each chunk in the file, with the following
% fields:
%   start: Byte offset of the first byte of the data chunk, after the 'data' string and chunk size.
%   size:  Size in bytes of the data chunk.
%   string: 4-character string which identifies the type of chunk.
%
% ok is 1 if no problems were encountered, 0 otherwise.
% msg is a string which describes the problem.
%
% If this function is invoked with no outputs, then it prints out a report of the chunks in the
% file.
%

msg = 'No problem found.'; 
ok  = 1;  % Change to 1 if we reach the end of main loop successfully.


% Prepare empty structures.
formatChunk = struct('start', 0, 'size', 0);
dataChunk = struct('start', 0, 'size', 0);
allChunk = struct('start', 0, 'size', 0, 'string', 0);
formatChunk = formatChunk([]);
dataChunk = dataChunk([]);
allChunk = allChunk([]);
% Done preparing empty structures.

% The main body of code is enclosed in a while loop to make it easy to jump to the end with a break
% statement.  This loop will never execute more than once.


h = fileOpen(u, file, 'rb');
riff_type = read_string_from_file(h, [], 4);
if ~strcmp(riff_type, 'RIFF')
  msg = 'Did not found RIFF string.';
  ok = 0;
end;

if ok
  riff_size = read_uint32_from_file(h, [], 1); % We don't use this.
end;

if ok
  wav_type = read_string_from_file(h, [], 4);
  if ~strcmp(wav_type, 'WAVE')
    ok = 0;
    msg = 'Did not find WAVE type where expected.';
  end;
end;

while (ok)
  % Parse chunks in the file.
  chunk_type = read_string_from_file(h, [], 4);
  if feof(h), break, end;
 

  allChunk(end+1).string = chunk_type;
  allChunk(end).size = read_uint32_from_file(h, [], 1);
  allChunk(end).start = ftell(h);
  fileSeek(u, h, allChunk(end).size, 'cof');
end; % while(1)

fclose(h);

if ok
  for k = 1 : length(allChunk)
    if strcmp(allChunk(k).string, 'fmt ')
      formatChunk(end+1).start = allChunk(k).start;
      formatChunk(end).size    = allChunk(k).size;
    elseif strcmp(allChunk(k).string, 'data')
      dataChunk(end+1).start = allChunk(k).start;
      dataChunk(end).size    = allChunk(k).size;
    % Else do nothing.
    end;
  end;
end;

if nargout==0
  if ok
    fprintf(1, 'WAVE file was parsed successfully.\n');
    fprintf(1, 'riff ID:    "%s"\n', riff_type);
    fprintf(1, 'riff size:  %g\n', riff_size);
    fprintf(1, 'wave ID:    "%s"\n', wav_type);
    fprintf(1, 'number of chunks: %d\n', length(allChunk));
    for k =1 : length(allChunk)
      fprintf(1, 'Chunk %d type %s start %d size %d\n', allChunk(k).string, allChunk(k).start, allChunk(k).size);
     end;
  else
    fprintf(1, 'WAVE file was not parsed successfully.\n');
    fprintf(1, '%s\n', msg);
  end;
end;
return;
%---------------------------------------------------------------------------------------------------
function y = read_string_from_file(h, start, len)
% If start is empty, then just read from current position.
if ~isempty(start)
  fileSeek(u, h, start, 'bof');
end;
y = fread(h, len, 'uint8');
y = char(y);
y = y(:).';
return;

function y = read_uint32_from_file(h, start, len)
% If start is empty, then just read from current position.
if ~isempty(start)
  fileSeek(u, h, start, 'bof');
end;
if nargin < 3
  len = 1;
end;

% This works as long as endian-ness is compatible.
y = fread(h, len, 'uint32');
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


