function data_start = wav_create(u, varargin) % filename, sampleRate, numChannels, bitsPerSample, numSamples)
% data_start = wav_create(u, ...)
% Creates a cannonical PCM WAV file with header, and zeroed data.
%
% Inputs: bits per sample, file, num channels, num samples sample rate
%
%---------------------------------------------------------------------------------------------------
% Detailed Description of Inputs:
%
% u is a vestigial utility01_class object.  The rest of the inputs are tag/value pairs.
% 
% 'bits per sample' is 8 or 16.
%
% 'file' is the filename to create.
%
% 'num channels' use 1 for mono, 2 for stereo, or more if you want.
%
% 'num samples' is the duration of the file, measured in samples.  Each channel will have this many
% samples.
%
% 'sample rate' is the sample rate in Hertz.
% 
% 'verbose' is 0 or 1.  It enables diagnostic output.
%---------------------------------------------------------------------------------------------------
% Detailed Description of Outputs
%
% data_start is the offset of the first data sample, in bytes, in the WAV file.
%---------------------------------------------------------------------------------------------------



% address content
% 0       'RIFF'
% 4       (size of file in bytes) - 8
% 8       'WAVE'
%  

% Inputs: bits per sample, file, num channels, num samples sample rate

verbose = 0;

getArg(u,...
  'bits per sample -> bitsPerSample', 16,     ...
  'file            -> fileName',              ...
  'num channels    -> numChannels',   1,      ...
  'num samples     -> numSamples',    0,      ...
  'sample rate     -> sampleRate'      );
  

h  = fileOpen(u, fileName, 'wb');

%%%%%%%%%%%%%%%%
% header chunk %
%%%%%%%%%%%%%%%%

% Write the 'RIFF' string.
if verbose, fprintf(1, 'At byte %d of the file.\n', ftell(h)); end;
fwrite(h, 'RIFF', 'char');
if verbose, fprintf(1, 'Wrote RIFF string.\n'); end;

% Write the chunk size.
if verbose, fprintf(1, 'At byte %d of the file.\n', ftell(h)); end;
position_of_riff_size = ftell(h); % This is 8 less than the file size in bytes.
fwrite(h, 0, 'uint32');  % We will fill in this value later.
if verbose, fprintf(1, 'Wrote placeholder for RIFF size.\n'); end;

% Write the 'WAVE' string.
if verbose, fprintf(1, 'At byte %d of the file.\n', ftell(h)); end;
fwrite(h, 'WAVE', 'char');
if verbose, fprintf(1, 'Wrote WAVE string.\n'); end;

%%%%%%%%%%%%%%%%
% format chunk %
%%%%%%%%%%%%%%%%

% Write the 'fmt ' string.
if verbose, fprintf(1, 'At byte %d of the file.\n', ftell(h)); end;
fwrite(h, 'fmt ', 'char');
if verbose, fprintf(1, 'Wrote fmt string.\n'); end;

% Write the chunk size (updated later).
if verbose, fprintf(1, 'At byte %d of the file.\n', ftell(h)); end;
position_of_format_chunk_size = ftell(h);
fwrite(h, 0, 'uint32');
if verbose, fprintf(1, 'Wrote placeholder for format chunk size.\n'); end;

% Write audio format.
if verbose, fprintf(1, 'We measure the format chunk size starting at byte %d.\n', ftell(h)); end;
measure_format_chunk_size_from_here = ftell(h);
fwrite(h, 1, 'int16'); % Format 1 for PCM.
if verbose, fprintf(1, 'Wrote audio format 1 (PCM).\n'); end;

% Write the number of channels.
if verbose, fprintf(1, 'At byte %d of the file.\n', ftell(h)); end;
fwrite(h, numChannels, 'uint16');
if verbose, fprintf(1, 'Wrote numChannels = %d.\n', numChannels); end;

% Write the sample rate.
if verbose, fprintf(1, 'At byte %d of the file.\n', ftell(h)); end;
fwrite(h, sampleRate, 'uint32');
if verbose, fprintf(1, 'Wrote sampleRate = %d.\n', sampleRate); end;

% Write the byte rate.
if verbose, fprintf(1, 'At byte %d of the file.\n', ftell(h)); end;
byteRate = sampleRate * numChannels * bitsPerSample / 8;
fwrite(h, byteRate, 'uint32');
if verbose, fprintf(1, 'Wrote byteRate = %d.\n', byteRate); end;

% Write blockAlign.
% This is the number of bytes per sample period.
if verbose, fprintf(1, 'At byte %d of the file.\n', ftell(h)); end;
blockAlign = numChannels * bitsPerSample / 8;
fwrite(h, blockAlign, 'uint16');
if verbose, fprintf(1, 'Wrote blockAlign = %d.\n', blockAlign); end;

% Write bits per sample.
if verbose, fprintf(1, 'At byte %d of the file.\n', ftell(h)); end;
fwrite(h, bitsPerSample, 'uint16');
if verbose, fprintf(1, 'Wrote bitsPerSample = %d.\n', bitsPerSample); end;

% Calculate size of format chunk.
end_of_format_chunk = ftell(h);
format_chunk_size = end_of_format_chunk - measure_format_chunk_size_from_here;
if verbose, fprintf(1, 'Format chunk size = %d.\n', format_chunk_size); end;

fileSeek(u, h, position_of_format_chunk_size, 'bof');
if verbose, fprintf(1, 'At byte %d of the file.\n', ftell(h)); end;
fwrite(h, format_chunk_size, 'uint32');
if verbose, fprintf(1, 'Wrote format chunk size.\n'); end;
fileSeek(u, h, end_of_format_chunk, 'bof');

%%%%%%%%%%%%%%
% data chunk %
%%%%%%%%%%%%%%
if verbose, fprintf(1, 'At byte %d of the file.\n', ftell(h)); end;
fwrite(h, 'data', 'char');
if verbose, fprintf(1, 'Wrote data string.\n'); end;

if verbose, fprintf(1, 'At byte %d of the file.\n', ftell(h)); end;
data_chunk_size = blockAlign * numSamples;
fwrite(h, data_chunk_size, 'uint32');
if verbose, fprintf(1, 'Wrote data chunk size = %d bytes.\n', data_chunk_size); end;

data_start = ftell(h);
if verbose, fprintf(1, 'Data samples start at byte %d of the file.\n', ftell(h)); end;

% Now, we need to write dummy data into the file.  We will write in blocks of a fixed size, except
% for possibly a partial block at the end.


full_block_size = 1e4;
num_full_blocks = floor(data_chunk_size / full_block_size);
num_leftover_bytes = data_chunk_size - num_full_blocks * full_block_size;

dummy_data = zeros(full_block_size, 1);
for k = 1 : num_full_blocks
  fwrite(h, dummy_data, 'char');
end;
if num_leftover_bytes > 0
  dummy_data = zeros(num_leftover_bytes,1);
  fwrite(h, dummy_data, 'char');
end;
file_size_in_bytes = ftell(h);

% Return to write the RIFF size, near the start of the file.
fileSeek(u, h, position_of_riff_size, 'bof');
fwrite(h, file_size_in_bytes - 8, 'uint32');

fclose(h);
return;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


