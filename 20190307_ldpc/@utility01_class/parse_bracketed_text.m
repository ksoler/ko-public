function varargout = parse_bracketed_text(u, varargin)
% [outputs...] = parse_bracketed_text(inputs...)
% Parses text organized into hierarchical elements with curly brackets.
%
% Inputs: lines, outputs
% Outputs: statements, levels, line numbers, ok
% 
%---------------------------------------------------------------------------------------------------
% Detailed Description of Inputs
% The inputs are tag/value pairs.
%
% 'lines' is a 1D cell array of strings.  Each string is one line of text.
% 'outputs' is a string which identifies the requested output arguments, separated by commas.
%
%---------------------------------------------------------------------------------------------------
% Detailed Description of Outputs
%
% 'statements' is a cell array of strings.  Each string contains the text from one statement.  A
% statement ends with a curly bracket (opening or closing).  An opening bracket increases the level
% number for the next statement, and a closing bracket decreases the level number of the next
% statement.
%
% 'levels' is a 1D numeric array of the level numbers for each statement.  Text which is outside of 
% any curly braces is level 0.  Text inside one level of braces is level 1.
%
% 'line numbers' is a 1D numeric array.  Each element is the line number of the first line of the
% corresponding statement.  Thus, if one statement spans multiple lines, only the first line is
% reported.
%
% 'ok' is 1 if no errors were enountered in parsing, 0 otherwise.  I think that the only error that
% can happen is if the braces do not match up.  In that case, statements can still be extracted, but
% they might not have the level intended by the user.
%
%---------------------------------------------------------------------------------------------------
% Future Improvements
% Keep track of the line and column number of each character, for improved  error reporting later
% on. Also, we can use that to report instances of opening and closing braces with different
% indentation.
%
% Allow user to specify if opening and closing braces must be individual tokens (surrounded by 
% whitespace) to be recognized, or if they can touch other characters.
%
% Allow user to disallow statements between a closing and opening brace, like "dog":
% cat
%  { cat1 }
% dog
%  { dog1 }
%
%---------------------------------------------------------------------------------------------------

getArg( u, ...
  'lines->line_arg', ...
  'outputs->output_arg' );

% First, scan through the text and count all of the opening and closing  braces.
num_braces = 0;
for k = 1 : length(line_arg)
  num_braces = num_braces + sum(line_arg{k}=='{');
  num_braces = num_braces + sum(line_arg{k}=='}');
end;
% Now, num_braces is the number of opening and closing braces.  The number of statements is one more
% than this.  Note that some of the statements are likely "empty", such as whitespace between
% braces.  We'll eliminate them later.
num_statements = num_braces+1;


% First, scan through the text and identify all opening and closing braces.

% Parse text, one line at a time.
line_numbers = zeros(num_statements, 1);
statements  =  cell(num_statements, 1);
levels      = zeros(num_statements, 1);

statement_number = 1; % Current statement is statements{statement_number}.
line_number = 1; % Current line is line_arg{line_number}.
char_number = 0; % The next character to parse is line_arg{line_number}(char_number).
current_level  =0;

done = 0;
line_numbers(statement_number) = line_number;
while ~done
  % Have we run out of lines to parse?
  if line_number > length(line_arg)
    done = 1;
    continue;
  end;
  
  % Have we run out of characters to parse?
  char_number = char_number + 1;
  if char_number > length(line_arg{line_number})
    line_number = line_number + 1;
    char_number = 0;
    continue; % Return to top of loop.
  end;
  next_char = line_arg{line_number}(char_number);
  if next_char == '{'
    levels(statement_number) = current_level;
    current_level = current_level + 1;
    statement_number = statement_number + 1;
    line_numbers(statement_number) = line_number;
    continue;
  end;
  if next_char == '}'
    levels(statement_number) = current_level;
    current_level = current_level - 1;
    statement_number = statement_number + 1;
    line_numbers(statement_number) = line_number;
    continue;
  end;
  
  statements{statement_number}(end+1) = next_char;
end;

if current_level==0
  ok = 1;
else
  ok = 0;
end;

for k = 1 : length(statements)
  if ~isempty(statements{k}) % Don't try to strtrim an empty array, [].
    statements{k} = strtrim(statements{k});
  end;
end;

% Remove all of the empty statements.  In the future, we might want to retain empty statements which
% meet certain conditions.  This implementation is not efficient.
for k = length(statements) : -1 : 1
  if isempty(statements{k}) || isempty(strtrim(statements{k}))
    statements(k) = [];  
    levels(k) = [];
    line_numbers(k) = [];
  end;
end;


  


  
% Inputs: lines, outputs
% Outputs: statements, levels, line numbers, ok

% Assign outputs.
outputs = split(u, output_arg, ',', 'trim');
outputs = removeEmptyCells(u, outputs);
n = length(outputs);
assertion(u, nargout==n);
varargout = cell(n,1);
for k = 1 : n
  switch outputs{k}
    case 'statements'
      varargout{k} = statements;
    case 'levels'
      varargout{k} = levels;      
    case 'ok'
      varargout{k} = ok;
    case 'line numbers'
      varargout{k} = line_numbers;
    otherwise
      error('Invalid output.');
  end;
end;

return;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


