function y = printTable2(u, varargin)
% Test code:
if 0
  u = utility01_class;
  data = [ 1 2 3; 4 5 6; 7 8 9];
  top = {'col1' 'col2' 'col3' };
  left = {'row1' 'row2' 'row3'};
  printTable2(u, 'data', data, 'top', top, 'left', left );
end;
% y = printTable2(u, varargin)
% Prints data as a text table.  For each column, the text is padded to uniform width.
% 
% Inputs: data, top, left, corner
%         data format, top format, left format, corner format
%         data alignment, top alignment, left alignment, corner alignment
%
%---------------------------------------------------------------------------------------------------
% Detailed description of inputs
%---------------------------------------------------------------------------------------------------
% 'data' is a Matlab numeric array, or a 1D cell array of a number of Matlab arrays  which will be
% concatenated.
%
% 'top' is a matlab numeric or cell array used as column headers.
%
% 'left' is a matlab numeric or cell array used as row labels on the left side of the data.
%
% 'corner' is a number or string for the top left corner, at the intersection of the row
% and column labels.
%
% 'data format'
% 'top format'
% 'left format'
% 'corner format'
% These are is a printf-style format strings which are applied to numeric data in the 
% corresponding region of the table (data, top, left, corner).  In the future, the 
% data format might have the option of being a cell array of strings, one for each column 
% of data.
%
% 'data alignment'
% 'top alignment'
% 'left alignment'
% 'corner alignment'
% These are strings which specify the alignment of the corresponding region of the table (data,
% top, left, corner).  They can be 'left', 'right', 'center', or maybe even 'auto'.  In the future,
% the data alignment might have the option of being a cell array of strings, one for each column of 
% data.
%---------------------------------------------------------------------------------------------------
% Detailed description of outputs
%---------------------------------------------------------------------------------------------------
% y is a 1D cell array.  Each cell holds the text of one line of the table.  If this function is 
% invoked without an output argument, the table is printed out.  
%
% Future Improvements:  Support complex numeric data.  If top or left arguments are a single string
% with one or more commas, then split into separte cells at the commas.
%

% Parse input arguments.
getArg( u, ...
  'data            ->data_arg            ', {''},       ...
  'top             ->top_arg             ', {''},       ...
  'left            ->left_arg            ', {''},       ...
  'corner          ->corner_arg          ', {''},       ...
  'data format     ->data_format_arg     ', {'%g'    }, ...
  'top format      ->top_format_arg      ', {'%g'    }, ...
  'left format     ->left_format_arg     ', {'%g'    }, ...
  'corner format   ->corner_format_arg   ', {'%g'    }, ...
  'data alignment  ->data_alignment_arg  ', {'right' }, ...
  'top alignment   ->top_alignment_arg   ', {'center'}, ...
  'left alignment  ->left_alignment_arg  ', {'left'  }, ...
  'corner alignment->corner_alignment_arg', {'left'  });
% Done parsing input arguments.

data = data_arg;
top =  top_arg;
left = left_arg;
corner = corner_arg;

% Convert strings to cell arrays.
if checkString(u, data),   data   = { data   }; end;
if checkString(u, top),    top    = { top    }; end;
if checkString(u, left),   left   = { left   }; end;
if checkString(u, corner), corner = { corner }; end;

% The data might be a cell array of numeric arrays.  If so, convert them 
% to cell arrays, pad to the same number of rows, and concatenate them horizontally.
if isa(data, 'cell')
  data = horzcat_and_pad_cells(u, data{:});
end;

% Convert numeric arrays to cell arrays of numbers.
data   = convert_numeric_array_to_cell(data);
top    = convert_numeric_array_to_cell(top);
left   = convert_numeric_array_to_cell(left);
corner = convert_numeric_array_to_cell(corner);


% If the data has more than one column, and the top argument is a single string with at least one
% comma, then split it up at the commas.
if size(data, 2)>1 && numel(top)==1 && isa(top{1}, 'char') && ~isempty(strfind(top{1}, ','))
  top  = split(u, top{1}, ',', 'trim');
end;

% If the data has more than one row, and the left argument is a single string with at least one
% comma, then split it up at the commas.
if size(data, 1)>1 && numel(left)==1 && isa(left{1}, 'char') && ~isempty(strfind(left{1}, ','))
  left  = split(u, left{1}, ',', 'trim');
end;

% Convert numbers to strings.
data   = convert_numbers_to_strings(data,   data_format_arg  );
top    = convert_numbers_to_strings(top,    top_format_arg   );
left   = convert_numbers_to_strings(left,   left_format_arg  );
corner = convert_numbers_to_strings(corner, corner_format_arg);


% Put all of the cells together, and determine the width of each column.
% If some components do not have consistent sizes, (e.g. less column titles than data columns), 
% then we pad with empty cells or blank strings.
num_rows = max(1+length(left), 1+size(data,1));
num_cols = max(1+length(top),  1+size(data,2));
a = repmat({''}, num_rows, num_cols);
a(1,1) = corner;
a(1, 2:length(top)+1) = top(:).';
a(2:length(left)+1,1) = left(:);
a(2:size(data,1)+1, 2:size(data,2)+1) = data;

column_widths = zeros(1, num_cols);
for i = 1 : num_rows
  for j = 1 : num_cols
    % Use the length of the longest string in each column.
    column_widths(j) = max(column_widths(j), length(a{i,j}));
  end;
end;

% Now, split the table back up into separate regions and apply alignment and padding.
corner = a(1,1);
top = a(1, 2:end);
left = a(2:end,1);
data = a(2:end, 2:end);

corner = apply_alignment(corner, column_widths(1),     corner_alignment_arg);
left   = apply_alignment(left,   column_widths(1),     left_alignment_arg  );
top    = apply_alignment(top,    column_widths(2:end), top_alignment_arg   );
data   = apply_alignment(data,   column_widths(2:end), data_alignment_arg  );

% Now, put the entire array back together.
a = [ corner top; left data ];


% Next, combine the cells in each column into one string.  Columns are separated by one space.
b = repmat({''}, num_rows, 1);
for i = 1 : num_rows
  for j = 1 : num_cols
    b{i} = [ b{i} a{i,j} ];
    if j < num_cols
      b{i} = [ b{i} ' ' ];
    end;      
  end;
end;

% If this function was invoked without an output argument, then print out the table.
if nargout==0
  for i = 1 : num_rows
    fprintf(1, '%s\n', b{i});
  end;
else
  y = b;
end;
return;

 
%---------------------------------------------------------
% convert_numeric_array_to_cell
%---------------------------------------------------------
% Converts a numeric array to a cell array.  If it's arleady a cell array, 
% this function leaves it alone.  Assumes that 
function y = convert_numeric_array_to_cell(x)
if ndims(x) > 2
  error('Invalid input.');
end;

if isnumeric(x)
  y = cell(size(x));
  for i = 1 : size(x,1)
    for j = 1 : size(x,2)
      y{i,j} = x(i,j);
    end;
  end;
elseif isa(x, 'cell')
  % Do nothing, leave it alone.
  y = x;
else
  error('Invalid input.');
end;
return;
% End of convert_numeric_array_to_cell.

%---------------------------------------------------------
% convert_numbers_to_strings
%---------------------------------------------------------
% Converts numbers to strings using sprintf.  An empty array is converted into an empty string. 
% Future improvement:  Support complex numeric data.
function y = convert_numbers_to_strings(x, format)
u = utility01_class;
% for now, format is expected to be a single format string.  In the future, we might allow
% it to be a cell array of strings, one for each column of x.
for i = 1 :  size(x,1)
  for j = 1 : size(x,2)
    if isempty(x{i,j})
      x{i,j} = '';
    elseif checkScalarReal(u, x{i,j}) 
      x{i,j} = sprintf(format, x{i,j});
    end;
  end;
end;
y = x;
return;

%---------------------------------------------------------
% apply_alignment
%---------------------------------------------------------
% Pads strings to specified length, with specified alignment.
%
% Inputs:
% x is a cell array of strings.
% widths is an vector, with one number for each column of x.
% align is 'left', 'right', or 'center'.
%
% Outputs:
% y is a cell array of strings.  Each string is taken from the corresponding element of x, and
% padded to the length specified in 'widths', aligned accordeing to the align argument.
%
function y = apply_alignment(x, widths, align)
u = utility01_class;
assertion(u, isa(x, 'cell'));

num_rows = size(x,1);
num_cols = size(x,2);
for i = 1 : num_rows
  for j = 1 : num_cols
    n = widths(j) - length(x{i,j});
    if n<0, error('This should not happen, string is longer than specified width.'); end;
    n1 = floor(n/2);
    n2 = n-n1;
    switch align
      case 'left'
        x{i,j} = [ x{i,j} repmat(' ', 1, n) ];
      case 'right'
        x{i,j} = [ repmat(' ', 1, n)  x{i,j} ];
      case 'center'
        x{i,j} = [ repmat(' ', 1, n1)  x{i,j} repmat(' ', 1, n2) ];
      otherwise
        error('Invalid alignment.');
    end;
  end;
end;
y = x;
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


