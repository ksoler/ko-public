function [ y, sampleRate ] = wav_read(u, filename)
% [y, sampleRate] = wav_read(u, file)
% Reads data from a WAVE file.
%
% Inputs:
% u is a vestigial utility01_class object.
%
% file is a filename. If this argument is omitted then the user is prompted to select a file.
%
% Outputs:
% y is a BigMatrixReal_v3 object with the file data.  It has one column per channel, one row per sample.
%
% sampleRate is the sample rate in Hertz.
%
% Test:
% wav_read(utility01_class, 'F:\2019-matlab data\wavRead\listen to the man, short.wav')

if nargin<2
  % Get the user to select a file.
  persistent saved_file;
  persistent saved_dir;
  if isempty(saved_dir)
    saved_file = '*.wav';
    saved_dir = 'C:\';
  end;
  original_dir = pwd;
  cd(saved_dir);
  [file1, dir1] = uigetfile(saved_file, 'Select a WAV file.');
  cd(original_dir);
  if isequal(file1,-1)
    fprintf(1, 'No file selected.\n');
    return;
  end;
  filename = fullfile(dir1, file1);
  saved_file = file1;
  saved_dir = dir1;
  % Done getting the user to select a file.
  verbose_mode = 1;
else
  verbose_mode = 0;
end;

% Now, 'filename' is the filename, with full path.
[ formatChunk, dataChunk, allChunk, ok, msg ] = wav_read_chunk_info2(u, filename);

if isempty(formatChunk)
  error('Did not find a format chunk.');
end;

if isempty(dataChunk)
  error('Did not find a data chunk.');
end;


if length(formatChunk)>1
  fprintf(1, 'Ignoring %d extra format chunk(s).\n', length(formatChunk)-1);
  formatChunk = formatChunk(1);
end;

if length(dataChunk)>1
  fprintf(1, 'Ignoring %d extra data chunk(s).\n', length(dataChunk)-1);
  dataChunk = dataChunk(1);
end;

[ formatCode, numChannels, sampleRate, bitsPerSample] = wav_read_fmt(u, filename, formatChunk.start, formatChunk.size);


y = wav_read_data2(u, filename, dataChunk.start, dataChunk.size, numChannels, bitsPerSample);

return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


