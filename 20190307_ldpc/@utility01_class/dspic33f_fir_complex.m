function varargout = dspic33f_fir_complex(u, varargin)
% [outputs...] = dspic33f_fir_complex(u, inputs...)
% Emulates FIR filtering of dsPIC33F processor, for complex signals.
%
% Inputs:  data, filter, flush, outputs, rounding mode, saturation mode, bypass fixed point effects
% Outputs: data
%
% See the Microchip dspPIC33F Data Sheet at http://ww1.microchip.com/downloads/en/DeviceDoc/70165d.pdf
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Detailed Description of Inputs:
%
% u is a vestigial utility01_class object.  The remaining inputs are tag/value pairs, and the
% outputs are requested by the caleer.
%
% 'filter' is an array of complex filter taps.  If bypass mode is not used, their real and imaginary
% components must be in the range of 16-bit signed numbers.  Note that this function does not
% conjugate the filter taps before mulitpliying with the data (some implementations do).
%
% 'data' is an array of input complex signal data.  If bypass mode is not used, their real and 
% imaginary components must be in the range of 16-bit signed numbers.
%
% 'saturation mode' can be 'bit31', 'bit39', 'overflow', or 'none'.
%    bit31:    accumulator saturates at 32-bit limits
%    bit39:    accumulator saturates at 40-bit limits
%    overflow: accumulator overflows at 40-bit limits.
%    none:     accumulator uses Matlab's full precision
%
% 'bypass fixed point effects' is 0 or 1.
%     0: do input quantization, accumulator rounding and saturation, and output saturation.
%     1: bypass fixed point effects, but still scales like the dsPIC.
%
% 'flush' is 0 or 1.  0: Do not insert extra zeros at the end  The last output sample is computed
% with the filter taps full of the last input samples.  1: Insert extra zeros (one less than
% the number of filter taps) to flush out the input signal, so that the last output sample is
% computed with the last input sample in the last filter tap.
%
% 'outputs' is a string which specifies the requested output arguments, separated by commas.
%
% 'rounding mode' is 'conventional' or 'convergent'.
%    'conventional' adds b15 to b16 of each final (output) accumulator result.
%    'convergent'   adds b16 to b16, unless b0:b15=0x8000.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Detailed Description of Outputs:
%
% 'data' is the result of convolving the input data with the filter.
%
% In the future, there may be support for saturation flags.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Notes:
% This function saturates its outputs at 16-bit values (unless bypass mode is used).  If you want 
% different behaviour, you need to edit this function.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Future Improvements:
% Consider supporting a mode which performs filtering without quantization, saturation, or overflow.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

q = 1;

getArg(u, ...
    'data           ->data_arg   ',           ...
    'filter         ->filter_arg ',           ...
    'flush          ->flush_arg  ',           ...
    'outputs        ->outputs_arg', {''},     ...
    'rounding mode  ->round_arg  ',           ...
    'bypass fixed point effects->bypass_arg', ...
    'saturation mode->sat_arg    ' );

xi = real(data_arg  (:));
xq = imag(data_arg  (:));
hi = real(filter_arg(:));
hq = imag(filter_arg(:));

xihi = dspic33f_fir_real(         u,           ...
    'data',                       xi,          ...
    'filter',                     hi,          ...
    'flush',                      flush_arg,   ...
    'outputs',                   'data',       ...
    'rounding mode',              round_arg,   ...
    'saturation mode',            sat_arg,     ...
    'bypass fixed point effects', bypass_arg );

xihq = dspic33f_fir_real(         u,           ...
    'data',                       xi,          ...
    'filter',                     hq,          ...
    'flush',                      flush_arg,   ...
    'outputs',                   'data',       ...
    'rounding mode',              round_arg,   ...
    'saturation mode',            sat_arg,     ...
    'bypass fixed point effects', bypass_arg );

xqhi = dspic33f_fir_real(         u,           ...
    'data',                       xq,          ...
    'filter',                     hi,          ...
    'flush',                      flush_arg,   ...
    'outputs',                   'data',       ...
    'rounding mode',              round_arg,   ...
    'saturation mode',            sat_arg,     ...
    'bypass fixed point effects', bypass_arg );

xqhq = dspic33f_fir_real(         u,           ...
    'data',                       xq,          ...
    'filter',                     hq,          ...
    'flush',                      flush_arg,   ...
    'outputs',                   'data',       ...
    'rounding mode',              round_arg,   ...
    'saturation mode',            sat_arg,     ...
    'bypass fixed point effects', bypass_arg );

n = length(xihi);

yi = xihi - xqhq;  % These sums could result in overflow.
yq = xihq + xqhi;

% Handle the potential overflow.
if ~bypass_arg
    min_int16 = -2^15;
    max_int16 =  2^15-1;
    if any(yi < min_int16) || any(yi>max_int16) || any(yq<min_int16) || any(yq>max_int16)
        warning(u, 'Accumulator exceeded numeric precision limit.');  % Later on we'll want to be able to shut off this warning.
        min_int16_array = repmat(min_int16, n, 1);
        max_int16_array = repmat(max_int16, n, 1);
        yi = min(yi, max_int16_array);
        yi = max(yi, min_int16_array);
        yq = min(yq, max_int16_array);
        yq = max(yq, min_int16_array);
    end;
end;
% Done handling the potential overflow.

y = yi + sqrt(-1) * yq;

% Assign output arguments.
outputs = split(u, outputs_arg, ',', 'trim');
outputs = removeEmptyCells(u, outputs);
n = length(outputs);
assertion(u, n==nargout);
varargout = cell(n,1);
for k = 1 : n
    switch outputs{k}
        case 'data', varargout{k} = y;
        otherwise error('Invalid output argument.');
    end;
end;
% Done assigning output arguments.

return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


