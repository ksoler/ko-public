function [ok, msg] = check(u, x, varargin)
% [ok, msg] = check(u, x, constraints, ...)
% Checks a value against one or more constraints.
%
% Inputs:
% u is a utility01_class object.
%
% x is the value to be checked.
% constraints can be a string containing one or more constraints, separated by one or more spaces.
% It can also be a cell array of such strings.  Additional constraints can be included as additional arguments.
%
% Outputs:
% ok is 1 if the constraint is met by the value, 0 if it is not.
% msg is a string describing one or more of the failures discovered, if
% any.
%
% These are the constrints:
% (size)    scalar, vector, matrix, 1D, 2D, 3D
% (numeric) int, real, complex, binary, positive, negative, nonnegative, nonpositive
% (text)    string,  string-list
% (other)   cell, isa-, none
%
% Details:
% isa-foo requires isa('foo') to be true.
%
% All of the numeric tests disallow Inf and NaN.
%

%

u = utility01_class;

constraints = {};  % This will be a cell array of constraints, one constraint per cell.

% Put all of the constraints into one list.
for k = 1 : length(varargin)
    current_arg = varargin{k};
    if checkString(u, current_arg)
        current_arg = split(u,current_arg, ' ', 'trim');
        current_arg = removeEmptyCells(u, current_arg);
        constraints = [ constraints(:); current_arg(:) ];
    elseif checkStringList(u, current_arg)
        constraints = [ constraints(:); current_arg(:) ];
    end;
end;
% Done putting all constraints into one list.

ok = 1; % Set to 0 if we find a problem.
msg = 'No error information is available.';

for k = 1 : length(constraints)
    constraint = constraints{k};
    
    % Before the switch statement, we will check for pattern-based constraints.
    p = 'isa-';
    if length(constraint)>length(p) && strcmp(constraint(1:length(p)),p)
        
        % The constraint starts with 'isa-'.
        c = constraint(length(p)+1:end);
        if ~isa(x,c)
            ok = 0;
        end;
    else
        % Done checking for pattern-based constraints.
        
        
        switch constraint
            case 'none', % Do nothing.
                
                % size
            case 'scalar'
                if ~isequal(size(x), [1 1])
                    ok = 0;
                end;
                
            case 'vector'
                s = size(x);
                if numel(x)~=length(x) || length(s)>2
                    ok = 0;
                end;
            case 'matrix'
                if length(size(x))>2
                    ok = 0;
                end;
                
            case '1D'
                s = size(x);
                if numel(x)~=length(x) || length(s)>2
                    ok = 0;
                end;
                
            case '2D'
                s = size(x);
                if length(s)>2
                    ok = 0;
                end;
                
            case '3D'
                s = size(x);
                if  length(s)>3
                    ok = 0;
                end;
                
                % numeric
            case 'int'
                if ~isnumeric(x) || any(isnan(x(:))) || any(isinf(x(:))) || any(x(:)~=round(x(:))) || any(imag(x(:)))
                    ok = 0;
                end;
                
            case 'real'
                if ~isnumeric(x) || any(isnan(x(:))) || any(isinf(x(:))) || any(imag(x(:)))
                    ok = 0;
                end;
                
            case 'complex'
                if ~isnumeric(x) || any(isnan(x(:))) || any(isinf(x(:)))
                    ok = 0;
                end;
                
            case 'binary'
                if ~isnumeric(x) || any(isnan(x(:))) || any(isinf(x(:))) || any(x(:)~=round(x(:))) || any(imag(x(:))) || any(x(:)<0) || any(x(:)>1)
                    ok = 0;
                end;
                
            case 'positive'
                if ~isnumeric(x) || any(isnan(x(:))) || any(isinf(x(:))) || any(imag(x(:))) || any(x(:)<=0)
                    ok = 0;
                end;
                
            case 'nonpositive'
                if ~isnumeric(x) || any(isnan(x(:))) || any(isinf(x(:))) || any(imag(x(:))) || any(x(:)>0)
                    ok = 0;
                end;
                
            case 'negative'
                if ~isnumeric(x) || any(isnan(x(:))) || any(isinf(x(:))) || any(imag(x(:))) || any(x(:)>=0)
                    ok = 0;
                end;
                
            case 'nonnegative'
                if ~isnumeric(x) || any(isnan(x(:))) || any(isinf(x(:))) || any(imag(x(:))) || any(x(:)<0)
                    ok = 0;
                end;
                
            case 'string'
                if ~ischar(x(:)) || length(x)~=numel(x)
                    ok = 0;
                end;
                
            case 'string-list'  % 1D cell array of strings
                if ~isa(x,'cell') || length(x)~=numel(x)
                    ok = 0;
                end;
                
                if ok
                    for i = 1 : length(x)
                        if ~ischar(x{i}) || length(x{i}) ~=numel(x{i})
                            ok = 0;
                            break;
                        end;
                    end;
                end;
                
            case 'string-matrix' % 2D cell array of strings
                s = size(x);
                if ~isa(x,'cell') || length(s) > 2
                    ok = 0;
                end;
                
                if ok
                    for i = 1 : size(x,1)
                        for j = 1 : size(x,2)
                            if ~ischar(x{i,j}) || length(x{i,j}) ~=numel(x{i,j})
                                ok = 0;
                                break;
                            end;
                        end;
                    end;
                end;
                
                
            case 'cell'
                if ~isa(x,'cell')
                    ok = 0;
                end;
                
            otherwise
                fprintf( 1, 'Error: in function %s, encountered unsupported constraint "%s".\n', whoAmI(u, 'my name'), constraint);
                keyboard;
                error('Unsupported condition.');
        end;
    end;
    if ~ok
        msg = sprintf('Constraint for "%s" failed for this data: %s', constraint, printShort(u,x));
        break;
    end;
end;

if ~ok && nargout==0
    fprintf(1, 'Error: %s\n', msg);
    error('Constraint test failed.');
end;

if nargout==0
    clear ok;
    clear msg;
end;
return;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


