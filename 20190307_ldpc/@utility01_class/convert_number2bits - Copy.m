function bits = convert_number2bits(u, number, num_bits)
% bits = convert_number2bits(u, number, num_bits)
% Converts a series of bits to a numeric value.
%
% Inputs:
% u is a vestigial utility01_class obect.
%
% number is a non-negative integer
% num_bits is the number of bits to use in the result.
%
% Outputs:
% bits is a 1D array of bits (values 0,1).  The MSB is stored in bits(1).
%
% Future Improvements:  Allow caller to specify a datatype which indicates signed/unsigned behavior
% and wordlength.  Allow caller to specify whether the MSB comes first or last.
%

checkScalarInt(u, number);
assertion(u, number>=0);
bits = zeros(num_bits,1);
for k = 1 : num_bits
  kb = num_bits - k + 1;
  bits(kb) = mod(number,2);
  number = floor(number/2);
end;
if number ~=0
  warning(u, 'Number does not fit into specified number of bits.');
end;

return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2017, Kevin Oler
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in the
%       documentation and/or other materials provided with the distribution.
%     * The name of the Kevin Oler may not be used to endorse or promote products
%       derived from this software without specific prior written permission.
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
% 
