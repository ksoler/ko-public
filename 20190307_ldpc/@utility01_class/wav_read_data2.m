function y = wav_read_data2(u, file, start, len, numChannels, bitsPerSample)
% y = wav_read_data2(u, file, start, len, numChannels, bitsPerSample)
% Reads data chunk of a WAVE file, into a BigMatrixReal_v3 object.
%
% Inputs:
% u is a vestigial utility01_class object.
%
% file is a filename.
%
% start is the start of the data chunk (after the 'fmt ' string and chunk size).
%
% len is the nominal chunk size in bytes.
%
% numChannels
%
% bitsPerSample
%
% Outputs:
% y is a BigMatrixReal_v3 object.  It has one column for each channel, and one row for each sample.
%


msg = 'No problem found.'; 
ok  = 0;  % Change to 1 if we reach the end of main loop successfully.

h = fileOpen(u, file, 'rb');
fileSeek(u, h, start, 'bof');

switch bitsPerSample
case 8
  bytesPerSample = 1;
  fmt = 'uint8';
  scale = 2^7;
  offset = 128;
  lowerLimit = 0;
  uppwerLimit = 255;
case 16
  bytesPerSample = 2;
  fmt = 'int16';
  scale = 2^15;
  offset = 0;
  lowerLimit = -2^15;
  upperLimit =  2^15-1;
case 32
  bytesPerSample = 4;
  scale = 2^31;
  offset  =0;
  lowerLimit = -2^31;
  upperLimit =  2^31-1;
otherwise
error('Unsupported word size');
end;



num_samples = len / (bytesPerSample * numChannels);
if num_samples~= floor(num_samples)
  warning(u, 'Non-integer number of samples.');
  num_samples = floor(num_samples);
end;

y = BigMatrixReal_v3;

if 0
% This is the slow way.
resize(y, 0, numChannels);
t1 = clock;
for k = 1  :num_samples
   x = fread(h, numChannels, fmt);
   if feof(h)
     warning(u, 'Encountered EOF early.');
     break;
   end;
   appendRow(y, x);
   if etime(clock,t1)>10
     fprintf(1, 'Read %.0fK of %.0fK samples.\n', k/1e3, num_samples/1e3);
     pause(0.1);
     t1 = clock;
   end;
end;
else
  resize(y, num_samples, numChannels);
  if isOpen(y), close(y); end;
  h2 = fileOpen(u, y.filename, 'r+'); % Get our own file handle.
  
% This is the fast way.
  blockSize = 1e4;
  remaining_samples = num_samples; % Here, a "sample" is a row of data.
t1 = clock;
  while remaining_samples >= blockSize
     x = fread(h, numChannels*blockSize, fmt);
     x - x - offset;
     x = x / scale;
     fwrite(h2, x, 'double');
   remaining_samples = remaining_samples - blockSize;
   if etime(clock,t1)>10
     fprintf(1, 'Read %.0fK of %.0fK samples.\n', (num_samples-remaining_samples)/1e3, num_samples/1e3);
     pause(0.1);
     t1 = clock;
   end;
  end;


  if remaining_samples > 0
     x = fread(h, numChannels*remaining_samples, fmt);
     x = x - offset;
     x = x / scale;
     fwrite(h2, x, 'double');
  end;
end;

fclose(h);
fclose(h2);
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


