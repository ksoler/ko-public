function varargout = viterbi02(u, varargin)
% [outputs...] = viterbi02(u, ...)
% Viterbi decoder, minimal features and options.
% This implementation was started on 19 September 2014.  It is is based on
% viterbi_decode_no_frills.  This implementation introduces the followng
% changes: 
% no!   (1) Instead of storing path metrics and bits for all ticks, only store
% no!   the current and previous or next one.  That should increase execution
% no!   speed and decrease memory requirements.
%
%    (2) Instead of using custom classes for data structures, use matlab
%    arrays.  This decreases indexing convenience, but increases execution
%    speed.
%
% Inputs:
% u is a vestigial utility01_class object.  The remaining inputs are
% tag/value pairs.  The tags are:
%
% (configuration)  (operational)    (diagnostics)  (other)
% constraint bits  rx code symbols  test bits      outputs
% encoder                           plot
%
% The available outputs are:
%   detected bits
%   decision metrics
%   winning path metric
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Detailed Descriptions of Input Tags
%
% (configuration)
%
% 'constraint bits'
% 1D array of bit values which constrain decoder decisions.
%    0: force received bit to be 0
%    1: force received bit to be 1
%    2: choose received bit with best candidate metric
% The length of this array determines the number of bits to be processed.
%
% 'encoder'
% 1D array of numbers which define the encoder.  Each number represents
% encoder connections for one encoder path, when represented in base 2.
% For example, the number 13 has the binary representation 1101.  Data bits
% enter the encoder at the leftmost bit (MSB).  NOT PSEUDO-OCTAL!
%
% (operational)
%
% 'rx code symbols'
% Received code symbols.  Sign indicates the polatity of the bit (positive
% for bit 0, negative for bit 1, this), and magnitude indicates the confidence.
%
% (diagnostics)
%
% 'plot' enables some plots, but I haven't figured out what to do with it
% yet.
%
% 'test bits' is a 1D array of bits (value 0,1) which do not affect
% decoding metrics or results, but are compared against the decoding
% process.  If you know what the data bits should be, you can use them here
% in order to assess decoding performance.
%
% (other)
%
% 'outputs' is a string which specifies the requested output arguments,
% separated by commas.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Detailed Description of Outputs:
%
% 'detected bits'
% 1D array, bits from the winning path, values 0,1.
%
% 'decision metrics'
% Decision metric for each of the test bits (not the winning path!).
%
% 'winning path metric'
% Path metric of the winning path.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Future Improvements:
%
% Implement a separate function with more inputs, such as an option for
% flush bits, code alphabet.  Also with additional outputs, such as
% decision metrics for
% the winning bits, code symbol corrections, estimated ebno, etc.
%

% Notes on implemnetation:
% Since this implemnetation uses matlab arrays for its data structures, all
% indices need to start at 1.  In the algorithm design, some data
% structures have a starting index of 0, some of -1.  In this
% implemnetation, I will use an index offset of 2 for all arrays.  Thus,
% data structures which don't need the -1 index will have an extra row or
% column.

u = utility01_class;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Process input arguments.                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
getArg(u, ...
    ... (configuration)
    'constraint bits   ->constraint_bits_arg   ', [],       ...
    'encoder           ->encoder_arg           ',           ...
    ... (operational)
    'rx code symbols   ->rx_code_symbols_arg   ',           ...
    ... (diagnostics)
    'test bits         ->test_bits_arg         ',           ...
    'plot              ->plot_arg              ', 0,        ...
    ... (other)
    'outputs           ->output_arg            ', {''}      );

checkVectorBinary(u, test_bits_arg);
assertion(u, length(test_bits_arg)==length(constraint_bits_arg));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialize                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
q = 2;  %Indexing offset for Matlab arrays, so that they start at a logical index of -1.

L = ceil(log(max(encoder_arg))/log(2)+0.0001);                      % constraint length
L1 = L-1;                                                           % one less than constraint length
R = length(encoder_arg);                                            % code ratio
G = zeros(1,R+1); % encoder, uses logical indices 0..R-1.
G((0:R-1)+q) = encoder_arg(:).';

% G = offset_array_class('extent', [0 0 0 R-1], 'data', encoder_arg(:).'); % encoder
NS = 2^(L-1);  % Number of survivor states.
NC = 2^L;      % Number of candidates.
ND = length(constraint_bits_arg);
%constraint_bits = offset_array_class('extent', [ 0 ND-1 0 0 ], 'data', constraint_bits_arg(:));
constraint_bits = [0; constraint_bits_arg(:)];

%rx_code_symbols = offset_array_class('extent', [ 0 ND-1 0 R-1 ]);
rx_code_symbols = zeros(ND+1, R+1); % logical indices 0..ND-1, 0...R-1
for t = 0 : ND-1
    for k = 0 : R-1
        rx_code_symbols(t+q,k+q) = rx_code_symbols_arg(t*R+k+1);
    end;
end;

%test_bits = offset_array_class('extent', [0 ND-1 0 0 ], 'data', test_bits_arg(:));
test_bits = zeros(ND+1,1); % Logical indices 0...ND-1
for k = 0 : ND-1
    test_bits(k+q) = test_bits_arg(k+1);
end;

%
% MS: survivor  metrics
% MB: branch    metrics
% MC: candidate metrics
% MD: decision  metrics
% PS: survivor  path history
% PC: candidate path history
% VS: valid survivor  flag.  0 for states deemed invalid.
% VC: valid candidate flag.
% KTS: survivor state number of test path
% KTC: candidate state number of test path
%
%MS = offset_array_class     ('extent', [-1 ND+L1-1 0 NS-1 ]);
MS = zeros(ND+L1-1+2, NS+1);  % Logical indices -1..ND+L1-1, 0..NS-1

%MB = offset_array_class     ('extent', [ 0 ND+L1-1 0 NC-1 ]);
MB = zeros(ND+L1+1, NC+1); % Logical indices 0...ND+L1-1, 0...NC-1

%MC = offset_array_class     ('extent', [ 0 ND+L1-1 0 NC-1 ]);
MC = zeros(ND+L1+1, NC+1); % Logical indices 0..ND+L1-1, 0...NC-1

%MD = offset_array_class     ('extent', [ 0 ND-1 0 0    ]);  % Decision metrics, for test path - ? Rename?
MD = zeros(ND+1, 1); % Logical indices 0..ND-1

%PS = offset_cell_array_class('extent', [-1 ND+L1-1 0 NS-1 ]);
PS = cell(ND+L1-1+2, NS+1);  % Logival indices -1...ND+L1-1, 0..NS-1

%PC = offset_cell_array_class('extent', [ 0 ND+L1-1 0 NC-1 ]);
PC = cell(ND+L1+1, NC+1);  % Logical indices 0..ND+L1-1, 0..NC-1

%KTS = offset_array_class('extent', [-1 ND+L1-1 0 0 ]);
KTS = zeros(ND+L1-1+2, 1); % Logical indices -1..ND+L1-1
KTS(-1+q) = 0;

%KTC = offset_array_class('extent', [ 0 ND+L1-1 0 0 ]);
KTC = zeros(ND+L1+1,1); % Logical indices 0..ND+L1-1

%VS = offset_array_class     ('extent', [-1 ND+L1-1 0 NS-1 ]);
VS = zeros(ND+L1-1+2, NS+1);  % Logical indices -1..ND+L1-1, 0..NS-1

%VC = offset_array_class     ('extent', [ 0 ND+L1-1 0 NC-1 ]);
VC = zeros(ND+L1, NC+1); % Logical indices 0..ND+L1-1, 0..NC-1

VS(-1+q, q) = 1;  % Only this state is valid, initiallly.
for k = 1:NS-1  % Disallow other states at t=-1.
    VS(-1+q, k+q) = 0;
end;

for k = 0:NS-1
    MS(-1+q,k+q) = 0; % Initialize state metrics to 0.
    % If we were storing state indices in PS, we'd do this: PS(-1,k) = k;
    PS{-1+q,k+q} = [];  % Path histories are initially empty.
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN THE DECODER                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear t

t_report = clock;
% t1 is the time of the newest bit we are processing.
% t2 is the time of the bit we are deciding on this time.
for t1 = 0 : ND+L1-1
    if etime(clock, t_report)>5
        fprintf( 1, 't1=%d\n', t1);
        pause(0.1);
        t_report = clock;
    end;
    
    %fprintf( 1, 't1=%d\n',t1);
    
    t2 = t1 - L1;
    startup  = 0;
    main     = 0;
    shutdown = 0;
    if t1<L1, startup = 1; end;
    if t1>=ND shutdown = 1; end;
    if ~startup && ~shutdown, main = 1; end;
    
    
    
    if ~shutdown
        % Calculate branch metrics for current survivors.
        current_rx_symbols = rx_code_symbols(t1+q, (0:R-1)+q); 
        current_rx_symbols = [ 0 current_rx_symbols ] ; % This is done only because of the offset indexing.
        predicted_symbols  = zeros(size(current_rx_symbols)); 
        for k = 0 : NS-1
            % Survivor state k goes to candidate states k, k+2^(L-1).
            for next_k = [ k k+NS ]
                bits = next_k;  % The candidate state is the base-2 numeric representation of the encoder's input bits.
                for j = 0 : R-1
                    predicted_symbols(j+q) = 1-2*mod(countBits(bitand(bits, G(j+q))),2);
                end;
                branch_metric = sum(predicted_symbols .* current_rx_symbols);
                MB(t1+q, next_k+q) = branch_metric;
            end; % next_k
        end; % k
        % Done calculating branch metrics.
    end; % Otherwise, branch metrics are all zeros.
    
    % Calculate test candidate state.
    
    if ~shutdown
        KTC(t1+q) = KTS(t1-1+q) + test_bits(t1+q)*NS;
    else % shutdown
        KTC(t1+q) = KTS(t1-1+q);
    end;
    
    % Calculate candidate metrics, candidate path histories.
    for k = 0 : NS-1
        % Survivor state k has branches to candidate states k, k+2^(L-1).
        next_k = k;
        MC(t1+q, next_k+q) = MS(t1-1+q,k+q) + MB(t1+q,next_k+q);
        VC(t1+q, next_k+q) = VS(t1-1+q,k+q);
        PC{t1+q, next_k+q} = [ PS{t1-1+q,k+q}; 0 ];  % Append 0 to this path.
        next_k = k + NS;
        MC(t1+q, next_k+q) = MS(t1-1+q,k+q) + MB(t1+q,next_k+q);
        VC(t1+q, next_k+q) = VS(t1-1+q,k+q);
        PC{t1+q, next_k+q} = [ PS{t1-1+q,k+q}; 1 ];  % Append 1 to this path.
    end; % k
    % Done calculating candidate metrics.
    
    % Choose survivors.
    % Candidate states k, k+1 compete for survivor state k/2 (k must be even).
    KTS(t1+q) = floor(KTC(t1+q)/2); % Next survivor state of test path.
    if mod(KTC(t1+q),2)==0
        k_competitor = KTC(t1+q)+1;  % Index of candidate which competes with the test path.
    else
        k_competitor = KTC(t1+q)-1;
    end;
    
    if ~startup
        MD(t2+q,0+q) = MC(t1+q,KTC(t1+q)+q) - MC(t1+q,k_competitor+q);  % Decision metric for test path is the difference between its path metric and that of its competitor.
    end;
    
    for k = 0 : 2 : NC-1
        next_k = floor(k/2);
        % Choose a winner based on constraint bit, validity flag, or metric.
        k_winner = NaN;
        while 1
            
            % Handle state valid flag.
            if VC(t1+q,k+q) > VC(t1+q,k+1+q)
                k_winner = k;
                break;
            end;
            if VC(t1+q,k+q) < VC(t1+q,k+1+q)
                k_winner = k+1;
                break;
            end;
            
            % Handle constraint bits.
            if ~startup
                if constraint_bits(t2+q)==0
                    k_winner = k;
                    break;
                end;
                if constraint_bits(t2+q)==1
                    k_winner = k+1;
                    break;
                end;
            end;
            
            % Decide based on metric.
            if MC(t1+q,k+q) > MC(t1+q,k+1+q)
                k_winner = k;
            else
                k_winner = k+1;
            end;
            break;
        end; % while
        MS(t1+q,next_k+q) = MC(t1+q,k_winner+q);
        PS(t1+q,next_k+q) = PC(t1+q,k_winner+q);
        VS(t1+q,next_k+q) = VC(t1+q,k_winner+q);
    end;
    % Done choosing survivors.
    checkLockFile(u);
end;

    % Now the winner is in state 0.
    detected_bits = PS{ND+L1-1+q, 0+q}; % Detected_bits isn't an offset array.
    detected_bits = detected_bits(1:ND);  % Remove those extra bits at the end.  Perhaps they should not have been inserted in the first place.
    winning_path_metric = MS(ND+L1-1+q,0+q);
    
    % Assign output arguments.
    outputs = split(u, output_arg, ',', 'trim');
    outputs = removeEmptyCells(u, outputs);
    n = length(outputs);
    varargout = cell(n,1);
    assertion(u, n==nargout);
    for k = 1 : n
        switch outputs{k}
            case 'detected bits',    y = detected_bits;
            case 'decision metrics', y = MD((0:ND-1)+q,0+q);
            case 'winning path metric', y = winning_path_metric;
            otherwise,      error('Unsupported output argument.');
        end;
        varargout{k} = y;
    end;
    % Done assigning output arguments.
    
    return;
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
function y = countBits(x)
% y = countbits(x)
% Counts the number of '1' bits in the binary representation of a number.
%
% Inputs:
% x is a nonnegative integer.
%
% Outputs:
% y is the number of '1' bits in the base-2 representation of x.
%

if numel(x)~=1 || x~=round(x) || x<0
    error('Invalid input.');
end;
y = 0;
while x>0
    y = y + bitand(x,1);
    x = floor(x/2);
end;
y = mod(y,2);
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


        