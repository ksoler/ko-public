function plot_background_multiple2(u, foreground_data, background_data)
% plot_background_multiple:  Plots data in the background of other data.  Handles complex data.
% 
% Inputs:
% u is a vestigial utility01_class object.
% 
% foreground_data is a cell array containing one or more repetitions of the following:
%   (1) x co-ordinates
%   (2) y co-ordinates
%   (3) string with matlab line style, e.g. 'b-'.
%   (4) 0 for normal plot, 1 for stem plot.
% This data is plotted as foreground.
%
% background_data is a cell array containing one or more repetitions of the following:
%  (1) x co-ordinates
%  (2) y co-ordinates
%  (3) string with matlab line style, e.g. 'g-'
%  (4) 0 for normal plot, 1 for stem plot.
% The background data is scaled and shifted so that it lies within the range of the foreground data,
% and the background data is also plotted with lighter colours.
%
%
if ishold
  saved_hold_state = 1;
else
  saved_hold_state = 0;
end;

num_foreground = length(foreground_data)/4;
num_background = length(background_data)/4;

% Parse arguments into structures.
fore.x = [];
fore.y = [];
fore.style = 'b-';
fore.stem = 0;
fore = fore([]);   % Make into an empty structure.
back = fore;

for k = 1 : num_foreground
   fore(k).x     = foreground_data{k*4-3};
   fore(k).y     = foreground_data{k*4-2};
   fore(k).style = foreground_data{k*4-1};
   fore(k).stem  = foreground_data{k*4-0};
   % I think this is not needed any more since this function now seems to handle complex data.
   % assertion(u, all(imag(fore(k).y)==0));
end;

for k = 1 : num_background
   back(k).x     = background_data{k*4-3};
   back(k).y     = background_data{k*4-2};
   back(k).style = background_data{k*4-1};
   back(k).stem  = background_data{k*4-0};
   % I think this is not needed any more since this function now seems to handle complex data.
   % assertion(u, all(imag(back(k).y)==0));
end;
% Done parsing arguments into structures.

% Determine the vertical extent of all of the foreground data.
[foreground_min, foreground_max ] =  minAndMaxComponents(fore(1).y);
for k = 2 : num_foreground
[foreground_min, foreground_max ] =  minAndMaxComponents(fore(k).y, foreground_min, foreground_max);
end;
% If the foreground data has zero span, as can happen if it is a single point or constant, then we
% will expand it.  Othersise, the background data gets compressed to zero vertical span.
if foreground_min==foreground_max
  foreground_min = foreground_min - 0.5;
  foreground_max = foreground_max + 0.5;
end;

% Determine the vertical extent for each set of background data.
background_min = zeros(num_background,1);
background_max = zeros(num_background,1);
for k = 1 : num_background
  [ background_min(k), background_max(k) ] = minAndMaxComponents(back(k).y);
end;

% Scale and shift each set of background data.
for k = 1 : num_background
  yi = real(back(k).y);
  yq = imag(back(k).y);
  
  yi = yi - background_min(k);
  yi = yi / (background_max(k) - background_min(k)); % Now it's in the range from 0 to 1.  
  yi = yi * (foreground_max - foreground_min);  
  yi = yi + foreground_min;
  back(k).y = yi;
  
  if any(yq)
    yq = yq - background_min(k);
    yq = yq / (background_max(k) - background_min(k)); % Now it's in the range from 0 to 1.
    yq = yq * (foreground_max - foreground_min);
    yq = yq + foreground_min;
    back(k).y = back(k).y + yq;
  end;
end;
% Done shifting and scaling each set of background data.

% Change line types into cell arrays.
for k = 1 : num_foreground
  s = split(u, fore(k).style, ' ', 'trim');
  if numel(s)==1
    s = [ s s ]; % If the user did not supply a second linetype for an imginary component, then duplicate the real one.
    % If we only plot the real component, then the second style will be ignored.
  end;
  fore(k).style = s;
end;

for k = 1 : num_background
  s = split(u, back(k).style, ' ', 'trim');
  if numel(s)==1
    s = [ s s ]; 
  end;
  back(k).style = s;
end;

% Plot background lines.
for k = 1: num_background
  if back(k).stem % && any(imag(back(k).y))
    h = stem(back(k).x, real(back(k).y), back(k).style{1});
    hold on;
    if any(imag(back(k).y))
      h(2) = stem(back(k).x, imag(back(k).y), back(k).style{2});
    end;
  else % Plot a line, instead of stem.
    h = plot(back(k).x, real(back(k).y), back(k).style{1});
    hold on;
    if any(imag(back(k).y))
      h(2) = plot(back(k).x, imag(back(k).y), back(k).style{2});
    end;
  end;
  for j = 1 : length(h)
    c = get(h(j), 'Color');
    c = 1/2 + c/2;
    set(h(j), 'Color', c);
  end;
end;
% Done plotting the background lines.

% Plot foreground lines.
for k = 1: num_foreground
  if fore(k).stem 
    h = stem(fore(k).x, real(fore(k).y), fore(k).style{1});
    hold on;
    if any(imag(fore(k).y))
      h(2) = stem(fore(k).x, imag(fore(k).y), fore(k).style{2});
    end;
  else % Plot a line, instead of stem.
    h = plot(fore(k).x, real(fore(k).y), fore(k).style{1});
    hold on;
    if any(imag(fore(k).y))
      h(2) = plot(fore(k).x, imag(fore(k).y), fore(k).style{2});
    end;
  end;
end;
% Done plotting the foreground lines.

if saved_hold_state
  hold on;
else
  hold off;
end;

return;


function [mn, mx] = minAndMaxComponents(x, old_min, old_max)
% If x is real, finds the minimum and maximum.
% If x is complex, finds the minimum and maximum of real and imaginary components.
% This is useful when plotting complex values.
% If old_min and old_max arguments (which must be real) are included, then they are included in the search for min  and
% max.
%
x = x(:);
if any(imag(x))
  mn = min([ real(x); imag(x) ]);
  mx = max([ real(x); imag(x) ]);
else
  mn = min(x);
  mx = max(x);
end;
if nargin > 1
  mn = min(mn, old_min);
end;
if nargin > 2
  mx = max(mx, old_max);
end;
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


