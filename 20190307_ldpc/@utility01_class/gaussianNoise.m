function y = gaussianNoise(u, varargin)
% y = gaussianNoise(u,...)
% Generates gaussian noise.
% 
% (noise level) snr, enr, ebno, num bits,
% (other)       signal,  length, complex 
% Outputs:  y is a signal containing the requested noise.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Detailed Desription of Inputs:
%
% u is a vestigial utility01_class object.  The remaining arguments are tag/value pairs.
%
% (noise level)
%
% 'snr' is the desired signal to noise ratio in dB.
%
% 'enr' is the desired energy-to-noise ratio in dB.
%
% 'ebno' is the desired bit energy to noise ratio (Eb/No).  If this is used, you also need to
% provide 'num bits'.
%
% 'num bits' is the number of bits in the signal.
%
% (other)
% 'signal' is the signal to use as a basis for generating the noise.  This function can use the
% following characteristics: real/complex, amplitude, size.
%
% 'length' is the length of the desired noise signal.  If this is omitted or 'auto', then the length
% of the input signal is used.
% 
% 'complex' is 1 for complex noise, 0 for real noise.  If this is absent or 'auto', then the noise 
% is complex if the input signal is complex.
%
%---------------------------------------------------------------------------------------------------

dB = 1;

persistent entry_count;
if isempty(entry_count), entry_count = 0; end;
entry_count = entry_count + 1;
if mod(entry_count, 1e4)==0
 warning(u, 'This function has not yet been tested against ber curves etc.');
end;

% (noise level) snr, enr, ebno, num bits,
% (other)       signal,  length, complex 

getArg( u,                                             ...
  ... (noise level)
  'snr     ->snr_arg     ->use_snr      ', 0,        ...
  'enr     ->enr_arg     ->use_enr      ', 0,        ...
  'ebno    ->ebno_arg    ->use_ebno     ', 0,        ...
  'num bits->num_bits_arg->use_num_bits ', 1,        ...
  ... (other)
  'signal  ->signal_arg  ->signal_flag  ', [],       ...
  'length  ->length_arg  ->use_length   ', {'auto'}, ...
  'complex ->complex_arg                ', {'auto'}  );

  if use_ebno && ~use_num_bits
    error('num bits must be specified when using the ebno option.');
  end;
  
  if ~use_ebno && use_num_bits
    warning(u, 'Ignoring num bits because the ebno option is not used.');
  end;
  
  if use_length
    % length_arg is the length of the desired noise signal.
  else
    length_arg = length(signal_arg);
  end;

if isequal(complex_arg, 'auto')
    if any(imag(signal_arg))
        cmplx = 1;
    else
        cmplx = 0;
    end;
else
    cmplx = complex_arg;
end;
% cmplx is the flag that selects real or complex noise.

if use_snr  + use_ebno + use_enr ~=1
  error('You must specify exactly one of these:  snr, ebno, enr.');
end;

if isempty(signal_arg)
  error('This function requires nonempty signal.');
end;

signal_energy = sum(abs(signal_arg).^2);
signal_power = signal_energy / length(signal_arg);
bit_energy = signal_energy / num_bits_arg;


if use_enr
  noise_scale = 10^(-enr_arg/20)/sqrt(2);
  noise_scale = noise_scale * sqrt(signal_energy);
elseif use_ebno
  noise_scale = 10^(-ebno_arg/20)/sqrt(2);
  noise_scale = noise_scale * sqrt(bit_energy);
elseif use_snr
  noise_scale =  10^(-snr_arg/20)/sqrt(2);
  noise_scale = noise_scale * sqrt(signal_power);
else
  error('This should not happen.');
end;

if cmplx
    y = randn(length_arg,1) + sqrt(-1)*randn(length_arg,1);
else
    y = randn(length_arg,1);
end;
y = y * noise_scale;
return;

    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    