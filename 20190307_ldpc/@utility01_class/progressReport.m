function progressReport(u, num_sec, msg, varargin)
% progressReport(u, num_sec, ....)
% Prints a message at specified intervals.
% 
% Inputs:
% u is a vestigial utility01_class object.
% num_sec is the number of seconds to allow between reports.
% msg is a string to print when reporting.  The characters "NAME" get replaced by the name of the
% function.  'LINE' gets replaced by the caller's line.
% 
% The remaining arguments are used as printf-style data for the msg string.
%

% This function is going to try to maintain a separate timer for each function that invokes it.
% I think I can do it using evalin.
%  that's complicated; maybe I will store a separate time value for each caller function.  This will
%  get confused if there are two different functions with the same name, but I'm not too worried
%  about that.

current_time = clock;
persistent last_time
persistent function_names    % Cell array, names of functions that have invoked progressReport
persistent timestamps        % Cell array, timestamp of last report for each function.

current_function_name = whoAmI(u, 'caller name');
current_function_line = sprintf('%d', whoAmI(u, 'caller line'));

if nargin<2
  num_sec = 10;
end;

if nargin < 3
  msg = current_function_name;
end;

if isempty(function_names)
  last_time = current_time;
  function_names = { current_function_name };
  timestamps = { current_time };
  last_time = current_time;
  return;
end;


k = findStringInList(u, current_function_name, function_names);
if k==0
  % Add a new function to the list.
  function_names{end+1} = current_function_name;
  timestamps{end+1} = current_time;
  last_time = current_time;
  return;
end;

% At this point, k is the index of the function in the list.
if etime(current_time, timestamps{k}) > num_sec
 
  % Replace "NAME" with the function name.
  i = strfind(msg, 'NAME');
  if ~isempty(i)
    i = i(1);
    msg = [ msg(1:i-1) current_function_name msg(i+4:end) ];
  end;
  % Done replacing "NAME" with the function name.

  % Replace "LINE" with the function line.
  i = strfind(msg, 'LINE');
  if ~isempty(i)
    i = i(1);
    msg = [ msg(1:i-1) current_function_line msg(i+4:end) ];
  end;
  % Done replacing "NAME" with the function name.

  fprintf(1, msg, varargin{:});
  fprintf(1, '\n');
  pause(0.1);
  timestamps{k} = current_time;
  checkLockFile(u);
end;

last_time = current_time;
return;




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


