function y = dir1(u, dir_arg, varargin)
% y = dir1(u, dir_arg, varargin)
% Prints out file and folder information.
%
% Inputs:
% u is a vestigial utility01_class object.
% dir_arg is a string containing a directory name.  The remaining arguments are tag/value pairs.
%
% 'prompt' enables a GUI which allows the user to select a directory.  1 enable, 0 disable.
%
% 'size' enables reporting of file sizes.  1:enable, 0:disable.
%
% 'excel' if 1, then an excel worksheet is created and output is stored in it.
%
% 'max level' is the deepest level to report.  By default, or if this is [], then all levels are
% reported.
%
% 'min size' files and directories with agregate size below this are suppressed from excel report.
%
% Future Improvements: 
% User specifies whether to ignore files or not (then, this function only prints out subdirectories).
% Can determine file and directory sizes, recursively.
% Show results to user in a GUI so aggregate sizes can be viewed.
% Compare files, directories to identify identical files and directory trees.
% Support sophisticated renaming of multiple files and directories, perhaps with wildcards or pattern matching.
% Optional limit for the depth to go to.
% 'indent'
% 'count'
% 'index'
% 'level'
% 'output'
% 

% Example:
% y = dir1(u,'C:\Users\oler\Documents\2017_work\15 matlab source\01 common\@utility01_class', 'excel', 1);
% y = dir1(u,'C:\Users\oler\Documents\2017_project_data', 'excel', 1);




getArg(u, ...
  'prompt->prompt_arg->prompt_flag', {''}, ...
  'size  ->size_arg  ->size_flag  ', 0,    ...
  'excel ->excel_arg'              , 0, ...
  'max level->max_level_arg->max_level_flag', [], ...
  'min size->min_size_arg', []);

if prompt_flag
  warning(u, 'For now, the prompt argument is ignored.');
end;

% This is what I want the function to do:
% Read the directory, print out information 
%
% How should this do it?  Collect all information into a structure before printing it out?  Yes, I think 
% it should be done that way.
% 
% Should this structure be recursive?  Maybe at first, no, I think we can go straight to linear.
% I do want to use a recursive function.  Should I declare a class for this?  Not now; maybe later.
%
% structure:
%   d.isDir   0, 1.
%   d.isFile  0, 1.  Complement of isDir.
%   d.barename: For files, filename without extension.  For directories, the directory name.
%   d.extension: filename extension including period.  Empty for directories.
%   d.parent: The full path of the object up to and including the parent of the object.
%   d.mySizeInByts; size of current element in bytes; 0 for directories?
%   d.aggregateSizeInBytes: size of self and all children in bytes.
%   d.level is 1 for top-level files and directories such as C:\temp.txt, 2 for their subdirectories, etc.
%
% Note that the directories . and .. are not allowed in this structure.
% Within one directory, subdirectories are listed before files.  Both groups are arranged in 
% alphabetical order.
[d, aggregateSize] = readDir(dir_arg);

base_level = sum(dir_arg=='\');

checkStructFields(u, d, 'isDir, isFile, barename, extension, parent, mySizeInBytes, aggregateSizeInBytes, level');

if 1  % Sort d by d.isFile.
  v = struct_get_field_array(u, 'struct', d, 'field', 'isFile');
  [~,i] = sort(v);
  d = d(i);
end;  % Sorted d by d.isFile.

% Optionally, remove all levels deeper than a threshold value.
if ~isempty(max_level_arg)
  levels = struct_get_field_array(u, 'struct', d, 'field', 'level');
  i = levels > max_level_arg;
  d(i) = [];  
%  for k = length(d):-1:1
%    if d(k).level > max_level_arg
%      d(k) = [];
%    end;
%  end;
end;

      
% Done removing levels deeper than a threshold value.

% Now, print out the information we have gathered.
tab = sprintf('\t');
y = {};
if size_arg
  for k = 1 : length(d)
    y{end+1} = sprintf('%d%s%s', d(k).aggregateSizeInBytes, repmat(tab, 1, d(k).level), [d(k).barename d(k).extension]);
  end;
else
  
  % Create a header line.
  s = {'index' 'type' 'level' 'blank' 'total size' 'path' 'name' }; 
  y = { join(u, s, tab) };
  
  for k = 1 : length(d)
    if isempty(min_size_arg) || d(k).aggregateSizeInBytes >= min_size_arg % Suppress reporting objects below specified size.
    s = {};
    s{end+1} = sprintf('%d', k); % Index.
    
    if d(k).isDir     % directory or file.
      s{end+1} = 'dir';
    else
      s{end+1} = 'file';
    end;

    s{end+1} = sprintf('%d', d(k).level);           % directory level

    if d(k).isFile     % filename extension
      s{end+1} = d(k).extension;
    else
      s{end+1} = '';
    end;
    
    s{end+1} = sprintf('%d', d(k).aggregateSizeInBytes);  % Aggregate size.
    
    % s{end+1} = [ repmat(' ', 1, d(k).level - base_level) d(k).barename d(k).extension ]; % Indented name.
    
    s{end+1} = d(k).parent;                         % parent directories
    s{end+1} = [ d(k).barename d(k).extension ];    % non-indented name
    y{end+1} = join(u, s, tab);
    y = y(:);
  end;
  end;
end;

% Report to excel.
if excel_arg
  sheet = writeIntoExcelSheet(u, 'output', 'sheet object');
  t1 = clock;
  for row = 1: length(y)
    y1 = y{row};
    y1 = split(u, y1, tab);
    for col = 1 : length(y1)
      writeIntoExcelSheet(u, 'sheet object', sheet, 'row', row, 'column', col, 'data', y1{col});
    end;
    if etime(clock,t1)>10
      fprintf(1, 'Wrote row %d of %d into spreadsheet.\n', row, length(y));
      pause(0.1);
      t1 = clock;
    end;
  end;
end;
% Done reporting to excel.

% If there is no output argument, then print the results.
if nargout==0
  for k = 1 : length(y)
    fprintf(1, '%s\n', y{k});
  end;
  clear y;
  return;
end;
return;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [d, aggregateSizeInBytes] = readDir(start_dir)
% Reads contents of a directory and sub-elements, recusively.
%
% d is a structure array of entries for the files and directories in start_dir.
%
% aggregateSize is the sum of the sizes in bytes of all files in start_dir, recursively.
%

u = utility01_class;
% Create zero-length structure arrays with the desired fields.

d = struct(...
  'isDir', {}, 'isFile', {}, 'barename', {}, 'extension', {}, 'parent', {}, ...
  'mySizeInBytes', {}, 'aggregateSizeInBytes', {}, 'level', {});
f = d;

if ~isdir(start_dir)
  error('Argument is not a directory');
end;

a = dir(start_dir);
% Sort the names in 'a' so that the files and names at this level go in order into d.
names_to_sort = cell(length(a),1);
for k = 1 : length(a)
  names_to_sort{k} = a(k).name;
end;
[~,i] = sort(names_to_sort);
a = a(i);
% Now, the entries in 'a' are in order.

aggregateSizeInBytes = 0;
names_to_sort = {};
% First, parse the directories.
%keyboard;
t1 = clock;
for k = 1  : length(a)
  % We want to parse directories, but ignore self and parent.
  if a(k).isdir && ~isequal(a(k).name, '.') && ~isequal(a(k).name, '..')
    n = length(d)+1;
    d(n).isDir = 1;
    d(n).isFile = 0;
    [pathstr, barename, ext] = fileparts(a(k).name);
    d(n).barename = [barename ext]; % For directories, we don't separate an extension.
    d(n).extension = '';         
    d(n).parent = a(k).folder; % Usually the same as start_dir.
    d(n).mySizeInBytes = 0;
    d(n).aggregateSizeInBytes = 0; % We'll fill this in later.
    d(n).level = sum(a(k).folder=='\'); % Might be messed up by paths that have a double-slash, if that's possible.
    [children, childSize] = readDir(fullfile(a(k).folder, a(k).name));
    d = [ d(:); children(:)]; % Add child directories and files to our list.
    d(n).aggregateSizeInBytes = childSize;
    aggregateSizeInBytes = aggregateSizeInBytes + childSize;
    names_to_sort{end+1} = a(k).name;
  end;
    if etime(clock,t1) > 120
      x = dbstack;
      y = repmat(' ', 1, length(x));
      fprintf(1, '%sIn %s, level %d.\n', y, whoAmI(u, 'my name'), length(x));
      pause(0.1);
      %keyboard;
      t1 = clock;
    end;
end;

  % Put subdirectories into alphabetical order.
  [~,i] = sort(names_to_sort);
  % d = d(i);
  % Done putting subdirectories into alphabetical order.
names_to_sort = {};

  for k = 1  : length(a)
  % We want to parse directories, but ignore self and parent.
  if ~a(k).isdir
    n = length(f)+1;
    f(n).isDir = 0;
    f(n).isFile = 1;
    [pathstr, barename, ext] = fileparts(a(k).name);
    f(n).barename = barename;
    f(n).extension = ext;
    f(n).parent = a(k).folder; % Usually the same as start_dir.
    f(n).mySizeInBytes = a(k).bytes;
    f(n).aggregateSizeInBytes = a(k).bytes; 
    f(n).level = sum(a(k).folder=='\'); % Might be messed up by paths that have a double-slash, if that's possible.
    aggregateSizeInBytes = aggregateSizeInBytes + a(k).bytes;
    names_to_sort{end+1} = a.name;
  end;
    if etime(clock,t1) > 120
      x = dbstack;
      y = repmat(' ', 1, length(x));
      fprintf(1, '%sIn %s, level %d.\n', y, whoAmI(u, 'my name'), length(x));
      %keyboard;
      pause(0.1);
      t1 = clock;
    end;
end;
  % Put files into alphabetical order.
  [~,i] = sort(names_to_sort);
  % f = f(i);
  % Done putting subdirectories into alphabetical order.

    d = [ d(:); f(:)];
  return;
  % End of readDir().


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    