function ok = checkVectorInt(u, x, varargin)
% ok = checkVectorInt(u, x, ...)
% Checks for a vector of integers.
%
% Inputs:
% u is a vestigial utility01_class object.
%
% x is the value to be checked.  It is checked for being a 1D array with integer values.
% NaN, Inf, and complex values are not allowed.
%
% Additional conditions can be specified using tag/value pairs.  
% 'range' is a 2-element vector which specifies the minimum and maximum
% allowed values.
%
% 'int16' allows values appropriate for signed 16-bit integers.
%
% Outputs:
% ok is 1 if x passes all tests, 0 otherwise.
%
% If this function is invoked without an output argument, and x fails any
% test, then an error message is printed.
%
% Future improvements:
% Check that the range limits are valid - i.e. no Nan, maybe no Inf values.
% Use a 'msg' variable and output to explain why the test failed.
%

% Parse optional inputs.
getArg(u, ...
    'range->range->check_range ', [], ...
    'int16->     ->int16_option');
% Done parsing optional inputs.

ok = 1;

if ok && any(isnan(x))
    ok = 0;
end;

if ok && any(isinf(x))
    ok = 0;
end;

if ok && any(imag(x)~=0)
    ok = 0;
end;

if ok && length(size(x))>2
    ok = 0;
end;

if ok && length(x) ~= numel(x)
    ok = 0;
end;

if ok && check_range
    assertion(u, numel(range)==2);
    if any(x)<range(1) && any(x) > range(2)
        ok = 0;
    end;
end;

if ok && int16_option
    if any(x < -2^15) || any(x >= 2^15)
        ok = 0;
    end;
end;
    
if nargout==0 && ~ok
    error('Test for vector int failed.');
end;
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


