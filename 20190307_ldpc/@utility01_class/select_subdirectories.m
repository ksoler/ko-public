function y = select_subdirectories(u, varargin)
% y = select_subdirectories(u,...)
% Selects subdirectories of a specified directory.
%
% Inputs: directory, include, exclude
% 
%---------------------------------------------------------------------------------------------------
% Detailed Description of Inputs
% 
% u is a vestigial utility01_class object.  The remaining inputs are tag/value pairs.
%
% 'directory' is the parent directory to use.
%
% 'include' is a string which specifies the a name or pattern which subdirectories must match in
% order to be selected.
%
% 'exclude' is a string which specifies a name or pattern which excludes subdirectories.
%
% Other Notes:  The 'include' and 'exclude' tags can be repeated up to 4 times.  In order to be 
% selected, a subdirectory must match at leastone of the 'include' conditions (if any), and none of 
% the 'exclude' conditions.  The wildcard characters are '?' and '*'.  '?' matches any one character,
% and '*' matches 0 or more arbitrary characters.  A condition which is an empty string or array
% will be ignored.
%

% Algorithm:
% (1) Get a list of all subdirectories.
% (2) If there are any include conditions, then reject all files which do not match one.
% (3) Reject all files which match an exclude condition.

% Parse inputs.
getArg( u, ...
  'directory->parent_dir', ...
  'include->include1', [], ...
  'include->include2', [], ...
  'include->include3', [], ...
  'include->include4', [], ...
  'exclude->exclude1', [], ...
  'exclude->exclude2', [], ...
  'exclude->exclude3', [], ...
  'exclude->exclude4', []);


include_filter = { include1 include2 include3 include4 };
include_filter = removeEmptyCells(u, include_filter);
exclude_filter = { exclude1 exclude2 exclude3 exclude4 };
exclude_filter = removeEmptyCells(u, exclude_filter);

% (1) Get a list of all subdirectories.
f1 = {};
if ~exist(parent_dir, 'dir')
  warning(u, 'Specified directory does not exist.');
else
  d = dir(parent_dir);
  for k = 1 : length(d)
    if d(k).isdir && ~strcmp(d(k).name,'.') && ~strcmp(d(k).name, '..')
      f1{end+1} = d(k).name;
    end;
  end;
end;

% Now, f1 is a list of the subdirectories before any filters are applied.

% (2) If there are any include conditions, then reject all files which do not match one.
if isempty(include_filter)
  f2 = f1; % Pass all of the subdirectories.
else
  f2 = {};
  for j = 1 : length(f1)
    for k = 1 : length(include_filter)
      if stringPatternMatch(u, include_filter{k}, f1{j})
        f2{end+1} = f1{j};
        break;
      end;
    end;
  end;
end;


% (3) Reject all files which match an exclude condition.
f3 = {};
for j = 1 : length(f2)
  reject_it = 0; % Set to 1 if it matches an exclude filter.
  for k = 1 : length(exclude_filter)
    if stringPatternMatch(u, exclude_filter{k}, f2{j})
      reject_it = 1;
    end;
  end;
  if ~reject_it
    f3{end+1} = f2{j};
  end;
end;

% Now, f3 is the final list of subdirectories.

y = sort(f3);
return;

      
      
  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


