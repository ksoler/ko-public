function ok = checkString(x)
% ok = checkString(x)
% Checks if a value is a string.
%
% Inputs:
% x is the value to be checked.
%
% Outputs:
% ok is 1 if x is a string, 0 otherwise.
% If this function is invoked without an output argument, and x is not a
% string, then an error message is printed.
%
% Copyright 2019, Kevin Oler, All rights reserved.

ok = 1;
if ok && ~ischar(x)
    ok = 0;
end;

    if ok && length(size(x))>2
    ok = 0;
end;

if ok && length(x) ~=numel(x)
    ok =  0;
end;

if ok && size(x,1)>1
    ok = 0;
end;

if ~ok && nargout==0
    error('Test for string failed.');
end;
return;
