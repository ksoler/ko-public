h = wave_write_header(u, varargin)
% h = wav_write_header(u, ....)
% Writes header information to a cannonical PCM WAV file.
%
% Inputs: sample rate, num channels, num samples, bits per sample
% 
% Detailed Description of Inputs:
% u is a vestigial utility01_class object.  The rest of the inputs are tag/value pairs.
% 
% 'file' is a filename or file handle.  
% 'sample rate' is the sample rate in Hertz.
% 'num channels' is the number of channels, e.g. 1 for mono, 2 for stereo, etc.
% 'num samples' is the number of samples in each channel.
% 'bits per sample' is 8 or 16.  
%
% Note that you can call this function with a pre-existing WAV file, and this function will update
% the header information accordingly.
%
% Outputs:
% h is a handle for the file.  This function always leaves the file open.
%

% address content
% 0       'RIFF'
% 4       (size of file in bytes) - 8
% 8       'WAVE'
%  
getArg(u,...
  'file->file', ...
  'sample rate->sampleRate', 8000*Hz, ...
  'num channnels->numChannels', 1, ...
  'num samples->numSamples', 0, ...
  'bits per sample->bitsPerSample', 16 )

% Open the file if necessary.
if isa(file, 'char')
  h = fileOpen(u, file, 'r+');
  close_file_when_done = 1;
else
  h = file;
  close_file_when_done = 0;
end;
% Done opening the file if necessary.

% Write the 'RIFF' string.
fwrite(h, 'RIFF', 'char');

% Write the chunk size.
fwrite(h, ???, 'uint32');

% Write the 'WAVE' string.
fwrite(hm 

sampleRate, numChannels, numSamples, samplesPerChannel, bitsPerSample, dataStart

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


