function varargout = viterbi_fec01(u, varargin)
% [outputs...] = viterbi_fec01(u, inputs...)
% Viterbi decoding of convolutionally-encoded data.
% This implementation follows the design in bug294, and is intended to be easy to port to firmware.
%
% Inputs: constraint bits, encoder, history length, num bits, outputs, symbol alphabet, test bits, y.
% Outputs: detected bits, bit metrics, num corrected symbols.
%
% FUTURE IMPROVEMENTS:
% Return the indices of corrrected symbols.  Estimate Eb/No by reconstructing the received signal,
% and estimating the channel gain and noise.
%
%---------------------------------------------------------------------------------------------------
% Detailed Description of Inputs
%---------------------------------------------------------------------------------------------------
% u is a vestigal utility01_class object.  The remaining input argumets are tag/value pairs.
%
% 'constraint bits' is an array of constraint bits.  0 and 1 constrain the corresponding data
% bit to that value, 2 allows the decoder to choose.  If this array is too short, 2 is assumed.
%
% 'encoder' is the pseudo-octal representation of the encoder.
%
% 'history length' is the number of history bits to retain with path.  Use 'all' to store the full
% length.
%
% 'num  bits' is the number of data bits, not including flush bits.
%
% 'outputs' is a string containing the requested outputs, separated by commas.
%
% 'symbol alphabet' is an array of two elements, the code symbol values corresponding to bit values
% 0, 1 repsectively.
%
% 'test bits' is an array of bits, value 0,1.  0 is substituted for any missing values.  Metrics are
% calculated against these bit values.  Usually, you will want to use the true values of data bits
% here, or decode once, then repeat with the detected bits for this arguument, to assess the
% confidence of each received bit.
%
%---------------------------------------------------------------------------------------------------
% Detailed Description of Outputs
%---------------------------------------------------------------------------------------------------
%
% 'bits' are the detected data bits.
% 'bit metrics'
% 'num symbol corrections'
%

q = 1;

% Parse input arguments.
getArg( u, ...
  'constraint bits->C',           [],      ...
  'encoder        ->encoder_arg',          ...
  'history length ->NH',          {'all'}, ...
  'num bits       ->NB',                   ...
  'outputs        ->outputs_arg', {''},    ...
  'symbol alphabet->W',           [0 1],   ...
  'test bits      ->T',           [],      ...
  'y              ->y'            );
% Done parsing input arguments.

% Parse the encoder.
[G, L, R] = parsePseudoOctalEncoder(u, 'encoder', encoder_arg, 'outputs', 'packed binary encoder, constraint length, code ratio');
NF = L-1;
% Done parsing the encoder.

% Prepare constraint bits.
if length(C)<NB
  C = [C(:); repmat(2, NB-length(C),1)]; % Fill in missing constraint bits.
end;
assertion(u, length(C)==NB);
C = [ C(:); repmat(2, NF, 1) ]; % Append constraints for flush bits.
% Done preparing constraint bits.

% Prepare test bits.
if length(T)<NB
  T = [T(:); repmat(0, NB-length(T),1)]; % Fill in missing constraint bits.
end;
assertion(u, length(T)==NB);
T = [ repmat(0, L-1, 1); T(:) ]; % Prepend test bits corresponding to the initial state.
% Done preparing test bits.

if isequal(NH, 'all')
  NH = NB;
end;

NS = 2^(L-1); % Number of survivor states.
NC = 2^L;

% Initialize data structures.
MS = zeros(NS,1);  % Survivor metrics.
MC = zeros(NC,1);  % Candidate metrics.
MB = zeros(NC,1);  % Branch metrics.

HS = zeros(NS,1);  % Survivor bit history.
HC = zeros(NC,1);  % Candidate bit history.

AS = zeros(NS,1);  % Survivor aliveness.
AC = zeros(NC,1);  % Candidate aliveness.

assertion(u, length(y)==R*(NB+NF));
ND = max(NB - NH,0);  % Number of detected bits to store outside of path histories.
D = zeros(ND,1);

AS(1) = 1;

MT = zeros(NB,1); % Metrics for test bits;
test_state  =0;  % survivor state associated with the test bit.
% Done initializing data structures.
verbose = 0;

for t = 0: NB + NF-1

    
  if 0
    % Print MS
    %fprintf( 1, 't=%2d, 
  end;
  
  if 0
    % Print AS.
    fprintf( 1, 't=%2d, ', t);
    fprintf( 1, 'AS = ');
    fprintf( 1, '%d ', AS );
    fprintf( 1, '\n');
  end;
  
  % Move a detected bit out from path histories.
  if t >=L+NH-1 % I'm not sure about this comparison.
    % Find survivor with highest metric, and copy LSB of its HW value in to D.
    assertion(u, all(AS));
    [dummy, i] = max(MS);
    b = mod(HS(i),2);
    D(t-L-NH+1+q) = b;  % Store the detected bit.
  end;
  
  % This loop generates candidates from the current survivor states.
  for s1 = 0:NS-1
    for b = 0 : 1
      % b is the tentative value of the next received bit.
      s2 = s1+b*NS; % Survivor s1 --> candidate s2.
      branch_metric = 0;
      for k = 0 : R-1
        code_symbol = encode(s2, G(k+q));
        code_symbol = W(code_symbol+q);
        branch_metric = branch_metric + code_symbol*y(t*R+k+q);
        if verbose
          fprintf( 1, 'branch_metric+= %g*%g=%g\n', code_symbol, y(t*R+k+q),code_symbol*y(t*R+k+q)); 
        end;
      end;
      if verbose
        fprintf( 1, 'branch _metric = %g\n', branch_metric);
      end;
      
      MB(s2+q) = branch_metric;
      MC(s2+q) = MS(s1+q) + MB(s2+q);
      HC(s2+q) = HS(s1+q);
      AC(s2+q) = AS(s1+q);
    end;
  end;
  
  if verbose
    fprintf(1, 't=%d, before choosing new survivors.\n', t);
    data = { MS MC HS HC }; % [ [MS; zeros(NS,1)] MC];
    % printTable2(u, 'data', data, 'top', {'MS', 'MC', 'HS', 'HC'} );
    printTable2(u, 'data', data, 'top', 'MS,MC,HS,HC' );
    fprintf(1, '\n');
  end;
  
  if 0
    % Print the candidate metrics.
    for s1 = 0:NC-1
      fprintf(1, 't=%2d, MC(%2d)=%5.1f\n', t, s1, MC(s1+q));
    end;
    fprintf( 1, '\n');
  end;
  
  % At this point, we have prepared the candidates.  The next step is to choose survivors from the
  % candidates.
  
  % Update the test metrics.
  % new_test_state = floor(test_state/2) + NS*T(t+q);
  new_test_state =  test_state+NS*T(t+q);
  new_test_state = floor(new_test_state/2);
  
  for s1 = 0 : NS-1
    s20 = s1*2;               % Candidate s20 --> survivor s1.
    s21 = s1*2+1;             % Candidate s21 --> survivor s1.
    winner = -1;
    if winner==-1 && AC(s20+q)==1 && AC(s21+q)==0
      winner = s20;  % One candidate is alive (b=0) and the other is not.
    end;
    if winner==-1 && AC(s20+q)==0 && AC(s21+q)==1
      winner = s21;  % One candidate is alive (b=1) and the other is not.  This shouldn't actually happen.
      warning(u, 'This should not happen.'); % Neither candidate is alive; this can happen in the early stages.  Fall through to other decision rules.
      keyboard;
    end;
    if winner==-1 && AC(s20+q)==0 && AC(s21+q)==0
      %warning(u, 'This should not happen.'); % Neither candidate is alive; this can happen in the early stages.  Fall through to other decision rules.
      %keyboard;
    end;
    if winner==-1 && t>=L-1 && C(t-L+1+q)==0
      winner = s20; % Constrained 0.
    end;
    if winner==-1 && t>=L-1 && C(t-L+1+q)==1
      winner = s21; % Constrained 1.
    end;
    if winner==-1 && MC(s20+q)>= MC(s21+q)
      winner = s20; % Bit 0 has a better metric.
    end;
    if winner==-1
      winner = s21; % Bit 1 has a better metric.
    end;
    b = mod(winner,2);  % This is the value of the bit we have decided on, for this survivor.
    MS(s1+q) = MC(winner+q);
    AS(s1+q) = AC(winner+q);
    HS(s1+q) = floor(HC(winner+q)/2) + 2^(NH-1)*b;
    
    % Calculate the test metric.
    if s1==new_test_state
      if T(t+q)==0
        MT(t+q) = MC(s20+q) - MC(s21+q);
      elseif T(t+q)==1
        MT(t+q) = MC(s21+q) - MC(s20+q);
      else
        error('Invalid value for test bit.');
      end;
    end;
    
    if 0
      % Print the test metric.
      fprintf(1, 't=%2d, MT(%2.0f) = %5.1f\n', t, t, MT(t+q));
    end;
    
    % Calculate the test metric.
    %if s1==new_test_state && t>=L-1
    %  if T(t-L+1+q)==0
    %    MT(t-L+1+q) = MC(s20+q) - MC(s21+q);
    %  elseif T(t-L+1+q)==1
    %    MT(t-L+1+q) = MC(s21+q) - MC(s20+q);
    %  else
    %    error('Invalid value for test bit.');
    %  end;
    %end;
    % Done calculating the test metric.
    
    
  end;
  test_state = new_test_state;
end;
% Retrieve the detected bits.

% At this point, the detected data bits are in HS(0+q), and in D too, if there were too many to fit
% in to HS.

nh = min(NB, NH); % Number of bits in HS(0).
nd = min(NB-nh,0); % Number of bits in D.
h = HS(0+q);  % Retrieve the history.
if nh<NH
  h = h / 2 ^ (NH-nh);
end;
for k = 0 : nh-1
  D(end+1) = mod(h,2);  % We should preallocate space in D instead - allocate to NB, then fill in these last few values from H.
  h = floor(h/2);
end;
% At this point, D should have the detected bits.

% Calculate the number of symbol corrections.
expected_code_symbols = convolutionalEncoder(u, ...
  'data bits', D, ...
  'pseudo-octal encoder', ...
  'outputs', 'code symbols', ...
  'add flush bits', 1, ...
  'symbol alphabet', [ 0 1 ] );
received_code_symbols = zeros(length(expected_code_symbols));

% Quantize each received symbol to the nearest value from the symbol alphabet.
for k = 1 : length(expected_code_symbols)
  d0 = abs(y(k)-symbol_alphabet_arg(1));
  d1 = abs(y(k)-symbol_alphabet_arg(2));
  if d1<d0, expected_code_symbols(k)=1; end;
end;
num_symbol_errors = sum(expected_code_symbols~=received_code_symbols);
symbol_error_indices = find(expected_code_symbols~=received_code_symbols);
if ~isempty(symbol_error_indices) symbol_error_indices = symbol_error_indices - 1; end;
% To do: Return symbol_error_indices too, it might be useful to the user.  
% Done calculating the number of symbol corrections.


% Assign output arguments.
outputs = split(u, outputs_arg, ',', 'trim');
n = length(outputs);
assertion(u, n==nargout);
varargout = cell(n,1);
for k = 1 : n
  switch outputs{k}
    case 'detected bits',          varargout{k} = D;
    case 'bit metrics',            varargout{k} = MT;
    case 'num corrected symbols',  varargout{k} = num_symbol_errors; % warning(u, 'num corrected symbols not currently supported.');
    otherwise
      error('Invalid output.');
  end;
end;
% Done assigning output arguments.

return;

%---------------------------------------------------------------------------------------------------
% encode
%---------------------------------------------------------------------------------------------------
function y = encode(state, encoder)
% Performs the core encoding for convolutional encoding.
%
% Inputs:
% state is an integer whose binary value represents the encoder state.  Its MSB, with weight
% 2^(L-1), where L is the encoder constraint length, is the newest bit.  Its LSB is the oldest bit
% in the encoder.
%
% encoder is an integer whose binary value represents the encoder connections for one encoder
% branch or path.
%
x = bitand(state, encoder);
n = 0;
loop_count = 0;
while x>0
  n = n + mod(x,2);
  x = floor(x/2);
  loop_count = loop_count + 1;
  if loop_count>100
    error('This should not happen.');
  end;
end;
y = mod(n,2);
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


