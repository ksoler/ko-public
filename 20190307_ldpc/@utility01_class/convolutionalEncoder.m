function varargout = convolutionalEncoder(u, varargin)
% [outputs...] = convolutionalEncoder(u, inputs...)
% Performs convolutional encoder and related functions.
%
%%%%%%%%%%
% Inputs %
%%%%%%%%%%
% u is a vestigial utility01_class object.  It is only used to invoke the
% namespace of the utility01_class member functions.
%
% The remaining inputs are tag/value pairs.  The tags are:
%   'data bits'
%   'pseudo-octal encoder'
%   'outputs'
%   'add flush bits'
%   'symbol alphabet'
%
%%%%%%%%%%%
% Outputs %
%%%%%%%%%%%
% See the description of 'outputs' argument.  The available outputs are:
%    'code symbols'
%    'constraint length'
%
%%%%%%%%%
% Notes %
%%%%%%%%%
% This function encodes data in one shot, as a single block.  You will need
% to use a different function or class if you want the encode a stream of
% data, one piece at a time.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Detailed Descriptions of Input Tags %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 'data bits' are data bits to be encoded.
%
% 'pseudo-octal encoder' is a 1D array of integers.  This function
% interprets each base-10 digit as 3 bits of encoder polynomial.  Thus, the
% integer 15 is 1,101.  Why?  This lets humans type and read these numbers
% as if they were octal, which is a common format for encoder polynomials,
% while each can be stored as a single element of a numeric matlab array.
%
% 'outputs' is a string list of requested output arguments, separated by
% commas.  Leading and trailing spaces around each argument are discarded.
%
% 'add flush bits' is 0 or 1.  If it is 0, then this function does not
% append flush bits to the data bits before encoding.  You might use this
% option if you don't want flush bits, or if they have already been added
% to the data.  If the value is 1, then a number of flush bits, equal to 
% one less than the constraint length, is appended to the data bits.  If
% not specified, then the default value of this argument is 1.
% 
% 'symbol alphabet' is a 2-element vector.  The first element is the value to use when
% the encoder outputs a binary 0, and the second element is the value to use when the encoder
% outputs a binary 1.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Detailed Descriptions of Outputs %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 'code symbols' is a 1D numeric array of values 0,1.  These are the result
% of encoding the data bits.
%
% 'constraint length' is the constraint length of the encoder.
%
%%%%%%%%%%%%%%%%%%%%%%%
% Future Improvements %
%%%%%%%%%%%%%%%%%%%%%%%
% Consider allowing the user to specify or request the encoder polynomial
% in other formats, such as binary, or an octal string.
% Add an option which produces diagnostic output showing the encoder state
% and intermediate values as each bit is processed, maybe even an
% animation.
%

q = 1; % 1 for Matlab arrays, 0 for C.

% Retrieve input arguments.
getArg( u, ...
    'data bits           ->x        ', [],               ...
    'pseudo-octal encoder->g        ', 1,                ...
    'outputs             ->outputs  ', {'code symbols'}, ...
    'add flush bits      ->add_flush', 1,                ...
    'symbol alphabet     ->symbol_arg', [0 1] );
% Done retrieving input arguments.

% Check arguments.
check(u, x,         'binary vector'      );
check(u, g,         'positive int vector');
check(u, outputs,   'string'             );
check(u, add_flush, 'binary scalar'      );
% Done checking arguments.

% Parse the encoder.
[ G, L ] = parsePseudoOctalEncoder( u, 'encoder', g, 'outputs', 'packed binary encoder, constraint length');
r = length(G); % Code ratio, reciprocal of code rate.
% Done parsing the encoder.

% Add optional flush bits.
if add_flush
    x = [ x(:); zeros(L-1,1) ];
end;
% Done adding optional flush bits.

% Encode data bits.
nx = length(x); % Number of data bits, including flush, if any.
ny = r * nx;    % Number of code symbols.
y = zeros(r*nx, 1);
state = 0;
ty = 0;

for tx = 0 : nx-1
    % Shift in a new data bit.
    state = floor(state/2);
    state = state + 2^(L-1) * x(tx+q);
    % Done shifting in a new bit.

    for j = 0 : r-1
        a = bitand( state, G(j+q) );
        b = xor_all(a);
        y(ty+q) = b;
        ty = ty + 1;
    end;
    
end;

% Map encoder output to symbol alphabet.
for ty = 0 : ny-1
  y(ty+q) = symbol_arg(y(ty+q)+q);
end;

% Assign outputs.
args = split(u, outputs, ',', 'trim');
args = removeEmptyCells(u,args);
n = length(args);
if n~=nargout
    error('Invalid number of output arguments.');
end;
varargout = cell(n,1);
for k = 1 : n
    switch args{k}
        case 'code symbols',      varargout{k} = y;
        case 'constraint length', varargout{k} = L; 
        otherwise
            error('Invalid output argument.');
    end;
end;
% Done assigning outputs.
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function y = xor_all(x)
% y = xor_all
% Performs XOR (exclusive OR) operation on all bits of a number.
%
% Inputs:
% x is a nonnegative integer.
%
% Outputs:
% y is 0 or 1.  It is the result of XORing together all of the digits of 
% the base-2 representation of x.
%
u = utility01_class;
checkScalarInt(u,x);
y = 0;
i = 0;
while x > 0 && i < 100
    i = i + 1;
    y = y + mod(x,2);
    x = floor(x/2);
end;
if i>=100
    error('This should not have happened.');
end;
y = mod(y,2);
return;
% End of xor_all().


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


