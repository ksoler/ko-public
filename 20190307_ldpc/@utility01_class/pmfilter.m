function varargout = pmfilter(u, varargin)
% [outputs...] = pmfilter(u, inputs...)
% Parks-McClellan FIR filter design.
%
% u is a vestigial utility01_class object, which serves to invoke the
% utility class's namespace.  The other inputs are tag/value sets.
%   'sample rate'
%   'pass'
%   'stop'
%   'outputs'
%   'verbose'
%   'plots'
%
% The available outputs are:
%   'taps'
%   'worst amplitude'
%   'worst frequency'
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Detailed Description of Inputs %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 'sample rate' sample_rate
% A single numeric argument specifies the sample rate for the filter.
%
% 'pass' start_freq, end_freq, gain, ripple
% Four numeric arguments.  Start and end frequencies of passband, passband
% gain in dB, and ripple specification in dB.
%
% 'stop' start_freq, end_freq, rejection
% Three numeric arguments.  Start and end frequencies of stopband, and 
% stopband rejection in dB.
%
% 'outputs' output_list
% One string argument.  Names of requested output arguments, with optional
% whitespace, separated by commas.
%
% 'verbose'
% This is 0 or 1.  It enables verbose output.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Detailed Description of Outputs %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 'taps'
% Filter taps, in a Matlab array.
%
% 'worst amplitude'
% Worst-case deviation of filter response from amplitude specifications, in
% dB.  If the filter fails to meet specifications somewhere, this number
% will be positive.  If the filter barely meets specifications, this is
% zero.  If the filter meets specifications with some margin, this number
% is negative.
%
% 'worst frequency'
% Frequency, in Hertz, corresponding to 'worst amplitude'.
%
% (End of detailed description of outputs.) 
%
% Future Improvements:  Allow caller to provide a set of filter taps, and
% this function checks them against the filter specifications.
% Verbose mode.
% Diagnostic plots.
% Allow caller to specify the filter order.
% The default value for 'outputs', should be 'taps'.
% Design a signal-class utility function that designs a pm filter and applies it to
% an input signal.

% Parse the plots option.
getArg_v2(u, 'plots->plots', 0, '...');

% Parse the sample rate.
getArg_v2(u, 'sample rate->sampleRate', '...' );
check(u, sampleRate, 'scalar real positive');

% Parse the requested outputs.
getArg_v2(u, 'outputs->outputs', {''}, '...' );
check(u, outputs, 'string');

getArg_v2(u, 'verbose->verbose', 0, '...');
check(u, verbose, 'scalar binary');
%
% Parse the passbasnds.
num_passbands = 0;
passband(1).freq = [];  % Two-element vector of passband start and end frequencies.
passband(1).gain = [];
passband(1).ripple = [];
while findInList(u, 'pass', varargin)
    num_passbands = num_passbands + 1;
    getArg_v2(u, 'pass->startFreq,endFreq,gain,ripple', '...');
    check(u, startFreq, 'scalar real nonnegative');
    check(u, endFreq,   'scalar real nonnegative');
    assertion(u, startFreq<endFreq);
    check(u, gain,   'scalar real');
    check(u, ripple, 'scalar real positive');
    passband(num_passbands).freq = [ startFreq endFreq ];
    passband(num_passbands).gain = gain;
    passband(num_passbands).ripple = ripple;
end;
assertion(u, num_passbands>0);
% Done parsing the passbands.

% Parse the stopbands.
num_stopbands = 0;
stopband(1).freq = [];  % Two-element vector of stopband start and end frequencies.
stopband(1).rejection = [];
while findInList(u, 'stop', varargin)
    num_stopbands = num_stopbands + 1;
    getArg(u, 'stop->startFreq, endFreq, rejection', '...');
    check(u, startFreq, 'scalar real nonnegative');
    check(u, endFreq,   'scalar real nonnegative');
    assertion(u, startFreq<endFreq);
    check(u, rejection, 'scalar real');  % This can be negative, you know.
    stopband(num_stopbands).freq = [startFreq, endFreq];
    stopband(num_stopbands).rejection = rejection;
end;
assertion(u, isempty(varargin));

% We won't require one or more stopbands - maybe the filter just has a
% number of passbands at different gains.
% Done parsing the stopbands.

% Put all passbands and stopbands into a unified data structure.
bands(1).freq = [];  % frequency band edges for firpmord function
bands(1).gain = [];  % band gain value firpmord function
bands(1).dev  = [];  % band deviation value for firpmord function

num_bands = 0;
for k = 1 : num_passbands
    num_bands = num_bands + 1;    
    bands(num_bands).freq = passband(k).freq;
    [g,d] = computeLinearRippleFactor(passband(k).gain, passband(k).ripple);
    bands(num_bands).gain = g;
    bands(num_bands).ripple = d;
end;

for k = 1 : num_stopbands
    num_bands = num_bands + 1;
    bands(num_bands).freq = stopband(k).freq;
    bands(num_bands).gain = 0; % For a stopband, the nominal gain is 0.
    bands(num_bands).ripple = 10^(-stopband(k).rejection/20);
end;
% Done putting all passbands and stopbands into a unified data structure.

% Sort bands into ascending order.
startFreq = zeros(num_bands,1);
for k = 1 : num_bands
    startFreq(k) = bands(k).freq(1);
end;
[dummy,i] = sort(startFreq);
bands = bands(i);
% Done sorting bands into ascending order.

% Extend band edges to 0 Hz and nyquist frequency.
if bands(1).freq(1) ~=0
    if verbose
        fprintf( 1, 'In %s, reducing start of first band from %d to 0 Hz.\n', whoAmI(u,'my name'), bands(1).freq(1));
    end;
    bands(1).freq(1) = 0;
end;
if bands(end).freq(2) ~= sampleRate/2
    if verbose
        fprintf( 1, 'In %s, adjustng end of last band by %d, from %d to %d Hz.\n', ...
            sampleRate/2 - bands(end).freq(2), bands(end).freq(2), sampleRate/2);
    end;
    bands(end).freq(2) = sampleRate/2;
end;
% Done extending band edges to 0 Hz and nyquist frequency.

% Put design parameters into vectors.
f = zeros(2*num_bands,1);  % Frequency band edges.
a = zeros(num_bands,1);    % gain values
d = zeros(num_bands,1);    % deviation values
for k = 0 : num_bands-1
    f(2*k+[1 2]) = bands(k+1).freq;
    a(k+1) = bands(k+1).gain;
    d(k+1) = bands(k+1).ripple;
end;
% Check that frequency bands do not touch nor overlap.
assertion(u, all(diff(f)>0));
% Remove the implied start and end frequencies.
f = f(2:end-1);
% Done putting design parameters into vectors.


% Estimate the order of filter required.
[n0, f0, a0, w0 ] = firpmord( f, a, d, sampleRate);

if n0 < 3
    warning(u, 'firpmord returned a filter order of %d but firpm requires at least 3.\n', n0 );
    n0 = 3;
end;

if n0 > 1000
    new_n0 = 1000;
    warning(u, 'n0 is too big!  Adjusting from %d to %d.\n', n0, new_n0);
    n0 = new_n0;
end;

% Design the filter.
b = firpm(n0, f0, a0, w0);

% Check if the filter meeets specifications.
[ ok, worstFreq, worstAmplitude ] = checkFilterPerformance( ...
    'passbands', passband, ...
    'stopbands', stopband, ...
    'sample rate', sampleRate, ....
    'taps', b );

retry_count = 0;
while ~ok  % Loop for repeated filter design attempts.
    if verbose
        fprintf( 1, 'Filter with %d taps failed by %.1f dB at %g Hz.\n', length(b), worstAmplitude, worstFreq);
    end;

    if plots
  figure(1);
  clf;
  plotFilterSpecAndResponse( ...
        'taps', b, ...
        'sample rate', sampleRate, ...
        'passbands', passband, ...
        'stopbands', stopband );
   pause(1.1);
    end;
    
   n0 = n0 + 1;
   b = firpm(n0, f0, a0, w0);

   % Check if the filter meeets specifications.
   [ ok, worstFreq, worstAmplitude ] = checkFilterPerformance( ...
      'passbands', passband, ...
      'stopbands', stopband, ...
      'sample rate', sampleRate, ....
      'taps', b );
   retry_count = retry_count+ 1;
   if retry_count > 100
      error('Too many retries for designing filter - giving up.');
   end;
end;
if verbose
    fprintf(1, 'Filter with %d taps passed specifications with margin %.1f dB at %g Hz.\n', length(b), -worstAmplitude, worstFreq);
end;

% Plot the final filter design.

if plots
  figure(1);
  clf;
  plotFilterSpecAndResponse( ...
        'taps', b, ...
        'sample rate', sampleRate, ...
        'passbands', passband, ...
        'stopbands', stopband );
end;
% Done plotting the final filter design.


% Assign output arguments.
outputs = split(u, outputs, ',', 'trim');
outputs = removeEmptyCells(u, outputs);
n = length(outputs);
if n~= nargout
  error('Unexpected number of outputs.');
end;
for k = 1 : n
  switch outputs{k}     
     case 'taps'
        varargout{k} = b;
    case 'worst amplitude'
       varargout{k} = worstAmplitude;
     case 'worst frequency'
       varargout{k} = worstFreq;
     otherwise
       error('Unsupported output requested.');
    end;
end;
return;


% Done assigning output arguments.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function  plotFilterSpecAndResponse( varargin )
% Plots a filter response and passband/stopband specifications.
%
% The inputs  are tag/value pairs.
% Inputs:  passbands, stopbands, sample rate, taps.
% Outputs: (none)
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Detailed Description of Inputs %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 'passbands'
% Array of structures of passband information.  One element for each
% passband.  The fieds are freq, gain, ripple.  freq is a 2-element vector
% containing the band start and end frequencies in Hertz.  gain is the desired 
% band gain in dB.  Ripple is the peak-peak band ripple specification, in dB.
%
% 'stopbands'
% Array of structures of stopband information.  One element for each
% stopband.  The fields are freq, rejection.  freq is a 2-element vector 
% containing the band start and end frequencies in Hertz.  rejection is the
% minimum rejection specification in dB.
%
% 'sampleRate'
% This is the sample rate of the filter, in Hertz.
%
% 'taps'
% This is a Matlab vector of the FIR filter taps.
%

u = utility01_class;
Hz = 1;
dB = 1;

% Parse inputs.
getArg_v2( u, ...
    'taps       ->b        ', ...
    'passbands  ->passband ', ...
    'stopbands  ->stopband ', ...
    'sample rate->sampleRate' );
holdstate = ishold;
f = linspace(0, sampleRate/2, 2000);
h = freqz(b, 1, f, sampleRate);

% Remove nulls from the spectrum.
i = find(h==0);
f(i) = [];
h(i) = [];
h = 20*log10(abs(h));

plot(f, h);
hold on;
for k = 1 : length(passband)
    plot(passband(k).freq, [1 1]*(passband(k).gain+passband(k).ripple/2), 'g+-');
    plot(passband(k).freq, [1 1]*(passband(k).gain-passband(k).ripple/2), 'g+-');    
    text(passband(k).freq(1), passband(k).gain+passband(k).ripple/2, 'passband', 'VerticalAlignment', 'bottom');
end;

for k = 1 : length(stopband)
    plot(stopband(k).freq, -[1 1]*stopband(k).rejection, 'r+-');
    text(stopband(k).freq(1), stopband(k).rejection, 'stopband', 'VerticalAlignment', 'bottom');
end;

% Find the minimum and maximum gain values specified in the stopbands and
% passbands.
min_gain = [];
max_gain = [];

for k = 1 : length(passband)
if isempty(min_gain) || passband(k).gain < min_gain
    min_gain = passband(k).gain;
end;
if isempty(max_gain) || passband(k).gain > max_gain
    max_gain = passband(k).gain;
end;
end;
for k = 1 : length(stopband)
if isempty(min_gain) || -stopband(k).rejection < min_gain
    min_gain = -stopband(k).rejection;
end;
if isempty(max_gain) || -stopband(k).rejection > max_gain
    max_gain = -stopbandband(k).rejection;
end;
end;
% Now, min_gain and max_gain are the minimum and maximum gain
% specifications for all of the passbands and stopbands.

axis([ 0 sampleRate/2 min_gain-10*dB max_gain+10*dB ]);

xlabel('frequency (Hz)');
ylabel('frequency response (dB)');

if holdstate
    hold on;
else
    hold off;
end;
return;




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ ok, worstFreq, worstAmplitude] = checkFilterPerformance(varargin)
% [ok, worstFreq, worstAmplitude] = checkFilterPerformance(inputs...)
% Checks if a filter meets performance specifications.
%
% The inputs  are tag/value pairs.
% Inputs:  passbands, stopbands, sample rate, taps.
% Outputs: ok, worstFreq, worstAmplitude.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Detailed Description of Inputs %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 'passbands'
% Array of structures of passband information.  One element for each
% passband.  The fieds are freq, gain, ripple.  freq is a 2-element vector
% containing the band start and end frequencies in Hertz.  gain is the desired 
% band gain in dB.  Ripple is the peak-peak band ripple specification, in dB.
%
% 'stopbands'
% Array of structures of stopband information.  One element for each
% stopband.  The fields are freq, rejection.  freq is a 2-element vector 
% containing the band start and end frequencies in Hertz.  rejection is the
% minimum rejection specification in dB.
%
% 'sampleRate'
% This is the sample rate of the filter, in Hertz.
%
% 'taps'
% This is a Matlab vector of the FIR filter taps.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Detailed Description of Outputs %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ok is 1 if the filter meets or exceeds specifications, 0 otherwise.
% 
% worstFreq is the frequency of the worst-case deviation from
% specifications, or if specifications are met, the closest point to
% failing specifications.
%
% worstAmplitude is the amount in dB by which the filter fails specifications.  
% If the filter meets specifications, then this is the negative of the
% margin by which the filter passes specifications.
%

% Define constants.
u = utility01_class;  
dB = 1;
large_dB = 100*dB; % An gain difference of Inf dB is replaced by this value.
% Done defining constants.

% Parse arguments.
getArg_v2( u, ...
    'passbands  ->passband  ',     ...
    'stopbands  ->stopband  ', [], ... % I'll try and make this work even if there are no stopbands.
    'sample rate->sampleRate',     ...
    'taps       ->b         ' );
% Done parsing arguments.

overallAmplitudeError = [];
overallFreqError = [];

% This is the fundamental frequency of the filter, which is a good
% indication of the spacing at which we need to sample the frequency
% response.
fundamental = sampleRate / length(b);

% Step through all of the passbands.
for j = 1 : length(passband)
    m = 3*ceil(diff(passband(j).freq)/fundamental); % number of frequency points
    f1 = linspace( passband(j).freq(1), passband(j).freq(2), m);
    h = freqz(b, 1, f1, sampleRate);
    h = abs(h);
    [ linearGain, linearRipple ] = computeLinearRippleFactor(passband(j).gain, passband(j).ripple);
    maximum_gain = 20*log10(linearGain+linearRipple);
    minimum_gain = 20*log10(linearGain-linearRipple);
    
    % Set bandAmplitudeError, bandFreqError to the worst-case error in the
    % current band.
    bandAmplitudeError = [];
    bandFreqError = [];
    for k = 1 : length(h)
        if h(k)==0
            amplitudeError = large_dB;
        else
            h_dB = 20*log10(h(k));
            if h_dB < minimum_gain
                % The filter is below the minimum gain specification, at the
                % current frequency.
                amplitudeError = minimum_gain - h_dB;
            elseif h_dB > maximum_gain
                % The filter is above the maximum gain specification, at
                % the current frequency.
                amplitudeError = h_dB - maximum_gain;
            else
                % The filter passes specificaions at the current frequency.
                amplitudeErrorLow = h_dB - minimum_gain;
                amplitudeErrorHigh = maximum_gain - h_dB;
                amplitudeError = -min(amplitudeErrorLow, amplitudeErrorHigh);
            end;
        end;
        if isempty(bandAmplitudeError) || amplitudeError > bandAmplitudeError
            % Update the worst-case amplitude error for the current
            % passband.
            bandAmplitudeError = amplitudeError;
            bandFreqError = f1(k);
        end;
        
    end;
    if isempty(overallAmplitudeError) || bandAmplitudeError > overallAmplitudeError
        % Update the worst-case amplitude error for all bands.
        overallAmplitudeError = bandAmplitudeError;
        overallFreqError = bandFreqError;
    end;
end;
% Done stepping through all of the passbands.


% Step through all of the stopbands.
for j = 1 : length(stopband)
    m = 3*ceil(diff(stopband(j).freq)/fundamental); % Number of test points in the current stopband.
    f1 = linspace(stopband(j).freq(1), stopband(j).freq(2), m);
    h = freqz(b, 1, f1, sampleRate);
    h = abs(h);
    bandAmplitudeError = [];
    bandFreqError = [];
    desiredGain = -stopband(j).rejection;
    for k = 1 : length(h)
        if h(k)==0
            amplitudeError = -large_dB;  % If the gain is 0 in a stopband, we have met specifications with a large margin.
        else
            h_dB = 20*log10(h(k));
            amplitudeError = h_dB - stopband(j).rejection;
        end;
        if isempty(bandAmplitudeError) || amplitudeError > bandAmplitudeError
            % Update the worst-case amplitude error for the current passband.
            bandAmplitudeError = amplitudeError;
            bandFreqError = f1(k);
        end;
    end;
    if isempty(overallAmplitudeError) || bandAmplitudeError > overallAmplitudeError
        % Update the worst-case amplitude error for all bands.
        overallAmplitudeError = bandAmplitudeError;
        overallFreqError = bandFreqError;
    end;
end;
% Done stepping through all of the stopbands.

if overallAmplitudeError > 0
   ok = 0;
else
    ok = 1;
end;

worstFreq = overallFreqError;
worstAmplitude = overallAmplitudeError;
return;
% End of checkFilterPerformance().


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [g,d] = computeLinearRippleFactor(gain_dB, ripple_dB)
% [g,d] = computeLinearRippleFactor(gain_dB, ripple_dB)
% Computes a gain and ripple factor required by firpmord function.
%
% Inputs:
% gain_dB is the passband gain in dB.
% ripple_dB isthe pek-to-peak ripple value in dB.
%
% Outputs:
% g isthe linear passband gain corresponding to gain_dB.
% d is the linear peak-to-peak ripple value corresponding to ripple_dB.
%
u = utility01_class;
check(u, gain_dB, 'scalar real');
check(u, ripple_dB, 'scalar real');
g = 10 ^ (gain_dB/20);
numerator = 10^(ripple_dB/20) - 1;
denominator = 10^(ripple_dB/20) + 1;
d = g*numerator / denominator;
return;
% End of computeLinearRippleFactor().


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


