function h = fileOpen(name, mode)
% h = fileOpen(name, mode)
% Opens a file.  Prints error message if there is a problem.
%
% Inputs:
% name is the filename of the file to open.
%
% mode is the mode.  This is like the permissions argument of the Matlab's
% fopen function.  Use 'rt' and 'wt' for text files.
%
% Outputs:
% h is a handle for the opened file.
%
% Copyright 2019, Kevin Oler, All rights reserved.

[h, msg] = fopen(name, mode);
if h == -1
    fprintf( 1, 'Error opening this file: "%s".\n', name);
    fprintf( 1, '%s\n', msg);
    error('Failed to open file.');
end;
return;

