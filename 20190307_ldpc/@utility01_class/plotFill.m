function h = plotFill(u, varargin)
% h = plotFill(u, varargin)
% Shades a plot region.
% 
% Inputs: x, y, colour, x limits, y limits, fill
% 
%---------------------------------------------------------------------------------------------------
% Detailed Description of Inputs
%
% u is a vestigial utility01_class object.  The remaining arguments are
% tag/value pairs.
%
% 'x' is a 1D array of x co-ordinates of a line.
%
% 'y' is a 1D array of y co-ordinates of a line.
%
% 'colour' is a colour designation, either a single character as described
% by 'help plot', or an RGB vector.
%
% 'x limits' is an array of the lower and upper x limits for the filled area.  Use NaN to
% leave a limit unspecified.
%
% 'y limits' is an array of the lower and upper  y limits for the filled
% area.  Use NaN to leave a limit unspecified.
%
% 'fill' specifies the direction to fill from the curve:  'above', 'below'.
% It might support 'left', 'right' in the future.
%
getArg( u, ...
  'x        ->x_arg', ...
  'y        ->y_arg', ...
  'colour   ->colour_arg->colour_present', {'b'}, ...
  'color    ->color_arg ->color_present ', {'b'}, ... 
  'x limits ->x_limits_arg', [NaN NaN], ...
  'y limits ->y_limits_arg', [NaN NaN], ...
  'fill     ->fill_arg', {'below'} );

% This function accepts either spelling of colour/color.
if color_present && ~colour_present
  colour_arg = color_arg;
end;
% Now, colour_arg holds the colour that we want to use.

assertion(u, numel(x_limits_arg)==2);
assertion(u, numel(y_limits_arg)==2);
assertion(u, isnumeric(x_limits_arg));
assertion(u, isnumeric(y_limits_arg));
checkVectorReal(u, x_arg);
checkVectorReal(u, y_arg);

if isnan(x_limits_arg(1))
  x_limits_arg(1) = min(x_arg);
end;
if isnan(x_limits_arg(2))
  x_limits_arg(2) = max(x_arg);
end;
if isnan(y_limits_arg(1))
  y_limits_arg(1) = min(y_arg);
end;
if isnan(y_limits_arg(2))
  y_limits_arg(2) = max(y_arg);
end;

% Remove samples outside of the specified limits.
i = find(x_limits_arg(1) <= x_arg & x_arg <= x_limits_arg(2));
x_arg=  x_arg(i);

i = find(y_limits_arg(1) <= y_arg & y_arg <= y_limits_arg(2));
y_arg=  y_arg(i);

switch fill_arg
  case 'above'
    y_base = y_limits_arg(2);
  case 'below'
    y_base = y_limits_arg(1);
  otherwise
    error('Unsupported option.');
end;

x_arg = x_arg(:);
y_arg = y_arg(:);
x = [ x_arg; x_arg(end); x_arg(1) ];
y = [ y_arg; y_base; y_base ];
h = patch(x, y, colour_arg);

if nargout == 0
  clear h;
end;
return;




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% 