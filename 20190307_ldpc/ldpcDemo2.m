function ldpcDemo2
%
% See this document:
% AN INTRODUCTION TO ERROR CORRECTING CODES, Part 3
% Jack Keil WolfECE 154 CSpring 2010
%


%---------------------------------------------------------------------------------------------------
% Configure the test.

% Received soft code bit values, nominally 0 or 1.
rx_code_symbols = [ 0.9 0.5 0.4 0.3 0.9 0.9 0.9 0.9 0.9 0.9 0.9 0.9 ];

num_iterations = 7;

% Parity check matrix.
H = [ ...
  0 0 1 0 0 1 1 1 0 0 0 0
  1 1 0 0 1 0 0 0 0 0 0 1
  0 0 0 1 0 0 0 0 1 1 1 0
  0 1 0 0 0 1 1 0 0 1 0 0
  1 0 1 0 0 0 0 1 0 0 1 0
  0 0 0 1 1 0 0 0 1 0 0 1
  1 0 0 1 1 0 1 0 0 0 0 0
  0 0 0 0 0 1 0 1 0 0 1 1
  0 1 1 0 0 0 0 0 1 1 0 0 ];

% End of configuration.
%---------------------------------------------------------------------------------------------------

u = utility01_class;
num_symbols = size(H,2);
num_nodes   = size(H,1);
assertion(u,length(rx_code_symbols)==num_symbols);
saved_output = BigMatrixReal_v3(0,num_symbols);

% metric1 holds soft metrics which are the inputs to the nodes.
% metric2 holds soft metrics which after they have been updated by the nodes.

% Initialize metric1 with received values.
metric1 = H;
for j = 1 : num_symbols
  metric1(:,j) = metric1(:,j) * rx_code_symbols(j);
end;

for n = 1 : num_iterations
  
  % Feed metric1 into the nodes, results go into metric2.
  metric2 = zeros(size(H)); % Holds soft values, after updates from nodes, before combining reuslts from multiple nodes.
  for i = 1 : num_nodes
    metric2(i,:) = node_calculation(H(i,:), metric1(i,:), i);
  end;
  
  % Now, combine results form nodes into the working code symbols.
  for j = 1 : num_symbols
    [metric1(:,j), final_metrics(j)] = symbol_calculation(H(:,j), metric2(:,j), rx_code_symbols(j), j);
  end;
  
  
  appendRow(saved_output, final_metrics(:).');
  plotColumns(saved_output);
  pause;
  
  fprintf(1, '\n-------------------------------------------------------------\n');
  
  fprintf(1, 'Iteration %d\n', n);
  display(metric1);
  display(metric2);
end;
figure(1);
clf;
plotColumns(saved_output);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function output_soft_bits = node_calculation(connections, input_soft_bits, node_index)
% output_soft_bits = node_calculation(connections, input_soft_bits)
% This function performs the calculations associated with a checksum node, or equivalently a 
% contstraint, or equivalently a row of the parity check matrix.
%
% Arguments:
% connections is a vector which corresponds to the soft bits.  It has a 1 for each soft bit
% which is connected to the current node, and a 0 for the other soft bits.
%
% input_soft_bits is a vector of soft bit values - all of them, not just the ones connected to the
% node.  For the first decoding iteration, these are the received soft bit values.  For subsequent 
% iterations, these are the soft bit values amalgamated from node calculations, excluding the 
% current (self) node.  Values for bits not connected to the node are ignored.
%
% node_index is the index of the current node.  This is used for diagnostic reports.
%
% output_soft_bits are the soft bit values after updating according to the current node.
% Soft bits which are not connected to the current node are set to zero.

% Here is a description of the soft bits used in this function.
% 1. input_soft_bits
% 2. connected_soft_bits
% 3. current_soft_bit
% 4. other_soft_bits
% 5. output_soft_bits
%
% 1. input_soft_bits is a vector of all of the soft bits, not just the ones connected to the current
% node.  However, soft bits which are not connected to it are ignored by this function (for input
% and output).  For the first decoding iteration, these should be the received soft bit values.  For
% subsequent iterations, these are the soft bits values obtained by combining results from all of
% the nodes.
% 
% 2. connected_soft_bits are the soft bits which are connected to the current node.
%
% 3. curent_soft_bit is one of the connected_soft_bits; a loop goes through all of them one at a
% time.
%
% 4. other_soft_bits are the connected soft bits besides the current_soft_bit.  They are used to
% calculate an updated estimate for current_soft_bit.
%
% 5. output_soft_bits is a vector sized to hold all of the soft bits (not just the ones connected to
% the node).  However, this function only provides updated estimates for the ones which are
% connected to the node.
%
% Note that in this function, "probability" may refer to an heuristic metric which generally
% indicates probability, not a precise probability metric.
%

if nargin < 3
  node_index = [];
end;

u = utility01_class;
verbose = 1;

num_connections = sum(connections); % Number of bits which sum into the current node.
connection_indices = find(connections);
output_soft_bits = zeros(size(input_soft_bits));

connected_soft_bits1 = input_soft_bits(connection_indices); % Probability that each soft bit is a 1.
connected_soft_bits0 = 1 - connected_soft_bits1;            % Probability that each soft bit is a 0. 

% We will loop over all of the connected soft bits.
for current_bit_index = 1 : num_connections
  % Select the metrics for the "other" connected soft bits.
  other_soft_bits0 = connected_soft_bits0( [ (1:current_bit_index-1) (current_bit_index+1:end) ] );
  other_soft_bits1 = connected_soft_bits1( [ (1:current_bit_index-1) (current_bit_index+1:end) ] );
  
  % Next, we will use the "other" bits to estimate the value of our current soft bit.
  % We do this by stepping through all of the binary words which can be formed by hard values of
  % the other bits, and doing the following for each word:  
  % (1) Calculate a probability that the other soft bits match the hard bits.
  % (2) Decide if this word implies a hard value of 0 or 1 for our current bit, based on the node's
  % parity constraint.  
  % (3) Add the corresponding probability to the '0' or '1' sum for the current bit.
  
  num_other_bits = length(other_soft_bits0); % One less than the number of connected bits.
  
  p0 = 0; % Initialize the probability estimates for our current soft bit.
  p1 = 0;
  
  % Loop through all possible combinations or hard bits for the 'other' bits.
  for k_word = 0 : 2^num_other_bits-1
    hard_bits = convert_number2bits(u, k_word, num_other_bits);
    p = 1;  % Probability that hard word matches other soft bits.
    
    % This loop collects probabilities for each of the other hard/soft bits.
    for k_bit = 1 : num_other_bits
      if hard_bits(k_bit)==0
        p = p * other_soft_bits0(k_bit);
      else
        p = p * other_soft_bits1(k_bit);
      end;
    end;
    % Now, p is the probability that the "other" bits match our hard value.
    
    % (2) Decide if this word implies a hard value of 0 or 1 for our current bit, based on the 
    % node's parity constraint.
    isEven = mod(sum(hard_bits),2)==0;
    % If the sum of these hard bits is even, then the current bit must be a 0, because the node
    % requires that the sum of all its input bits is even.
    if isEven
      % The probability we calculated for our selected hard bits corresponds to a '1' value for the 
      % current bit.
      p0 = p0 + p;
    else
      p1 = p1 + p;
    end;
  end;
  % Now, we have finished calculating contributions to p0 and p1 from all of the hard words.
  % We expect that p0 + p1 = 1.
  if abs(p0+p1-1) > 1e-6
    warning(u, sprintf('Unexpected result in %s.', whoAmI(u, 'my name')));
    keyboard;
  end;
  
  output_soft_bits(connection_indices(current_bit_index)) = p1; % Store probability that current bit is a 1.
  
end;
  if verbose && ~isempty(node_index)
    fprintf(1, 'In %s, ', whoAmI(u, 'my name'));
    fprintf(1, 'node %d is connected to code symbols [%s].\n', node_index, printVector(u, connection_indices, 'limit', 10));
    fprintf(1, 'The input metrics for its connections are %s, and the output metrics are %s.\n', ...
      printVector(u, input_soft_bits(connection_indices),  'limit', 10), ...
      printVector(u, output_soft_bits(connection_indices), 'limit', 10));
  end;
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [extrinsic_metrics, final_metric] = symbol_calculation(connections, metrics_from_nodes, rx_metric, symbol_index)
% For one symbol, combines the soft estimates from its connected nodes.
% 
% Arguments:
% connections is a column from the parity check matrix which specifies which nodes are connected to
% the current symbol.
% 
% metrics_from_nodes is a vector of the metrics for the current symbol, calculated by the connected nodes.
%  This has the same size as 'connections', and elements for unconnected nodes are ignored.
% 
% rx_metric is the original received metric for the current code symbol.
%
% symbol_index is the index of the current symbol, used for diagnostics.
%
% extrinsic_metrics is a vector, the same size as in_metrics, which has the extrinsic metrics to be
% given to the node updates.  For each node, the extrinsic metric is obtained by combining
% the contributes from the other nodes, and the received metric.  We provide a separate metric for 
% each connected node.  A node's own contribution is omitted from its updated metric.
%
% final_metric is calculated similarly to the extrinsic metrics, but it uses all of the nodes, and
% the received metric.  At the last decoder iteration, this becomes the final estimate for the
% current code symbol.

u = utility01_class;

num_connections = sum(connections);
connection_indices = find(connections);
extrinsic_metrics = zeros(size(metrics_from_nodes));

for k = 1 : num_connections
  current_node = connection_indices(k);
  other_nodes = connection_indices; 
  other_nodes(k) = [];
  other_metrics = metrics_from_nodes(other_nodes);
  other_metrics(end+1) = rx_metric;  % Add in the received metric.
  K = prod(other_metrics) + prod(1-other_metrics); % Normalization constant.
  extrinsic_metrics(connection_indices(k)) = prod(other_metrics) / K;
end;

all_metrics = metrics_from_nodes(connection_indices);
all_metrics(end+1) = rx_metric;
K = prod(all_metrics) + prod(1-all_metrics);
final_metric = prod(all_metrics) / K;
return;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler                                                  %
% All rights reserved.                                                            %
%                                                                                 %
% Note that the following license and disclaimer, is commonly referred to         %
% the "3-clause BSD license".  This license applies to this file and the          %
% accompanying files provided by the author, unless otherwise noted.              %
%                                                                                 %
% Redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:     %
%     * Redistributions of source code must retain the above copyright            %
%       notice, this list of conditions and the following disclaimer.             %
%     * Redistributions in binary form must reproduce the above copyright         %
%       notice, this list of conditions and the following disclaimer in the       %
%       documentation and/or other materials provided with the distribution.      %
%     * Neither the name of the Kevin Oler nor the                                %
%       names of other contributors may be used to endorse or promote products    %
%       derived from this software without specific prior written permission.     %
%                                                                                 %
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED   %
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE          %
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY                      %
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES      %
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    %
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND     %
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS   %
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
