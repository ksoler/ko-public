function [validation_features, validation_labels] = load_validation_data(features_file, labels_file)
% [validation_features, validation_labels] = load_validation_data(features_file, labels_file)
% Reads validation data from files.
%
% Inputs:
% features_file is the name of the data file that holds the validation features, which is the input
% to the neural network.  In this case, that is image data.
%
% labels_file is the name of the data file that holds the validation labels, which is the expected
% output of the neural network.  In this case, that is digit values, 0-9.
%
% Outputs:
% validation_features is a cell array of digit images.  Each digit image is a 1x784 array of pixel
% values.  The first 28 pixels in each cell are for the first row of the image.
%
% validation_labels is a 1D array of the true digit values.
%
% Copyright 2019, Kevin Oler, All rights reserved.

d = '.\data files';  % Assumed location for data files.

validation_features = fileRead2(fullfile(d, features_file));

% Partition the validation features so there is 1 image of 784 pixels per cell.
n = length(validation_features);
if mod(n,784) ~= 0
  fprintf(1, 'Error, validation features does not contain a whole number of digit images.\n');
  error('Operation failed.');
end;
n = n / 784;
y = cell(n,1);
for k = 0 : n-1
  a = validation_features(k*784 + (1:784)); % Get a vector of pixels for current digit.
  %a = reshape(a,28,28)';  % The first 28 values are the first row of the image.
  y{k+1} = a;
end;
validation_features = y;
% Done partitioning the validation features.

validation_labels   = fileRead2(fullfile(d, labels_file));

if 0
  % Optionally show all of them.
  figure(1);
  clf;
  for k = 1 : n
    a = validation_features{k};
    a = reshape_digit_vector_to_image(obj, a);
    image(a*256);
    fprintf(1,'The digit label is: %d\n', validation_labels(k));
    pause(3);
  end;
end;

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in the
%       documentation and/or other materials provided with the distribution.
%     * The name of the Kevin Oler may not be used to endorse or promote products
%       derived from this software without specific prior written permission.
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
% 
