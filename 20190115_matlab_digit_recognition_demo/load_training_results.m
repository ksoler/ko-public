function [layer1_coefficients, layer1_biases, layer2_coefficients, layer2_biases] = ...
  load_training_results
% Loads neural network coeffiicents and biases from data files.
%
% Outputs:
% layer1_coefficients is a 2D array of the coefficients for layer 1.
% layer1_biases is a 1D array of the biases for layer 1.
% layer2_coefficients is a 2D array of the coefficients for layer 2.
% layer2_biases is a 1D array of the biases for layer 2.
%
% Copyright 2019, Kevin Oler, All rights reserved.

d = '.\data files'; % Assumed location for data files.

layer1_coefficients = fileRead2(fullfile(d,'layer1_coefficients.txt'));
layer1_biases       = fileRead2(fullfile(d,'layer1_biases.txt'      ));
layer2_coefficients = fileRead2(fullfile(d,'layer2_coefficients.txt'));
layer2_biases       = fileRead2(fullfile(d,'layer2_biases.txt'      ));

layer1_coefficients = reshape(layer1_coefficients, 512, 784)';
layer2_coefficients = reshape(layer2_coefficients, 10,  512)';

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in the
%       documentation and/or other materials provided with the distribution.
%     * The name of the Kevin Oler may not be used to endorse or promote products
%       derived from this software without specific prior written permission.
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
% 
