function y = run_layer2_once(coefficients, biases, x)
% y = run_layer2_once
% Runs estimation for layer 2.  This is a dense neural layer with 512 inputs and 10 outputs.
% The activation function is the softmax function.
%
% Inputs:
% coefficients is an array (512,10) of coefficients.  The first column is used to calculate the 
% first element of the output vector.
%
% biases is a vector of bias values, one for each of the 10 outputs.
%
% x is a vector (512) of input values.
%
% Outputs:
% y is a vector (10) of output values.
%
% Copyright 2019, Kevin Oler, All rights reserved.


y = zeros(10,1);

if size(x,2) ~=1
  error('The input should be a column vector.');
end;

for j = 1 : 10
  a = x(:) .* coefficients(:,j);
  a = sum(a);
  a = a + biases(j);
  y(j) = a;
end;

% Normalize the outputs.
denominator = sum(exp(y));
y = exp(y) / denominator;
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in the
%       documentation and/or other materials provided with the distribution.
%     * The name of the Kevin Oler may not be used to endorse or promote products
%       derived from this software without specific prior written permission.
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
% 
 