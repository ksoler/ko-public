function main
% This is the main function for demonstrating neural network estimation.
% Copyright 2019, Kevin Oler, All rights reserved.

data_choices = { ...
  '100 samples' 'validation_features 100.txt' 'validation_labels 100.txt'
  '1K samples'  'validation_features 1k.txt'  'validation_labels 1k.txt'
  '10K samples' 'validation_features 10k.txt' 'validation_labels 10k.txt' };

k = menu('Select the test data.', data_choices{:,1});
fprintf(1, 'You have selected this data:  %s.\n', data_choices{k,1});
validation_features_file = data_choices{k,2};
validation_labels_file   = data_choices{k,3};

data_choice = data_choices{k};
  
[layer1_coefficients, layer1_biases, layer3_coefficients, layer3_biases] = ...
  load_training_results;

[features, labels] = load_validation_data(validation_features_file, validation_labels_file);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% first pass 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perform estimation on all of the validation data in order to compute quality metrics.
qual = zeros(length(labels),1);
for k = 1 : length(labels)
  fprintf(1, 'Runnning neural net for digit %d, which has expected label %d.\n', k-1, labels(k));
  y = run_net_once(layer1_coefficients, layer1_biases, layer3_coefficients, layer3_biases, features{k});
  [y2,i] = sort(y);
  y2 = log10(y2);
  if i(end) == labels(k)+1
    % The net detected the correct digit.
    qual(k) = y2(end) - y2(end-1);
  else
    % The net detected the wrong digit.
    qual(k) = log10(y(labels(k)+1)) - y2(end);
  end;
end;
qual = qual-min(qual);
qual = 0.5 * qual / max(qual);
% Now, the quality metrics are between 0 and 0.5.
order = labels - qual;

% Sort by digit and quality.
[~, i] = sort(order);
features = features(i);
labels = labels(i);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% second pass 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Redo estimation, and generate videos of images and metrics.

for actual_digit = 0 : 9
  % Create the video writer with 1 fps.
  digit_string = actual_digit + '0';
  filename = sprintf('20190115_matlab_digit_recognition_demo %s.avi', actual_digit+'0');
  writerObj = VideoWriter(fullfile(pwd, 'results', filename));
  writerObj.FrameRate = 1.2;
  % open the video writer
  open(writerObj);

  
  for k = 1 : length(labels)
    if labels(k) == actual_digit
      fprintf(1, 'Runnning neural net for digit %d, which has expected label %d.\n', k-1, labels(k));
      
      y = run_net_once(layer1_coefficients, layer1_biases, layer3_coefficients, layer3_biases, features{k});
      
      if (1)
        x = features{k};
        x = reshape_digit_vector_to_image(x) * 256; % Reshape from vector to 2D image.
        
        figure(1);
        setFigureSize(gcf, 30,10);
        clf;
        subplot(1,2,1);
        image(x);
        axis equal;
        title(sprintf('image of digit %d, value %d', k-1, labels(k)));
        
        subplot(1,2,2);
        if 0 
          % Convert data to log scale then plot on linear scale.
          y = log10(y) + 6;
          for j = 1 : length(y)
            y(j) = max(y(j),0);
          end;
          bar2(0:9, y, labels(k)+1);
          ylabel('log10 scale');
        else
          % Plot data on log scale.
          bar2(0:9, y, labels(k)+1);
          set(gca, 'YScale', 'log');
          ax = axis;
          axis([ax(1) ax(2) 1e-6 1e0]);
          ylabel('estimated probability');

        end;

        title('estimated labels');
        xlabel('labels (digit values)');
        
        grid on;
        pause(0.01);
        frame = getframe(gcf);
        writeVideo(writerObj, frame);
        pause(0.01);
      end;
      
    end;
  end; % if labels(k) == actual_digit
  close(writerObj);
end; % for actual_digit = 0 : 9
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in the
%       documentation and/or other materials provided with the distribution.
%     * The name of the Kevin Oler may not be used to endorse or promote products
%       derived from this software without specific prior written permission.
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
% 
