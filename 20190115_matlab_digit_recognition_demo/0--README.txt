================================================================================
Filename:     0--README.txt
Project:      20190115_matlab_digit_recognition_demo
Description:  Matlab demonstration of neural network digit recognition.  
================================================================================


================================================================================
How to Run
================================================================================

1. In MATLAB, set the current directory to the provided MATLAB source code 
(20190115_matlab_digit_recognition_demo).  
2. Invoke the function "main".
3. Select a data size from the menu.
4. The function will process the test data and generate video files of the 
results (in the "results" subfolder").  Note that these video files are also 
included in this respository, so you can view them without running the demo.

In order to run the corresponding TensorFlow example and regenerate the 
data files used by the MATLAB code, see these two files:
  - shell_commands.txt
  - tensorflow_export.py


================================================================================
Neural Network Design
================================================================================
This network has two layers.

Input
  - 784 pixels, from a 28x28 image of a digit.
Layer 1
  - 784 inputs.
  - Fully connected.
  - RelU activation function.
  - 512 outputs.
Layer 2
  - 512 inputs.
  - Fully connected.
  - Softmax activation function.
  - 10 outputs.
Output
  - 10 probability scores, one for each digit, 0-9.
  
The following references may help you understand some terms such as 
"fully connected", "relu", and "softmax".

https://www.tensorflow.org/tutorials/estimators/cnn
https://en.wikipedia.org/wiki/Convolutional_neural_network
http://neuralnetworksanddeeplearning.com/ (especially chapter 1)

A neural network typically requires training and estimation capability, but 
this example only performs estimation.  The training was performed using 
TensorFlow, and the results are stored in the available data files.

This Matlab is indented as a demonstration. Many parameters are hard-coded, 
and it is not optimized for efficiency.


================================================================================
Test Results
================================================================================
The generated videos show the operation of this neural network. Each frame shows
 an image of a handwritten digit, which is the input to the neural network, and 
 a graph of the 10 probability scores for the digits.

In each video, the digits are arranged from best detection quality to worst. 
The score for the correct digit is red, and the others are blue. When a blue bar
 is higher than a red bar, the network has detected an incorrect digit.

Each video shows about 1000 digits at a rate of 1 input per second, so you may 
want to skip ahead while viewing.

Here are links to the test videos.
0. https://youtu.be/ypgIONaAiWg
1. https://youtu.be/T3GfKNc8_jA
2. https://youtu.be/-Ozz0MVouD
3. https://youtu.be/u2thpu6ahbU
4. https://youtu.be/o6JTKQmAeFg
5. https://youtu.be/XGBEUAhtw9o
6. https://youtu.be/zixaHbAorQQ
7. https://youtu.be/CZ_XdBoMBtw
8. https://youtu.be/D3104R5vKZc
9. https://youtu.be/KeL-CdiwXHg

=================================================================================
Source Code
==================================================================================

This section describes selected files.

** main.m **
Entry point for this demonstration.

** tensorflow_export.py **
Python code which you can run (locally, not on Google Colab) in order to 
regenerate the data files (which are already included).  You can edit it to 
select the size of validation data. You will need to rename the output files to
work with the Matlab source code.

** shell_commands.txt **
Commands that I used in a Ubuntu BNU Bash shell, to set up the python 
environment.

This Matlab example is based on the following TensorFlow tutorial example, from 
here:  https://www.tensorflow.org/tutorials/
--------------------------------------------------------------------------------
import tensorflow as tf
mnist = tf.keras.datasets.mnist

(x_train, y_train),(x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

model = tf.keras.models.Sequential([
  tf.keras.layers.Flatten(),
  tf.keras.layers.Dense(512, activation=tf.nn.relu),
  tf.keras.layers.Dropout(0.2),
  tf.keras.layers.Dense(10, activation=tf.nn.softmax)
])
model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

model.fit(x_train, y_train, epochs=5)
model.evaluate(x_test, y_test)
--------------------------------------------------------------------------------


