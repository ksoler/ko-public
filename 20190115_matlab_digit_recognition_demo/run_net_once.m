function y = run_net_once(layer1_coefficients, layer1_biases, layer2_coefficients, layer2_biases, x)
% y = run_net_once(layer1_coefficients, layer1_biases, layer3_coefficients, layer3_biases, x)
% Performs a single estimation run of the neural network.
%
% Inputs:
% layer1_coefficients is a 2D array of the coefficients for layer 1.  Note that layer 1 has 784 
% inputs and 512 outputs.  The dimensions of this array are (784,512).  The inputs are multiplied by
% column k of these coefficients to produce output k from layer 1.
%
% layer1_biases is a 1D array of 512 biases for layer 1.
%
% layer2_coefficients is a 2D array of the coefficients for layer 3.  Its dimensions are (512,10).
%
% layer2_biases is a 1D array of 10 biases for layer 2.
%
% x is is a 1D array (784 values) of input values.
%
% Outputs:
% y is a vector (10) of output values.
%
% Copyright 2019, Kevin Oler, All rights reserved.


if 1
  y1 = run_layer1_once(layer1_coefficients, layer1_biases, x);
else
  y1 = run_layer1_once_v2(layer1_coefficients, layer1_biases, x);  % Enables additional diagnostics.
end;

y3 = run_layer2_once(layer2_coefficients, layer2_biases, y1);

if (0)
figure(1);
setFigureSize(gcf, 20,10);
clf;
subplot(1,2,1);
image(reshape_digit_vector_to_image(obj, x)*256);
axis equal;
title('input image');
subplot(1,2,2);
y3 = log10(y3) + 6;
for k = 1 : length(y3)
  y3(k) = max(y3(k),0);
end;
bar(0:9, y3);
title('estimated labels');
ylabel('log10 scale');
xlabel('labels (digit values)');

%semilogy(0:9, y3);
%ax = axis;
%axis([ ax(1) ax(2) 1e-6 1.1 ]);
grid on;
pause;
end;

y = y3;
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in the
%       documentation and/or other materials provided with the distribution.
%     * The name of the Kevin Oler may not be used to endorse or promote products
%       derived from this software without specific prior written permission.
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
% 
