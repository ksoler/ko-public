function y = run_layer1_once_v2(obj, coefficients, biases, x)
% y = run_layer1_once_v2
% New:  Shows activations.
%
% coefficients is an array (784,512) of coefficients.  The first column is used to calculate the 
% first element of the output vector.
%
% biases is a vector of bias values, one for each of the 512 outputs.
%
% x is a vector (784) of input values.
%
% y is a vector (512) of output values.
%

if size(x,2) ~=1
  error('The input should be a column vector.');
end;

y = zeros(512,1);
x = reshape_digit_vector_to_image(obj, x);


activations = cell(512,1);

for j = 1 : 512
  c = reshape_digit_vector_to_image(obj, coefficients(:,j));
  activations{j} = x .* c;
  a = sum(activations{j}(:));
  a = a + biases(j);
  a = relu(a);
  y(j) = a;
end;

activations = reshape_512_to_image(obj, activations);
z = [];

for i = 1 : 16
  row = [];
  for j = 1 : 32
    row = [ row activations{i,j} ];
  end;
  z = [ z; row ];
end;
z = z - min(z(:));
z = z / max(z(:));
z = z * 256;

y2 = reshape_512_to_image(obj,y);
figure(1);
u = utility01_class;
clf;
setFigureSize(u, gcf, 30, 10);
subplot(121);
image(z);
title('Activations');

y2 = y2 - min(y2(:));
y2 = y2 / max(y2(:));
y2 = y2 * 256;

figure(2);
image(y2);
title('layer output');
keyboard;
return;


function y = relu(x)
% Rectified linear activation function.
y = max(x,0);
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019, Kevin Oler
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in the
%       documentation and/or other materials provided with the distribution.
%     * The name of the Kevin Oler may not be used to endorse or promote products
%       derived from this software without specific prior written permission.
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL KEVIN OLER BE LIABLE FOR ANY
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
% 

  
  